function Usage() {
  echo
  echo "***************************************************************************"
  echo "Build BIOS rom for Kunlun BIOS."
  echo
  echo "Usage: buildD2000.sh  [option]"
  echo
  echo "       option:   D/d for Debug"
  echo "                 R/r for Release"
  echo "                 init for the necessary init"
  echo "                 clean to clean Build"
  echo "!!!!!!Please do \"buildD2000.sh init\" before build! first!!!!!!"
  echo "***************************************************************************"
  exit 0
}
#[jckuang-0024]
function JudgeIsInit() {
  local flag=0
  local status=0

  if [ ! -d "$CONF_PATH" ];then
    flag=1
    mkdir -p $CONF_PATH
  fi

  rm -f $CONF_PATH/.start_md5_log
  find $EDK_TOOLS_PATH ! -path "$EDK_TOOLS_PATH/Source/C/bin/*" -type f -exec md5sum {} >> $CONF_PATH/.start_md5_log \;
  
  if [ ! -f "$CONF_PATH/.run_md5_log" ];then
    flag=1
  else
    diff $CONF_PATH/.start_md5_log  $CONF_PATH/.run_md5_log > $CONF_PATH/.temp.pach
    status=$?
    if [ $status -ne 0 ] || [ ! -s "$CONF_PATH/.run_md5_log" ];then
      flag=1
    fi
    rm -f $CONF_PATH/.temp.pach
  fi

  if [ $flag -eq 1 ];then
    echo "enter init ..."
    mkdir -p $CORE_PATH/Conf
    rm -rf $CORE_PATH/Conf/* 
    cd $CORE_PATH/BaseTools
    make clean 
    cd ../
    source ./edksetup.sh BaseTools
    make -C BaseTools ARCH=AARCH64
    status=$?
    if [ $status -ne 0 ];then
      echo "init error!!!"
      exit
    fi
    chmod -R 777 BaseTools/BinWrappers/PosixLike
    rm -f $CONF_PATH/.run_md5_log
    find $EDK_TOOLS_PATH ! -path "$EDK_TOOLS_PATH/Source/C/bin/*" -type f -exec md5sum {} >> $CONF_PATH/.run_md5_log \;
    echo "init finish"
  fi
}
#[jckuang-0024]  
cd ..

export WORKSPACE=$(pwd)
export CORE_PATH=$WORKSPACE/edk2
export CONF_PATH=$CORE_PATH/Conf
export EDK_TOOLS_PATH=$CORE_PATH/BaseTools
export PROJECT_PATH=$WORKSPACE/Kunlun
export PLATFORM_PATH=$WORKSPACE/PhytiumPkg
export PACKAGES_PATH=$WORKSPACE:$PLATFORM_PATH:$PROJECT_PATH:$CORE_PATH

export PROJECT_NAME=KunlunD2000BoardPkg
export PROJECT_PACKAGE=$PROJECT_PATH/$PROJECT_NAME
export EDK2_DSC=$PROJECT_PACKAGE/Project.dsc

#echo WORKSPACE=$WORKSPACE
#echo EDK2_DSC=$EDK2_DSC

#source $CORE_PATH/edksetup.sh

if [ "$(echo $1 | tr 'a-z' 'A-Z')" == "D" ]; then
  EDK2_BUILD=DEBUG
  BUILD_TYPE=D
elif [ "$(echo $1 | tr 'a-z' 'A-Z')" == "R" ]; then  
  EDK2_BUILD=RELEASE
  BUILD_TYPE=R
elif [ "$(echo $1 | tr 'a-z' 'A-Z')" == "INIT" ]; then
  echo ""
elif [ "$(echo $1 | tr 'a-z' 'A-Z')" == "CLEAN" ]; then
  echo ""  
else 
  Usage
  exit
fi

DATE=$(date +%y%m%d)
TWO_DIGIT_YEAR=${DATE:0:2}
TWO_DIGIT_MONTH=${DATE:2:2}
TWO_DIGIT_DAY=${DATE:4:2}

BUILD_BIOS_NAME=KL4.27.CRB.D.001.${TWO_DIGIT_YEAR}${TWO_DIGIT_MONTH}${TWO_DIGIT_DAY}.$BUILD_TYPE.Test
BUILD_UUID=`uuidgen +%s`
BUILD_CPU_TYPE=D2000

echo \#define BIOS_BUILD_TIME `date +%m/%d/%Y` `date +%T` > $PROJECT_PACKAGE/Include/Version.h
echo \#define BIOS_BUILD_TIME_STR  L\"`date +%m/%d/%Y` `date +%T`\" >> $PROJECT_PACKAGE/Include/Version.h
echo \#define BIOS_TAG ${BUILD_BIOS_NAME} >> $PROJECT_PACKAGE/Include/Version.h
echo \#define BIOS_TAG_STR L\"${BUILD_BIOS_NAME}\" >> $PROJECT_PACKAGE/Include/Version.h
echo \#define BIOS_GUID L\"${BUILD_UUID}\" >> $PROJECT_PACKAGE/Include/Version.h
echo \#define CPU_TYPE ${BUILD_CPU_TYPE} >> $PROJECT_PACKAGE/Include/Version.h

export PATH=$PATH:$CORE_PATH/BaseTools/Source/C/bin
export LANGUAGE=en_US.UTF-8

BUILD_RESULT=0

if [ "$1" == "init" ];then
    mkdir -p $CORE_PATH/Conf
    rm $CORE_PATH/Conf/* -rf 
    cd $CORE_PATH/BaseTools 
    make clean 
    cd ../
    source ./edksetup.sh BaseTools
    make -C BaseTools ARCH=AARCH64
    chmod -R 777 BaseTools/BinWrappers/PosixLike
    rm -f $CONF_PATH/.run_md5_log
    find $EDK_TOOLS_PATH ! -path "$EDK_TOOLS_PATH/Source/C/bin/*" -type f -exec md5sum {} >> $CONF_PATH/.run_md5_log \;
    exit
elif [ "$1" == "clean" ];then
    echo "remove $CORE_PATH/Build"
    rm -rf Build
    cd $CORE_PATH/BaseTools 
    make clean 
    cd ../
    exit
else
    JudgeIsInit
    cd $WORKSPACE/Kunlun
    make -f $PROJECT_PACKAGE/Makefile EDK2_BUILD=$EDK2_BUILD
    BUILD_RESULT=$?
    cd ..
fi
# build error
if [ $? -ne 0 ] || [ ${BUILD_RESULT} -ne 0 ]; then
  echo "build error!!!"
  exit
fi
# Add PBF header after build BIOS 
export OUTPUT_DIRECTORY=$WORKSPACE/Build/$PROJECT_NAME
export OUTPUT_DIR=${OUTPUT_DIRECTORY}/${EDK2_BUILD}_GCC49/FV
cp ${OUTPUT_DIR}/*.fd biosD2000.bin
#
#
#FindResult=`find $PROJECT_PACKAGE/pbf -type d -name "*image_fix*"`
#if [ "$FindResult" == "" ];then
#  cd $PROJECT_PACKAGE/pbf
#  echo "unzip pbf"
#  tar -xzvf image_fix_v1.70.tar.gz
#  cd $WORKSPACE
#else
#echo "PBF already unzip"
#fi
#
#export PBF_DIR=$PROJECT_PACKAGE/pbf/image_fix_v1.70
#cp biosD2000.bin ${PBF_DIR}/bl33_new.bin
#rm biosD2000.bin
#cd ${PBF_DIR}
#./my_scripts/image-fix.sh
#fw=$BUILD_BIOS_NAME.fd
#cp fip-all.bin ../../$fw
#cd ../..
#
echo "build end."

