/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/

#include "SmbiosOemLibInternal.h"

/**
  This service will be call by each time process SMBIOS string.
  OEM can provide a way to get replace string for OEM depend.

  @param[in]  Type           SMBIOS type number.
  @param[in]  StringIndex    String Index.
  @param[out] StrLength      String length.
  @param[out] String         String point.

  @retval    EFI_UNSUPPORTED       Returns unsupported by default.
  @retval    EFI_SUCCESS           N/A.
  @retval    EFI_MEDIA_CHANGED     The value of OUT parameter is changed.
**/
EFI_STATUS
OemInterfaceGetSmbiosReplaceString (
  IN  EFI_SMBIOS_TYPE   Type,
  IN  UINT8             StringIndex,
  OUT UINTN             *StrLength,
  OUT CHAR8             **String
  )
{
  return EFI_UNSUPPORTED;
}
