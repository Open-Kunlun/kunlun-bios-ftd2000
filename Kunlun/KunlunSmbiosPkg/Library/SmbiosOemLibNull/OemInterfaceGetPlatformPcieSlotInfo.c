/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/

#include "SmbiosOemLibInternal.h"
#include <Library/SmbiosOemLib.h>
#include <Pcd/KlSmbiosBoardInfo.h>   //[yfliu-0004]

KL_SMBIOS_SYSTEM_SLOT_INFO mSystemSlotInfo[] = {
  //
  // SlotType                   SlotDataBusWidth       SlotLength       Characteristics1          Characteristics2 SlotDesignation   DevicePathStr
  // ========================== ====================== ================ ========================= ================ ================= =============
  {  SlotTypePciExpressGen2X1,  SlotDataBusWidth1X,    SlotLengthOther, {0, 1, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "PCIE1X_1",      "PciRoot(0x0)/Pci(0x1,0x0)"},
  {  SlotTypePciExpressGen2X8,  SlotDataBusWidth8X,    SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "PCIE8X_1",      "PciRoot(0x0)/Pci(0x3,0x0)/Pci(0x0,0x0)"},
  {  SlotTypePciExpressGen2X16, SlotDataBusWidth16X,   SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "PCIE16X1",      "PciRoot(0x0)/Pci(0x4,0x0)/Pci(0x0,0x0)"}
  //{  SlotTypePciExpressGen3X16, SlotDataBusWidth8X,    SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "PCIE x8 SLOT",   "PciRoot(0x4)/Pci(0x0,0x0)/Pci(0x10,0x0)/Pci(0x0,0x0)"},
  //{  SlotTypePciExpressGen3X8,  SlotDataBusWidth8X,    SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "HBA SLOT",       "PciRoot(0x6)/Pci(0x0,0x0)"},
  //{  SlotTypeIORiserCardSlot,   SlotDataBusWidthOther, SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "PCIE x24 SLOT2", "PciRoot(0x7)/Pci(0x0,0x0)"},
  //{  0x24,                      SlotDataBusWidth4X,    SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "U.2 SLOT3",      "PciRoot(0x9)/Pci(0x0,0x0)"},
  //{  0x24,                      SlotDataBusWidth4X,    SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "U.2 SLOT4",      "PciRoot(0xA)/Pci(0x0,0x0)"}
};

/**
  Return the System Slot information.

  @param  PcieSlotInfo          Return System Slot information
  @param  PcieSlotNum           Return System Slot Number

  @retval RETURN_SUCCESS        Get data successfully.

**/
EFI_STATUS
OemInterfaceGetPlatformPcieSlotInfo (
  OUT KL_SMBIOS_SYSTEM_SLOT_INFO   **PcieSlotInfo,
  OUT UINTN                        *PcieSlotNum
  )
{
  //
  // Sample Implementation
  //
#if 1
  if ((PcieSlotInfo == NULL) || (PcieSlotNum == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  *PcieSlotNum = ARRAY_SIZE(mSystemSlotInfo);
  *PcieSlotInfo = (KL_SMBIOS_SYSTEM_SLOT_INFO *)mSystemSlotInfo;

  return EFI_SUCCESS;
#endif
  return EFI_NOT_FOUND;
}
