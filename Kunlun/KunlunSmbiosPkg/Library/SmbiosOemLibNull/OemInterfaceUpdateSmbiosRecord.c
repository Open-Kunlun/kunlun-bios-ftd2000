/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/
#include "SmbiosOemLibInternal.h"

/**
  This service will be call by each time add SMBIOS record.
  OEM can modify SMBIOS record in run time.

  Notice the SMBIOS protocol is unusable when service is runing.

  @param[in, out]  *RecordBuffer         Each SMBIOS record data.
                                         The max length of this buffer is SMBIOS_TABLE_MAX_LENGTH.

  @retval    EFI_UNSUPPORTED       Returns unsupported by default.
  @retval    EFI_SUCCESS           Don't add this SMBIOS record to system.
  @retval    EFI_MEDIA_CHANGED     The value of IN OUT parameter is changed.
**/
EFI_STATUS
OemInterfaceUpdateSmbiosRecord (
  IN OUT EFI_SMBIOS_TABLE_HEADER *RecordBuffer
  )
{
  EFI_STATUS Status;

  Status = EFI_UNSUPPORTED;

  //sample implementation
  if (RecordBuffer == NULL) {
    return EFI_UNSUPPORTED;
  }

//  switch (RecordBuffer->Type) {
//  case EFI_SMBIOS_TYPE_SYSTEM_INFORMATION:
//    break;
//  default:
//    break;
//  }

  return Status;
}
