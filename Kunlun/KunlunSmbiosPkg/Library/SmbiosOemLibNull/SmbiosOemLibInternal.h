/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/
#ifndef _SMBIOS_OEM_LIB_INTERNAL_H_
#define _SMBIOS_OEM_LIB_INTERNAL_H_
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/KlSmbiosLib.h>
#include <Library/TimerLib.h>
#include <Protocol/Smbios.h>
#include <Protocol/PciIo.h>

#endif