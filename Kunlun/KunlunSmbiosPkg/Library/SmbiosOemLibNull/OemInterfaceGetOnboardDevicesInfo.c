/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/

#include "SmbiosOemLibInternal.h"
#include <Library/SmbiosOemLib.h>
#include <Pcd/KlSmbiosBoardInfo.h>   //[yfliu-0004]

STATIC KL_SMBIOS_ONBOARD_DEVICE_INFO mOnBoardDeviceInfo[] = {
  //
  // Enabled DeviceType                               ReferenceDesignation          DevicePathStr
  // ======= ======================================== ====================          ============================
  {  TRUE,   OnBoardDeviceExtendedTypeVideo,          "VGA",               "PciRoot(0x0)/Pci(0x6,0x1)"},   //[yfliu-0004]
  {  TRUE,   OnBoardDeviceExtendedTypeSATAController, "SATA Controller",   "PciRoot(0x0)/Pci(0x8,0x0)"},   //[yfliu-0004]
};

/**
  This function is used for oem interface to get onboard devices info.

  @param  OnBoardDeviceInfo    The klsmbios onboard device info.
  @param  DeviceCount          A pointer to the device count.

  @retval EFI_SUCCESS

**/
EFI_STATUS
OemInterfaceGetOnboardDevicesInfo (
  OUT KL_SMBIOS_ONBOARD_DEVICE_INFO   **OnBoardDeviceInfo,
  OUT UINTN                           *DeviceCount
  )
{
  //
  // Sample Implementation
  //

  if ((OnBoardDeviceInfo == NULL) || (DeviceCount == NULL)) {
    return EFI_INVALID_PARAMETER;
  }

  *DeviceCount = ARRAY_SIZE(mOnBoardDeviceInfo);
  *OnBoardDeviceInfo = (KL_SMBIOS_ONBOARD_DEVICE_INFO *)mOnBoardDeviceInfo;

  return EFI_SUCCESS;

}
