/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/
#include "SmbiosOemLibInternal.h"

/**
  This function is used for oem interface end of klsmbios dxe.

  @param  VOID

  @retval EFI_UNSUPPORTED

**/
EFI_STATUS
EFIAPI
OemInterfaceEndOfKlSmbiosDxe (
  VOID
  )
{
  return EFI_UNSUPPORTED;
}
