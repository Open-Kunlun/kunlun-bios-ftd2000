/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef __KL_SMBIOS_PLATFORM_DATA_H__
#define __KL_SMBIOS_PLATFORM_DATA_H__
#include <IndustryStandard/SmBios.h>

#define KL_SMBIOS_MAX_DESIGNATION_STR_LEN 0x20
#define KL_SMBIOS_MAX_DP_STR_LEN          0x100
#pragma pack(1)
typedef struct {
  UINT8                       SlotType;                 ///< The enumeration value from MISC_SLOT_TYPE.
  UINT8                       SlotDataBusWidth;         ///< The enumeration value from MISC_SLOT_DATA_BUS_WIDTH.
  UINT8                       SlotLength;               ///< The enumeration value from MISC_SLOT_LENGTH.
  MISC_SLOT_CHARACTERISTICS1  SlotCharacteristics1;
  MISC_SLOT_CHARACTERISTICS2  SlotCharacteristics2;
  CHAR8                       SlotDesignationStr[KL_SMBIOS_MAX_DESIGNATION_STR_LEN];
  CHAR8                       DevicePathStr[KL_SMBIOS_MAX_DP_STR_LEN];
} KL_SMBIOS_SYSTEM_SLOT_INFO;

typedef struct {
  BOOLEAN    Enabled;
  UINT8      DeviceType;               ///< The enumeration value from ONBOARD_DEVICE_EXTENDED_INFO_TYPE
  CHAR8      ReferenceDesignationStr[KL_SMBIOS_MAX_DESIGNATION_STR_LEN];
  CHAR8      DevicePathStr[KL_SMBIOS_MAX_DP_STR_LEN];
} KL_SMBIOS_ONBOARD_DEVICE_INFO;
#pragma pack()

#endif
