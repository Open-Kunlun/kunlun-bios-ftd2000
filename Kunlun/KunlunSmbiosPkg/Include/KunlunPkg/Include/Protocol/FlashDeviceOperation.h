/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/

#ifndef __FLASH_DEVICE_OPERATION_H__
#define __FLASH_DEVICE_OPERATION_H__

#define FLASH_DEVICE_OPERATION_PROTOCOL_GUID \
  { 0x3F50F196, 0x7A09, 0x4F16, { 0x84, 0xB0, 0xDA, 0xEC, 0xA5, 0x6F, 0x29, 0x6D } }


typedef struct _FLASH_DEVICE_OPERATION_PROTOCOL FLASH_DEVICE_OPERATION_PROTOCOL;

//
// Status codes common to all execution phases
//
//typedef UINTN RETURN_STATUS;

//
//VendorId: The VendorId of the flash chip
//DeviceId: The DeviceId of the flash chip
//
#define FLASH_MAX_NAME_LENG 32
typedef struct{
  UINT8   VendorId;
  UINT8   DeviceId;
  CHAR8   VendorName[FLASH_MAX_NAME_LENG];
  CHAR8   DeviceName[FLASH_MAX_NAME_LENG];
}SENWEI_FLASH_IDENTIFICATION;

//
//Size:      The capacity of the flash chip, unit is KB
//NumOfSector:The Sector number of the flash chip
//NumOfBlock: The Block number of the flash chip
//
typedef struct{
  UINT32  Size;
  UINT32  NumOfSector;
  UINT32  NumOfBlock;
}SENWEI_FLASH_LAYOUT;

typedef struct{
  EFI_LOCK                      Lock;
  SENWEI_FLASH_IDENTIFICATION Id;
  SENWEI_FLASH_LAYOUT         Layout;
  UINT8                         ErasePolarity;
}SENWEI_FLASH_INFO;

//
//Routine Description:
//Execute SPI commands from the host controller to get the SENWEI_FLASH_INFO
//

//Arguments:
// This              Pointer to the FLASH_DEVICE_OPERATION_PROTOCOL instance.
// FlashInfo         Pointer to caller-allocated buffer containing the FlashInfo received from the SPI cycle.
//
typedef
RETURN_STATUS
(EFIAPI *FLASH_DEVICE_GET_INFO)(
  IN  FLASH_DEVICE_OPERATION_PROTOCOL *This,
  OUT SENWEI_FLASH_INFO             *FlashInfo
);

//
//Routine Description:
//Execute SPI commands from the host controller to get the SENWEI_FLASH_INFO
//
//Arguments:
// This              Pointer to the FLASH_DEVICE_OPERATION_PROTOCOL instance.
// Offset            For BIOS Region,this value specifies the offset from the start of the BIOS Image.
// Buffer            Pointer to caller-allocated buffer containing the dada sent during the SPI cycle.
// BytesCount        Pointer to caller-allocated buffer containing Number of bytes in the data portion of the SPI cycle.
//
typedef
RETURN_STATUS
(EFIAPI *FLASH_DEVICE_WRITE)(
  IN      FLASH_DEVICE_OPERATION_PROTOCOL *This,
  IN      UINTN                            Offset,
  IN      UINT8                           *Buffer,
  IN OUT  UINT32                          *BytesCount
);

//
//Routine Description:
//Execute SPI commands from the host controller to get the SENWEI_FLASH_INFO
//
//Arguments:
// This              Pointer to the FLASH_DEVICE_OPERATION_PROTOCOL instance.
// Offset            For BIOS Region,this value specifies the offset from the start of the BIOS Image.
// Buffer            Pointer to caller-allocated buffer containing the dada received during the SPI cycle.
// BytesCount        Pointer to caller-allocated buffer containing Number of bytes in the data portion of the SPI cycle.
//
typedef
RETURN_STATUS
(EFIAPI *FLASH_DEVICE_READ)(
  IN      FLASH_DEVICE_OPERATION_PROTOCOL *This,
  IN      UINTN                            Offset,
  IN      UINT8                           *Buffer,
  IN OUT  UINT32                          *BytesCount
);

//
//Routine Description:
//Execute SPI commands from the host controller to get the SENWEI_FLASH_INFO
//
//Arguments:
// This              Pointer to the FLASH_DEVICE_OPERATION_PROTOCOL instance.
// Offset            For BIOS Region,this value specifies the offset from the start of the BIOS Image.
// BytesCount        Pointer to caller-allocated buffer containing Number of bytes erased during the SPI cycle.
//
typedef
RETURN_STATUS
(EFIAPI *FLASH_DEVICE_ERASE)(
  IN      FLASH_DEVICE_OPERATION_PROTOCOL *This,
  IN      UINTN                            Offset,
  IN OUT  UINT32                          *BytesCount
);


struct _FLASH_DEVICE_OPERATION_PROTOCOL{
  FLASH_DEVICE_GET_INFO GetInfo;
  FLASH_DEVICE_ERASE      Erase;
  FLASH_DEVICE_WRITE      Write;
  FLASH_DEVICE_READ       Read;
};

extern EFI_GUID gFlashDeviceOperationProtocolGuid;
#endif


