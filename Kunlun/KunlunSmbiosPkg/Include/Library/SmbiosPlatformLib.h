/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/

#ifndef SMBIOSPLATFORMLIB_H_
#define SMBIOSPLATFORMLIB_H_

#include <OEMSvc.h>

typedef struct {
  UINTN CpuTypeId;
  CHAR8 *CpuTypeName;
} CPUTYPE;

typedef struct {
   UINTN   mCpuTypeId;
   CHAR8   mCpuTypeName[100];
   UINT8   mCpuCount;
   UINT8   mCoreCount;
   UINT16  mCoreMaxFrequency;
   UINT16  mCoreFrequency;
   UINT64  mCpuL3CacheSize;
}SMBIOSPLATFORMCPUINFO;

typedef struct {
   UINT64   mSize;
   UINT16   mNumberOfDevices;
}SMBIOSPLATFORMMEMINFO;


typedef struct {
   SMBIOSPLATFORMCPUINFO mCpuInfo;
   SMBIOSPLATFORMMEMINFO mMemInfo;
}SMBIOSPLATFORMINFO;

PHYTIUM_CPU_INFO *
SmbiosGetCpuInfoHob();

EFI_STATUS
SmbiosGetPlatformInfo(
   OUT SMBIOSPLATFORMINFO * SmbiosPlatInfo
);

EFI_STATUS
SmbiosGetCpuInfo(
  OUT SMBIOSPLATFORMCPUINFO * SmbiosCpuInfo
 );

//  EFI_STATUS
//  SmbiosGetMemInfo(
//   OUT SMBIOSPLATFORMMEMINFO * SmbiosMemInfo
//  );

#endif /* __SmbiosPlatformLib_H_ */
