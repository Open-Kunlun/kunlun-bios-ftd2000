/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/


#ifndef _KL_SMBIOS_LIB_H_
#define _KL_SMBIOS_LIB_H_
#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/PrintLib.h>
#include <Protocol/Smbios.h>
#include <IndustryStandard/SmBios.h>

#define SMBIOS_TABLE_MAX_LENGTH    0xFFFF
#define MAX_BUFFER_LENGTH          SMBIOS_TABLE_MAX_LENGTH

///
/// Non-static SMBIOS table data to be filled later with a dynamically generated value
///
#define TO_BE_FILLED  0
#define TO_BE_FILLED_STRING  " "        ///< Initial value should not be NULL

///
/// String references in SMBIOS tables. This eliminates the need for pointers.
/// See the DMTF SMBIOS Specification v2.7.1, section 6.1.3.
///
#define NO_STRING_AVAILABLE  0
#define STRING_1  1
#define STRING_2  2
#define STRING_3  3
#define STRING_4  4
#define STRING_5  5
#define STRING_6  6
#define STRING_7  7

#pragma pack(1)
///
/// Template for SMBIOS table initialization.
/// The SMBIOS_TABLE_STRING types in the formated area must match the
/// StringArray sequene.
///
typedef struct {
  //
  // formatted area of a given SMBIOS record
  //
  SMBIOS_STRUCTURE    *Entry;
  //
  // NULL terminated array of ASCII strings to be added to the SMBIOS record.
  //
  CHAR8               **StringArray;
} SMBIOS_TEMPLATE_ENTRY;

typedef struct {
  EFI_SMBIOS_TABLE_HEADER  Header;
  UINT8                    Tailing[2];
} EFI_SMBIOS_TABLE_END_STRUCTURE;

#pragma pack()



extern EFI_SMBIOS_PROTOCOL               *mSmbios;

EFI_STATUS
EFIAPI
SmbiosLibCreateEntry (
  IN  SMBIOS_STRUCTURE      *SmbiosEntry,
  IN  CHAR8                 **StringArray,
  IN  EFI_SMBIOS_PROTOCOL   *Smbios,
  OUT EFI_SMBIOS_HANDLE     *SmbiosHandle
  );

//----------------------------------------------------
UINTN
GetSmbiosStructureSize (
  IN   EFI_SMBIOS_TABLE_HEADER    *Head
  );

/**
  The caller have to make sure that buffer size is enough
**/
EFI_STATUS
SetOptionalStringByIndex (
  IN CHAR8             *OptionalStrStart,
  IN UINT8             Index,
  IN CHAR8             *String
  );

EFI_STATUS
EFIAPI
InitOptionalStrings (
  IN CHAR8              *OptionalStrStart,
  IN CHAR8              **StringArray
  );

EFI_STATUS
EFIAPI
GetPciLocationFromDpStr (
  IN  CHAR8                *DevicePathStr,         //[yfliu-0004]
  OUT UINT8                *Seg,
  OUT UINT8                *Bus,
  OUT UINT8                *Dev,
  OUT UINT8                *Func
  );

//[yfliu-0001]
EFI_STATUS
UpdateSmbiosType32 (
  IN UINT8 BOOTSTATUS
);
//[yfliu-0001]

#endif
