/** @file
  Brief description of the file's purpose.
  Detailed description of the file's contents and other useful
  information for a person viewing the file for the first time.
  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..
  @par Revision Reference:18
  - PI Version 1.0
  @par Glossary:
  - IETF - Internet Engineering Task Force
  - NASA - National Aeronautics and Space Administration
**/

#ifndef __KL_DMI_DEF_H__
#define __KL_DMI_DEF_H__

#include <Uefi.h>

#define EFI_KUNLUN_DMI_REGION_SIGNATURE        SIGNATURE_64 ('$', 'K', 'L', '_', 'D', 'M', 'I', '$')

typedef enum {
  DmiStsDeleted = 0x1F,
  DmiStsEnabled = 0x7F,
  DmiStsEmpty   = 0xFF
} DMI_RECORE_STATUS;

#pragma pack(1)
typedef struct _DMI_REGION_HEADER {
  UINT64  Signature;
  UINT16  Version;
  UINT16  HdrLength;
  UINT32  RegionLength;
  UINT8   Reserved[16];
} DMI_REGION_HEADER;

typedef struct _DMI_DATA_RECORD {
  UINT8  Valid;
  UINT8  Type;
  UINT8  Offset;
  UINT8  Reserved;
  UINT16 DataLen;    // Data length
  UINT16 Length;     // Total length: DMI_DATA_RECORD + DataLen + Padding
  UINT8  Data[1];    // Variable length field
} DMI_DATA_RECORD;
#pragma pack()

#endif
