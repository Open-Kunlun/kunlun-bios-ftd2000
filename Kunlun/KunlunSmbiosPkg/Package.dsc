## @file
#  Brief description of the file's purpose.
#  Detailed description of the file's contents and other useful
#  information for a person viewing the file for the first time.
#  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
#  Rights Reserved.
#  You may not reproduce, distribute, publish, display, perform, modify, adapt,
#  transmit, broadcast, present, recite, release, license or otherwise exploit
#  any part of this publication in any form, by any means, without the prior
#  written permission of Kunlun Technology (Beijing) Co., Ltd..
#  @par Revision Reference:18
#  - PI Version 1.0
#  @par Glossary:
#  - IETF - Internet Engineering Task Force
#  - NASA - National Aeronautics and Space Administration
##

[LibraryClasses]
  KlSmbiosLib|KunlunSmbiosPkg/Library/KlSmbiosLib/KlSmbiosLib.inf
  #SmbiosOemLib|KunlunSmbiosPkg/Library/SmbiosOemLib/SmbiosOemLib.inf
  SmbiosOemLib|KunlunSmbiosPkg/Library/SmbiosOemLibNull/SmbiosOemLib.inf
  SmbiosPlatformLib|KunlunSmbiosPkg/Library/SmbiosPlatformLib/SmbiosPlatformLib.inf

[Components]
  KunlunSmbiosPkg/KlSmbiosDxe/KlSmbiosDxe.inf
  KunlunSmbiosPkg/SmbiosMemoryDxe/SmbiosMemoryDxe.inf
  KunlunSmbiosPkg/SmbiosSystemSlotDxe/SmbiosSystemSlotDxe.inf
  KunlunSmbiosPkg/SmbiosProcessorDxe/SmbiosProcessorDxe.inf
  KunlunSmbiosPkg/SmbiosOnBoardDeviceDxe/SmbiosOnBoardDeviceDxe.inf
  #KunlunSmbiosPkg/SmbiosEventLogDxe/SmbiosEventLogDxe.inf

[PcdsDynamicExDefault]
  gKlSmbiosTokenSpaceGuid.PcdSmbiosActiveRecordType|{0, 1, 2, 3, 4, 8, 11, 12, 13, 15, 17, 32, 127}
  gKlSmbiosTokenSpaceGuid.PcdType000Record |{000, 0x00, UINT16(0x0000),  1, 2, UINT16(0xE000), 3, UINT8(0x7F), UINT64(0x0000000010011880), UINT16(0x800A), 0x00, 0x01, 0xFF, 0xFF}
  gKlSmbiosTokenSpaceGuid.PcdType000Strings|"refer PcdFirmwareVendor;refer BIOS_TAG;refer TODAY;"
  !if $(BOARD) == "Z706_HTDD208"
  gKlSmbiosTokenSpaceGuid.PcdType001Record |{001, 0x00, UINT16(0x0000),  1, 2, 3, 4, GUID("00112233-4455-6677-8899-AABBCCDDEEFF"), UINT8(0x08), 5, 6}
  gKlSmbiosTokenSpaceGuid.PcdType001Strings|"Beijing Institute of Computer Technology and Application;TianYi TR1265;1.0;TBD by OEM;Embedway_SP2510_SKU;Type1Family;"

  gKlSmbiosTokenSpaceGuid.PcdType002Record000 |{002, 0x00, UINT16(0x0000),  1, 2, 3, 4, 5, UINT8(0x09), 6, UINT16(0x0000), 0x0A, 0x00}
  gKlSmbiosTokenSpaceGuid.PcdType002Strings000|"Beijing Institute of Computer Technology and Application;AM2.335.17874-LDX;V1.0;Reserved for BaseBoardVendor;Reserved for BaseBoardVendor;Reserved for BaseBoardVendor;"

  gKlSmbiosTokenSpaceGuid.PcdType003Record000 |{003, 0x00, UINT16(0x0000),  1, 0x03, 2, 3, 4, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 5}
  gKlSmbiosTokenSpaceGuid.PcdType003Strings000|"Beijing Institute of Computer Technology and Application;V1.0;Chassis Board Serial#To Be Filled By O.E.M;Chassis Board Asset Tag#To Be Filled By O.E.M;Type3SKU;"
  !else
  gKlSmbiosTokenSpaceGuid.PcdType001Record |{001, 0x00, UINT16(0x0000),  1, 2, 3, 4, GUID("00112233-4455-6677-8899-AABBCCDDEEFF"), UINT8(0x06), 5, 6}
  gKlSmbiosTokenSpaceGuid.PcdType001Strings|"Embedway;SP2510;0.1;TBD by OEM;Embedway_SP2510_SKU;Type1Family;"

  gKlSmbiosTokenSpaceGuid.PcdType002Record000 |{002, 0x00, UINT16(0x0000),  1, 2, 3, 4, 5, UINT8(0x09), 6, UINT16(0xFFFF), 0x0A, 0x00}
  gKlSmbiosTokenSpaceGuid.PcdType002Strings000|"Embedway;Board Product Name;Board Version;Reserved for BaseBoardVendor;Board Asset Tag;Board Chassis Location;"

  gKlSmbiosTokenSpaceGuid.PcdType003Record000 |{003, 0x00, UINT16(0x0000),  1, 0x17, 2, 3, 4, 0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 5}
  gKlSmbiosTokenSpaceGuid.PcdType003Strings000|"Embedway;Version To Be Filled By O.E.M;Chassis Board Serial#To Be Filled By O.E.M;Chassis Board Asset Tag#To Be Filled By O.E.M;Type3SKU;"
  !endif
  gKlSmbiosTokenSpaceGuid.PcdType008Record000 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x0F, 0x0D}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings000|"J1A1;Keyboard;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record001 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x0F, 0x0E}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings001|"J1A1;Mouse;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record002 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x0D, 0x1C}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings002|"J2A1;TV OUT;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record003 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x07, 0x1C}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings003|"J2A2;CRT;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record004 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x08, 0x09}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings004|"J2A2;COM 1;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record005 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x12, 0x10}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings005|"J3A1;USB;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record008 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x12, 0x10}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings008|"J5A1;USB;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record010 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x12, 0x10}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings010|"J5A2;USB;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record011 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x0B, 0x1F}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings011|"J5A1;Network;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record012 |{008, 0x00, UINT16(0x0000), 1, 0x17, 2, 0x00, 0xFF}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings012|"J9G2;OnBoard Floppy Type;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record013 |{008, 0x00, UINT16(0x0000), 1, 0x16, 2, 0x00, 0xFF}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings013|"J7J1;OnBoard Primary IDE;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record014 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x1F, 0x1D}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings014|"J30;Microphone In;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record015 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x1F, 0x1D}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings015|"J30;Line In;"
  gKlSmbiosTokenSpaceGuid.PcdType008Record016 |{008, 0x00, UINT16(0x0000), 1, 0x00, 2, 0x1F, 0x1D}
  gKlSmbiosTokenSpaceGuid.PcdType008Strings016|"J30;Speaker Out;"

  # The field of peer grouping count will affect the length of type 9
  gKlSmbiosTokenSpaceGuid.PcdType009Record000 |{009, 0x00, UINT16(0x0000), 1, 0xA5, 0x08, 0x00, 0x01, 0x01, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xE0, 0x08, 0x00}
  gKlSmbiosTokenSpaceGuid.PcdType009Strings000|"J6C1;"
  gKlSmbiosTokenSpaceGuid.PcdType009Record001 |{009, 0x00, UINT16(0x0000), 1, 0xA6, 0x08, 0x00, 0x01, 0x02, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0xE1, 0x08, 0x00}
  gKlSmbiosTokenSpaceGuid.PcdType009Strings001|"J6D2;"

  gKlSmbiosTokenSpaceGuid.PcdType011Record |{011, 0x00, UINT16(0x0000), 0x03}
  gKlSmbiosTokenSpaceGuid.PcdType011Strings|"Oem String 1;Oem String 2;Oem String 3;"
  gKlSmbiosTokenSpaceGuid.PcdType012Record |{012, 0x00, UINT16(0x0000), 0x02}
  gKlSmbiosTokenSpaceGuid.PcdType012Strings|"ConfigOptions String 1;ConfigOptions String 2;"
  gKlSmbiosTokenSpaceGuid.PcdType013Record |{013, 0x00, UINT16(0x0000), 0x02, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02}
  gKlSmbiosTokenSpaceGuid.PcdType013Strings|"enUS;zhCN;"
  #gKlSmbiosTokenSpaceGuid.PcdType015Record |{015, 0x00, UINT16(0x0000), UINT16(0x0000), UINT16(0x0000), UINT16(0x2000), 0x04, 0x01, UINT32(0x12345678), UINT32(0x00000000), 0x80, 0x03, 0x02, 0x07, 0x00, 0x08, 0x04, 0x16, 0x00}
  gKlSmbiosTokenSpaceGuid.PcdType028Record000 |{028, 0x00, UINT16(0x0000), 1, 0x42, UINT16(0x8000), UINT16(0x8000), UINT16(0x8000), UINT16(0x8000), UINT16(0x8000), UINT32(0x00000000), UINT16(0x8000)}
  gKlSmbiosTokenSpaceGuid.PcdType028Strings000|"CPU Temperature;"
  gKlSmbiosTokenSpaceGuid.PcdType032Record |{032, 0x00, UINT16(0x0000), 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}

  gKlSmbiosTokenSpaceGuid.PcdKlSmbiosSystemSlotInfo|{CODE({
    // SlotType                   SlotDataBusWidth       SlotLength       Characteristics1          Characteristics2 SlotDesignation  DevicePathStr
    // ========================== ====================== ================ ========================= ================ ================ =============
    {  SlotTypePciExpressGen2X1,  SlotDataBusWidth1X,    SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "PCIE1X_1",      "PciRoot(0x0)/Pci(0xd,0x0)/Pci(0x0,0x0)"},
    {  SlotTypePciExpressGen2X8,  SlotDataBusWidth8X,    SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "PCIE8X_1",      "PciRoot(0x0)/Pci(0x11,0x0)/Pci(0x0,0x0)"},
    {  SlotTypePciExpressGen2X16, SlotDataBusWidth16X,   SlotLengthOther, {1, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0},    "PCIE16X1",      "PciRoot(0x0)/Pci(0xf,0x0)/Pci(0x0,0x0)"}
  })}

  gKlSmbiosTokenSpaceGuid.PcdKlSmbiosOnboardDeviceInfo|{CODE({
    // Enabled DeviceType                               ReferenceDesignation DevicePathStr
    // ======= ======================================== ==================== ============================
    {  TRUE,   OnBoardDeviceExtendedTypeVideo,          "VGA Controller",    "PciRoot(0x0)/Pci(0x1,0x1)/Pci(0x0,0x0)/Pci(0x0,0x0)"},
    {  TRUE,   OnBoardDeviceExtendedTypeSATAController, "SATA Controller",   "PciRoot(0x0)/Pci(0x8,0x1)/Pci(0x0,0x2)"},
  })}