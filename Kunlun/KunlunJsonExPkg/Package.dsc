## @file
# Brief description of the file's purpose.
#
# Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
# Rights Reserved.
#
# You may not reproduce, distribute, publish, display, perform, modify, adapt,
# transmit, broadcast, present, recite, release, license or otherwise exploit
# any part of this publication in any form, by any means, without the prior
# written permission of Kunlun Technology (Beijing) Co., Ltd..
#
#
##

[Defines]
  DEFINE SETUP_IMPORT_EXPORT_BINARY = TRUE

[LibraryClasses.common]
!if !$(SETUP_IMPORT_EXPORT_BINARY)
  Ucs2Utf8Lib|KunlunJsonExPkg/Library/BaseUcs2Utf8Lib/BaseUcs2Utf8Lib.inf
  KlCrtLib|KunlunJsonExPkg/Library/KlCrtLib/KlCrtLib.inf
  JsonExLib|KunlunJsonExPkg/Library/JsonExLib/JsonExLib.inf
  FileExplorerExLib|KunlunJsonExPkg/Library/FileExplorerExLib/FileExplorerExLib.inf
!endif
[LibraryClasses.common.SEC, LibraryClasses.common.PEI_CORE, LibraryClasses.common.PEIM, LibraryClasses.common.DXE_CORE]

[PcdsFixedAtBuild]


[PcdsFeatureFlag]

[Components.common]
!if $(SETUP_IMPORT_EXPORT_BINARY)
  KunlunJsonExPkg/Application/SetupParaImportAppBin/SetupParaImportApp.inf
  KunlunJsonExPkg/Application/SetupParaExportAppBin/SetupParaExportApp.inf
  KunlunJsonExPkg/Drivers/ConfigImExportDxeBin/ConfigImExportDxe.inf
!else
  KunlunJsonExPkg/Application/SetupParaImportApp/SetupParaImportApp.inf
  KunlunJsonExPkg/Application/SetupParaExportApp/SetupParaExportApp.inf
  KunlunJsonExPkg/Drivers/ConfigImExportDxe/ConfigImExportDxe.inf
!endif

