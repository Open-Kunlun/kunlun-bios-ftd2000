/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef _CHIPX100_CONFIG_DATA_H_
#define _CHIPX100_CONFIG_DATA_H_

#define VAR_X100_PCI_INFO                        L"X100PciInfoVar"
#define PLATFORM_SETUP_VARIABLE_FLAG             (EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE)

#pragma pack(1)
//[gliu-0009]
typedef struct _X100_DP_CONFIG {
  UINT8  IsUsed;
  UINT8  DownSpreadEnable;
} X100_DP_CONFIG;

typedef struct _X100_DEV_PCI_INFO {
  UINT8         Enable;                      //0 - disable, 1 - enable
  UINT64        Seg;
  UINT64        Bus;
  UINT64        Dev;
  UINT64        Func;
} X100_DEV_PCI_INFO;

//[gliu-0009]

typedef struct _X100_SATA_PORT {
  UINT8  IsUsed;        //1 - used, 0 - not used
  UINT8  Enable;        //1 - enable, 0 - disable
} X100_SATA_PORT;
//[gliu-0031]add-start
typedef struct _X100_CONFIG {
  UINT8         X100IsInitFlag;         //x100 initialization flag
  UINT8         X100IsExisted;          //0 - not existed, 1 - existed
  UINT8         UsbEnable;                   //0 - disable, 1 - enable
  UINT8         SataEnable;                  //0 - disable, 1 - enable
  UINT8         DisplayEnable;               //0 - disable, 1 - enable
  //[gliu-0009]
  X100_DP_CONFIG  X100DpConfig[3];
  //[gliu-0009]
  X100_SATA_PORT  SataPort[4];

// pcie control
  UINT8         PcieX2Dn4Enable;               //0 - disable, 1 - enable
  UINT8         PcieX2Dn5Enable;               //0 - disable, 1 - enable
  UINT8         PcieX1Dn6Enable;               //0 - disable, 1 - enable
  UINT8         PcieX1Dn7Enable;               //0 - disable, 1 - enable
  UINT8         PcieX1Dn8Enable;               //0 - disable, 1 - enable
  UINT8         PcieX1Dn9Enable;               //0 - disable, 1 - enable
} X100_CONFIG;
//[gliu-0031]add-end
typedef struct X100_PCI_INFO {
  UINT8              X100IsExisted;          //0 - not existed, 1 - existed
  UINT8              DpChannel;
  X100_DEV_PCI_INFO  UsbSwitch;
  X100_DEV_PCI_INFO  SataSwitch;
  X100_DEV_PCI_INFO  DisplaySwitch;
} X100_PCI_INFO;
#pragma pack()

#endif
