/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef _CONTROLLER_SETUP_LIB_H_
#define _CONTROLLER_SETUP_LIB_H_

#include <Uefi.h>
#include <SetupConfig.h>

typedef enum {
  SATA_CONTROLLER = 0,
  NIC_CONTROLLER,
  USB_CONTROLLER,
  CONTROLLER_TYPE_MAX,
  INVALID_CONTROLLER
}DISABLE_CONTROLLER_TYPE_E;




EFI_STATUS
DisableControllerUnderPciBridge(
    SYSTEM_SETUP_CONFIGURATION *SysCfg
  );
#endif
