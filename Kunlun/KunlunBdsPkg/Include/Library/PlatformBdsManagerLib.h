/** @file
  System Firmware descriptor producer.

  Copyright (c) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/



#ifndef __PLATFORM_BDS_MANAGER_LIB_H_
#define __PLATFORM_BDS_MANAGER_LIB_H_


VOID
EFIAPI
PlatformBdsBeforeConsole (
  VOID
  );

VOID
EFIAPI
PlatformBdsAfterConsole (
  VOID
  );

#endif
