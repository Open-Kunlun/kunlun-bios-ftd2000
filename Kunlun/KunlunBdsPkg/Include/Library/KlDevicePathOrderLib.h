/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef _KL_DEVICE_PATH_ORDER_LIB_H_
#define _KL_DEVICE_PATH_ORDER_LIB_H_

#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/DevicePathLib.h>
#include <Library/DebugLib.h>

#define  BDS_EFI_MESSAGE_ATAPI_BOOT       0x0301 // Type 03; Sub-Type 01
#define  BDS_EFI_MESSAGE_SCSI_BOOT        0x0302 // Type 03; Sub-Type 02
#define  BDS_EFI_MESSAGE_USB_DEVICE_BOOT  0x0305 // Type 03; Sub-Type 05
#define  BDS_EFI_MESSAGE_SATA_BOOT        0x0312 // Type 03; Sub-Type 18
#define  BDS_EFI_MESSAGE_MAC_BOOT         0x030b // Type 03; Sub-Type 11
#define  BDS_EFI_MESSAGE_MISC_BOOT        0x03FF
//add-klk-lyang-P000A-start//
#define  BDS_EFI_MESSAGE_USB_CDROM_BOOT   0x03FE //not spec
//add-klk-lyang-P000A-end//
#define  BDS_EFI_ACPI_FLOPPY_BOOT         0x0201
#define  BDS_EFI_MEDIA_HD_BOOT            0x0401 // Type 04; Sub-Type 01
#define  BDS_EFI_MEDIA_CDROM_BOOT         0x0402 // Type 04; Sub-Type 02
//add-klk-lyang-P000A-start//
#define  BDS_EFI_MEDIA_FW_FILE_BOOT       0x0406 // Type 04; Sub-Type 06
//add-klk-lyang-P000A-end//
#define  BDS_LEGACY_BBS_BOOT              0x0501 //  Type 05; Sub-Type 01
#define  BDS_EFI_MESSAGE_NVME_BOOT        0x0317 // Type 03; Sub-Type 23
#define  BDS_EFI_UNSUPPORT                0xFFFF

UINT16
EFIAPI
GetBootDeviceTypeFromDevicePath (
  IN  EFI_DEVICE_PATH_PROTOCOL     *DevicePath
);

#endif
