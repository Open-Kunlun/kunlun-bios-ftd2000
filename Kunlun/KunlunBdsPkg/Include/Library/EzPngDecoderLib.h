/** @file

*****************************************************************************
 Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technologies (Beijing) Ltd.. All
 Rights Reserved.

 You may not reproduce, distribute, publish, display, perform, modify, adapt,
 transmit, broadcast, present, recite, release, license or otherwise exploit
 any part of this publication in any form, by any means, without the prior
 written permission of Kunlun Technologies (Beijing) Ltd..

*****************************************************************************
**/

#ifndef _EZ_PNG_DECODER_LIB_H_
#define _EZ_PNG_DECODER_LIB_H_
#include <Uefi.h>

EFI_STATUS
EzPngDecoderDecodePng (
  IN  UINT8  *SourceImage,
  IN  UINTN  SourceImageSize,
  OUT UINT8  **DecodedData,
  OUT UINTN  *DecodedDataSize,
  OUT UINTN  *Height,
  OUT UINTN  *Width
  );

#endif /*_EZ_PNG_DECODER_LIB_H_ inclusion guard*/
