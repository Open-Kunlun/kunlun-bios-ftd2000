/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef _KL_SETUP_VAR_RW_LIB_H_
#define _KL_SETUP_VAR_RW_LIB_H_

#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <SetupConfig.h>

EFI_STATUS
KlSetupVarRead (
  IN SYSTEM_SETUP_CONFIGURATION            **Configuration
);

EFI_STATUS
KlSetupVarWrite (
  IN SYSTEM_SETUP_CONFIGURATION            *Configuration
);

#endif
