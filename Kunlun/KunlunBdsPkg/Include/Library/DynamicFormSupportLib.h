/** @file
  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.

  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..


**/

#ifndef __DYNAMIC_FORM_SUPPORT_LIB_H_
#define __DYNAMIC_FORM_SUPPORT_LIB_H_

/**

  Update the Dynamic menus in the Setup.

**/
VOID
UpdateSetupDynamicForm (
  IN EFI_HII_HANDLE              HiiHandle,
  IN EFI_GUID                    *ClassGuid,
  IN EFI_GUID                    *FormSetGuid,
  IN EFI_FORM_ID                 FormId,
  IN UINT16                      Label
  );



#endif
