/** @file
  This library class defines a set of interfaces to customize Display module

Copyright (c) 2013 - 2018, Intel Corporation. All rights reserved.<BR>
Copyright (c) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.
SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef __KL_CUSTOMIZED_DISPLAY_LIB_H__
#define __KL_CUSTOMIZED_DISPLAY_LIB_H__
//add-klk-lyang-P000A-start//
#include <Protocol/KlHiiPopup.h>
//add-klk-lyang-P000A-end//

//add-klk-lyang-P000A-start//
typedef enum {
  DlgYesNo,
  DlgYesNoCancel,
  DlgOk,
  DlgOkCancel
} UI_DIALOG_OPERATION;
//add-klk-lyang-P000A-end//

//add-klk-lyang-P000A-start//

UINTN
DisplayDialog (
  IN  CHAR16                    *ShowString,
  IN  UI_DIALOG_OPERATION       DialogOperator,
  IN  UINTN                     Color
  );

EFI_STATUS
EFIAPI
ExitConfirmDialog (
  BOOLEAN                     IsNvChange,
  EFI_HII_POPUP_SELECTION*    UserSelection
);

UINT8
EFIAPI
GetFieldOptionColor (
  VOID
  );

UINT8
EFIAPI
GetClearLineColor (
  VOID
  );
//add-klk-lyang-P000A-end//
#endif
