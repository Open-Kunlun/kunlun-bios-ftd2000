/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef _SETUP_LIBRARY_H_
#define _SETUP_LIBRARY_H_
#include <Protocol/SetupHiiDataMgr.h>
#include <Protocol/HiiConfigAccess.h>
#include <Protocol/Smbios.h>

typedef struct {
  UINT32                                Signature;
  EFI_HANDLE                            CallbackHandle;
  EFI_HII_CONFIG_ACCESS_PROTOCOL        DriverCallback;
  EFI_HII_HANDLE                        HiiHandle;
  EFI_GUID                              FormsetGuid;
} EFI_CALLBACK_INFO;

#define EFI_CALLBACK_INFO_SIGNATURE SIGNATURE_32('K','C','I','S')
#define EFI_CALLBACK_INFO_FROM_THIS(a) CR (a, EFI_CALLBACK_INFO, DriverCallback, EFI_CALLBACK_INFO_SIGNATURE)



UINT16
GetVarStoreSize (
  IN EFI_HII_HANDLE            HiiHandle,
  IN EFI_GUID                  *FormsetGuid,
  IN EFI_GUID                  *VarStoreGuid,
  IN CHAR8                     *VarStoreName
  );

BOOLEAN
CheckSetupHiiDataManager (
  VOID
  );

CHAR16 *
SetupGetToken (
  IN  EFI_STRING_ID                Token,
  IN  EFI_HII_HANDLE               HiiHandle
  );
EFI_STATUS
EFIAPI
GenericExtractConfig (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  CONST EFI_STRING                       Request,
  OUT EFI_STRING                             *Progress,
  OUT EFI_STRING                             *Results
  );

EFI_STATUS
EFIAPI
GenericRouteConfig (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  CONST EFI_STRING                       Configuration,
  OUT EFI_STRING                             *Progress
  );

/**
  According platform setting to configure setup variable

  @param  VariableGuid           An optional field to indicate the target variable GUID name to use.
  @param  VariableName           An optional field to indicate the target human-readable variable name.
  @param  BufferSize             On input: Length in bytes of buffer to hold retrived data.
                                 On output:
                                   If return EFI_BUFFER_TOO_SMALL, containg length of buffer desired.
  @param  Buffer                 Buffer to hold retrived data.
  @param  RetrieveData           TRUE : Get setup variable from broswer
                                 FALSE: Set current setuputility setting to browser

  @retval EFI_SUCCESS            Setup variable configuration successful
  @retval EFI_ABORTED            Setup variable configuration failed

**/
EFI_STATUS
SetupVariableConfig (
  IN EFI_GUID        *VariableGuid, OPTIONAL
  IN CHAR16          *VariableName, OPTIONAL
  IN UINTN           BufferSize,
  IN UINT8           *Buffer,
  IN BOOLEAN         RetrieveData
  );

EFI_STATUS
InstallMainCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  );

EFI_STATUS
InstallAdvanceCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  );

//[jiangfengzhang-0004-begin]
EFI_STATUS
InstallPowerManageCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  );
//[jiangfengzhang-0004-end]


EFI_STATUS
InstallExitCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  );

/**
  Update the information for the Front Page based on SMBIOS information.

**/
VOID
UpdateSetupPageStrings (
  IN EFI_HII_HANDLE                         HiiHandle
  );

CHAR8 *
KlGetSmbiosString (
  IN VOID                 *SmbiosTable,
  IN SMBIOS_TABLE_STRING  String
  );

BOOLEAN
CheckSmBiosProtocol (
  VOID
  );

VOID InitString(EFI_HII_HANDLE HiiHandle, EFI_STRING_ID StrRef, CHAR16 *sFormat, ...);
VOID InitString1(EFI_HII_HANDLE HiiHandle, EFI_STRING_ID StrRef, CHAR16 *sFormat, ...);

EFI_STATUS
SetupSetConsoleMode (
  VOID
);

extern SETUP_HII_DATA_MANAGMENT_PROTOCOL   *gSetupDataManagment;
//[jiangfengzhang-0004-begin]
extern UINT8 PowerManageVfrBin[];
//[jiangfengzhang-0004-end]
extern unsigned char SetupLibStrings[];
extern UINT8 MainVfrBin[];
extern UINT8 AdvanceVfrBin[];
extern UINT8 ExitVfrBin[];
#endif
