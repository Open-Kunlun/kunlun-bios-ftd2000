/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef _SETUP_HIIDATA_MANAGER_LIB_H_
#define _SETUP_HIIDATA_MANAGER_LIB_H_

#include <Uefi.h>

#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Protocol/SetupHiiDataMgr.h>
#include <SetupConfig.h>


BOOLEAN
CheckSetupHiiDataManager (
  VOID
  );
//[gliu-0060]add-begin
extern    SETUP_HII_DATA_MANAGMENT_PROTOCOL   *gSetupDataManagment;
VOID
RepaintFrameLine (
  VOID
);
//[gliu-0060]add-end
//add-klk-lyang-setup-004-add//
BOOLEAN
CheckValidVer(
  IN UINT32    Ver
);
//add-klk-lyang-setup-004-end//
#endif
