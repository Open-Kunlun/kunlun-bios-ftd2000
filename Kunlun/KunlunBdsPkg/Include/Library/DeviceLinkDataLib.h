/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef _DEVICE_LINK_DATA_LIB_H_
#define _DEVICE_LINK_DATA_LIB_H_

#define NUMBER_ATA_SMART_ATTRIBUTES     30
#define ETH_ALEN    6  /* Size of Ethernet address */

typedef enum {
  PciDevice,
  PciP2pBridge,
  PciCardBusBridge,
  PciUndefined
} PCI_HEADER_TYPE;

typedef enum {
  FieldWidthUINT8,
  FieldWidthUINT16,
  FieldWidthUINT32
} PCIE_CAPREG_FIELD_WIDTH;

typedef enum {
  PcieExplainTypeCommon,
  PcieExplainTypeDevice,
  PcieExplainTypeLink,
  PcieExplainTypeSlot,
  PcieExplainTypeRoot,
  PcieExplainTypeMax
} PCIE_EXPLAIN_TYPE;

//[jiangfengzhang-0003-begin]
typedef enum _NIC_INTERFACE_TYPE_
{
  PcieNic,
  NotPcieNic
}EN_NIC_INTERFACE_TYPE;
//[jiangfengzhang-0003-end]

#pragma pack(1)
//
// Data region after PCI configuration header(for cardbus bridge)
//
typedef struct {
  UINT16  SubVendorId;  // Subsystem Vendor ID
  UINT16  SubSystemId;  // Subsystem ID
  UINT32  LegacyBase;   // Optional 16-Bit PC Card Legacy
  // Mode Base Address
  //
  UINT32  Data[46];
} PCI_CARDBUS_DATA;

typedef union {
  PCI_DEVICE_HEADER_TYPE_REGION Device;
  PCI_BRIDGE_CONTROL_REGISTER   Bridge;
  PCI_CARDBUS_CONTROL_REGISTER  CardBus;
} NON_COMMON_UNION;

typedef struct {
  PCI_DEVICE_INDEPENDENT_REGION Common;
  NON_COMMON_UNION              NonCommon;
  UINT32                        Data[48];
} PCI_CONFIG_SPACE;

typedef struct {
  CHAR16 MaxLinkSpeed[16];
  CHAR16 CurLinkSpeed[16];
  UINT8  MaxLinkWidth;
  UINT8  NegotiatedLinkWidth;
  UINT16 MaxPayloadSize;
  UINT16 MaxReadRequestSize;
  UINT8  PcieCapabilityPtr;
} PCI_CAPABILITY;

typedef struct  {
  UINT8      Id;
  UINT16     Flags;
  UINT8      Current;
  UINT8      Worst;
  UINT8      Raw[6];
  UINT8      Reserv;
} ATA_SMART_ATTRIBUTE;

typedef struct  {
  UINT16                 RevNumber;
  ATA_SMART_ATTRIBUTE    VendorAttributes [NUMBER_ATA_SMART_ATTRIBUTES];
  UINT8                  OfflineDataCollectionStatus;
  UINT8                  SelfTestExecStatus;  //IBM # segments for offline collection
  UINT16                 TotalTimeToCompleteOffLine; // IBM different
  UINT8                  VendorSpecific366; // Maxtor & IBM curent segment pointer
  UINT8                  OfflineDataCollectionCapability;
  UINT16                 SmartCapability;
  UINT8                  Errorlog_Capability;
  UINT8                  VendorSpecific371;  // Maxtor, IBM: self-test failure checkpoint see below!
  UINT8                  ShortTestCompletionTime;
  UINT8                  ExtendTestCompletionTimeB; // If 0xff, use 16-bit value below
  UINT8                  ConveyanceTestCompletionTime;
  UINT16                 ExtendTestCompletionTimeW; // e04130r2, added to T13/1699-D Revision 1c, April 2005
  UINT8                  Reserved_377_385[9];
  UINT8                  VendorSpecific_386_510[125]; // Maxtor bytes 508-509 Attribute/Threshold Revision #
  UINT8                  Chksum;
} ATA_SMART_DATA;

#pragma pack()

typedef struct _PCI_CONFIG {
  PCI_CONFIG_SPACE            PciInfo;
  PCI_CAPABILITY              PciCap;
} PCI_CONFIG;

typedef struct _PCI_CONFIG_FORM_ENTRY {
  LIST_ENTRY                    Link;
  EFI_HANDLE                    Controller;
  UINTN                         Bus;
  UINTN                         Device;
  UINTN                         Function;
  EFI_DEVICE_PATH_PROTOCOL      *DevicePath;
  CHAR16                        PciString[64];
  EFI_STRING_ID                 TitleToken;
  EFI_STRING_ID                 TitleHelpToken;

  PCI_CONFIG                    IfrData;
  EFI_PCI_IO_PROTOCOL           *PciioInstance;
} PCI_CONFIG_FORM_ENTRY;

typedef struct _SATA_INFO_COMMON {
  CHAR8                         SerialNumber[21];
  CHAR8                         ModeName[41];
  CHAR8                         FirmwareVersion[9];
} SATA_INFO_COMMON;

typedef struct _SATA_CONFIG {
  BOOLEAN                     HddFlag;         //TRUE:HDD   FALSE:CDROM
  BOOLEAN                     SmartSupport;
  SATA_INFO_COMMON            Common;
  UINT64                      Rotation;
  UINT64                      SizeInByte;
//smart data
  UINT64                      PowerCycleCount;
  UINT64                      PowerOnHours;
  INT64                       Temperature;
  UINT64                      ReadErrorRate;
  UINT64                      RetiredBlockCount;
  UINT64                      UnexpectedPowerLossCount;
  UINT64                      TotalWrite;
  UINT64                      LifeLeft;
  CHAR16                      CurLinkSpeed[16];
  UINT8                       SmartStatus;
} SATA_CONFIG;

typedef struct _SATA_CONFIG_FORM_ENTRY {
  LIST_ENTRY                    Link;
  EFI_HANDLE                    Controller;
  UINTN                         Bus;
  UINTN                         Device;
  UINTN                         Function;
  UINT16                        Port;
  UINT16                        PortMultiplierPort;
  EFI_DEVICE_PATH_PROTOCOL      *DevicePath;
  EFI_ATA_PASS_THRU_PROTOCOL    *AtaPassThru;
  EFI_PCI_IO_PROTOCOL           *PciioInstance;
  EFI_BLOCK_IO_PROTOCOL         *BlkIo;
  EFI_DISK_INFO_PROTOCOL        *DiskInfo;
  CHAR16                        SataString[64];
  EFI_STRING_ID                 PromptToken;
  EFI_STRING_ID                 HelpToken;
  EFI_STRING_ID                 TextToken;
  SATA_CONFIG                   SataData;
} SATA_CONFIG_FORM_ENTRY;



typedef struct _NVME_CONFIG {
  UINT16 Vid;                 /* PCI Vendor ID */
  UINT16 Ssvid;               /* PCI sub-system vendor ID */
  UINT8  Sn[21];              /* Product serial number */

  UINT8  Mn[41];              /* Product model number */
  UINT8  Fr[9];               /* Firmware Revision */
  UINT8  Mdts;                /* Maximum Data Transfer Size */
  UINT64 SizeInByte;
} NVME_CONFIG;

typedef struct _NVME_CONFIG_FORM_ENTRY {
  LIST_ENTRY                            Link;
  EFI_HANDLE                            Controller;
  UINTN                                 Bus;
  UINTN                                 Device;
  UINTN                                 Function;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;
  EFI_NVM_EXPRESS_PASS_THRU_PROTOCOL    *PassThru;
  EFI_PCI_IO_PROTOCOL                   *PciioInstance;
  EFI_BLOCK_IO_PROTOCOL                 *BlkIo;
  CHAR16                                NvmeString[64];
  EFI_STRING_ID                         PromptToken;
  EFI_STRING_ID                         HelpToken;
  EFI_STRING_ID                         TextToken;
  NVME_CONFIG                           NvmeData;
} NVME_CONFIG_FORM_ENTRY;


typedef struct _NIC_CONFIG {
  UINT64                   VendorId;
  UINT64                   DeviceId;
  EFI_SIMPLE_NETWORK_MODE  Mode;
  UINT8                    MacAddress[ETH_ALEN];
  BOOLEAN                  SnpSuport;
} NIC_CONFIG;

typedef struct _NIC_CONFIG_FORM_ENTRY {
  LIST_ENTRY                            Link;
  EFI_HANDLE                            Controller;
  UINTN                                 Bus;
  UINTN                                 Device;
  UINTN                                 Function;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;
  EFI_PCI_IO_PROTOCOL                   *PciioInstance;
  EFI_BLOCK_IO_PROTOCOL                 *BlkIo;
  CHAR16                                NicString[64];
  EFI_STRING_ID                         PromptToken;
  EFI_STRING_ID                         HelpToken;
  EFI_STRING_ID                         TextToken;
  NIC_CONFIG                            NicData;
//[jiangfengzhang-0003-begin]
 EN_NIC_INTERFACE_TYPE                  NicType;
//[jiangfengzhang-0003-end]
  UINT8                                 FtGmacNicLinkChange;
} NIC_CONFIG_FORM_ENTRY;

typedef struct{
  CHAR8   ClassName[24];
  CHAR16  Manufacturer[40];
  CHAR16  Product[40];
  CHAR16  SerialNum[40];
  UINT16  BcdUSB;
  UINT64  VendorId;
  UINT64  ProductId;
}USB_CONFIG;

typedef struct _USB_CONFIG_FORM_ENTRY {
  LIST_ENTRY                    Link;
  EFI_HANDLE                    Controller;
  UINTN                         Bus;
  UINTN                         Device;
  UINTN                         Function;
  UINT16                        Port;
  EFI_DEVICE_PATH_PROTOCOL      *DevicePath;
  EFI_USB_IO_PROTOCOL           *UsbIo;
  EFI_PCI_IO_PROTOCOL           *PciioInstance;
  CHAR16                        UsbString[64];
  EFI_STRING_ID                 PromptToken;
  EFI_STRING_ID                 HelpToken;
  EFI_STRING_ID                 TextToken;
  USB_CONFIG                    UsbData;
} USB_CONFIG_FORM_ENTRY;
//[jckuang-0003]
//[gliu-0031]add-start
typedef struct _X100_CONFIG_VER {
  BOOLEAN                  IsX100Exit;
  UINT8                    MainVersion;
  UINT8                    SubVersion;
  UINT8                    Reserver;
} X100_CONFIG_VER;
//[gliu-0031]add-end

typedef struct _X100_CONFIG_FORM_ENTRY {
  LIST_ENTRY                            Link;
  EFI_HANDLE                            Controller;
  UINTN                                 Bus;
  UINTN                                 Device;
  UINTN                                 Function;
  EFI_DEVICE_PATH_PROTOCOL              *DevicePath;
  EFI_PCI_IO_PROTOCOL                   *PciioInstance;
  X100_CONFIG_VER                       X100Data;
} X100_CONFIG_FORM_ENTRY;
//[jckuang-0003]

typedef EFI_STATUS (*PCIE_EXPLAIN_FUNCTION) (IN PCI_CAPABILITY_PCIEXP *PciExpressCap, IN  PCI_CONFIG_FORM_ENTRY         *ConfigFormEntry);

typedef struct
{
  //UINT16                  Token;
  UINTN                   Offset;
  PCIE_CAPREG_FIELD_WIDTH Width;
  PCIE_EXPLAIN_FUNCTION   Func;
  PCIE_EXPLAIN_TYPE       Type;
} PCIE_EXPLAIN_STRUCT;


PCI_CONFIG_FORM_ENTRY *
PciGetConfigFormEntryByIndex (
  IN UINT32 Index
  );

typedef struct _DEVICE_LINK_DATA_PROTOCOL{
  UINT32                             Version;
  LIST_ENTRY                         *PciLinkHead;
  LIST_ENTRY                         *UsbLinkHead;
  LIST_ENTRY                         *SataLinkHead;
  LIST_ENTRY                         *NicLinkHead;
  LIST_ENTRY                         *NvmeLinkHead;
  //[jckuang-0003]
  LIST_ENTRY                         *X100LinkHead;
  //[jckuang-0003]
}DEVICE_LINK_DATA_PROTOCOL;

EFI_STATUS
EFIAPI
DeviceLinkDataInit (
IN  DEVICE_LINK_DATA_PROTOCOL     **DLinkDataProtocol
  );

EFI_STATUS
UsbLinkConstructor(
IN LIST_ENTRY                *ListHead
 );

EFI_STATUS
SataLinkConstructor(
IN LIST_ENTRY                *ListHead
 );

EFI_STATUS
NicLinkConstructor(
IN LIST_ENTRY                *ListHead
 );

EFI_STATUS
NvmeLinkConstructor(
IN LIST_ENTRY                *ListHead
 );

EFI_STATUS
PciLinkConstructor(
IN LIST_ENTRY                *ListHead
 );
//[jckuang-0003]
 EFI_STATUS
X100LinkConstructor(
IN LIST_ENTRY                *ListHead
 );
//[jckuang-0003]
extern EFI_GUID gDeviceLinkDataProtocolGuid;
#endif
