/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef _BUZZER_LIB_H_
#define _BUZZER_LIB_H_

#include <Uefi.h>


#define ERROR_TYPE0  0
#define ERROR_TYPE1  1
#define ERROR_TYPE2  2
#define ERROR_TYPE3  3
#define ERROR_TYPE4  4
#define ERROR_TYPE5  5
#define ERROR_TYPE6  6
#define ERROR_TYPE7  7




VOID SetupPromptWait_Boot(

 );

 VOID SetupPromptWait_CpuFan(
 );

 VOID Buzzer_Beep(
  UINTN ErrorType
  );
#endif
