/** @file
Copyright (c) 2006-2023, Kunlun BIOS, KunLun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of KunLun Technology (Beijing) Co., Ltd..

Module Name:

  KlCpuAndDdrSettingLib.h

**/
#ifndef _KL_CPU_AND_DDR_SETTING_H
#define _KL_CPU_AND_DDR_SETTING_H

#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/KlSetupVarRWLib.h>
#include <Uefi/UefiBaseType.h>
#include <Uefi/UefiSpec.h>

#include <Library/FlashLib.h>
#include <Library/PhytiumSpiNorFlashLib.h>


#define MIN_CORE_FREQ           1000
#define MAX_CORE_FREQ           2300


#define BE64KSIZE                   0x10000

#define PARAMETER_ADDRESS64         0x000A0000
#define PARAMETER_ADDRESS64_PBF     0x80000
#define CPU_SPEED_OFFSET            0x0010
#define LMU_SPEED_OFFSET            0x0018
#define CPU_SETTING_OFFSET          0x0210
#define PCIE_SETTING_PEU_OFFSET     0x0100
#define PM_PBF_OFFSET               0x600
#define PM_MCU_OFFSET               0x300

#define PM_PLL_OFFSET              (0x0)
#define PM_PCIE_OFFSET             (0x100)
#define PM_COMMON_OFFSET           (0x200)
#define PM_DDR_OFFSET              (0x300)
#define PM_BOARD_OFFSET            (0x400)
#define PM_SECURE_OFFSET           (0x500)

#define PARAMETER_PLL_MAGIC        0x54460010
#define PARAMETER_PCIE_MAGIC       0x54460011
#define PARAMETER_COMMON_MAGIC     0x54460013
#define PARAMETER_DDR_MAGIC        0x54460014
#define PARAMETER_BOARD_MAGIC      0x54460015
#define PARAMETER_PBF_MAGIC        0x54460000
#define PARAMETER_MCU_MAGIC			   0x54460014

#define TOTAL_PEU_NUMBERS           2
#define OTAL_PEU_CORE_NUMBERS       3

#define PEU_C_OFFSET_ENABLE         9
#define PEU1_OFFSET                16

typedef struct {
    UINT32 Magic;
    UINT32 Version;
    UINT32 Size;
    UINT8  Rev1[4];
}__attribute__((aligned(sizeof(UINT64)))) PARAMETER_HEADER;

typedef struct {
  PARAMETER_HEADER Head;
  UINT32 IndependentTree;
  UINT32 FunctionConfig;
  UINT8  Rev0[16];
  UINT32 Peu0ControllerConfig[3];
  UINT32 Peu0ControllerEqualization[3];
  UINT8  Rev1[80];
  UINT32 Peu1ControllerConfig[3];
  UINT32 Peu1ControllerEqualization[3];
}__attribute__((aligned(sizeof(UINT64)))) PARAMETER_PEU_CONFIG;

EFI_STATUS
EFIAPI
KlCpuAndDdrSettingInit(
  VOID
);

EFI_STATUS
FastBootPrintLevelSet(
  SYSTEM_SETUP_CONFIGURATION  *SetupConfigVal
);

EFI_STATUS
StoreDdrTrainInfo (
  VOID
  );
#endif
