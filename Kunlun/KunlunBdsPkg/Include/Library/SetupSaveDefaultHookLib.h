/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef __SETUP_SAVEDEFAULT_HOOK_LIB_H_
#define __SETUP_SAVEDEFAULT_HOOK_LIB_H_


EFI_STATUS
SetupDefaultCallBack (
  VOID
  );

EFI_STATUS
SetupSubmitCallBack (
  VOID
  );

EFI_STATUS
SetupDefaultAuth (
  VOID
  );
EFI_STATUS
SetupSubmitAuth (
  VOID
  );
#endif
