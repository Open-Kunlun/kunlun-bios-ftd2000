/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef _SETUP_CONFIG_H_
#define _SETUP_CONFIG_H_
//[jckuang-0003]
#include "ChipX100ConfigData.h"
#include "CpuConfigData.h"

//[jckuang-0003]
#define SETUP_VARIABLE_NAME             L"SystemSetupConfig"
#define SCREENSHOT_POPUP_VARIABLE_NAME  L"ScreenshotPopupEnable"   //[yfliu-0003]

#define SETUP_UTILITY_CLASS             1
#define SETUP_UTILITY_SUBCLASS          0
#define EFI_USER_ACCESS_TWO             0x04
#define EFI_USER_ACCESS_THREE           0x05
#define ROOT_FORM_ID                    1

#define FORM_BOOT_CHG_ID                0x1004
#define FORM_BOOT_LABLE                 0x2000
#define CONFIGURATION_VARSTORE_ID       0x1234
#define BOOT_VARSTORE_ID                0x1489
#define BOOT_OPTION_ORDER_QUESTION_ID   0x148A
#define BOOT_TEMP_VARSTORE_ID           0x2234

#define SYSTEM_PASSWORD_ADMIN 0
#define SYSTEM_PASSWORD_USER  1
#define PASSWORD_MAX_SIZE 32
//[jqmao-001]>>>
//#define PASSWORD_MIN_SIZE 8
#define PASSWORD_MIN_SIZE 1
//[jqmao-001]<<<




#define PASSWORD_HASH_MAX_SIZE          64//SHA256 Digest Length, since of password max lenght is 32 characters, whichi is equal to 64 bytes

#define KEY_ADMIN_PASSWORD              0x3000
#define KEY_USER_PASSWORD               0x3001

#define FORM_PBF_SET                    0x3002
//[gliu-0009]
#define FORM_MEMORY_FAST_TRAIN_SET      0x3003
//[gliu-0009]

#define GRAPHICS_RESOLUTION_CHOOSE      0x3004//[zhyli-0008]-add
#define MONITOR_INFO_FORM_ID            10500
#define HW_MONITOR_FORM_ID              10503

#define CPU_CONFIG_FORM_ID              10260
#define MEMORY_CONFIG_FORM_ID           10259
#define DEVICE_INFO_FORM_ID               0xA87D
#define SATA_DEVICE_INFO_FORM_ID          0xA87E
#define USB_DEVICE_INFO_FORM_ID           0xA87F
#define PCI_DEVICE_INFO_FORM_ID            0xA880
#define NVME_DEVICE_INFO_FORM_ID          0xA881
#define NIC_DEVICE_INFO_FORM_ID           0xA882
#define DISP_DEVICE_FORM_ID               0xA883
//[gliu-0004]
#define WATCHDOG_CONFIG_FORM_ID           0xA884
//[gliu-0004]
//[jckuang-0003]
#define X100_CONFIG_FORM_ID               0xA885
//[jckuang-0003]
//[jiangfengzhang-0002]
#define SR_IOV_CONFIG_FORM_ID             0xA886
//[jiangfengzhang-0002]
//[jiangfengzhang-0004]
#define RTC_WAKEUP_FORM_ID          0xA897
//[jiangfengzhang-0004]
#define TERMINAL_FORM_ID                    10044
//[jckuang-0013]
#define LPC_CONFIG_FORM_ID                  10045
//[jckuang-0013]
#define STACK_FORM_ID                       10185

#define EXIT_MAIN_SAVE_AND_RESET_ID         10402
#define EXIT_MAIN_DISCARD_AND_EXIT_ID       10403
#define EXIT_MAIN_RESTORE_DEFAULTS_ID       10404
#define EXIT_RESET_SYSTEM_ID                10405
#define EXIT_SHUTDOWN_SYSTEM_ID             10406
#define EXIT_UPDATE_BIOS_ID                 10407
#define EXIT_SAVE_ID                        10408
#define EXIT_RESOTRE_MODIFY_ID              10409
#define EXIT_UPDATE_X100_ID                 10410
#define PREVENT_HDD_REPLACE_ID              10411
#define UPDATA_REPLACE_FORM_KEY             10412
#define SETUP_PARA_IMPORT_ID                10413
#define SETUP_PARA_EXPORT_ID                10414
#define POWER_MANAGE_RTC_WAKEUP_ID          10415
#define SETUP_UART_UPDATE_ID                10416
#define SETUP_EC_UPDATE_ID                  10417

#define LABEL_THIRD_PARTY_DRIVER_INFORMATION     0x1000
#define LABEL_END                           0xffff
#define LABEL_SECURITY_DRIVER               0x1001
#define LABEL_BOOT_OPTION                   0x1100
#define LABEL_BOOT_OPTION_END               0x1101
#define LABEL_UPDATE_BIOS                   0x1102

#define SATA_DEVICE_ENTRY_LABEL       0x3333
#define SATA_DEVICE_LABEL_END         0xffff


#define NIC_DEVICE_ENTRY_LABEL        0x3334
#define NIC_DEVICE_LABEL_END          0xffff

#define NVME_DEVICE_ENTRY_LABEL       0x3335
#define NVME_DEVICE_LABEL_END         0xffff

#define USB_DEVICE_ENTRY_LABEL        0x3336
#define USB_DEVICE_LABEL_END          0xffff

#define PCI_DEVICE_ENTRY_LABEL        0x3337
#define PCI_DEVICE_LABEL_END          0xffff

#define MEM_DEVICE_ENTRY_LABEL        0x3338
#define MEM_DEVICE_LABEL_END          0xffff
//[jckuang-0003]
#define X100_DEVICE_ENTRY_LABEL        0x3339
#define X100_DEVICE_LABEL_END          0xffff
//[jckuang-0003]
#define NIC_LINK_ENTRY_LABEL          0x333A
#define NIC_LINK_ENTRY_END            0xffff

#define HTTP_BOOT_CONFIG_LABEL        0x333B
#define HTTP_BOOT_CONFIG_END          0xffff
//[gliu-0060]add-begin
#define PXE_IPV4_CONFIG_LABEL         0x333C
#define PXE_IPV4_CONFIG_END           0xffff
#define PXE_IPV6_CONFIG_LABEL         0x333D
#define PXE_IPV6_CONFIG_END           0xffff
//[gliu-0060]add-end

#define SELECT_GRAPHICS_RES_LABEL     0x333E
#define SELECT_GRAPHICS_RES_END       0xffff

#define FORMID_PCI_DEVICE_INFO_FORM        0xA884


#define MAX_BOOT_MENU_NUMBER                50

#define SYSTEM_CONFIGURATION_GUID { 0x3336B68B, 0x9F08, 0x47DA, {0xBC, 0x4F, 0xC9, 0x95, 0xC0, 0xA4, 0x7C, 0x4C} }
#define SYSTEM_ACCESS_GUID        { 0xE770BB69, 0xBCB4, 0x4D04, {0x9E, 0x97, 0x23, 0xFF, 0x94, 0x56, 0xFE, 0xAC} }
#define KUNLUN_HIDE_HOEKEY_GUID   { 0x9B6D7CD8, 0x192C, 0x44AB, {0xAF, 0xE2, 0x67, 0x01, 0x78, 0xBE, 0x3B, 0x06} }

#define BOOT_TEMP_ORDER_GUID      { 0xc0fa32f7, 0x28dd, 0x44b9, {0xb9, 0xae, 0xf6, 0xFF, 0x94, 0x56, 0x09, 0x31} }
#define SETUP_UTILITY_FORMSET_CLASS_GUID {0x9f85453e, 0x2f03, 0x4989, 0xad, 0x3b, 0x4a, 0x84, 0x07, 0x91, 0xaf, 0x3a}

#define FORMSET_ID_GUID_MAIN      {0xc1e0b01a, 0x607e, 0x4b75, {0xb8, 0xbb, 0x06, 0x31, 0xec, 0xfa, 0xac, 0xf2} }
#define FORMSET_ID_GUID_BOOT      {0x6C54FFF0, 0xF3A3, 0x4435, {0xAA, 0xDD, 0x93, 0x6E, 0xC8, 0x30, 0x8, 0x4F } }
#define FORMSET_ID_GUID_SECURITY  {0x5204f764, 0xdf25, 0x48a2, {0xb3, 0x37, 0x9e, 0xc1, 0x22, 0xb8, 0x5e, 0x0d} }
#define FORMSET_ID_GUID_EXIT      {0xb6936426, 0xfb04, 0x4a7b, {0xaa, 0x51, 0xfd, 0x49, 0x39, 0x7c, 0xdc, 0x01} }
#define FORMSET_ID_GUID_POWER_MANAGE    {0xb06546e3,0xfd5f,0x4556,{0xa2,0x16,0xe2,0xfb,0x70,0x7a,0x9f,0x87}}




#define FORMSET_ID_GUID_ADVANCE   {0x64a5ba22, 0xbd4, 0x4a35, {0xbd, 0x8f, 0xe5, 0xdc, 0xc8, 0x90, 0x1d, 0x1d } }
#define DEVICE_INFO_FORMSET_GUID  {0x182d38f4, 0xf5e6, 0x6de6, {0x7a, 0xf9, 0xa3, 0x4a, 0xbb, 0xc9, 0xd0, 0xed} }
#define MONITOR_INFO_FORMSET_GUID {0xb0c4155c, 0x2ce4, 0x4416, {0x95, 0x4c, 0xf7, 0xc9, 0xce, 0xfd, 0x3a, 0x28} }

#define FORMSET_ID_GUID_PCI       {0x21423ee8, 0x72e1, 0x49cb, {0xad, 0xa0, 0xa4, 0xbc, 0xcd, 0xc8, 0x02, 0x00} }
#define FORMSET_ID_GUID_SATA      {0x6e1a69c0, 0xa06c, 0x4edd, {0xa0, 0x92, 0x79, 0xd5, 0x19, 0xd0, 0xf5, 0x97} }
#define FORMSET_ID_GUID_NVME      {0x2815b339, 0x264f, 0x41ee, {0xaa, 0x23, 0x0b, 0xe4, 0x4d, 0xa0, 0x32, 0xd8} }
#define FORMSET_ID_GUID_NIC       {0xc1222be4, 0xeff8, 0x4f6b, {0x91, 0x4d, 0x6d, 0x11, 0x9d, 0xa2, 0xd4, 0xa9} }
#define FORMSET_ID_GUID_USB       {0x995def3b, 0x2c45, 0x4804, {0xbf, 0x0e, 0x77, 0x01, 0xdb, 0x59, 0x93, 0x44} }
//[jckuang-0003]
#define FORMSET_ID_GUID_X100      {0x86CCC1BC, 0x2B5E, 0x6FFF, {0xAA, 0xDE, 0xE0, 0xBA, 0x5F, 0x7D, 0x57, 0xF7} }
//[jckuang-0003]
#define SUBTITLE(Text) subtitle text = Text;
#define SEPARATOR SUBTITLE(STRING_TOKEN(STR_EMPTY))

#define MAIN_DATE_AND_TIME_FORM             10010
#define LABEL_SELECT_LANGUAGE               10502
#define LABEL_SELECT_LANGUAGE_END           10503



#define MIN_MEDIA_DETECT_COUNT       1
#define MAX_MEDIA_DETECT_COUNT      10
#define DEFAULT_MEDIA_DETECT_COUNT  MAX_MEDIA_DETECT_COUNT  // Should be within MIN_MEDIA_DETECT_COUNT and MAX_MEDIA_DETECT_COUNT range
#define FRAME_PER_SECOND        100
#define TOTAL_SERIAL_PORTS      4
//#define SYSTEM_CONFIGURATION_SETUP_GUID  { 0x66851C1D, 0x9D2, 0x4ABF, { 0x92, 0xFD, 0x71, 0x75, 0x4B, 0xEE, 0x0, 0xB7 } }
//[gliu-0009]
#define DDR_TRAIN_INFO_CHECK  0x524444
//[gliu-0009]

#define PWDUNLOCK             0
#define PWDADMINLOCK          1
#define PWDUSERLOCK           2
#define MIN_PWR_HISTORY_NUM   3
#define MAX_PWR_HISTORY_NUM   6

#pragma pack(1)

typedef struct {
  UINT8 Hour;
  UINT8 Minute;
  UINT8 Second;
} WAKEUP_TIME;

typedef struct {
  UINT16 Year;
  UINT8  Month;
  UINT8  Date;
} WAKEUP_DATE;

typedef struct _SYSTEM_ACCESS
{
  //
  // Passwords
  //
  UINT8   Lock;
  UINT8   Access;     // 0 means can modify the content of SETUP, 1 means cannot modify
  UINT16  BootTime;   // 0 means the first boot
  UINT16  UserPassword[PASSWORD_MAX_SIZE];
  UINT8   UserSalt[PASSWORD_HASH_MAX_SIZE];
  UINT16  AdminPassword[PASSWORD_MAX_SIZE];
  UINT8   AdminSalt[PASSWORD_HASH_MAX_SIZE];
  UINT16  AdminSaveYear;
  UINT8   AdminSaveMonth;
  UINT8   AdminSaveDay;
  UINT8   AdminHour;
  UINT8   AdminMinute;
  UINT16  UserSaveYear;
  UINT8   UserSaveMonth;
  UINT8   UserSaveDay;
  UINT8   UserHour;
  UINT8   UserMinute;
} SYSTEM_ACCESS;
//[gliu-0004]
typedef struct{
  UINT8             Policy;//[gliu-0064]
  UINT8             Enable;
  UINT8             WatchdogTimeOut;
} WATCHDOG_CONFIG;
typedef struct {
    UINT16         BootOptionOrder[MAX_BOOT_MENU_NUMBER];
}BOOT_TEMP_VAR;

//[-start-klk-lyang-010-20221202-add]//
typedef struct {
    UINT8         HideSwitch;
}KUNLUN_HIDE_HOEKEY;
//[-end-klk-lyang-010-20221202-add]//
//[gliu-0004]
typedef struct {

  //
  // Kernel system configuration
  //


 //[jqmao-001]
    UINT16  PasswordLen;
   //[jqmao-001]

  //[jqmao-003]
    UINT16   NumberLock;
     //[jqmao-003]
    UINT16 TimeLock;
   //[jqmao-002]
    UINT16  HistoryCodeTime;
    //[jqmao-002]
    UINT8 ValidTime;


  //[gliu-0004]
  WATCHDOG_CONFIG watchdog;
  //[gliu-0004]
  //[gliu-0005]
  UINT8           IsEcExisted;                     //0-no existed , 1-existed
  //[gliu-0005]
  //CPU config
  UINT32          CoreSpeed;
  UINT8           FastTrainEnable;//[gliu-0009]
  UINT32          LmuSpeed;
#define TOTAL_CORE_NUMBERS 64
  UINT8           CoreSettingEnable[TOTAL_CORE_NUMBERS];
#define TOTAL_PEU_NUMBERS           2
#define TOTAL_PEU_CORE_NUMBERS      3
  UINT8           PcieSettingSplitMode[TOTAL_PEU_NUMBERS];
  UINT8           PcieSettingPeu0Enable[TOTAL_PEU_CORE_NUMBERS];
  UINT8           PcieSettingPeu1Enable[TOTAL_PEU_CORE_NUMBERS];
  UINT8           PcieSettingPeu0Speed[TOTAL_PEU_CORE_NUMBERS];
  UINT8           PcieSettingPeu1Speed[TOTAL_PEU_CORE_NUMBERS];
  UINT8           PcieSettingPeu0Eq[TOTAL_PEU_CORE_NUMBERS];
  UINT8           PcieSettingPeu1Eq[TOTAL_PEU_CORE_NUMBERS];

  //UINT8          MemSlotTotal;
  UINT8          RESERVE[5];
  //UINT16         KunlunBootTimeOut;
  //[jckuang-0013]
  UINT8          LpcSerirqMode;
  //[jckuang-0013]
  UINT8          NetController;
  UINT8          SATAController;
  UINT8          USBController;
  //[gliu-0065]add-begin
  UINT8          SmartUsb;
  //[gliu-0065]add-end
  UINT8          NVMEController;
  UINT8          DisplaySwtich;
  UINT8          DisplayPolicy;
  //[jckuang-0003]
  X100_CONFIG    X100Config;//[gliu-0031]
  //[jckuang-0003]
//Boot start
  //
  // Three questions displayed at the main page
  // for Timeout, BootNext, Variables respectively
  //
  UINT16         BootTimeOut;
  UINT32         BootNext;
  UINT8          FastBoot;
  UINT8          QuietBoot;
  UINT8          NumLock;
  //[jckuang-0010]
  UINT8          SetupWaitTimeOut;
  //[jckuang-0010]
  //
  // Boot or Driver Option Order storage
  // The value is the OptionNumber+1 because the order list value cannot be 0
  // Use UINT32 to hold the potential value 0xFFFF+1=0x10000
  //
  //UINT16         BootOptionOrder[MAX_BOOT_MENU_NUMBER];
  UINT8          BootReserve[100];
//remove-klk-lyang-P000A-start//
  //
  // Boot or Driver Option Delete storage
  //
  //BOOLEAN        BootOptionDel[MAX_BOOT_MENU_NUMBER];
  //BOOLEAN        BootOptionDelMark[MAX_BOOT_MENU_NUMBER];


  //UINT16         BootDescriptionData[MAX_BOOT_MENU_NUMBER];
  //UINT16         BootOptionalData[127];
  //BOOLEAN        BootOptionChanged;
//remove-klk-lyang-P000A-end//
  UINT8          NewDevicePolicy;
//boot end

//NETWORK_STACK
  UINT8             NetStackEnable; //0:off 1:ipv4 2:ipv6
//[jiangfengzhang-0003-begin]
  UINT8       Ipv4Pxe;
  UINT8       Ipv6Pxe;
  UINT8       Ipv4Http;
  UINT8       Ipv6Http;
//[jiangfengzhang-0003-end]

  UINT8             IpsecCertificate;
  UINT8             PxeBootWaitTime;
  UINT8             MediaDetectCount;

//wake up
//[jiangfengzhang-0004-begin]
//[gliu-0024]add-start
  UINT8     RtcWakeUpPolicy;
//[gliu-0024]add-end
//[jiangfengzhang-0004-end]
//NETWORK_STACK end
  UINT8     NicWakeUpPolicy;
  //
  // Kernel system configuration
  //
  UINT8             FpControl;
  UINT8             Replace;

  UINT8             DispCtlSta;
  UINT8             SaveSysVar;
  WAKEUP_TIME       WakeUpTime;
  WAKEUP_DATE       WakeUpDate;
  UINT8             WakeUpWeek;
  UINT8             KernelRSV[52];
  //
  // Chipset system configuration
  //
  UINT8             ChipsetRSV[50];

  //PBF Level Change add by fc
  UINT16         PBFLevel;
  //PBF Level Change end

 //[jiangfengzhang-0002]
  UINT8      SriovSwitch;           //0-disable , 1-enable
//[jiangfengzhang-0002]
//[jckuang-0016]
  UINT8      ShellBootSwitch;       //0-disable , 1-enable
//[jckuang-0016]
  UINT8   Policy;
  UINT8   Encrypt;
  UINT16  IterationCount;
  UINT8   VersionVerify;
  UINT8   SignatureVerify;
  // Libo: The Event Log Policy when storage is full.
  UINT8   EventLogPolicy;
  AUTO_WAKE_UP_CONFIG_DATA    AutoWakeUpConfig;
  UINT8   GraphicsResolution;//[zhyli-0008]-add
  //Reserve for OEM:when add a new variable,it's size must substrct from OEMRSV or KernelRSV or ChipsetRSV
  UINT8          OEMRSV[28];  // Reserve for OEM
} SYSTEM_SETUP_CONFIGURATION;

#define TOTAL_USB_KB_NUMBER 10
#define TOTAL_USB_MS_NUMBER 10
#define PATH_TEXT_LENGTH    300

#pragma pack()
extern EFI_GUID gSystemConfigurationGuid;  // SYSTEM_CONFIGURATION_GUID
extern EFI_GUID gSystemAccessGuid;         // SYSTEM_ACCESS_GUID
extern EFI_GUID gKunlunHideHotkeyGuid;     // SYSTEM_ACCESS_GUID
extern EFI_GUID gFormsetIdGuidMain;        // FORMSET_ID_GUID_MAIN
extern EFI_GUID gFormsetIdGuidAdvance;     // FORMSET_ID_GUID_ADVANCE
extern EFI_GUID gFormsetIdGuidSecurity;    // FORMSET_ID_GUID_SECURITY
extern EFI_GUID gFormsetIdGuidBoot;        // FORMSET_ID_GUID_BOOT
extern EFI_GUID gFormsetIdGuidExit;        // FORMSET_ID_GUID_EXIT
extern EFI_GUID gFormsetIdGuidPci;         // FORMSET_ID_GUID_PCI
extern EFI_GUID gFormsetIdGuidSata;        // FORMSET_ID_GUID_SATA
extern EFI_GUID gFormsetIdGuidNvme;        // FORMSET_ID_GUID_NVME
extern EFI_GUID gFormsetIdGuidNic;         // FORMSET_ID_GUID_NIC
extern EFI_GUID gFormsetIdGuidUsb;         // FORMSET_ID_GUID_USB
extern EFI_GUID gFormsetIdGuidRaidHdd;     // FORMSET_ID_GUID_RAID_HDD
#endif
