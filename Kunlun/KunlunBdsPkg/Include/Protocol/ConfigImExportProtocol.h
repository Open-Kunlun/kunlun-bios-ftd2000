/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef _CONFIG_IMEXPORT_PROTOCOL_H_
#define _CONFIG_IMEXPORT_PROTOCOL_H_

#define EFI_CONFIG_IMEXPORT_PROTOCOL_GUID \
  {0x440D72B0, 0x528B, 0x92DE, {0x80, 0x30, 0x1F, 0x5E, 0x39, 0x52, 0x53, 0x28}}

//
//EFI_CONFIG_EXPORT function Flags
//
#define EXPORT_JSON_MAX_INDENT        0x1F
#define EXPORT_JSON_INDENT(n)         ((n) & EXPORT_JSON_MAX_INDENT)

#define EXPORT_JSON_COMPACT           0x20
#define EXPORT_JSON_ENSURE_ASCII      0x40
#define EXPORT_JSON_SORT_KEYS         0x80
#define EXPORT_JSON_PRESERVE_ORDER    0x100
#define EXPORT_JSON_ENCODE_ANY        0x200
#define EXPORT_JSON_ESCAPE_SLASH      0x400
#define EXPORT_JSON_REAL_PRECISION(n) (((n) & 0x1F) << 11)
#define EXPORT_JSON_EMBED             0x10000



typedef struct _EFI_CONFIG_IMEXPROT_PROTOCOL EFI_CONFIG_IMEXPROT_PROTOCOL;


#define IMPORT_TEXT_ERROR_LENGTH   160
typedef struct {
    INT64   Line;
    INT64   Column;
    CHAR8   Text [IMPORT_TEXT_ERROR_LENGTH];
} IMPORT_TEXT_ERROR;


/**
  The json configuration parameters of the buffer are imported into the setup's flash

  @param[in]  This      A pointer to EFI_CONFIG_IMEXPROT_PROTOCOL Protocol instance
  @param[in]  Buffer    The buffer of The json configuration parameters file
  @param[in]  Length    The Size of The json configuration parameters buffer file
  @param[out] Error     The struct that determines whether an import file is in error
                        Error->Line:    Which line is the error, If the default value is less than zero,
                                        there is no error.
                        Error->Column:  Which Column is the error, If the default value is less than zero,
                                        there is no error.
                        Error->Text:    The error is displayed in the approximate range of the file, If
                                        the Error->Text[0] value is zero, there is no error.

  @retval EFI_SUCCESS

**/
typedef
EFI_STATUS
(EFIAPI *EFI_CONFIG_IMPORT)(
  IN  EFI_CONFIG_IMEXPROT_PROTOCOL     *This,
  IN  UINT8                            *Buffer,
  IN  UINT64                           Length,
  IN  OUT IMPORT_TEXT_ERROR            *Error
  );

/**
  The configuration parameters of the setup's flash are exported to the buffer of the json text

  @param[in]   This      A pointer to EFI_CONFIG_IMEXPROT_PROTOCOL Protocol instance
  @param[in]   Flags     The Index position before removement. The value
                         could be the combination of below flags.
                            - EXPORT_JSON_INDENT(n)
                            - EXPORT_JSON_COMPACT
                            - EXPORT_JSON_ENSURE_ASCII
                            - EXPORT_JSON_SORT_KEYS
                            - EXPORT_JSON_PRESERVE_ORDER
                            - EXPORT_JSON_ENCODE_ANY
                            - EXPORT_JSON_ESCAPE_SLASH
                            - EXPORT_JSON_REAL_PRECISION(n)
                            - EXPORT_JSON_EMBED
                          See below URI for the JSON encoding flags reference.
                          https://jansson.readthedocs.io/en/2.13/apiref.html#encoding
  @param[out]  Buffer     The buffer of the exported json configuration parameters file
  @param[out]  Length     The size of the exported json configuration parameters file

  @retval EFI_SUCCESS

**/
typedef
EFI_STATUS
(EFIAPI *EFI_CONFIG_EXPORT)(
  IN  EFI_CONFIG_IMEXPROT_PROTOCOL     *This,
  IN  UINT64                           Flags,
  OUT UINT8                            **Buffer,
  OUT UINT64                           *Length
  );

//
//Setup configuration parameters to import and export protocol
//
struct  _EFI_CONFIG_IMEXPROT_PROTOCOL {
  EFI_CONFIG_IMPORT    Import;
  EFI_CONFIG_EXPORT    Export;
};


extern EFI_GUID gEfiConfigImExportProtocolGuid;

#endif
