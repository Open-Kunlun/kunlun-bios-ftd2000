/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/


#ifndef _SETUP_HII_DATA_PROTOCOL_H_
#define _SETUP_HII_DATA_PROTOCOL_H_

//
// Setup data manager Protocol GUID
//
#define EFI_SETUP_DATA_PROTOCOL_GUID \
{ 0x8f48c017, 0xce8d, 0x403c, 0x94, 0xcd, 0xcb, 0x59, 0x84, 0x6f, 0xba, 0x86 }

#define SETUP_DATA_MANAGER_EXTENSION_SIGNATURE            SIGNATURE_32 ('s', 'd', 'm', 'e')

typedef struct _SETUP_HII_DATA_MANAGMENT_PROTOCOL SETUP_HII_DATA_MANAGMENT_PROTOCOL;

//extern EFI_GUID gSetupHiiDataManagmentProtocolGuid;

#define MAX_ITEMS  10

#define GET_SETUP_CONFIG          0x4756
#define SET_SETUP_CONFIG          0x5356

//add-klk-lyang-P000A-start//
//[-start-klk-lyang-009-20221202-add]//
#define PASSWORD_FLAG_EXIST       0x0000
//[-end-klk-lyang-009-20221202-add]//
#define PASSWORD_FLAG_CLEAR       0x0001
#define PASSWORD_FLAG_OLD         0x0002
#define PASSWORD_FLAG_NEW         0x0004
#define PASSWORD_FLAG_MODIFY_CHECK  0x0006
#define PASSWORD_FLAG_CONFIRM     0x0008
#define PASSWORD_FLAG_CMP         0x0100
#define PASSWORD_FLAG_RETRY       0x0200
//[xqwang-0001]>>>
#define PASSWORD_FLAG_WARNING   0x0400
//[xqwang-0001]<<<
//add-klk-lyang-P000A-end//
//
//The following status is the return value when developing the calling function BiosPasswdProtocol->CheckPasswordValid
//
#define SAME_WITH_CURRENT_ADMIM_PASSWORD EFI_INVALID_PARAMETER        //Passwords cannot be the same as the admin password!
#define SAME_WITH_CURRENT_USER_PASSWORD  EFI_NOT_READY                //Passwords cannot be the same as the user password!
//[-start-klk-lyang-026-20230205-add]//
#define SAME_WITH_CURRENT_PO_PASSWORD    EFI_END_OF_FILE              //Passwords cannot be the same as the power on password!
#define SAME_WITH_OLD_PO_PASSWORD        EFI_END_OF_MEDIA             //Passwords cannot be the same as the old power on password!
//[-end-klk-lyang-026-20230205-add]//
#define SAME_WITH_OLD_ADMIM_PASSWORD     EFI_NO_RESPONSE              //Passwords cannot be the same as the old Admin password!
#define SAME_WITH_OLD_USER_PASSWORD      EFI_NO_MAPPING               //Passwords cannot be the same as the old User password!
#define SAME_WITH_OLD_PASSWORD           EFI_CRC_ERROR                //Passwords cannot be the same as the old password!
#define USER_NO_ACCESS                   EFI_LOAD_ERROR               //Permission Denied!
#define PASSWORD_LENGTH_NOT_MATCH        EFI_OUT_OF_RESOURCES         //Passwords length does not meet the requirements!
#define PASSWORD_LOCK_COUNT_LEFT         EFI_ABORTED                  //The number of password verification is %d and the password is locked when it is 0!
#define PASSWORD_LOCK                    EFI_ACCESS_DENIED            //the password is locked!
#define PASSWORD_EASY_CONFIRM            EFI_VOLUME_FULL
#define OEM_RESERVE_WARNING_STATUS       EFI_ICMP_ERROR               //OEM  POPUP warning  and continue setting password
#define OEM_RESERVE_ERROR_STATUS         EFI_TFTP_ERROR               //OEM  POPUP error and does not setting password


typedef struct{
  UINT8               Operand;
  EFI_STRING_ID       Prompt;
  EFI_HII_HANDLE      FomesetHandle;
  CHAR16              *PromptStr;
  CHAR16              *VarStoreName;
  CHAR16              *SorceStr;     //for oneof  orderlist
  CHAR16              *ChangeStr;    //for oneof  orderlist
  UINT64              SoruceValue;  //for numiric checkbox
  UINT64              ChangeValue;  //for numiric checkbox
}CHANGE_LOG;

typedef struct {
  VOID           *Page;
  EFI_STRING_ID  MenuTitle;
  CHAR16         *String;
  EFI_GUID       *FormSetGuid;
} MENU_ITEM;

//
// Direction Enumeration
//
typedef enum {
  NoChange,
  Left,
  Right,
  Jump,
} MOVE_TYPE;

//typedef struct {
//  EFI_HANDLE        DriverHandle;
//  EFI_HII_HANDLE    HiiHandle;
//} HII_HANDLE_TABLE;

typedef struct {
  //
  // for PowerOnSecurity
  //


  //
  // Security Menu
  //
  UINT16                                Flag;
  CHAR16                                *InputString;
  UINTN                                 StrLength;

  //
  // Boot Menu
  //
  UINT16                                *BootOrder;
  UINT16                                *BootPriority;
  UINT16                                AdvBootDeviceNum;
  UINT16                                LegacyBootDeviceNum;
  UINT16                                EfiBootDeviceNum;
} SETUP_UTILITY_CONFIGURATION;

/**
  after browser load default user can Execute its action.
  @retval
**/
typedef
EFI_STATUS
(EFIAPI *SETUP_DEFAULT_CALLBACK)(
  VOID
);

/**
  after browser submit user can Execute its action.
  @retval
**/
typedef
EFI_STATUS
(EFIAPI *SETUP_SUBMIT_CALLBACK)(
  VOID
);

/**
  before browser submit will check action permision.

  @retval EFI_ACCESS_DENIED       browser will popup permision denie.
  @retval EFI_SUCCESS             browser continue triggering Hotkey Action;
**/
typedef
EFI_STATUS
(EFIAPI *SETUP_DEFAULT_AUTH)(
  VOID
);

/**
  before browser submit will check action permision.

  @retval EFI_ACCESS_DENIED       browser will popup permision denie.
  @retval EFI_SUCCESS             browser continue triggering Hotkey Action;
**/
typedef
EFI_STATUS
(EFIAPI *SETUP_SUBMIT_AUTH)(
  VOID
);

typedef
EFI_STATUS
(EFIAPI *GET_BROWSER_MODIFY_DATE)(
  IN OUT CHANGE_LOG          **CLogPtr,
  IN OUT UINT16              *LogCount
);

struct _SETUP_HII_DATA_MANAGMENT_PROTOCOL{
  EFI_GUID                                  *Guid;              // GUID of setup variable used in NvRAM
  UINTN                                     Size;               // Size of SCBuffer
  UINT8                                     *SCBuffer;          // Pointer to the SYSTEM_CONFIGURATION of SetupUility
  UINT8                                     *MyIfrNVData;       // Pointer of NvRamMap in OemSetupBrowser engine
  UINTN                                     PreviousMenuEntry;  // Saved previous menu item
  UINT8                                     CurRoot;            // Index of menu item currently selected
  UINT8                                     PreviousRoot;       // Index of menu item previous selected
  BOOLEAN                                   AtRoot;             // Is the DeviceManager at the root of the tree
  BOOLEAN                                   Finished;           // Has the user hit save and exit?
  BOOLEAN                                   Changed;            // Has any settings been changed?
  BOOLEAN                                   Firstin;            // First time entry SendFrom
  BOOLEAN                                   UseMenus;           // Are the following structures used
  BOOLEAN                                   LayoutIsChanged;    // UI layout changed?
  BOOLEAN                                   RepaintFrameLine;   //
  MOVE_TYPE                                 Direction;          // Direction of the menu change
  UINT8                                     MenuItemCount;      // Number of valid menu items
  MENU_ITEM                                 MenuList[MAX_ITEMS];// Menu item list.
  SETUP_UTILITY_CONFIGURATION               *SUCInfo;
  UINT32                                    ExtensionSignature;
  UINT32                                    ExtensionVersion;
  SETUP_DEFAULT_CALLBACK                    SetupDefaultCallBack;// ver >= 1
  SETUP_SUBMIT_CALLBACK                     SetupSubmitCallBack;
  SETUP_DEFAULT_AUTH                        SetupDefaultAuth;
  SETUP_SUBMIT_AUTH                         SetupSubmitAuth;
  BOOLEAN                                   PopUpHistory;  // ver >= 2
  GET_BROWSER_MODIFY_DATE                   GetBrowserModifyData; //instance in sendform
  BOOLEAN                                   SetupMouseVirkKey;//ver >=3
//add-klk-lyang-setup-004-add//
  BOOLEAN                                   SetupBootOptionXkeyEnable;//ver >=4
  BOOLEAN                                   SetupBootOptionChanged;//ver >=4
//add-klk-lyang-setup-004-end//
//[-start-klk-lyang-010-20221202-add]//
  BOOLEAN                                   RefreshSetupKeyPressEnable;//ver >=5 F11
  BOOLEAN                                   RefreshSetupKeyPress;//ver >=5 F11
  BOOLEAN                                   HideHotKeyPressEnable;//ver >=5      F12
  BOOLEAN                                   HideHotKeyPress;//ver >=5
//[-end-klk-lyang-010-20221202-add]//
//[-start-klk-lyang-014-20221202-add]//
  BOOLEAN                                   GrayoutAllUserOpcode;//ver >=6
//[-end-klk-lyang-014-20221202-add]//
};

#endif
