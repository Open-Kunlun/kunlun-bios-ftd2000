/** @file
  Brief description of the file's purpose.

  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
  All Rights Reserved.

  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..

  Module Name: DeviceMonitor.h

**/


#ifndef _EFI_DEVICE_MONITOR_PROTOCOL_H
#define _EFI_DEVICE_MONITOR_PROTOCOL_H

#define EFI_DEVICE_MONITOR_PROTOCOL_GUID \
  { 0xF7A1811, 0xECA4, 0x4291, { 0x81, 0x63, 0xA1, 0xE4, 0x82, 0x91, 0x7B, 0x2E } }

#define DEVICE_IDENTIFIER_ASCII_LENGTH  20

typedef struct _EFI_DEVICE_MONITOR_PROTOCOL EFI_DEVICE_MONITOR_PROTOCOL;

typedef struct {
  ///
  /// Version of this data structure.
  ///
  UINT32         Version;
  EFI_GUID       DeviceType;
  EFI_HANDLE     DeviceHandle;
  CHAR8          DeviceIdentifier[DEVICE_IDENTIFIER_ASCII_LENGTH];
} MONITOR_DEVICE_IDENTIFIER;

#define MONITOR_DEVICE_IDENTIFIER_REVISION  0x00010000

typedef
EFI_STATUS
(EFIAPI *MONITOR_DEVICE_IDENTIFY)(
  IN EFI_DEVICE_MONITOR_PROTOCOL        *This,
  OUT MONITOR_DEVICE_IDENTIFIER         *DeviceId
  );

struct _EFI_DEVICE_MONITOR_PROTOCOL {
  UINT64                                Revision;
  MONITOR_DEVICE_IDENTIFY               MonitorDeviceIdentify;
};

extern EFI_GUID gEfiDeviceMonitorProtocolGuid;

#endif

