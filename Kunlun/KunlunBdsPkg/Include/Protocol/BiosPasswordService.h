/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef _EFI_BIOS_PASSWORD_SERVICE_PROTOCOL_H
#define _EFI_BIOS_PASSWORD_SERVICE_PROTOCOL_H

#include <Library/UefiBootManagerLib.h>

#define EFI_BIOS_PASSWORD_SERVICE_PROTOCOL_GUID \
  { 0xC3FD65A5, 0x570C, 0x4073, {0xBE, 0x2E, 0x4C, 0xF2, 0x2A, 0x9F, 0xDA, 0xD1}}


typedef struct _EFI_BIOS_PASSWORD_SERVICE_PROTOCOL EFI_BIOS_PASSWORD_SERVICE_PROTOCOL;

#define BIOS_PASSWORD_ADMIN   0
#define BIOS_PASSWORD_USER    1
#define BIOS_PASSWORD_ANY     2

#define SAME_WITH_CURRENT_ADMIM_PASSWORD EFI_INVALID_PARAMETER
#define SAME_WITH_CURRENT_USER_PASSWORD  EFI_NOT_READY
#define SAME_WITH_OLD_ADMIM_PASSWORD     EFI_NO_RESPONSE
#define SAME_WITH_OLD_USER_PASSWORD      EFI_NO_MAPPING
#define SAME_WITH_OLD_PASSWORD           EFI_CRC_ERROR
#define USER_NO_ACCESS                   EFI_LOAD_ERROR

typedef enum {
  SystemSupervisor     = 0,
  SystemUser,
  SystemAnyUser
} PASSWORD_TYPE;

typedef enum {
  Lock     = 0,
  UnLock,
  Froze
} PASSWORD_STATUS;

typedef enum {
  Post     = 0,
  Setup,
  SaveConfirm,
  Upgrade
} PASSWORD_PHASE;

//add-klk-lyang-P000A-start//
typedef
EFI_STATUS
(EFIAPI *EFI_GET_LOGIN_STATUS) (
    IN  EFI_BIOS_PASSWORD_SERVICE_PROTOCOL   *This,
    IN  OUT UINT8                            *Access
);
//add-klk-lyang-P000A-end//

typedef
EFI_STATUS
(EFIAPI *EFI_GET_PASSWORD_STATUS) (
    IN  EFI_BIOS_PASSWORD_SERVICE_PROTOCOL    *This,
    IN  PASSWORD_TYPE                         PassWordType
);

typedef
EFI_STATUS
(EFIAPI *EFI_SET_PASSWORD) (
    IN  EFI_BIOS_PASSWORD_SERVICE_PROTOCOL   *This,
    IN  VOID                                 *PasswordPtr,
    IN  UINTN                                PasswordLength,
    IN  PASSWORD_TYPE                        PassWordType,
    IN  UINT8                                PopFlag
);

typedef
EFI_STATUS
(EFIAPI *EFI_CHECK_PASSWORD) (
    IN  EFI_BIOS_PASSWORD_SERVICE_PROTOCOL   *This,
    IN  VOID                                 *PasswordPtr,
    IN  UINTN                                PasswordLength,
    IN  PASSWORD_TYPE                        PassWordType,
    OPTIONAL OUT UINT8                       *Access,
    IN  UINT8                                PopFlag
);

typedef
EFI_STATUS
(EFIAPI *EFI_CLEAR_PASSWORD) (
    IN  EFI_BIOS_PASSWORD_SERVICE_PROTOCOL    *This,
    IN  PASSWORD_TYPE                         PassWordType,
    IN  UINT8                                 PopFlag
);

typedef
EFI_STATUS
(EFIAPI *EFI_POWERON_VERIFY) (
    IN  EFI_BIOS_PASSWORD_SERVICE_PROTOCOL *This,
    IN  UINT8                              Phase,
    IN  UINT8                              PopFlag
  );

typedef
EFI_STATUS
EFIAPI
(EFIAPI *EFI_CHECK_PASSWORD_VALID) (
    IN  EFI_BIOS_PASSWORD_SERVICE_PROTOCOL *This,
    IN  PASSWORD_TYPE     PassWordType,
    IN  CHAR16            *Password,
    IN  UINTN             PasswordLength
  );

typedef
EFI_STATUS
EFIAPI
(EFIAPI *EFI_CHECK_PASSWORD_TIMEOUT) (
    IN  EFI_BIOS_PASSWORD_SERVICE_PROTOCOL *This,
    IN  UINT8         Phase,
    IN  OUT PASSWORD_TYPE     *PassWordTypes,
    IN  OUT UINT8             *State,
    IN  OUT UINTN             *Days
  );

typedef
VOID
EFIAPI
(EFIAPI *EFI_SYNC_PASSWORD) (
    VOID
  );

struct _EFI_BIOS_PASSWORD_SERVICE_PROTOCOL {
  EFI_GET_LOGIN_STATUS                  GetLoginStatus;
  EFI_GET_PASSWORD_STATUS               GetStatus;
  EFI_SET_PASSWORD                      SetPassword;
  EFI_CHECK_PASSWORD                    CheckPassword;
  EFI_CLEAR_PASSWORD                    ClearPassword;
  EFI_POWERON_VERIFY                    PowerOnVerify;
  EFI_CHECK_PASSWORD_VALID              CheckPasswordValid;
  EFI_CHECK_PASSWORD_TIMEOUT            CheckPasswordTimeout;
  EFI_SYNC_PASSWORD                     SyncPassword;
  PASSWORD_STATUS                       PasswordStatus;
  EFI_BOOT_MANAGER_LOAD_OPTION          LoadOptions;
  EFI_EVENT                             PostEvent;
  EFI_EVENT                             SetupEvent;
};

extern EFI_GUID gEfiBiosPasswordServiceProtocolGuid;

#endif

