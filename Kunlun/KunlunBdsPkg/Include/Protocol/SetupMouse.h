/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef __SETUP_MOUSE_PROTOCOL_H__
#define __SETUP_MOUSE_PROTOCOL_H__

#define SETUP_MOUSE_PROTOCOL_GUID \
  { \
    0x489337bb, 0x17a9, 0x49c1, { 0xa5, 0xbf, 0xc2, 0x0f, 0x4d, 0xf7, 0x73, 0xba }\
  }

extern EFI_GUID gEfiSetupMouseProtocolGuid;

typedef struct {
  UINTN           StartRow;
  UINTN           StartColumn;
  UINTN           EndRow;
  UINTN           EndColumn;
  EFI_INPUT_KEY   Key;
} MOUSE_CURSOR_RANGE;

typedef struct {
  INTN        XCor;
  INTN        YCor;
  BOOLEAN     LeftClicK;
  BOOLEAN     LeftDoubleClicK;
  BOOLEAN     RightClick;
} MOUSE_ACTION_MODE;

typedef enum {
  Mouse_Left_Click = 1,
  Mouse_Left_Double_Click,
  Mouse_Right_Click,
  Mouse_Max
} MOUSE_ACTION_FLAG;

typedef struct {
  UINTN               Column;
  UINTN               Row;
  UINTN               CursorX;
  UINTN               CursorY;
  MOUSE_ACTION_FLAG   Action;
  BOOLEAN             LeftClicK;
} MOUSE_ACTION_ATTRIBUTES;

typedef struct _EFI_SETUP_MOUSE_PROTOCOL  EFI_SETUP_MOUSE_PROTOCOL;

typedef
EFI_STATUS
(EFIAPI *EFI_START_MOUSE) (
  IN  EFI_SETUP_MOUSE_PROTOCOL           *This
);

typedef
EFI_STATUS
(EFIAPI *EFI_CLOSE_MOUSE) (
  IN  EFI_SETUP_MOUSE_PROTOCOL           *This
  );

typedef
EFI_STATUS
(EFIAPI *EFI_SETUP_MOUSE_QUERY_MODE) (
  IN  EFI_SETUP_MOUSE_PROTOCOL          *This,
  IN  OUT MOUSE_ACTION_MODE             *Mode
);

typedef
EFI_STATUS
(EFIAPI *EFI_SETUP_MOUSE_SET_MODE) (
  IN  EFI_SETUP_MOUSE_PROTOCOL           *This,
  IN  MOUSE_ACTION_MODE                  *Mode
  );

typedef
EFI_STATUS
(EFIAPI *EFI_SETUP_MOUSE_GET_MODE) (
  IN  EFI_SETUP_MOUSE_PROTOCOL           *This,
  IN OUT MOUSE_ACTION_ATTRIBUTES         *Action
  );

struct _EFI_SETUP_MOUSE_PROTOCOL {
  EFI_EVENT                               Event;
  EFI_START_MOUSE                         Start;
  EFI_CLOSE_MOUSE                         Close;
  EFI_SETUP_MOUSE_QUERY_MODE              QueryMode;
  EFI_SETUP_MOUSE_SET_MODE                SetMode;
  EFI_SETUP_MOUSE_GET_MODE                GetMode;
};

#endif
