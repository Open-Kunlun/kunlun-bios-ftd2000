/** @file
  Extension Form Browser Protocol provides the services that can be used to
  register the different hot keys for the standard Browser actions described in UEFI specification.

Copyright (c) 2011 - 2018, Intel Corporation. All rights reserved.<BR>
Copyright (c) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.
SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef __KL_FORM_BROWSER_EXTENSION_H__
#define __KL_FORM_BROWSER_EXTENSION_H__

#define BROWSER_ACTION_OEM_ESC      BIT7

#endif

