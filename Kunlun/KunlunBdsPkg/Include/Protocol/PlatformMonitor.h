/** @file
  Brief description of the file's purpose.

  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
  All Rights Reserved.

  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..

  Module Name: PlatformMonitor.h

**/


#ifndef _EFI_PLATFORM_MONITOR_PROTOCOL_H
#define _EFI_PLATFORM_MONITOR_PROTOCOL_H

#include <Protocol/DeviceMonitor.h>

#define EFI_PLATFORM_MONITOR_PROTOCOL_GUID \
  { 0x2D69F8C3, 0xAE58, 0x4FCF, { 0x89, 0x4F, 0xD3, 0xFB, 0xF3, 0xE7, 0x72, 0x42 } }


typedef struct _EFI_PLATFORM_MONITOR_PROTOCOL EFI_PLATFORM_MONITOR_PROTOCOL;


//
// Device Identifier GUID value
//
#define MONITOR_DEVICE_IDENTIFIER_TYPE_ATA_GUID \
    { \
      0x4A361117, 0xDF87, 0x4A57, { 0xAD, 0x85, 0x95, 0x9A, 0xE4, 0x3, 0x33, 0x62 } \
    }

#define MONITOR_DEVICE_IDENTIFIER_TYPE_DRAM_GUID \
    { \
      0xC396D015, 0x5FD0, 0x462E, { 0xB3, 0xA2, 0xCC, 0xFC, 0xEC, 0x23, 0x2D, 0xE2 } \
    }

#define MONITOR_DEVICE_IDENTIFIER_TYPE_CPU_GUID \
    { \
      0x521003CF, 0xD8CE, 0x4697, { 0xB6, 0xBB, 0xB7, 0x1C, 0x4C, 0x90, 0xE4, 0xF8 } \
    }

#define PLATFORM_MONITOR_PROTOCOL_REVISION  0x00010000

typedef
EFI_STATUS
(EFIAPI *MONITOR_DEVICE_STATE)(
  IN EFI_PLATFORM_MONITOR_PROTOCOL     *This,
  IN MONITOR_DEVICE_IDENTIFIER         *DeviceId
  );

struct _EFI_PLATFORM_MONITOR_PROTOCOL {
  UINT64                                Revision;
  MONITOR_DEVICE_STATE                  MonitorDeviceState;
};

extern EFI_GUID gEfiPlatformMonitorProtocolGuid;

#endif

