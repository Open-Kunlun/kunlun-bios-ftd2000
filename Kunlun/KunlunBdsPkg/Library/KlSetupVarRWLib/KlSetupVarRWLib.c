/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:

  MonitorUiLib.c

Abstract:


Revision History:


**/

#include <Library/KlSetupVarRWLib.h>


/**
  Brief description of KlSetupVarRead.

  @param  Configuration

  @retval EFI_SUCCESS   Function successful returned.
**/
EFI_STATUS
KlSetupVarRead (
  IN SYSTEM_SETUP_CONFIGURATION            **Configuration
)
{
  EFI_STATUS                Status;
  UINTN                    BufferSize;
  BufferSize = sizeof (SYSTEM_SETUP_CONFIGURATION);
  *Configuration = AllocateZeroPool (BufferSize);
  if (*Configuration == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }
  Status= gRT->GetVariable(
                SETUP_VARIABLE_NAME,
                &gSystemConfigurationGuid,
                NULL,
                &BufferSize,
                *Configuration
                );
  return Status;
}

/**
  Brief description of KlSetupVarWrite.

  @param  Configuration

  @retval EFI_SUCCESS   Function successful returned.
**/
EFI_STATUS
KlSetupVarWrite (
  IN SYSTEM_SETUP_CONFIGURATION           *Configuration
)
{
  EFI_STATUS                Status;
//remove-klk-lyang-P000A-start//
  //UINTN                    BufferSize;
  //BufferSize = sizeof (SYSTEM_SETUP_CONFIGURATION);
//remove-klk-lyang-P000A-end//
//  *Configuration = AllocateZeroPool (BufferSize);
//  if (*Configuration == NULL) {
//    return EFI_OUT_OF_RESOURCES;
//  }
  Status = gRT->SetVariable(
                  SETUP_VARIABLE_NAME,
                  &gSystemConfigurationGuid,
                  EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS,
                  sizeof (SYSTEM_SETUP_CONFIGURATION),
                  Configuration
                  );
  return Status;
}

/**
  Brief description of KlSetupVarRWLibConstructor.

  @param  ImageHandle
  @param  SystemTable

  @retval EFI_SUCCESS   Function successful returned.
**/
EFI_STATUS
EFIAPI
KlSetupVarRWLibConstructor (
  IN EFI_HANDLE                   ImageHandle,
  IN EFI_SYSTEM_TABLE             *SystemTable
  )
{
  return EFI_SUCCESS;
}

