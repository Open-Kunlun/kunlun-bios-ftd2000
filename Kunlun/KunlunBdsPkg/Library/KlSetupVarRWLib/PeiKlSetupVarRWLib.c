/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:

  MonitorUiLib.c

Abstract:


Revision History:


**/

#include <Library/PeiKlSetupVarRWLib.h>

/**
  Brief description of PeiKlSetupVarRead.

  @param  Configuration

  @retval EFI_SUCCESS   Function successful returned.
**/
EFI_STATUS
PeiKlSetupVarRead (
  IN SYSTEM_SETUP_CONFIGURATION            **Configuration
)
{
  EFI_STATUS                          Status;
  UINTN                              BufferSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI    *Variable;
  Status = PeiServicesLocatePpi (
               &gEfiPeiReadOnlyVariable2PpiGuid,
               0,
               NULL,
               (VOID **)&Variable
               );
  if (!EFI_ERROR (Status)) {
    BufferSize = sizeof(SYSTEM_SETUP_CONFIGURATION);
    *Configuration = AllocateZeroPool (BufferSize);
    if (*Configuration == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    Status = Variable->GetVariable (
                         Variable,
                         SETUP_VARIABLE_NAME,
                         &gSystemConfigurationGuid,
                         NULL,
                         &BufferSize,
                         *Configuration
                         );
  }
  return Status;
}


