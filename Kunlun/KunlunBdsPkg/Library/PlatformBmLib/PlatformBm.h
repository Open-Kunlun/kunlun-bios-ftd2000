/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:
  Header file for platform boot manager.


Revision History:


**/

#ifndef _PLATFORM_BM_H_
#define _PLATFORM_BM_H_

#include <PiDxe.h>

//#include <Guid/EmuSystemConfig.h>
#include <Guid/EventGroup.h>
//#include <Protocol/EmuThunk.h>
//#include <Protocol/EmuIoThunk.h>
//#include <Protocol/EmuGraphicsWindow.h>
#include <Protocol/GenericMemoryTest.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/FirmwareVolume2.h>
//add-klk-lyang-P000A-start//
#include <Protocol/EsrtManagement.h>
//add-klk-lyang-P000A-end//
#include <Protocol/Smbios.h>
#include <IndustryStandard/Pci.h>
#include <IndustryStandard/Nvme.h>
#include <Protocol/SimpleNetwork.h>
#include <Protocol/UsbIo.h>
#include <Protocol/PciIo.h>
#include <Protocol/AtaPassThru.h>
#include <Protocol/DiskInfo.h>
#include <Protocol/BlockIo.h>
#include <Protocol/NvmExpressPassthru.h>
#include <Protocol/ScsiPassThru.h>
#include <Protocol/ScsiPassThruExt.h>
#include <Protocol/ScsiIo.h>

#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiBootManagerLib.h>
#include <Library/DevicePathLib.h>
#include <Library/UefiLib.h>
#include <Library/BootLogoLib.h>
#include <Library/HobLib.h>
#include <Library/HiiLib.h>
//start-klk-alt-P000A-add//
#include <Library/KlSetupVarRWLib.h>
#include <Library/KlBootLogoLib.h>
#include <Library/CapsuleLib.h>
#include <Library/DeviceLinkDataLib.h>
#include <Library/PrintLib.h>

#include <IndustryStandard/Pci.h>
#include <Protocol/PciIo.h>
#include <Guid/TtyTerm.h>
#include <SetupConfig.h>
#include <Protocol/BiosPasswordService.h>
#include <Library/KlDevicePathOrderLib.h>
//end-klk-alt-P000A-add//

//[gliu-0009]
#include <Library/X100Lib.h>
//[gliu-0009]


#define CONSOLE_OUT 0x00000001
#define STD_ERROR   0x00000002
#define CONSOLE_IN  0x00000004
#define CONSOLE_ALL (CONSOLE_OUT | CONSOLE_IN | STD_ERROR)

#define SMBIOS_MEMORY_DEV_TYPE_DDR3         0x18
#define SMBIOS_MEMORY_DEV_TYPE_DDR4         0x1A
#define SMBIOS_MEMORY_DEV_SIZE_EXTENDED     0x7FFF      //Mb

typedef struct {
  EFI_DEVICE_PATH_PROTOCOL  *DevicePath;
  UINTN                     ConnectType;
} BDS_CONSOLE_CONNECT_ENTRY;

extern BDS_CONSOLE_CONNECT_ENTRY  gPlatformConsole[];

#define GENDENTIRE \
  { \
    END_DEVICE_PATH_TYPE,\
    END_ENTIRE_DEVICE_PATH_SUBTYPE,\
    { \
      END_DEVICE_PATH_LENGTH,\
      0\
    }\
  }


//typedef struct {
//  EMU_VENDOR_DEVICE_PATH_NODE     EmuBus;
//  EMU_VENDOR_DEVICE_PATH_NODE     EmuGraphicsWindow;
//  EFI_DEVICE_PATH_PROTOCOL        End;
//} EMU_PLATFORM_UGA_DEVICE_PATH;

//start-klk-alt-P000A-add//
#define CON_OUT_CANDIDATE_NAME    L"ConOutCandidateDev"
#define CON_IN_CANDIDATE_NAME     L"ConInCandidateDev"
#define ERR_OUT_CANDIDATE_NAME    L"ErrOutCandidateDev"
//
// Platform Root Bridge
//
typedef struct {
  ACPI_HID_DEVICE_PATH      PciRootBridge;
  EFI_DEVICE_PATH_PROTOCOL  End;
} PLATFORM_ROOT_BRIDGE_DEVICE_PATH;

typedef struct {
  ACPI_HID_DEVICE_PATH      PciRootBridge;
  PCI_DEVICE_PATH           PCIeRootPort;
  PCI_DEVICE_PATH           VgaDevice;
  ACPI_ADR_DEVICE_PATH      DisplayDevice;
  EFI_DEVICE_PATH_PROTOCOL  End;
} PLATFORM_PLUG_IN_VGA_DEVICE_PATH;

typedef enum {
  OnBoard = 0,
  PlugIn,
  PossibleVgaTypeMax
} POSSIBLE_VGA_TYPE;

typedef enum {
  DisplayModePlugIn = 0,
  DisplayModeOnBoard,
} PRIMARY_DISPLAY;

typedef struct {
  EFI_HANDLE                   Handle;
  POSSIBLE_VGA_TYPE            VgaType;
  UINT8                        Priority;
} VGA_DEVICE_INFO;

//
// the short form device path for Usb keyboard
//
#define CLASS_HID           3
#define SUBCLASS_BOOT       1
#define PROTOCOL_KEYBOARD   1

//
// Time definitions
//
#define ONE_SECOND  10000000
#define HALF_SECOND 5000000

typedef struct {
  USB_CLASS_DEVICE_PATH           UsbClass;
  EFI_DEVICE_PATH_PROTOCOL        End;
} USB_CLASS_FORMAT_DEVICE_PATH;

extern EFI_DEVICE_PATH_PROTOCOL  *gPlatformRootBridges[];
extern EFI_DEVICE_PATH_PROTOCOL  *gPlatformAllPossiblePcieConsole[];
extern EFI_DEVICE_PATH_PROTOCOL  *gPlatformAllPossibleOnBoardConsole[];
//end-klk-alt-P000A-add//
//
// Platform BDS Functions
//

/**
  Perform the memory test base on the memory test intensive level,
  and update the memory resource.

  @param  Level         The memory test intensive level.

  @retval EFI_STATUS    Success test all the system memory and update
                        the memory resource

**/
EFI_STATUS
PlatformBootManagerMemoryTest (
  IN EXTENDMEM_COVERAGE_LEVEL Level
  );


VOID
PlatformBdsConnectSequence (
  VOID
  );

INTN
PlatformRegisterFvBootOption (
  EFI_GUID                         *FileGuid,
  CHAR16                           *Description,
  UINT32                           Attributes
  );

/**
  Display Post Message.

  @retval EFI_SUCCESS       Display Post message successfully.

**/
EFI_STATUS
DisplayPostMessage (
  IN SYSTEM_SETUP_CONFIGURATION  *Configuration
  );

#endif // _PLATFORM_BM_H
