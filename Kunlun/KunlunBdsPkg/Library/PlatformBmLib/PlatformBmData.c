/** @file @file

Copyright (c) 2006 - 2018, Intel Corporation. All rights reserved.<BR>
Portions copyright (c) 2011, Apple Inc. All rights reserved.
Copyright (c) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.
SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#include "PlatformBm.h"

#define SERIAL_DXE_FILE_GUID { \
            0xD3987D4B, 0x971A, 0x435F, \
            { 0x8C, 0xAF, 0x49, 0x67, 0xEB, 0x62, 0x72, 0x41 } \
            }

#define PCI_ROOT_BRIDGE(RootBridgeNum) \
  {\
    { \
      ACPI_DEVICE_PATH,\
      ACPI_DP,\
      { \
      (UINT8)(sizeof(ACPI_HID_DEVICE_PATH)),\
      (UINT8)((sizeof(ACPI_HID_DEVICE_PATH)) >> 8),\
      } \
    }, \
    EISA_PNP_ID(0x0A03),\
    RootBridgeNum\
  }

#define DEVICE_PATH(DeviceNumber,FunctionNumber) \
{\
  { \
    HARDWARE_DEVICE_PATH,\
    HW_PCI_DP,\
    {\
    (UINT8)(sizeof(PCI_DEVICE_PATH)),\
    (UINT8)((sizeof(PCI_DEVICE_PATH)) >> 8),\
    }\
  }, \
  FunctionNumber,\
  DeviceNumber\
}

#define PCI_PCIE_ROOT_PORT(DeviceNumber,FunctionNumber) \
{\
  { \
    HARDWARE_DEVICE_PATH,\
    HW_PCI_DP,\
    {\
    (UINT8)(sizeof(PCI_DEVICE_PATH)),\
    (UINT8)((sizeof(PCI_DEVICE_PATH)) >> 8),\
    }\
  }, \
  FunctionNumber,\
  DeviceNumber\
}

#if (EFI_SPECIFICATION_VERSION >= 0x00020000)
  #define GDISPLAYDEVICE \
    {\
      { \
        ACPI_DEVICE_PATH,\
        ACPI_ADR_DP,\
        {\
        (UINT8) (sizeof (ACPI_ADR_DEVICE_PATH)),\
        (UINT8) ((sizeof (ACPI_ADR_DEVICE_PATH)) >> 8),\
        }\
      }, \
      ACPI_DISPLAY_ADR (1, 0, 0, 1, 0, ACPI_ADR_DISPLAY_TYPE_VGA, 0, 0)\
    }
#else
  #define GDISPLAYDEVICE NULL
#endif

//
// Platform specific plug in PCH PCIE VGA device path
//

#define PLUG_IN_VGA_DEVICE_PATH(DeviceNumber,FunctionNumber) \
{\
  PCI_ROOT_BRIDGE(0),\
  PCI_PCIE_ROOT_PORT(DeviceNumber,FunctionNumber),\
  DEVICE_PATH(0,0),\
  GDISPLAYDEVICE,\
  GENDENTIRE\
}

#define INVALID_VGA_DEVICE_PATH \
{\
  PCI_ROOT_BRIDGE(0xff),\
  PCI_PCIE_ROOT_PORT(0xff,0xff),\
  DEVICE_PATH(0xff,0xff),\
  GDISPLAYDEVICE,\
  GENDENTIRE\
}
typedef struct {
  VENDOR_DEVICE_PATH         SerialDxe;
  UART_DEVICE_PATH           Uart;
  VENDOR_DEFINED_DEVICE_PATH TermType;
  EFI_DEVICE_PATH_PROTOCOL   End;
} PLATFORM_SERIAL_CONSOLE;

#define DP_NODE_LEN(Type) { (UINT8)sizeof (Type), (UINT8)(sizeof (Type) >> 8) }

USB_CLASS_FORMAT_DEVICE_PATH gUsbClassKeyboardDevicePath = {
  {
    {
      MESSAGING_DEVICE_PATH,
      MSG_USB_CLASS_DP,
      {
        (UINT8) (sizeof (USB_CLASS_DEVICE_PATH)),
        (UINT8) ((sizeof (USB_CLASS_DEVICE_PATH)) >> 8)
      }
    },
    0xffff,           // VendorId
    0xffff,           // ProductId
    CLASS_HID,        // DeviceClass
    SUBCLASS_BOOT,    // DeviceSubClass
    PROTOCOL_KEYBOARD // DeviceProtocol
  },

  {
    END_DEVICE_PATH_TYPE,
    END_ENTIRE_DEVICE_PATH_SUBTYPE,
    {
      END_DEVICE_PATH_LENGTH,
      0
    }
  }
};

///
/// Predefined platform default time out value
///
UINT16                      gPlatformBootTimeOutDefault = 10;

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge0 = {
  PCI_ROOT_BRIDGE(0),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge1 = {
  PCI_ROOT_BRIDGE(1),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge2 = {
  PCI_ROOT_BRIDGE(2),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge3 = {
  PCI_ROOT_BRIDGE(3),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge4 = {
  PCI_ROOT_BRIDGE(4),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge5 = {
  PCI_ROOT_BRIDGE(5),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge6 = {
  PCI_ROOT_BRIDGE(6),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge7 = {
  PCI_ROOT_BRIDGE(7),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge8 = {
  PCI_ROOT_BRIDGE(8),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge9 = {
  PCI_ROOT_BRIDGE(9),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge10 = {
  PCI_ROOT_BRIDGE(10),
  GENDENTIRE
};

PLATFORM_ROOT_BRIDGE_DEVICE_PATH  gPlatformRootBridge11 = {
  PCI_ROOT_BRIDGE(11),
  GENDENTIRE
};

EFI_DEVICE_PATH_PROTOCOL          *gPlatformRootBridges[] = {
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge0,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge1,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge2,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge3,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge4,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge5,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge6,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge7,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge8,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge9,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge10,
  (EFI_DEVICE_PATH_PROTOCOL *) &gPlatformRootBridge11,
  NULL
};

//STATIC PLATFORM_PLUG_IN_VGA_DEVICE_PATH gdVgaDevicePathArray [3] = {
//  PLUG_IN_VGA_DEVICE_PATH(1,0),
//  PLUG_IN_VGA_DEVICE_PATH(2,0),
//  PLUG_IN_VGA_DEVICE_PATH(3,0),
//};

EFI_DEVICE_PATH_PROTOCOL* gPlatformAllPossibleOnBoardConsole [] = {
//  (EFI_DEVICE_PATH_PROTOCOL*)&gVgaDevicePathArray[2],
  NULL
};
EFI_DEVICE_PATH_PROTOCOL* gPlatformAllPossiblePcieConsole [] = {
//  (EFI_DEVICE_PATH_PROTOCOL*)&gVgaDevicePathArray[1],
  NULL
};

//
// Predefined platform default console device path
//
BDS_CONSOLE_CONNECT_ENTRY         gPlatformConsole[] = {
  //
  // need update dynamically
  //
  {
    (EFI_DEVICE_PATH_PROTOCOL*) &gUsbClassKeyboardDevicePath,
    CONSOLE_IN
  },
  //[jckuang-0025]
  #if 0
  {
    (EFI_DEVICE_PATH_PROTOCOL *) &mSerialConsole,
    (CONSOLE_OUT | CONSOLE_IN | STD_ERROR)
  },
  #endif
  //[jckuang-0025]
  {
    NULL,
    0
  }
};

