/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:
  This Library is used for platform boot manager.


Revision History:


**/

#include "PlatformBm.h"
#define FIVE_SECOND        5
typedef struct _GPU_NANE  GPU_NAME;
typedef struct _XGPU_NANE  XGPU_NAME;

struct _GPU_NANE {
  UINT64        VendorId;
  UINT64        DeviceId;
  CHAR16*       VendorName;
  CHAR16*       Description;
};

struct _XGPU_NANE {
  UINT64        VendorId;
  UINT16        SubvendorId;
  UINT16        DeviceId;
  UINT16        SubsystemId;
  CHAR16*       VendorName;
  CHAR16*       Description;
};

STATIC EFI_HII_HANDLE   gPlatformBmStringPackHandle = NULL;
STATIC EFI_GUID         mPlatformBmStringPackGuid = { 0xbd5873d0, 0x0eaf, 0x4fcb, {0x52, 0xaf, 0x45, 0xf8, 0xd8, 0x8d, 0xd0, 0x83 } };

GPU_NAME gGpus[] = {
  {0x0014, 0x7a36, L"Loongson",  L"Loongson GPU"},

  {0x0731, 0x9200, L"JM",  L"JM9200 GPU"},
  {0x0731, 0x920b, L"JM",  L"JH920-I GPU"},
  {0x0731, 0x920c, L"JM",  L"JH920-M GPU"},
  {0x0731, 0x9211, L"JM",  L"JM9210-I GPU"},
  {0x0731, 0x930b, L"JM",  L"JH930-M GPU"},
  {0x1002, 0x9591, L"AMD", L"RV635/M86 [Mobility Radeon HD 3650]"},
  {0x1002, 0x9596, L"AMD", L"RV635 PRO [Radeon HD 3650 AGP]"},
  {0x1002, 0x9597, L"AMD", L"RV635 PRO [Radeon HD 3650 AGP]"},

  {0x1002, 0x9599, L"AMD", L"RV635 PRO [Radeon HD 3650 AGP]"},
  {0x1002, 0x9715, L"AMD", L"RS880 [Radeon HD 4250]"},
  {0x1002, 0x9540, L"AMD", L"RV710 [Radeon HD 4550]"},
  {0x1002, 0x68b9, L"AMD", L"Juniper LE [Radeon HD 5670 640SP Edition]"},
  {0x1002, 0x6763, L"AMD", L"Seymour [Radeon E6460]"},
  {0x1002, 0x9996, L"AMD", L"Richland [Radeon HD 8470D]"},

  {0x1002, 0x1309, L"AMD", L"Kaveri [Radeon R6/R7 Graphics]"},
  {0x1002, 0x130a, L"AMD", L"Kaveri [Radeon R6 Graphics]"},
  {0x1002, 0x130b, L"AMD", L"Kaveri [Radeon R4 Graphics]"},
  {0x1002, 0x130c, L"AMD", L"Kaveri [Radeon R7 Graphics]"},
  {0x1002, 0x130d, L"AMD", L"Kaveri [Radeon R6 Graphics]"},
  {0x1002, 0x130e, L"AMD", L"Kaveri [Radeon R5 Graphics]"},
  {0x1002, 0x130f, L"AMD", L"Kaveri [Radeon R7 Graphics]"},
  {0x1002, 0x1313, L"AMD", L"Kaveri [Radeon R7 Graphics]"},

  {0x1002, 0x1315, L"AMD", L"Kaveri [Radeon R5 Graphics]"},
  {0x1002, 0x1316, L"AMD", L"Kaveri [Radeon R5 Graphics]"},
  {0x1002, 0x1318, L"AMD", L"Kaveri [Radeon R5 Graphics]"},
  {0x1002, 0x131b, L"AMD", L"Kaveri [Radeon R4 Graphics]"},

  {0x1002, 0x131c, L"AMD", L"Kaveri [Radeon R7 Graphics]"},
  {0x1002, 0x131d, L"AMD", L"Kaveri [Radeon R6 Graphics]"},
  {0x1002, 0x6664, L"AMD", L"Jet XT [Radeon R5 M240]"},
  {0x1002, 0x6667, L"AMD", L"Jet ULT [Radeon R5 M230]"},
  {0x1002, 0x6901, L"AMD", L"Topaz PRO [Radeon R5 M255]"},
  {0x1002, 0x6907, L"AMD", L"Meso XT [Radeon R5 M315]"},

  {0x1002, 0x9850, L"AMD", L"Mullins [Radeon R3 Graphics]"},
  {0x1002, 0x9851, L"AMD", L"Mullins [Radeon R4/R5 Graphics]"},
  {0x1002, 0x9852, L"AMD", L"Mullins [Radeon R2 Graphics]"},
  {0x1002, 0x9853, L"AMD", L"Mullins [Radeon R2 Graphics]"},
  {0x1002, 0x9854, L"AMD", L"Mullins [Radeon R3E Graphics]"},
  {0x1002, 0x9855, L"AMD", L"Mullins [Radeon R6 Graphics]"},

  {0x1002, 0x9856, L"AMD", L"Mullins [Radeon R1E/R2E Graphics]"},
  {0x1002, 0x9857, L"AMD", L"Mullins [Radeon APU XX-2200M with R2 Graphics]"},
  {0x1002, 0x665d, L"AMD", L"Bonaire [Radeon R7 200 Series]"},
  {0x1da2, 0xe26a, L"Sapphire", L"Radeon R7 250"},
  {0x1002, 0x6646, L"AMD", L"Bonaire XT [Radeon R9 M280X]"},
  {0x1002, 0x67b8, L"AMD", L"Hawaii XT [Radeon R9 290X Engineering Sample]"},
  {0x1002, 0x67b9, L"AMD", L"Vesuvius [Radeon R9 295X2]"},
  {0x1002, 0x6835, L"AMD", L"Cape Verde PRX [Radeon R9 255 OEM]"},

  {0x1002, 0x6920, L"AMD", L"Amethyst [Radeon R9 M395/ M395X Mac Edition]"},
  {0x1002, 0x6921, L"AMD", L"Amethyst XT [Radeon R9 M295X / M390X]"},
  {0x1043, 0x0464, L"AMD", L"Radeon R9 270x GPU"},
};


XGPU_NAME gXGpus[] =
{
//jingjiawei
  {0x0731, 0x0731, 0x7200, 0x7201, L"JM", L"JM7201 GPU"},
  {0x0731, 0x0731, 0x7200, 0x7202, L"JM", L"JM7202 GPU"},
  {0x0731, 0x0731, 0x7200, 0x7208, L"JM", L"JM7200 GPU"},
  {0x0731, 0x0731, 0x7200, 0x7212, L"JM", L"JM7200 GPU"},
  {0x0731, 0x0731, 0x7200, 0x7214, L"JM", L"JM7500 GPU"},
  {0x0731, 0x0731, 0x7200, 0x7215, L"JM", L"JM7200 GPU"},
  {0x0731, 0x0731, 0x9100, 0x9101, L"JM", L"JM9100 GPU"},
  {0x0731, 0x0731, 0x9100, 0x9102, L"JM", L"JM9100-I GPU"},
  {0x0731, 0x0731, 0x910a, 0x910a, L"JM", L"JH910 GPU"},
  {0x0731, 0x0731, 0x910a, 0x910b, L"JM", L"JH910-I GPU"},
  {0x0731, 0x0731, 0x910a, 0x910c, L"JM", L"JH910-M GPU"},
  {0x0731, 0x0731, 0x920a, 0x920a, L"JM", L"JH920 GPU"},
  {0x0731, 0x0731, 0x920a, 0x920b, L"JM", L"JH920-I GPU"},
  {0x0731, 0x0731, 0x920a, 0x920c, L"JM", L"JH920-M GPU"},
  {0x0731, 0x0731, 0x9210, 0x9210, L"JM", L"JM9210 GPU"},
  {0x0731, 0x0731, 0x9210, 0x9211, L"JM", L"JM9210-I GPU"},
  {0x0731, 0x0731, 0x9230, 0x9230, L"JM", L"JM9230 GPU"},
  {0x0731, 0x0731, 0x9230, 0x9231, L"JM", L"JM9230-I GPU"},
  {0x0731, 0x0731, 0x9250, 0x9250, L"JM", L"JM9250 GPU"},
  {0x0731, 0x0731, 0x930a, 0x930a, L"JM", L"JH930-I GPU"},
  {0x0731, 0x0731, 0x930a, 0x930B, L"JM", L"JH930-M GPU"},
 //AMD/ATI

  {0x1002, 0x1002, 0x9598, 0x9598, L"AMD", L"Mobility Radeon HD 3600 GPU"},
  {0x1002, 0x1043, 0x9598, 0x01d6, L"ASUS", L"EAH3650 Silent GPU"},
  {0x1002, 0x1043, 0x9598, 0x3001, L"AMD", L"Radeon HD 4570 GPU"},
  {0x1002, 0x174b, 0x9598, 0x3001, L"AMD", L"Radeon HD 3750 GPU"},
  {0x1002, 0x174b, 0x9598, 0x4580, L"AMD", L"RV635 PRO [Radeon HD 4580]"},
  {0x1002, 0x17af, 0x9598, 0x3011, L"AMD", L"RV635 PRO [Radeon HD 4580]"},
  {0x1002, 0x1462, 0x954f, 0x1618, L"AMD", L"RV710 [R4350 MD512H (MS-V161)]"},
  {0x1002, 0x1458, 0x9552, 0x21ac, L"AMD", L"RV710/M92 [Radeon HD 4350]"},
  {0x1002, 0x1458, 0x9552, 0x21ed, L"AMD", L"RV710/M92 [Radeon HD 4550]"},
  {0x1002, 0x148c, 0x9552, 0x3000, L"AMD", L"RV710/M92 [Radeon HD 4350 Green 512MB GDDR3]"},
  {0x1002, 0x174b, 0x9552, 0x3000, L"AMD", L"RV710/M92 [Radeon HD 4350/4550 HyperMemory DDR2]"},
  {0x1002, 0x103c, 0x9555, 0x1411, L"AMD", L"RV710/M92 [ProBook 4720s GPU (Mobility Radeon HD 4350)]"},


  {0x1002, 0x1019, 0x68f9, 0x0001, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1019, 0x68f9, 0x0002, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1019, 0x68f9, 0x0019, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x1025, 0x68f9, 0x0518, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1025, 0x68f9, 0x0519, L"AMD", L"Cedar [Radeon HD 5450]"},

  {0x1002, 0x1028, 0x68f9, 0x010e, L"AMD", L"Cedar [XPS 8300]"},
  {0x1002, 0x1028, 0x68f9, 0x2126, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x103c, 0x68f9, 0x2126, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x103c, 0x68f9, 0x2aac, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x103c, 0x68f9, 0x2aae, L"AMD", L"Cedar [Radeon HD 5450]"},

  {0x1002, 0x103c, 0x68f9, 0x3580, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1043, 0x68f9, 0x0386, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1043, 0x68f9, 0x03c2, L"ASUS", L"Cedar [EAH5450 SILENT/DI/512MD2 (LP)]"},
  {0x1002, 0x1462, 0x68f9, 0x2130, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1462, 0x68f9, 0x2131, L"AMD", L"Cedar [Radeon HD 5450]"},

  {0x1002, 0x1462, 0x68f9, 0x2133, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x1462, 0x68f9, 0x2180, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1462, 0x68f9, 0x2181, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1462, 0x68f9, 0x2182, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x1462, 0x68f9, 0x2183, L"AMD", L"Cedar [Radeon HD 6350]"},

  {0x1002, 0x1462, 0x68f9, 0x2230, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1462, 0x68f9, 0x2231, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1462, 0x68f9, 0x2495, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x148c, 0x68f9, 0x3001, L"AMD", L"Cedar [Radeon HD 5530/6250]"},
  {0x1002, 0x148c, 0x68f9, 0x3002, L"AMD", L"Cedar [Radeon HD 6290]"},

  {0x1002, 0x148c, 0x68f9, 0x3003, L"AMD", L"Cedar [Radeon HD 6230]"},
  {0x1002, 0x148c, 0x68f9, 0x3004, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x148c, 0x68f9, 0x7350, L"AMD", L"Cedar [Radeon HD 7350]"},
  {0x1002, 0x148c, 0x68f9, 0x8350, L"AMD", L"Cedar [Radeon HD 8350]"},
  {0x1002, 0x1545, 0x68f9, 0x7350, L"AMD", L"Cedar [Radeon HD 7350]"},

  {0x1002, 0x1642, 0x68f9, 0x3983, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1642, 0x68f9, 0x3984, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x1642, 0x68f9, 0x3987, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x1642, 0x68f9, 0x3997, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1642, 0x68f9, 0x3a05, L"AMD", L"Cedar [Radeon HD 5450]"},

  {0x1002, 0x1642, 0x68f9, 0x3b31, L"AMD", L"Cedar [Radeon HD 6350A]"},
  {0x1002, 0x1682, 0x68f9, 0x3270, L"AMD", L"Cedar [Radeon HD 7350]"},
  {0x1002, 0x174b, 0x68f9, 0x3000, L"AMD", L"Cedar [Radeon HD 6230]"},
  {0x1002, 0x174b, 0x68f9, 0x3987, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x174b, 0x68f9, 0x5470, L"AMD", L"Cedar [Radeon HD 5470]"},

  {0x1002, 0x174b, 0x68f9, 0x5490, L"AMD", L"Cedar [Radeon HD 5490]"},
  {0x1002, 0x174b, 0x68f9, 0x5530, L"AMD", L"Cedar [Radeon HD 5530]"},
  {0x1002, 0x174b, 0x68f9, 0x6230, L"AMD", L"Cedar [Radeon HD 6230]"},
  {0x1002, 0x174b, 0x68f9, 0x6250, L"AMD", L"Cedar [Radeon HD 6250]"},
  {0x1002, 0x174b, 0x68f9, 0x6290, L"AMD", L"Cedar [Radeon HD 6290]"},

  {0x1002, 0x174b, 0x68f9, 0x6350, L"AMD", L"Cedar [Radeon HD 6350]"},
  {0x1002, 0x174b, 0x68f9, 0x7350, L"AMD", L"Cedar [Radeon HD 7350]"},
  {0x1002, 0x174b, 0x68f9, 0x8350, L"AMD", L"Cedar [Radeon HD 8350]"},
  {0x1002, 0x174b, 0x68f9, 0xe127, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x174b, 0x68f9, 0xe145, L"AMD", L"Cedar [Radeon HD 5450]"},

  {0x1002, 0x174b, 0x68f9, 0xe153, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x1787, 0x68f9, 0x3000, L"AMD", L"Cedar [Radeon HD 5470]"},
  {0x1002, 0x1787, 0x68f9, 0x3001, L"AMD", L"Cedar [Radeon HD 5530]"},
  {0x1002, 0x1787, 0x68f9, 0x3002, L"AMD", L"Cedar [Radeon HD 5490]"},
  {0x1002, 0x1787, 0x68f9, 0x3602, L"AMD", L"Cedar [Radeon HD 5450]"},

  {0x1002, 0x17aa, 0x68f9, 0x3603, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x17aa, 0x68f9, 0x360f, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x17aa, 0x68f9, 0x3619, L"AMD", L"Cedar [Radeon HD 5450]"},
  {0x1002, 0x17af, 0x68f9, 0x3000, L"AMD", L"Cedar [Radeon HD 6250]"},
  {0x1002, 0x17af, 0x68f9, 0x3001, L"AMD", L"Cedar [Radeon HD 6230]"},

  {0x1002, 0x17af, 0x68f9, 0x3002, L"AMD", L"Cedar [Radeon HD 6290]"},
  {0x1002, 0x17af, 0x68f9, 0x3011, L"AMD", L"Cedar [Radeon HD 5470]"},
  {0x1002, 0x17af, 0x68f9, 0x3012, L"AMD", L"Cedar [Radeon HD 5490]"},
  {0x1002, 0x17af, 0x68f9, 0x3013, L"AMD", L"Cedar [Radeon HD 5470]"},
  {0x1002, 0x17af, 0x68f9, 0x3014, L"AMD", L"Cedar [Radeon HD 6350]"},

  {0x1002, 0x103c, 0x68e0, 0x1469, L"AMD", L"Park [Mobility Radeon HD 5450]"},
  {0x1002, 0x103c, 0x68e0, 0x146b, L"AMD", L"Park [Mobility Radeon HD 5450]"},
  {0x1002, 0x103c, 0x68e0, 0x1486, L"AMD", L"Park [TouchSmart tm2-2050er discrete GPU (Mobility Radeon HD 5450)]"},
  {0x1002, 0x103c, 0x68e0, 0x1622, L"AMD", L"Park [Mobility Radeon HD 5450]"},
  {0x1002, 0x103c, 0x68e0, 0x1623, L"AMD", L"Park [Mobility Radeon HD 5450]"},

  {0x1002, 0x103c, 0x68e0, 0xeeee, L"AMD", L"Park [Mobility Radeon HD 5450]"},
  {0x1002, 0x104d, 0x68e0, 0x9076, L"AMD", L"Park [Mobility Radeon HD 5450]"},
  {0x1002, 0x1682, 0x68e0, 0x304e, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x1682, 0x68e0, 0x6000, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x17aa, 0x68e0, 0x9e52, L"AMD", L"Park FirePro M3800"},
  {0x1002, 0x17aa, 0x68e0, 0x9e53, L"AMD", L"Park FirePro M3800"},

  {0x1002, 0x1043, 0x68e1, 0x041f, L"AMD", L"Park Caicos [Radeon HD 7350]"},
  {0x1002, 0x1043, 0x68e1, 0x3000, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x148c, 0x68e1, 0x3000, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x148c, 0x68e1, 0x3001, L"AMD", L"Park Caicos [Radeon HD 6230]"},
  {0x1002, 0x148c, 0x68e1, 0x3002, L"AMD", L"Park Caicos [Radeon HD 6250]"},

  {0x1002, 0x148c, 0x68e1, 0x3003, L"AMD", L"Park Caicos [Radeon HD 6350]"},
  {0x1002, 0x148c, 0x68e1, 0x7350, L"AMD", L"Park Caicos [Radeon HD 7350]"},
  {0x1002, 0x148c, 0x68e1, 0x8350, L"AMD", L"Park Caicos [Radeon HD 8350]"},
  {0x1002, 0x1545, 0x68e1, 0x5450, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x1545, 0x68e1, 0x7350, L"AMD", L"Park Caicos [Radeon HD 7350]"},

  {0x1002, 0x1682, 0x68e1, 0x3000, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x1682, 0x68e1, 0x6000, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x1682, 0x68e1, 0x7350, L"AMD", L"Park Caicos [Radeon HD 7350]"},
  {0x1002, 0x174b, 0x68e1, 0x3000, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x174b, 0x68e1, 0x5470, L"AMD", L"Park Caicos [Radeon HD 5470]"},

  {0x1002, 0x174b, 0x68e1, 0x6000, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x174b, 0x68e1, 0x6230, L"AMD", L"Park Caicos [Radeon HD 6230]"},
  {0x1002, 0x174b, 0x68e1, 0x6350, L"AMD", L"Park Caicos [Radeon HD 6350]"},
  {0x1002, 0x174b, 0x68e1, 0x7350, L"AMD", L"Park Caicos [Radeon HD 7350]"},
  {0x1002, 0x1787, 0x68e1, 0x3000, L"AMD", L"Park Caicos [Radeon HD 5450]"},

  {0x1002, 0x17af, 0x68e1, 0x3000, L"AMD", L"Park Caicos [Radeon HD 5450]"},
  {0x1002, 0x17af, 0x68e1, 0x3001, L"AMD", L"Park Caicos [Radeon HD 6230]"},
  {0x1002, 0x17af, 0x68e1, 0x3014, L"AMD", L"Park Caicos [Radeon HD 6350]"},
  {0x1002, 0x17af, 0x68e1, 0x3015, L"AMD", L"Park Caicos [Radeon HD 7350]"},
  {0x1002, 0x17af, 0x68e1, 0x8350, L"AMD", L"Park Caicos [Radeon HD 8350 OEM]"},

  {0x1002, 0x1019, 0x68e4, 0x2386, L"AMD", L"Robson CE [Radeon HD 6350M]"},
  {0x1002, 0x1019, 0x68e4, 0x2387, L"AMD", L"Robson CE [Radeon HD 6350M]"},
  {0x1002, 0x1019, 0x68e4, 0x238d, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1019, 0x68e4, 0x238e, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1025, 0x68e4, 0x0382, L"AMD", L"Robson CE [Radeon HD 6370M]"},

  {0x1002, 0x1025, 0x68e4, 0x0489, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1025, 0x68e4, 0x048a, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1025, 0x68e4, 0x048b, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1025, 0x68e4, 0x048c, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1028, 0x68e4, 0x04c1, L"AMD", L"Robson CE [Radeon HD 6370M]"},


  {0x1002, 0x1028, 0x68e4, 0x04ca, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1028, 0x68e4, 0x04cc, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1028, 0x68e4, 0x04cd, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1028, 0x68e4, 0x04d7, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x1411, L"AMD", L"Robson CE [Radeon HD 6370M]"},


  {0x1002, 0x103c, 0x68e4, 0x1421, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x1426, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x1428, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x142a, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x142b, L"AMD", L"Robson CE [Radeon HD 6370M]"},


  {0x1002, 0x103c, 0x68e4, 0x143a, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x143c, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x1445, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x162c, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x162d, L"AMD", L"Robson CE [Radeon HD 6370M]"},

  {0x1002, 0x103c, 0x68e4, 0x162e, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x162f, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x1639, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x163a, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x163b, L"AMD", L"Robson CE [Radeon HD 6370M]"},

  {0x1002, 0x103c, 0x68e4, 0x163c, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x163d, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x163e, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x163f, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x1641, L"AMD", L"Robson CE [Radeon HD 6370M]"},


  {0x1002, 0x103c, 0x68e4, 0x1643, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x3578, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x357a, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x3673, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x103c, 0x68e4, 0x3675, L"AMD", L"Robson CE [Radeon HD 6370M]"},

  {0x1002, 0x1043, 0x68e4, 0x1c92, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1043, 0x68e4, 0x84a1, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1043, 0x68e4, 0x84ad, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x104d, 0x68e4, 0x9081, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1545, 0x68e4, 0x7350, L"AMD", L"Robson CE Cedar [Radeon HD 7350]"},

  {0x1002, 0x1558, 0x68e4, 0x4510, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x1558, 0x68e4, 0x5505, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x174b, 0x68e4, 0x5450, L"AMD", L"Robson CE Cedar [Radeon HD 5450]"},
  {0x1002, 0x17aa, 0x68e4, 0x21dd, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x17aa, 0x68e4, 0x21e9, L"AMD", L"Robson CE [Radeon HD 6370M]"},

  {0x1002, 0x17aa, 0x68e4, 0x3971, L"AMD", L"Robson CE [Radeon HD 6370M]"},
  {0x1002, 0x17aa, 0x68e4, 0x3972, L"AMD", L"Robson CE [Radeon HD 7370M]"},
  {0x1002, 0x17aa, 0x68e4, 0x397a, L"AMD", L"Robson CE [Radeon HD 6370M/7370M]"},
  {0x1002, 0x17aa, 0x68e4, 0x397b, L"AMD", L"Robson CE [Radeon HD 6370M/7370M]"},
  {0x1002, 0x17aa, 0x68e4, 0x397f, L"AMD", L"Robson CE [Radeon HD 7370M]"},

  {0x1002, 0x1179, 0x68e5, 0xfd3c, L"AMD", L"Robson LE [Radeon HD 6330M]"},
  {0x1002, 0x1179, 0x68e5, 0xfd50, L"AMD", L"Robson LE [Radeon HD 6330M]"},
  {0x1002, 0x1179, 0x68e5, 0xfd52, L"AMD", L"Robson LE [Radeon HD 6330M]"},
  {0x1002, 0x1179, 0x68e5, 0xfd63, L"AMD", L"Robson LE [Radeon HD 6330M]"},
  {0x1002, 0x1179, 0x68e5, 0xfd65, L"AMD", L"Robson LE [Radeon HD 6330M]"},

  {0x1002, 0x1179, 0x68e5, 0xfd73, L"AMD", L"Robson LE [Radeon HD 6330M]"},
  {0x1002, 0x1179, 0x68e5, 0xfd75, L"AMD", L"Robson LE [Radeon HD 6330M]"},
  {0x1002, 0x1179, 0x68e5, 0xfdd0, L"AMD", L"Robson LE [Radeon HD 6330M]"},
  {0x1002, 0x1179, 0x68e5, 0xfdd2, L"AMD", L"Robson LE [Radeon HD 6330M]"},
  {0x1002, 0x1179, 0x68e5, 0xfdea, L"AMD", L"Robson LE [Radeon HD 6330M]"},

  {0x1002, 0x1179, 0x68e5, 0xfdf8, L"AMD", L"Robson LE [Radeon HD 6330M]"},
  {0x1002, 0x148c, 0x68e5, 0x5450, L"AMD", L"Robson LE Cedar [Radeon HD 5450]"},
  {0x1002, 0x148c, 0x68e5, 0x6350, L"AMD", L"Robson LE Cedar [Radeon HD 6350]"},
  {0x1002, 0x148c, 0x68e5, 0x7350, L"AMD", L"Robson LE Cedar [Radeon HD 7350]"},
  {0x1002, 0x148c, 0x68e5, 0x8350, L"AMD", L"Robson LE Cedar [Radeon HD 8350]"},
  {0x1002, 0x148c, 0x68e5, 0x7350, L"AMD", L"Robson LE Cedar [Radeon HD 7350]"},

  {0x1002, 0x1025, 0x68c1, 0x0205, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0293, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0294, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0296, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0308, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x030a, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0311, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0312, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x031c, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x031d, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x033d, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x033e, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x033f, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0346, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0347, L"AMD", L"Madison Aspire 7740G"},

  {0x1002, 0x1025, 0x68c1, 0x0348, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0356, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0357, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0358, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0359, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x035a, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x035b, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x035c, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x035d, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x035e, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x0360, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0362, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0364, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0365, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0366, L"AMD", L"Madison Mobility Radeon HD 5650"},


  {0x1002, 0x1025, 0x68c1, 0x0367, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0368, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x036c, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x036d, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x036e, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x036f, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0372, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0373, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0377, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0378, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x0379, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x037a, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x037b, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x037e, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x037f, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x0382, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0383, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0384, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0385, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0386, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x0387, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0388, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x038b, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x038c, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x039a, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x0411, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0412, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0418, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0419, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0420, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x0411, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0412, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0418, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0419, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0420, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x0421, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0425, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x042a, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x042e, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x042f, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x0432, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0433, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0442, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x044c, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x044e, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1025, 0x68c1, 0x0451, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0454, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0455, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0475, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0476, L"AMD", L"Madison Mobility Radeon HD 5650"},


  {0x1002, 0x1025, 0x68c1, 0x0487, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0489, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0498, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1025, 0x68c1, 0x0517, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x051a, L"AMD", L"Madison Radeon HD 6550M"},


  {0x1002, 0x1025, 0x68c1, 0x051b, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x051c, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x051d, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x0525, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x0526, L"AMD", L"Madison Radeon HD 6550M"},

  {0x1002, 0x1025, 0x68c1, 0x052b, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x052c, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x053c, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x053d, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x053e, L"AMD", L"Madison Radeon HD 6550M"},

  {0x1002, 0x1025, 0x68c1, 0x053f, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1025, 0x68c1, 0x0607, L"AMD", L"Madison Radeon HD 6550M"},
  {0x1002, 0x1028, 0x68c1, 0x041b, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1028, 0x68c1, 0x0447, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1028, 0x68c1, 0x0448, L"AMD", L"Madison Mobility Radeon HD 5650"},


  {0x1002, 0x1028, 0x68c1, 0x0456, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1028, 0x68c1, 0x0457, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x1436, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x1437, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x1440, L"AMD", L"Madison Mobility Radeon HD 5650"},


  {0x1002, 0x103c, 0x68c1, 0x0456, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x0457, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x1436, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x1437, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x1440, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x103c, 0x68c1, 0x1448, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x1449, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x144a, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x144b, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x147b, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x103c, 0x68c1, 0x149c, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x149e, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x103c, 0x68c1, 0x1521, L"AMD", L"Madison Pro [FirePro M5800]"},
  {0x1002, 0x1043, 0x68c1, 0x1bc2, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x104d, 0x68c1, 0x9071, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x104d, 0x68c1, 0x9077, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x104d, 0x68c1, 0x9081, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1179, 0x68c1, 0xfd00, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1179, 0x68c1, 0xfd12, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1179, 0x68c1, 0xfd1a, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x1179, 0x68c1, 0xfd30, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1179, 0x68c1, 0xfd31, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1179, 0x68c1, 0xfd50, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1179, 0x68c1, 0xfd52, L"AMD", L"Madison Radeon HD 6530M"},
  {0x1002, 0x1179, 0x68c1, 0xfd63, L"AMD", L"Madison Radeon HD 6530M"},

  {0x1002, 0x1179, 0x68c1, 0xfd65, L"AMD", L"Madison Radeon HD 6530M"},
  {0x1002, 0x1179, 0x68c1, 0xfdd0, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x1179, 0x68c1, 0xfdd2, L"AMD", L"Madison Radeon HD 6530M"},
  {0x1002, 0x144d, 0x68c1, 0xc07e, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x144d, 0x68c1, 0xc085, L"AMD", L"Madison Mobility Radeon HD 5650"},

  {0x1002, 0x14c0, 0x68c1, 0x0043, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x14c0, 0x68c1, 0x004d, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x17aa, 0x68c1, 0x3928, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x17aa, 0x68c1, 0x3951, L"AMD", L"Madison Mobility Radeon HD 5650"},
  {0x1002, 0x17aa, 0x68c1, 0x3977, L"AMD", L"Madison Radeon HD 6550M"},


  {0x1002, 0x1028, 0x68d8, 0x68e0, L"AMD", L"Redwood XT Radeon HD 5670"},
  {0x1002, 0x174b, 0x68d8, 0x5690, L"AMD", L"Redwood XT Radeon HD 5690"},
  {0x1002, 0x174b, 0x68d8, 0x5730, L"AMD", L"Redwood XT Radeon HD 5730"},
  {0x1002, 0x174b, 0x68d8, 0xe151, L"AMD", L"Redwood XT Radeon HD 5670"},
  {0x1002, 0x174b, 0x68d8, 0x3000, L"AMD", L"Redwood XT Radeon HD 5730"},
  {0x1002, 0x17af, 0x68d8, 0x3010, L"AMD", L"Redwood XT Radeon HD 5730"},
  {0x1002, 0x17af, 0x68d8, 0x3011, L"AMD", L"Redwood XT Radeon HD 5690"},

  {0x1002, 0x103c, 0x6759, 0x3130, L"AMD", L"Turks PRO Radeon HD 6570"},
  {0x1002, 0x1043, 0x6759, 0x0403, L"AMD", L"Turks PRO Radeon HD 6570"},
  {0x1002, 0x1462, 0x6759, 0x2500, L"AMD", L"Turks PRO Radeon HD 6570"},
  {0x1002, 0x1462, 0x6759, 0x2509, L"AMD", L"Turks PRO Radeon HD 7570"},
  {0x1002, 0x148c, 0x6759, 0x7570, L"AMD", L"Turks PRO Radeon HD 7570"},

  {0x1002, 0x1642, 0x6759, 0x3a67, L"AMD", L"Turks PRO Radeon HD 6570"},
  {0x1002, 0x1682, 0x6759, 0x3280, L"AMD", L"Turks PRO Radeon HD 7570"},
  {0x1002, 0x1682, 0x6759, 0x3530, L"AMD", L"Turks PRO Radeon HD 8550"},
  {0x1002, 0x1682, 0x6759, 0x5230, L"AMD", L"Turks PRO Radeon R5 230"},
  {0x1002, 0x1682, 0x6759, 0x6450, L"AMD", L"Turks PRO Radeon HD 6450"},


  {0x1002, 0x174b, 0x6759, 0x7570, L"AMD", L"Turks PRO Radeon HD 7570"},
  {0x1002, 0x174b, 0x6759, 0x8550, L"AMD", L"Turks PRO Radeon HD HD8550 OEM"},
  {0x1002, 0x174b, 0x6759, 0x8570, L"AMD", L"Turks PRO Radeon HD HD8550 OEM"},
  {0x1002, 0x174b, 0x6759, 0xe142, L"AMD", L"Turks PRO Radeon HD 6570"},
  {0x1002, 0x174b, 0x6759, 0xe181, L"AMD", L"Turks PRO Radeon HD 6570"},


  {0x1002, 0x1787, 0x6759, 0xa230, L"AMD", L"Turks PRO Radeon R5 230 "},
  {0x1002, 0x1787, 0x6759, 0xa450, L"AMD", L"Turks PRO Radeon HD 6450"},
  {0x1002, 0x1b0a, 0x6759, 0x908f, L"AMD", L"Turks PRO Radeon HD 6570"},
  {0x1002, 0x1b0a, 0x6759, 0x9090, L"AMD", L"Turks PRO Radeon HD 6570"},
  {0x1002, 0x1b0a, 0x6759, 0x9091, L"AMD", L"Turks PRO Radeon HD 6570"},

  {0x1002, 0x1b0a, 0x6759, 0x9092, L"AMD", L"Turks PRO Radeon HD 6570"},
  {0x1002, 0x1b0a, 0x6759, 0x909e, L"AMD", L"Turks PRO Radeon HD 6570"},
  {0x1002, 0x1b0a, 0x6759, 0x90b5, L"AMD", L"Turks PRO Radeon HD 7570"},
  {0x1002, 0x1b0a, 0x6759, 0x90b6, L"AMD", L"Turks PRO Radeon HD 7570"},

  {0x1002, 0x1002, 0x6760, 0x0124, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1002, 0x6760, 0x0134, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1019, 0x6760, 0x238b, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1019, 0x6760, 0x238e, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1019, 0x6760, 0x2390, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x1019, 0x6760, 0x9985, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x04c1, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x04c3, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x04ca, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x04cb, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x1028, 0x6760, 0x04cc, L"AMD", L"Seymour Vostro 3350"},
  {0x1002, 0x1028, 0x6760, 0x04d1, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x04d3, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x04d7, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x0502, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x1028, 0x6760, 0x0503, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x0506, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x0507, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x0514, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1028, 0x6760, 0x051c, L"AMD", L"Seymour Radeon HD 6450M"},

  {0x1002, 0x1028, 0x6760, 0x051d, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x161a, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x161b, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x161e, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x161f, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x103c, 0x6760, 0x1622, L"AMD", L"Seymour Radeon HD 6450M"},
  {0x1002, 0x103c, 0x6760, 0x1623, L"AMD", L"Seymour Radeon HD 6450M"},
  {0x1002, 0x103c, 0x6760, 0x164a, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x164d, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x1651, L"AMD", L"Seymour Radeon HD 6470M"},


  {0x1002, 0x103c, 0x6760, 0x1656, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x1658, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x1659, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x165b, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x165d, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x103c, 0x6760, 0x165f, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x1661, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x1663, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x1665, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x1667, L"AMD", L"Seymour Radeon HD 6470M"},


  {0x1002, 0x103c, 0x6760, 0x1669, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x166b, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x166c, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x166e, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x1670, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x103c, 0x6760, 0x1672, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x167a, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x167b, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x167d, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x167f, L"AMD", L"Seymour Radeon HD 6490M"},


  {0x1002, 0x103c, 0x6760, 0x168c, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x168f, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x1694, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x1696, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x1698, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x103c, 0x6760, 0x169a, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x169c, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x1855, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x103c, 0x6760, 0x1859, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x103c, 0x6760, 0x185c, L"AMD", L"Seymour Radeon HD 7450M"},

  {0x1002, 0x103c, 0x6760, 0x169a, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x169c, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x1855, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x103c, 0x6760, 0x1859, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x103c, 0x6760, 0x185c, L"AMD", L"Seymour Radeon HD 7450M"},

  {0x1002, 0x103c, 0x6760, 0x185d, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x103c, 0x6760, 0x185f, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x103c, 0x6760, 0x1863, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x103c, 0x6760, 0x355c, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x355f, L"AMD", L"Seymour Radeon HD 6490M"},

  {0x1002, 0x103c, 0x6760, 0x3563, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x3565, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x3567, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x3569, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x3581, L"AMD", L"Seymour Radeon HD 6490M"},

  {0x1002, 0x103c, 0x6760, 0x3584, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x358c, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x358f, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x3592, L"AMD", L"Seymour Radeon HD 6490M"},
  {0x1002, 0x103c, 0x6760, 0x3596, L"AMD", L"Seymour Radeon HD 6490M"},

  {0x1002, 0x103c, 0x6760, 0x366b, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x103c, 0x6760, 0x3671, L"AMD", L"Seymour FirePro M3900"},
  {0x1002, 0x103c, 0x6760, 0x3673, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1043, 0x6760, 0x100a, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1043, 0x6760, 0x100c, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x1043, 0x6760, 0x101b, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1043, 0x6760, 0x101c, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1043, 0x6760, 0x102a, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x1043, 0x6760, 0x102c, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1043, 0x6760, 0x104b, L"AMD", L"Seymour Radeon HD 7470M"},

  {0x1002, 0x1043, 0x6760, 0x105d, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1043, 0x6760, 0x106b, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1043, 0x6760, 0x106d, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1043, 0x6760, 0x107d, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1043, 0x6760, 0x1cb2, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x1043, 0x6760, 0x1d22, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1043, 0x6760, 0x1d32, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1043, 0x6760, 0x2001, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1043, 0x6760, 0x2002, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1043, 0x6760, 0x2107, L"AMD", L"Seymour Radeon HD 7470M"},

  {0x1002, 0x1043, 0x6760, 0x2108, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1043, 0x6760, 0x2109, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1043, 0x6760, 0x84a0, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1043, 0x6760, 0x84e9, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1043, 0x6760, 0x8515, L"AMD", L"Seymour Radeon HD 7470M"},

  {0x1002, 0x1043, 0x6760, 0x8517, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1043, 0x6760, 0x855a, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x104d, 0x6760, 0x907b, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x104d, 0x6760, 0x9081, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x104d, 0x6760, 0x9084, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x104d, 0x6760, 0x9085, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1179, 0x6760, 0x0001, L"AMD", L"Seymour Radeon HD 6450M"},
  {0x1002, 0x1179, 0x6760, 0x0003, L"AMD", L"Seymour Radeon HD 6450M"},
  {0x1002, 0x1179, 0x6760, 0x0004, L"AMD", L"Seymour Radeon HD 6450M"},
  {0x1002, 0x1179, 0x6760, 0xfb22, L"AMD", L"Seymour Radeon HD 7470M"},


  {0x1002, 0x1179, 0x6760, 0xfb23, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb2c, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb31, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb32, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb33, L"AMD", L"Seymour Radeon HD 7470M"},

  {0x1002, 0x1179, 0x6760, 0xfb38, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb39, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb3a, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb40, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb41, L"AMD", L"Seymour Radeon HD 7470M"},

  {0x1002, 0x1179, 0x6760, 0xfb42, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb47, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb48, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb51, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb52, L"AMD", L"Seymour Radeon HD 7470M"},


  {0x1002, 0x1179, 0x6760, 0xfb53, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb81, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb82, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfb83, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfc51, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x1179, 0x6760, 0xfc52, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfc56, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfcd3, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfcd4, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x1179, 0x6760, 0xfcee, L"AMD", L"Seymour Radeon HD 7470M"},

  {0x1002, 0x1179, 0x6760, 0xfdee, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x144d, 0x6760, 0xb074, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x144d, 0x6760, 0xb084, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x144d, 0x6760, 0xc095, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x144d, 0x6760, 0xc0b3, L"AMD", L"Seymour Radeon HD 6490M"},

  {0x1002, 0x144d, 0x6760, 0xc538, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x144d, 0x6760, 0xc581, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x144d, 0x6760, 0xc589, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x144d, 0x6760, 0xc609, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x144d, 0x6760, 0xc625, L"AMD", L"Seymour Radeon HD 7470M"},

  {0x1002, 0x144d, 0x6760, 0xc636, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x1462, 0x6760, 0x10ac, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x152d, 0x6760, 0x0916, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x17aa, 0x6760, 0x21e5, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x17aa, 0x6760, 0x3900, L"AMD", L"Seymour Radeon HD 7470M"},

  {0x1002, 0x17aa, 0x6760, 0x3902, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x17aa, 0x6760, 0x3969, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x17aa, 0x6760, 0x3970, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x17aa, 0x6760, 0x3976, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x17aa, 0x6760, 0x397b, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x17aa, 0x6760, 0x397d, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x17aa, 0x6760, 0x5101, L"AMD", L"Seymour Radeon HD 7470M"},
  {0x1002, 0x17aa, 0x6760, 0x5102, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x17aa, 0x6760, 0x5103, L"AMD", L"Seymour Radeon HD 7450M"},
  {0x1002, 0x17aa, 0x6760, 0x5106, L"AMD", L"Seymour Radeon HD 7450M"},


  {0x1002, 0x1854, 0x6760, 0x0897, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1854, 0x6760, 0x0900, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1854, 0x6760, 0x0908, L"AMD", L"Seymour Radeon HD 6470M"},
  {0x1002, 0x1854, 0x6760, 0x2015, L"AMD", L"Seymour Radeon HD 6470M"},

  {0x1002, 0x1002, 0x6742, 0x6570, L"AMD", L"Whistler  Turks [Radeon HD 6570]"},
  {0x1002, 0x1019, 0x6742, 0x2393, L"AMD", L"Whistler  Radeon HD 6610M"},
  {0x1002, 0x1043, 0x6742, 0x1d82, L"AMD", L"Whistler  K53SK Laptop Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb22, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb23, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb27, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb2a, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb2c, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb30, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb31, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb32, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb38, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb39, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb3a, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb3b, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb40, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb41, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb47, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb48, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb49, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb51, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb52, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb53, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb56, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb81, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb82, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfb83, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfc56, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfcd4, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1179, 0x6742, 0xfcee, L"AMD", L"Whistler  Radeon HD 7610M"},
  {0x1002, 0x1458, 0x6742, 0x6570, L"AMD", L"Whistler  Turks [Radeon HD 6570]"},
  {0x1002, 0x1462, 0x6742, 0x6570, L"AMD", L"Whistler  Turks [Radeon HD 6570]"},
  {0x1002, 0x148c, 0x6742, 0x6570, L"AMD", L"Whistler  Turks [Radeon HD 6570]"},
  {0x1002, 0x1682, 0x6742, 0x6570, L"AMD", L"Whistler  Turks [Radeon HD 6570]"},
  {0x1002, 0x174b, 0x6742, 0x5570, L"AMD", L"Whistler  Turks [Radeon HD 5570]"},
  {0x1002, 0x174b, 0x6742, 0x6570, L"AMD", L"Whistler  Turks [Radeon HD 6570]"},
  {0x1002, 0x174b, 0x6742, 0x7570, L"AMD", L"Whistler  Turks [Radeon HD 7570]"},
  {0x1002, 0x174b, 0x6742, 0x8510, L"AMD", L"Whistler  Turks [Radeon HD 8510]"},
  {0x1002, 0x174b, 0x6742, 0x8570, L"AMD", L"Whistler  Turks [Radeon HD 8570]"},
  {0x1002, 0x1787, 0x6742, 0x6570, L"AMD", L"Whistler  Turks [Radeon HD 6570]"},
  {0x1002, 0x17af, 0x6742, 0x6570, L"AMD", L"Whistler  Turks [Radeon HD 6570]"},
  {0x1002, 0x8086, 0x6742, 0x2111, L"AMD", L"Whistler  Radeon HD 6625M"},
  {0x1002, 0x103c, 0x6759, 0x3130, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1043, 0x6759, 0x0403, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1462, 0x6759, 0x2500, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1462, 0x6759, 0x2509, L"AMD", L"Turks  Radeon HD 7570"},
  {0x1002, 0x148c, 0x6759, 0x7570, L"AMD", L"Turks  Radeon HD 7570"},
  {0x1002, 0x1642, 0x6759, 0x3a67, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1682, 0x6759, 0x3280, L"AMD", L"Turks  Radeon HD 7570"},
  {0x1002, 0x1682, 0x6759, 0x3530, L"AMD", L"Turks  Radeon HD 8550"},
  {0x1002, 0x1682, 0x6759, 0x5230, L"AMD", L"Turks  Radeon R5 230 series"},
  {0x1002, 0x1682, 0x6759, 0x6450, L"AMD", L"Turks  Radeon HD 6450 series"},
  {0x1002, 0x174b, 0x6759, 0x7570, L"AMD", L"Turks  Radeon HD 7570"},
  {0x1002, 0x174b, 0x6759, 0x8550, L"AMD", L"Turks  Radeon HD8550 OEM"},
  {0x1002, 0x174b, 0x6759, 0x8570, L"AMD", L"Turks  Radeon HD8550 OEM"},
  {0x1002, 0x174b, 0x6759, 0xe142, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x174b, 0x6759, 0xe181, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1787, 0x6759, 0xa230, L"AMD", L"Turks  Radeon R5 230 series"},
  {0x1002, 0x1787, 0x6759, 0xa450, L"AMD", L"Turks  Radeon HD 6450 series"},
  {0x1002, 0x1b0a, 0x6759, 0x908f, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1b0a, 0x6759, 0x9090, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1b0a, 0x6759, 0x9091, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1b0a, 0x6759, 0x9092, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1b0a, 0x6759, 0x909e, L"AMD", L"Turks  Radeon HD 6570"},
  {0x1002, 0x1b0a, 0x6759, 0x90b5, L"AMD", L"Turks  Radeon HD 7570"},
  {0x1002, 0x1b0a, 0x6759, 0x90b6, L"AMD", L"Turks  Radeon HD 7570"},
  {0x1002, 0x1462, 0x6837, 0x2796, L"AMD", L"Cape  Radeon HD 8730"},
  {0x1002, 0x1462, 0x6837, 0x8092, L"AMD", L"Cape  Radeon HD 8730"},
  {0x1002, 0x148c, 0x6837, 0x8730, L"AMD", L"Cape  Radeon HD 8730"},
  {0x1002, 0x1787, 0x6837, 0x3000, L"AMD", L"Cape  Radeon HD 6570"},
  {0x1002, 0x1019, 0x68c0, 0x2383, L"AMD", L"Madison  Mobility Radeon HD 5730"},
  {0x1002, 0x1028, 0x68c0, 0x02a2, L"AMD", L"Madison  Mobility Radeon HD 5730"},
  {0x1002, 0x1028, 0x68c0, 0x02fe, L"AMD", L"Madison  Mobility Radeon HD 5730"},
  {0x1002, 0x1028, 0x68c0, 0x0419, L"AMD", L"Madison  Mobility Radeon HD 5730"},
  {0x1002, 0x103c, 0x68c0, 0x147d, L"AMD", L"Madison  Mobility Radeon HD 5730"},
  {0x1002, 0x103c, 0x68c0, 0x1521, L"AMD", L"Madison  Madison XT [FirePro M5800]"},
  {0x1002, 0x103c, 0x68c0, 0x1593, L"AMD", L"Madison  Mobility Radeon HD 6570"},
  {0x1002, 0x103c, 0x68c0, 0x1596, L"AMD", L"Madison  Mobility Radeon HD 6570"},
  {0x1002, 0x103c, 0x68c0, 0x1599, L"AMD", L"Madison  Mobility Radeon HD 6570"},
  {0x1002, 0x1043, 0x68c0, 0x1c22, L"AMD", L"Madison  Mobility Radeon HD 5730"},
  {0x1002, 0x17aa, 0x68c0, 0x3927, L"AMD", L"Madison  Mobility Radeon HD 5730"},
  {0x1002, 0x17aa, 0x68c0, 0x3952, L"AMD", L"Madison  Mobility Radeon HD 5730"},
  {0x1002, 0x17aa, 0x68c0, 0x3978, L"AMD", L"Madison  Radeon HD 6570M"},
  {0x1002, 0x1462, 0x6750, 0x2670, L"AMD", L"Onega  Radeon HD 6670A"},
  {0x1002, 0x17aa, 0x6750, 0x3079, L"AMD", L"Onega  Radeon HD 7650A"},
  {0x1002, 0x17aa, 0x6750, 0x307a, L"AMD", L"Onega  Radeon HD 6650A"},
  {0x1002, 0x17aa, 0x6750, 0x3087, L"AMD", L"Onega  Radeon HD 7650A"},
  {0x1002, 0x17aa, 0x6750, 0x3618, L"AMD", L"Onega  Radeon HD 6650A"},
  {0x1002, 0x17aa, 0x6750, 0x3623, L"AMD", L"Onega  Radeon HD 6650A"},
  {0x1002, 0x17aa, 0x6750, 0x3627, L"AMD", L"Onega  Radeon HD 6650A"},
  {0x1002, 0x1028, 0x6758, 0x0b0e, L"AMD", L"Turks  Radeon HD 6670"},
  {0x1002, 0x103c, 0x6758, 0x6882, L"AMD", L"Turks  Radeon HD 6670"},
  {0x1002, 0x1462, 0x6758, 0x250a, L"AMD", L"Turks  Radeon HD 7670"},
  {0x1002, 0x148c, 0x6758, 0x7670, L"AMD", L"Turks  Radeon HD 7670"},
  {0x1002, 0x1545, 0x6758, 0x7670, L"AMD", L"Turks  Radeon HD 7670"},
  {0x1002, 0x1682, 0x6758, 0x3300, L"AMD", L"Turks  Radeon HD 7670"},
  {0x1002, 0x174b, 0x6758, 0x7670, L"AMD", L"Turks  Radeon HD 7670"},
  {0x1002, 0x174b, 0x6758, 0xe181, L"AMD", L"Turks  Radeon HD 6670"},
  {0x1002, 0x1787, 0x6758, 0x2309, L"AMD", L"Turks  Radeon HD 6670"},
  {0x1002, 0x17aa, 0x6770, 0x308d, L"AMD", L"Caicos   Radeon HD 7450A"},
  {0x1002, 0x17aa, 0x6770, 0x3623, L"AMD", L"Caicos   Radeon HD 6450A"},
  {0x1002, 0x17aa, 0x6770, 0x3627, L"AMD", L"Caicos   Radeon HD 6450A"},
  {0x1002, 0x17aa, 0x6770, 0x3629, L"AMD", L"Caicos   Radeon HD 6450A"},
  {0x1002, 0x17aa, 0x6770, 0x363c, L"AMD", L"Caicos   Radeon HD 6450A"},
  {0x1002, 0x17aa, 0x6770, 0x3658, L"AMD", L"Caicos   Radeon HD 7470A"},
  {0x1002, 0x1019, 0x6778, 0x0024, L"AMD", L"Caicos XT   Radeon HD 7470"},
  {0x1002, 0x1019, 0x6778, 0x0027, L"AMD", L"Caicos XT   Radeon HD 8470"},
  {0x1002, 0x1028, 0x6778, 0x2120, L"AMD", L"Caicos XT   Radeon HD 7470"},
  {0x1002, 0x1462, 0x6778, 0xb491, L"AMD", L"Caicos XT   Radeon HD 8470"},
  {0x1002, 0x1462, 0x6778, 0xb492, L"AMD", L"Caicos XT   Radeon HD 8470"},
  {0x1002, 0x1462, 0x6778, 0xb493, L"AMD", L"Caicos XT   Radeon HD 8470 OEM"},
  {0x1002, 0x1462, 0x6778, 0xb499, L"AMD", L"Caicos XT   Radeon R5 235 OEM"},
  {0x1002, 0x1642, 0x6778, 0x3c65, L"AMD", L"Caicos XT   Radeon HD 8470"},
  {0x1002, 0x1642, 0x6778, 0x3c75, L"AMD", L"Caicos XT   Radeon HD 8470"},
  {0x1002, 0x174b, 0x6778, 0x8145, L"AMD", L"Caicos XT   Radeon HD 8470"},
  {0x1002, 0x174b, 0x6778, 0xd145, L"AMD", L"Caicos XT   Radeon R5 235 OEM"},
  {0x1002, 0x174b, 0x6778, 0xd335, L"AMD", L"Caicos XT   Radeon R5 310 OEM"},
  {0x1002, 0x174b, 0x6778, 0xe145, L"AMD", L"Caicos XT   Radeon HD 7470"},
  {0x1002, 0x17aa, 0x6778, 0x3694, L"AMD", L"Caicos XT   Radeon R5 A220"},
  {0x1002, 0x1019, 0x6779, 0x0016, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1019, 0x6779, 0x0017, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1019, 0x6779, 0x0018, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1028, 0x6779, 0x2120, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x103c, 0x6779, 0x2128, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x103c, 0x6779, 0x2aee, L"AMD", L"Caicos   Radeon HD 7450A"},
  {0x1002, 0x1092, 0x6779, 0x6450, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1462, 0x6779, 0x2125, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1462, 0x6779, 0x2346, L"AMD", L"Caicos   Radeon HD 7450"},
  {0x1002, 0x1462, 0x6779, 0x2490, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1462, 0x6779, 0x2494, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1462, 0x6779, 0x2496, L"AMD", L"Caicos   Radeon HD 7450"},
  {0x1002, 0x148c, 0x6779, 0x7450, L"AMD", L"Caicos   Radeon HD 7450"},
  {0x1002, 0x148c, 0x6779, 0x8450, L"AMD", L"Caicos   Radeon HD 8450 OEM"},
  {0x1002, 0x1545, 0x6779, 0x7470, L"AMD", L"Caicos   Radeon HD 7470"},
  {0x1002, 0x1642, 0x6779, 0x3a65, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1642, 0x6779, 0x3a66, L"AMD", L"Caicos   Radeon HD 7450"},
  {0x1002, 0x1642, 0x6779, 0x3a75, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1642, 0x6779, 0x3a76, L"AMD", L"Caicos   Radeon HD 7450"},
  {0x1002, 0x1682, 0x6779, 0x3200, L"AMD", L"Caicos   Radeon HD 7450"},
  {0x1002, 0x174b, 0x6779, 0x7450, L"AMD", L"Caicos   Radeon HD 7450"},
  {0x1002, 0x174b, 0x6779, 0xe127, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x174b, 0x6779, 0xe153, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x174b, 0x6779, 0xe164, L"AMD", L"Caicos   Radeon HD 6450 1 GB DDR3"},
  {0x1002, 0x174b, 0x6779, 0xe180, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x174b, 0x6779, 0xe201, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1787, 0x6779, 0x2311, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x17af, 0x6779, 0x8450, L"AMD", L"Caicos   Radeon HD 8450 OEM"},
  {0x1002, 0x1b0a, 0x6779, 0x9096, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1b0a, 0x6779, 0x9097, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1b0a, 0x6779, 0x90a8, L"AMD", L"Caicos   Radeon HD 6450A"},
  {0x1002, 0x1b0a, 0x6779, 0x90b1, L"AMD", L"Caicos   Radeon HD 6450"},
  {0x1002, 0x1b0a, 0x6779, 0x90b3, L"AMD", L"Caicos   Radeon HD 7450A"},
  {0x1002, 0x1b0a, 0x6779, 0x90bb, L"AMD", L"Caicos   Radeon HD 7450A"},
  {0x1002, 0x1002, 0x683d, 0x0030, L"AMD", L"Cape Verde XT   Radeon HD 8760 OEM"},
  {0x1002, 0x1019, 0x683d, 0x0030, L"AMD", L"Cape Verde XT   Radeon HD 8760 OEM"},
  {0x1002, 0x103c, 0x683d, 0x6890, L"AMD", L"Cape Verde XT   Radeon HD 8760 OEM"},
  {0x1002, 0x1043, 0x683d, 0x8760, L"AMD", L"Cape Verde XT   Radeon HD 8760 OEM"},
  {0x1002, 0x1462, 0x683d, 0x2710, L"AMD", L"Cape Verde XT   R7770-PMD1GD5"},
  {0x1002, 0x174b, 0x683d, 0x8304, L"AMD", L"Cape Verde XT   Radeon HD 8760 OEM"},
  {0x1002, 0x1043, 0x6819, 0x042c, L"AMD", L"Pitcairn PRO   Radeon HD 7850"},
  {0x1002, 0x1682, 0x6819, 0x7269, L"AMD", L"Pitcairn PRO   Radeon R9 270 1024SP"},
  {0x1002, 0x1682, 0x6819, 0x9278, L"AMD", L"Pitcairn PRO   Radeon R9 270 1024SP"},
  {0x1002, 0x174b, 0x6819, 0xa008, L"AMD", L"Pitcairn PRO   Radeon R9 270 1024SP"},
  {0x1002, 0x174b, 0x6819, 0xe221, L"AMD", L"Pitcairn PRO   Radeon HD 7850 2GB GDDR5 DVI-I/DVI-D/HDMI/DP"},
  {0x1002, 0x1019, 0x6610, 0x0030, L"AMD", L"Oland XT   Radeon HD 8670"},
  {0x1002, 0x1028, 0x6610, 0x0081, L"AMD", L"Oland XT   Radeon R7 350X OEM"},
  {0x1002, 0x1028, 0x6610, 0x0083, L"AMD", L"Oland XT   Radeon R5 340X OEM"},
  {0x1002, 0x1028, 0x6610, 0x2120, L"AMD", L"Oland XT   Radeon R7 250"},
  {0x1002, 0x1028, 0x6610, 0x2322, L"AMD", L"Oland XT   Radeon R7 250"},
  {0x1002, 0x1462, 0x6610, 0x2910, L"AMD", L"Oland XT   Radeon HD 8670"},
  {0x1002, 0x1462, 0x6610, 0x2911, L"AMD", L"Oland XT   Radeon HD 8670"},
  {0x1002, 0x148c, 0x6610, 0x7350, L"AMD", L"Oland XT   Radeon R7 350"},
  {0x1002, 0x1642, 0x6610, 0x3c81, L"AMD", L"Oland XT   Radeon HD 8670"},
  {0x1002, 0x1642, 0x6610, 0x3c91, L"AMD", L"Oland XT   Radeon HD 8670"},
  {0x1002, 0x1642, 0x6610, 0x3f09, L"AMD", L"Oland XT   Radeon R7 350"},
  {0x1002, 0x1028, 0x6611, 0x1001, L"AMD", L"Oland   Radeon R5 430 OEM"},
  {0x1002, 0x1028, 0x6611, 0x1002, L"AMD", L"Oland   Radeon R5 430 OEM"},
  {0x1002, 0x1028, 0x6611, 0x1711, L"AMD", L"Oland   R5 430 OEM"},
  {0x1002, 0x1028, 0x6611, 0x210b, L"AMD", L"Oland   Radeon R5 240 OEM"},
  {0x1002, 0x1028, 0x6611, 0x2121, L"AMD", L"Oland   Radeon HD 8570 OEM"},
  {0x1002, 0x10cf, 0x6611, 0x1889, L"AMD", L"Oland   Radeon HD 8570 OEM"},
  {0x1002, 0x1642, 0x6611, 0x1869, L"AMD", L"Oland   Radeon 520 OEM"},
  {0x1002, 0x174b, 0x6611, 0x4248, L"AMD", L"Oland   Radeon R7 240 OEM"},
  {0x1002, 0x174b, 0x6611, 0xa240, L"AMD", L"Oland   Radeon R7 240 OEM"},
  {0x1002, 0x174b, 0x6611, 0xd340, L"AMD", L"Oland   Radeon R7 340 OEM"},
  {0x1002, 0x1b0a, 0x6611, 0x90d3, L"AMD", L"Oland   Radeon R7 240 OEM"},
  {0x1002, 0x148c, 0x6613, 0x7340, L"AMD", L"Oland PRO   Radeon R7 340"},
  {0x1002, 0x1682, 0x6613, 0x7240, L"AMD", L"Oland PRO   R7 240 2048 MB"},
  {0x1002, 0x1dcf, 0x6613, 0x3000, L"AMD", L"Oland PRO   Oland PRO [Radeon R7 240/340 / Radeon 520]"},
  {0x1002, 0x1028, 0x6660, 0x05ea, L"AMD", L"Sun XT   Radeon HD 8670M"},
  {0x1002, 0x1028, 0x6660, 0x06bf, L"AMD", L"Sun XT   Radeon R5 M335"},
  {0x1002, 0x103c, 0x6660, 0x1970, L"AMD", L"Sun XT   Radeon HD 8670M"},
  {0x1002, 0x103c, 0x6660, 0x80be, L"AMD", L"Sun XT   Radeon R5 M330"},
  {0x1002, 0x103c, 0x6660, 0x8136, L"AMD", L"Sun XT   Radeon R5 M330"},
  {0x1002, 0x103c, 0x6660, 0x8329, L"AMD", L"Sun XT   Radeon R7 M520"},
  {0x1002, 0x17aa, 0x6660, 0x3633, L"AMD", L"Sun XT   Radeon R5 A330"},
  {0x1002, 0x17aa, 0x6660, 0x3804, L"AMD", L"Sun XT   Radeon R5 M330"},
  {0x1002, 0x17aa, 0x6660, 0x3809, L"AMD", L"Sun XT   Radeon R5 M330"},
  {0x1002, 0x17aa, 0x6660, 0x381a, L"AMD", L"Sun XT   Radeon R5 M430"},
  {0x1002, 0x17aa, 0x6660, 0x390c, L"AMD", L"Sun XT   Radeon R5 M330"},
  {0x1002, 0x17aa, 0x6665, 0x1309, L"AMD", L"Jet PRO   Z50-75 Radeon R7 M260DX"},
  {0x1002, 0x17aa, 0x6665, 0x368f, L"AMD", L"Jet PRO   Radeon R5 A230"},
  {0x1002, 0x1019, 0x68fa, 0x0019, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x1019, 0x68fa, 0x0021, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x1019, 0x68fa, 0x0022, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x1019, 0x68fa, 0x0026, L"AMD", L"Cedar   Radeon HD 8350"},
  {0x1002, 0x103c, 0x68fa, 0x2adf, L"AMD", L"Cedar   Radeon HD 7350A"},
  {0x1002, 0x103c, 0x68fa, 0x2ae8, L"AMD", L"Cedar   Radeon HD 7350A"},
  {0x1002, 0x1043, 0x68fa, 0x8350, L"AMD", L"Cedar   Radeon HD 8350"},
  {0x1002, 0x1462, 0x68fa, 0x2128, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x1462, 0x68fa, 0x2184, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x1462, 0x68fa, 0x2186, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x1462, 0x68fa, 0x2495, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x1462, 0x68fa, 0xb490, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x1642, 0x68fa, 0x3985, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x174b, 0x68fa, 0x3510, L"AMD", L"Cedar   Radeon HD 8350"},
  {0x1002, 0x174b, 0x68fa, 0x3521, L"AMD", L"Cedar   Radeon R5 220"},
  {0x1002, 0x174b, 0x68fa, 0x3522, L"AMD", L"Cedar   Radeon R5 220"},
  {0x1002, 0x174b, 0x68fa, 0x7350, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x174b, 0x68fa, 0x8153, L"AMD", L"Cedar   Radeon HD 8350"},
  {0x1002, 0x174b, 0x68fa, 0xe127, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x174b, 0x68fa, 0xe153, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x174b, 0x68fa, 0xe180, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x17af, 0x68fa, 0x3015, L"AMD", L"Cedar   Radeon HD 7350"},
  {0x1002, 0x1002, 0x9874, 0x1871, L"AMD", L"Wani   Radeon R5 Graphics"},
  {0x1002, 0x1002, 0x9874, 0x1e20, L"AMD", L"Wani   Radeon R7 Graphics"},
  {0x1002, 0x1028, 0x9874, 0x06bd, L"AMD", L"Wani   Radeon R6 Graphics"},
  {0x1002, 0x103c, 0x9874, 0x2b44, L"AMD", L"Wani   Radeon R6 Graphics"},
  {0x1002, 0x103c, 0x9874, 0x8221, L"AMD", L"Wani   Radeon R5 Graphics"},
  {0x1002, 0x103c, 0x9874, 0x8223, L"AMD", L"Wani   Radeon R5 Graphics"},
  {0x1002, 0x103c, 0x9874, 0x8238, L"AMD", L"Wani   Radeon R7 Graphics"},
  {0x1002, 0x103c, 0x9874, 0x8353, L"AMD", L"Wani   Radeon R7 Graphics"},
  {0x1002, 0x1458, 0x9874, 0xd000, L"AMD", L"Wani   Radeon R7 Graphics"},
  {0x1002, 0x17aa, 0x9874, 0x5113, L"AMD", L"Wani   Radeon R6 Graphics"},
  {0x1002, 0x17aa, 0x9874, 0x5116, L"AMD", L"Wani   Radeon R6 Graphics"},
  {0x1002, 0x17aa, 0x9874, 0x5118, L"AMD", L"Wani   Radeon R5 Graphics"},
  {0x1002, 0x1025, 0x6604, 0x0776, L"AMD", L"Opal XT   Aspire V5 Radeon R7 M265"},
  {0x1002, 0x103c, 0x6604, 0x8006, L"AMD", L"Opal XT   FirePro M4170"},
  {0x1002, 0x103c, 0x6604, 0x814f, L"AMD", L"Opal XT   Litho XT [Radeon R7 M365X]"},
  {0x1002, 0x103c, 0x6604, 0x82aa, L"AMD", L"Opal XT   Litho XT [Radeon R7 M465]"},
  {0x1002, 0x17aa, 0x6604, 0x3643, L"AMD", L"Opal XT   Radeon R7 A360"},
  {0x1002, 0x103c, 0x6605, 0x2259, L"AMD", L"Opal PRO   FirePro M4150"},
  {0x1002, 0x1043, 0x6658, 0x048f, L"AMD", L"Bonaire XTX   R7260X-DC2OC-2GD5"},
  {0x1002, 0x1043, 0x6658, 0x04d3, L"AMD", L"Bonaire XTX   AMD Radeon R7 260X"},
  {0x1002, 0x1458, 0x6658, 0x227b, L"AMD", L"Bonaire XTX   Radeon R7 260X"},
  {0x1002, 0x148c, 0x6658, 0x0907, L"AMD", L"Bonaire XTX   Radeon R7 360"},
  {0x1002, 0x1682, 0x6658, 0x0907, L"AMD", L"Bonaire XTX   Radeon R7 360"},
  {0x1002, 0x1682, 0x6658, 0x7360, L"AMD", L"Bonaire XTX   Radeon R7 360"},
  {0x1002, 0x1043, 0x665c, 0x0452, L"AMD", L"Bonaire XT   Radeon HD 7790 DirectCU II OC"},
  {0x1002, 0x1462, 0x665c, 0x2930, L"AMD", L"Bonaire XT   Radeon HD 7790 OC"},
  {0x1002, 0x1462, 0x665c, 0x2932, L"AMD", L"Bonaire XT   Radeon HD 8770"},
  {0x1002, 0x1462, 0x665c, 0x2934, L"AMD", L"Bonaire XT   Radeon R9 260 OEM"},
  {0x1002, 0x1462, 0x665c, 0x2938, L"AMD", L"Bonaire XT   Radeon R9 360 OEM"},
  {0x1002, 0x148c, 0x665c, 0x0907, L"AMD", L"Bonaire XT   Radeon R7 360"},
  {0x1002, 0x148c, 0x665c, 0x9260, L"AMD", L"Bonaire XT   Radeon R9 260 OEM"},
  {0x1002, 0x148c, 0x665c, 0x9360, L"AMD", L"Bonaire XT   Radeon R9 360 OEM"},
  {0x1002, 0x1682, 0x665c, 0x0907, L"AMD", L"Bonaire XT   Radeon R7 360"},
  {0x1002, 0x1682, 0x665c, 0x3310, L"AMD", L"Bonaire XT   Radeon HD 7790 Black Edition 2 GB"},
  {0x1002, 0x174b, 0x665c, 0xe253, L"AMD", L"Bonaire XT   Radeon HD 7790 Dual-X OC"},
  {0x1002, 0x1787, 0x665c, 0x2329, L"AMD", L"Bonaire XT   Radeon HD 7790 TurboDuo"},
  {0x1002, 0x1028, 0x665f, 0x0b04, L"AMD", L"Tobago PRO   Radeon R9 360 OEM"},
  {0x1002, 0x1462, 0x665f, 0x2938, L"AMD", L"Tobago PRO   Radeon R9 360 OEM"},
  {0x1002, 0x1462, 0x665f, 0x3271, L"AMD", L"Tobago PRO   Radeon R9 360 OEM"},
  {0x1002, 0x1682, 0x665f, 0x7360, L"AMD", L"Tobago PRO   Radeon R7 360"},
  {0x1002, 0x106b, 0x6810, 0x012a, L"AMD", L"Curacao XT / Trinidad XT   FirePro D300"},
  {0x1002, 0x106b, 0x6810, 0x012b, L"AMD", L"Curacao XT / Trinidad XT   FirePro D300"},
  {0x1002, 0x148c, 0x6810, 0x0908, L"AMD", L"Curacao XT / Trinidad XT   Radeon R9 370 OEM"},
  {0x1002, 0x1682, 0x6810, 0x7370, L"AMD", L"Curacao XT / Trinidad XT   Radeon R7 370"},
  {0x1002, 0x1028, 0x6811, 0x0b00, L"AMD", L"Curacao PRO   Trinidad PRO [Radeon R9 370 OEM]"},
  {0x1002, 0x1043, 0x6811, 0x2016, L"AMD", L"Curacao PRO   Trinidad PRO [Radeon R9 370 OEM]"},
  {0x1002, 0x1458, 0x6811, 0x2016, L"AMD", L"Curacao PRO   Trinidad PRO [Radeon R9 370 OEM]"},
  {0x1002, 0x1462, 0x6811, 0x2016, L"AMD", L"Curacao PRO   Trinidad PRO [Radeon R9 370 OEM]"},
  {0x1002, 0x1462, 0x6811, 0x3050, L"AMD", L"Curacao PRO   R9 270 Gaming OC"},
  {0x1002, 0x148c, 0x6811, 0x2016, L"AMD", L"Curacao PRO   Trinidad PRO [Radeon R9 370 OEM]"},
  {0x1002, 0x1682, 0x6811, 0x2015, L"AMD", L"Curacao PRO   Trinidad PRO [Radeon R7 370]"},
  {0x1002, 0x174b, 0x6811, 0x2015, L"AMD", L"Curacao PRO   NITRO Radeon R7 370"},
  {0x1002, 0x174b, 0x6811, 0x2016, L"AMD", L"Curacao PRO   Trinidad PRO [Radeon R9 370 OEM]"},
  {0x1002, 0x1787, 0x6811, 0x2016, L"AMD", L"Curacao PRO   Trinidad PRO [Radeon R9 370 OEM]"},
  {0x1002, 0x103c, 0x6820, 0x1851, L"AMD", L"Venus XTX   Radeon HD 7750M"},
  {0x1002, 0x17aa, 0x6820, 0x3643, L"AMD", L"Venus XTX   Radeon R9 A375"},
  {0x1002, 0x17aa, 0x6820, 0x3801, L"AMD", L"Venus XTX   Radeon R9 M275"},
  {0x1002, 0x17aa, 0x6820, 0x3824, L"AMD", L"Venus XTX   Radeon R9 M375"},
  {0x1002, 0x1da2, 0x6820, 0xe26a, L"AMD", L"Venus XTX   Radeon R7 250"},
  {0x1002, 0x0128, 0x682b, 0x079c, L"AMD", L"Cape Verde PRO / Venus LE / Tropo PRO-L   Radeon R7 465X"},
  {0x1002, 0x1462, 0x682b, 0x3012, L"AMD", L"Cape Verde PRO / Venus LE / Tropo PRO-L   Radeon R7 250"},
  {0x1002, 0x1025, 0x6900, 0x1056, L"AMD", L"Topaz XT   Radeon R7 M360 / R8 M365DX"},
  {0x1002, 0x1028, 0x6900, 0x0640, L"AMD", L"Topaz XT   Radeon R7 M260/M265"},
  {0x1002, 0x1028, 0x6900, 0x0643, L"AMD", L"Topaz XT   Radeon R7 M260/M265"},
  {0x1002, 0x1028, 0x6900, 0x067f, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x1028, 0x6900, 0x0767, L"AMD", L"Topaz XT   Radeon R7 M445"},
  {0x1002, 0x1028, 0x6900, 0x0810, L"AMD", L"Topaz XT   Radeon 530"},
  {0x1002, 0x1028, 0x6900, 0x130a, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x103c, 0x6900, 0x2263, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x103c, 0x6900, 0x2269, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x103c, 0x6900, 0x22c6, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x103c, 0x6900, 0x22c8, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x103c, 0x6900, 0x2b45, L"AMD", L"Topaz XT   Radeon R7 A360"},
  {0x1002, 0x103c, 0x6900, 0x808c, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x103c, 0x6900, 0x8099, L"AMD", L"Topaz XT   Radeon R7 M360"},
  {0x1002, 0x103c, 0x6900, 0x80b5, L"AMD", L"Topaz XT   Radeon R7 M360"},
  {0x1002, 0x103c, 0x6900, 0x80b9, L"AMD", L"Topaz XT   Radeon R7 M360"},
  {0x1002, 0x103c, 0x6900, 0x811c, L"AMD", L"Topaz XT   Radeon R7 M340"},
  {0x1002, 0x103c, 0x6900, 0x8226, L"AMD", L"Topaz XT   Radeon R7 M440"},
  {0x1002, 0x10cf, 0x6900, 0x1906, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x1170, 0x6900, 0x9979, L"AMD", L"Topaz XT   Radeon R7 M360"},
  {0x1002, 0x1179, 0x6900, 0xf903, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x1179, 0x6900, 0xf922, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x1179, 0x6900, 0xf923, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x1179, 0x6900, 0xf934, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x17aa, 0x6900, 0x3822, L"AMD", L"Topaz XT   Radeon R7 M360"},
  {0x1002, 0x17aa, 0x6900, 0x3824, L"AMD", L"Topaz XT   Radeon R7 M360"},
  {0x1002, 0x17aa, 0x6900, 0x5021, L"AMD", L"Topaz XT   Radeon R7 M260"},
  {0x1002, 0x106b, 0x6640, 0x014b, L"AMD", L"Saturn XT   Tropo XT [Radeon R9 M380 Mac Edition]"},
  {0x1002, 0x1043, 0x6647, 0x223d, L"AMD", L"Saturn PRO/XT   N551ZU laptop Radeon R9 M280X"},
  {0x1002, 0x1002, 0x6798, 0x3000, L"AMD", L"Tahiti XT   Tahiti XT2 [Radeon HD 7970 GHz Edition]"},
  {0x1002, 0x1002, 0x6798, 0x3001, L"AMD", L"Tahiti XT   Tahiti XTL [Radeon R9 280X]"},
  {0x1002, 0x1002, 0x6798, 0x4000, L"AMD", L"Tahiti XT   Radeon HD 8970 OEM"},
  {0x1002, 0x1043, 0x6798, 0x041c, L"AMD", L"Tahiti XT   HD 7970 DirectCU II"},
  {0x1002, 0x1043, 0x6798, 0x0420, L"AMD", L"Tahiti XT   HD 7970 DirectCU II TOP"},
  {0x1002, 0x1043, 0x6798, 0x0444, L"AMD", L"Tahiti XT   HD 7970 DirectCU II TOP"},
  {0x1002, 0x1043, 0x6798, 0x0448, L"AMD", L"Tahiti XT   HD 7970 DirectCU II TOP"},
  {0x1002, 0x1043, 0x6798, 0x044a, L"AMD", L"Tahiti XT   Tahiti XT2 [Matrix HD 7970]"},
  {0x1002, 0x1043, 0x6798, 0x044c, L"AMD", L"Tahiti XT   Tahiti XT2 [Matrix HD 7970 Platinum]"},
  {0x1002, 0x1043, 0x6798, 0x3001, L"AMD", L"Tahiti XT   Tahiti XTL [ROG Matrix R9 280X]"},
  {0x1002, 0x1043, 0x6798, 0x3006, L"AMD", L"Tahiti XT   Tahiti XTL [Radeon R9 280X DirectCU II TOP]"},
  {0x1002, 0x1043, 0x6798, 0x9999, L"AMD", L"Tahiti XT   ARES II"},
  {0x1002, 0x106b, 0x6798, 0x0127, L"AMD", L"Tahiti XT   FirePro D700"},
  {0x1002, 0x106b, 0x6798, 0x0128, L"AMD", L"Tahiti XT   FirePro D700"},
  {0x1002, 0x1092, 0x6798, 0x3000, L"AMD", L"Tahiti XT   Tahiti XT2 [Radeon HD 7970 GHz Edition]"},
  {0x1002, 0x1458, 0x6798, 0x2261, L"AMD", L"Tahiti XT   Tahiti XT2 [Radeon HD 7970 GHz Edition OC]"},
  {0x1002, 0x1458, 0x6798, 0x3001, L"AMD", L"Tahiti XT   Tahiti XTL [Radeon R9 280X OC]"},
  {0x1002, 0x1462, 0x6798, 0x2774, L"AMD", L"Tahiti XT   HD 7970 TwinFrozr III Boost Edition OC"},
  {0x1002, 0x1682, 0x6798, 0x3001, L"AMD", L"Tahiti XT   Tahiti XTL [Radeon R9 280X]"},
  {0x1002, 0x1682, 0x6798, 0x3211, L"AMD", L"Tahiti XT   Double D HD 7970 Black Edition"},
  {0x1002, 0x1682, 0x6798, 0x3213, L"AMD", L"Tahiti XT   HD 7970 Black Edition"},
  {0x1002, 0x1682, 0x6798, 0x3214, L"AMD", L"Tahiti XT   Double D HD 7970"},
  {0x1002, 0x1787, 0x6798, 0x201c, L"AMD", L"Tahiti XT   HD 7970 IceQ XA2"},
  {0x1002, 0x1787, 0x6798, 0x2317, L"AMD", L"Tahiti XT   Radeon HD 7990"},
  {0x1002, 0x1787, 0x6798, 0x3000, L"AMD", L"Tahiti XT   Tahiti XT2 [Radeon HD 7970 GHz Edition]"},
  {0x1002, 0x1002, 0x679a, 0x0b01, L"AMD", L"Tahiti PRO   Radeon HD 8950 OEM"},
  {0x1002, 0x1002, 0x679a, 0x3000, L"AMD", L"Tahiti PRO   Tahiti PRO2 [Radeon HD 7950 Boost]"},
  {0x1002, 0x1462, 0x679a, 0x3000, L"AMD", L"Tahiti PRO   Radeon HD 8950 OEM"},
  {0x1002, 0x174b, 0x679a, 0xa003, L"AMD", L"Tahiti PRO   Radeon R9 280"},
  {0x1002, 0x1028, 0x67b0, 0x0b00, L"AMD", L"Hawaii XT / Grenada XT   Grenada XT [Radeon R9 390X]"},
  {0x1002, 0x103c, 0x67b0, 0x6566, L"AMD", L"Hawaii XT / Grenada XT   Radeon R9 390X"},
  {0x1002, 0x1043, 0x67b0, 0x046a, L"AMD", L"Hawaii XT / Grenada XT   R9 290X DirectCU II"},
  {0x1002, 0x1043, 0x67b0, 0x046c, L"AMD", L"Hawaii XT / Grenada XT   R9 290X DirectCU II OC"},
  {0x1002, 0x1043, 0x67b0, 0x0474, L"AMD", L"Hawaii XT / Grenada XT   Matrix R9 290X Platinum"},
  {0x1002, 0x1043, 0x67b0, 0x0476, L"AMD", L"Hawaii XT / Grenada XT   ARES III"},
  {0x1002, 0x1043, 0x67b0, 0x04d7, L"AMD", L"Hawaii XT / Grenada XT   Radeon R9 390X"},
  {0x1002, 0x1043, 0x67b0, 0x04db, L"AMD", L"Hawaii XT / Grenada XT   Radeon R9 390X"},
  {0x1002, 0x1043, 0x67b0, 0x04df, L"AMD", L"Hawaii XT / Grenada XT   Radeon R9 390X"},
  {0x1002, 0x1043, 0x67b0, 0x04e9, L"AMD", L"Hawaii XT / Grenada XT   Radeon R9 390X"},
  {0x1002, 0x1458, 0x67b0, 0x227c, L"AMD", L"Hawaii XT / Grenada XT   R9 290X WindForce 3X OC"},
  {0x1002, 0x1458, 0x67b0, 0x2281, L"AMD", L"Hawaii XT / Grenada XT   R9 290X WindForce 3X OC"},
  {0x1002, 0x1458, 0x67b0, 0x228c, L"AMD", L"Hawaii XT / Grenada XT   R9 290X WindForce 3X"},
  {0x1002, 0x1458, 0x67b0, 0x228d, L"AMD", L"Hawaii XT / Grenada XT   R9 290X WindForce 3X OC"},
  {0x1002, 0x1458, 0x67b0, 0x2290, L"AMD", L"Hawaii XT / Grenada XT   R9 290X WindForce 3X"},
  {0x1002, 0x1458, 0x67b0, 0x22bc, L"AMD", L"Hawaii XT / Grenada XT   Radeon R9 390X"},
  {0x1002, 0x1458, 0x67b0, 0x22c1, L"AMD", L"Hawaii XT / Grenada XT   Grenada PRO [Radeon R9 390]"},
  {0x1002, 0x1462, 0x67b0, 0x2015, L"AMD", L"Hawaii XT / Grenada XT   Radeon R9 390X"},
  {0x1002, 0x1462, 0x67b0, 0x3070, L"AMD", L"Hawaii XT / Grenada XT   R9 290X Lightning"},
  {0x1002, 0x1462, 0x67b0, 0x3071, L"AMD", L"Hawaii XT / Grenada XT   R9 290X Lightning"},
  {0x1002, 0x1462, 0x67b0, 0x3072, L"AMD", L"Hawaii XT / Grenada XT   R9 290X Lightning LE"},
  {0x1002, 0x1462, 0x67b0, 0x3080, L"AMD", L"Hawaii XT / Grenada XT   R9 290X Gaming"},
  {0x1002, 0x1462, 0x67b0, 0x3082, L"AMD", L"Hawaii XT / Grenada XT   R9 290X Gaming OC"},
  {0x1002, 0x148c, 0x67b0, 0x2347, L"AMD", L"Hawaii XT / Grenada XT   Devil 13 Dual Core R9 290X"},
  {0x1002, 0x148c, 0x67b0, 0x2357, L"AMD", L"Hawaii XT / Grenada XT   Grenada XT [Radeon R9 390X]"},
  {0x1002, 0x1682, 0x67b0, 0x9290, L"AMD", L"Hawaii XT / Grenada XT   Double Dissipation R9 290X"},
  {0x1002, 0x1682, 0x67b0, 0x9395, L"AMD", L"Hawaii XT / Grenada XT   Grenada XT [Radeon R9 390X]"},
  {0x1002, 0x174b, 0x67b0, 0x0e34, L"AMD", L"Hawaii XT / Grenada XT   Radeon R9 390X"},
  {0x1002, 0x174b, 0x67b0, 0xe282, L"AMD", L"Hawaii XT / Grenada XT   Vapor-X R9 290X Tri-X OC"},
  {0x1002, 0x174b, 0x67b0, 0xe285, L"AMD", L"Hawaii XT / Grenada XT   R9 290X Tri-X OC"},
  {0x1002, 0x174b, 0x67b0, 0xe324, L"AMD", L"Hawaii XT / Grenada XT   Grenada XT2 [Radeon R9 390X]"},
  {0x1002, 0x1787, 0x67b0, 0x2020, L"AMD", L"Hawaii XT / Grenada XT   R9 290X IceQ XA2 Turbo"},
  {0x1002, 0x1787, 0x67b0, 0x2357, L"AMD", L"Hawaii XT / Grenada XT   Grenada XT [Radeon R9 390X]"},
  {0x1002, 0x1043, 0x67b1, 0x04dd, L"AMD", L"Hawaii PRO   STRIX R9 390"},
  {0x1002, 0x148c, 0x67b1, 0x2358, L"AMD", L"Hawaii PRO   Radeon R9 390"},
  {0x1002, 0x174b, 0x67b1, 0xe324, L"AMD", L"Hawaii PRO   Sapphire Nitro R9 390"},
  {0x1002, 0x1002, 0x6801, 0x0124, L"AMD", L"Neptune XT   Radeon HD 8970M"},
  {0x1002, 0x1462, 0x6801, 0x1117, L"AMD", L"Neptune XT   Radeon R9 M290X"},
  {0x1002, 0x8086, 0x6801, 0x2110, L"AMD", L"Neptune XT   Radeon HD 8970M"},
  {0x1002, 0x8086, 0x6801, 0x2111, L"AMD", L"Neptune XT   Radeon HD 8970M"},
  {0x1002, 0x1002, 0x6821, 0x031e, L"AMD", L"Venus XT   FirePro SX4000"},
  {0x1002, 0x1028, 0x6821, 0x05cc, L"AMD", L"Venus XT   FirePro M5100"},
  {0x1002, 0x1028, 0x6821, 0x15cc, L"AMD", L"Venus XT   FirePro M5100"},
  {0x1002, 0x106b, 0x6821, 0x0149, L"AMD", L"Venus XT   Radeon R9 M370X Mac Edition"},
  {0x1002, 0x1043, 0x6938, 0x04f5, L"AMD", L"Tonga XT / Amethyst XT   Radeon R9 380X"},
  {0x1002, 0x1043, 0x6938, 0x04f7, L"AMD", L"Tonga XT / Amethyst XT   Radeon R9 380X"},
  {0x1002, 0x106b, 0x6938, 0x013a, L"AMD", L"Tonga XT / Amethyst XT   Radeon R9 M295X Mac Edition"},
  {0x1002, 0x1458, 0x6938, 0x22c8, L"AMD", L"Tonga XT / Amethyst XT   Radeon R9 380X"},
  {0x1002, 0x148c, 0x6938, 0x2350, L"AMD", L"Tonga XT / Amethyst XT   Radeon R9 380X"},
  {0x1002, 0x1682, 0x6938, 0x9385, L"AMD", L"Tonga XT / Amethyst XT   Radeon R9 380X"},
  {0x1002, 0x174b, 0x6938, 0xe308, L"AMD", L"Tonga XT / Amethyst XT   Radeon R9 380X Nitro 4G D5"},
  {0x1002, 0x17af, 0x6938, 0x2006, L"AMD", L"Tonga XT / Amethyst XT   Radeon R9 380X"},
  {0x1002, 0x1462, 0x6939, 0x2015, L"AMD", L"Tonga PRO   Radeon R9 380 Gaming 4G"},
  {0x1002, 0x148c, 0x6939, 0x9380, L"AMD", L"Tonga PRO   Radeon R9 380"},
  {0x1002, 0x174b, 0x6939, 0xe308, L"AMD", L"Tonga PRO   Radeon R9 380 Nitro 4G D5"},
  {0x1002, 0x174b, 0x6939, 0xe315, L"AMD", L"Tonga PRO   Radeon R9 285"},
  {0x1002, 0x1002, 0x7300, 0x0b36, L"AMD", L"Fiji   Radeon R9 FURY X / NANO"},
  {0x1002, 0x1002, 0x7300, 0x1b36, L"AMD", L"Fiji   Radeon Pro Duo"},
  {0x1002, 0x1043, 0x7300, 0x049e, L"AMD", L"Fiji   Radeon R9 FURY"},
  {0x1002, 0x1043, 0x7300, 0x04a0, L"AMD", L"Fiji   Radeon R9 FURY X"},
  {0x1002, 0x174b, 0x7300, 0xe329, L"AMD", L"Fiji   Radeon R9 FURY"},
  {0x1a03, 0x15d9, 0x2000, 0x0832, L"ASPEED", L"ASPEED Graphics Family  X10SRL-F"},
  {0x1a03, 0x15d9, 0x2000, 0x1b95, L"ASPEED", L"ASPEED Graphics Family  H12SSL-i"},
};

/**
  Get string by index.

  @param  OptionalStrStart    The optional string start offset.
  @param  Index               The index.
  @param  String              The returned string.

  @retval Status

**/
EFI_STATUS
BdsLibGetOptionalStringByIndex (
  IN      CHAR8                   *OptionalStrStart,
  IN      UINT8                   Index,
  OUT     CHAR16                  **String
  )
{
  UINTN          StrSize;

  if (Index == 0) {
    *String = AllocateZeroPool (sizeof (CHAR16));
    if (*String == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    return EFI_SUCCESS;
  }

  StrSize = 0;
  do {
    Index--;
    OptionalStrStart += StrSize;
    StrSize           = AsciiStrSize (OptionalStrStart);
  } while (OptionalStrStart[StrSize] != 0 && Index != 0);

  if ((Index != 0) || (StrSize == 1)) {
    //
    // Meet the end of strings set but Index is non-zero, or
    // Find an empty string
    //
    *String = HiiGetString (gPlatformBmStringPackHandle, STRING_TOKEN (STR_MISSING_STRING), "en-US");
    if (*String == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
  } else {
    *String = AllocatePool (StrSize * sizeof (CHAR16));
    if (*String == NULL) {
      return EFI_OUT_OF_RESOURCES;
    }
    AsciiStrToUnicodeStrS (OptionalStrStart, *String, StrSize);
  }

  return EFI_SUCCESS;
}


/**
  Get string by string id from HII Interface.

  @param[in] Id          String ID.

  @retval    CHAR16 *    String from ID.
  @retval    NULL        If error occurs.

**/
CHAR16 *
KlGetStringById (
  IN  EFI_STRING_ID   Id
  )
{
  return HiiGetString (gPlatformBmStringPackHandle, Id, NULL);
}

/**
  This function prints a series of strings.

  @param  ConOut                Pointer to EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL
  @param  ...                   A variable argument list containing series of
                                strings, the last string must be NULL.

  @retval EFI_SUCCESS           Success print out the string using ConOut.
  @retval EFI_STATUS            Return the status of the ConOut->OutputString ().

**/
EFI_STATUS
EFIAPI
BdsLibOutputStrings (
  IN EFI_SIMPLE_TEXT_OUTPUT_PROTOCOL   *ConOut,
  ...
  )
{
  VA_LIST     Args;
  EFI_STATUS  Status;
  CHAR16      *String;

  Status = EFI_SUCCESS;
  VA_START (Args, ConOut);

  while (!EFI_ERROR (Status)) {
    //
    // If String is NULL, then it's the end of the list
    //
    String = VA_ARG (Args, CHAR16 *);
    if (String == NULL) {
      break;
    }

    Status = ConOut->OutputString (ConOut, String);

    if (EFI_ERROR (Status)) {
      break;
    }
  }

  VA_END(Args);
  return Status;
}

EFI_STATUS
GetGpuDescription(
  IN     EFI_PCI_IO_PROTOCOL              *PciIo,
  OUT    CHAR16                           *GpuName,
  OUT    CHAR16                           *VendorName
)
{

  UINT32                           Index;
  UINT16                           DeviceId;
  UINT16                           SubDeviceId;
  UINT16                           VendorId;
  UINT16                           SubVendorId;


  if ((PciIo == NULL) || (GpuName == NULL) || (VendorName == NULL)) {
    return RETURN_INVALID_PARAMETER;
  }

  PciIo->Pci.Read (PciIo,EfiPciIoWidthUint16,2,1,&DeviceId);
  PciIo->Pci.Read (PciIo,EfiPciIoWidthUint16,0,1,&VendorId);
  PciIo->Pci.Read (PciIo,EfiPciIoWidthUint16,0x2E,1,&SubDeviceId);
  PciIo->Pci.Read (PciIo,EfiPciIoWidthUint16,0x2C,1,&SubVendorId);

  for (Index = 0; Index < ARRAY_SIZE(gGpus); Index++){
    if ((DeviceId == gGpus[Index].DeviceId)&&(VendorId == gGpus[Index].VendorId)){
      CopyMem(GpuName, gGpus[Index].Description, StrLen(gGpus[Index].Description)*2 + 2);
      CopyMem(VendorName, gGpus[Index].VendorName, StrLen(gGpus[Index].VendorName)*2 + 2);
      return EFI_SUCCESS;
    }
  }

  for (Index = 0; Index < ARRAY_SIZE(gXGpus); Index++){
    if ((DeviceId == gXGpus[Index].DeviceId)
        &&(VendorId == gXGpus[Index].VendorId)
        &&(SubDeviceId == gXGpus[Index].SubsystemId)
        &&(SubVendorId == gXGpus[Index].SubvendorId)){
      CopyMem(GpuName, gXGpus[Index].Description, StrLen(gXGpus[Index].Description)*2 + 2);
      CopyMem(VendorName, gXGpus[Index].VendorName, StrLen(gXGpus[Index].VendorName)*2 + 2);
      return EFI_SUCCESS;
    }
  }

  for (Index = 0; Index < ARRAY_SIZE(gXGpus); Index++){
    if ((DeviceId == gXGpus[Index].DeviceId) && (VendorId == gXGpus[Index].VendorId)){
      CopyMem(GpuName, gXGpus[Index].Description, StrLen(gXGpus[Index].Description)*2 + 2);
      CopyMem(VendorName, gXGpus[Index].VendorName, StrLen(gXGpus[Index].VendorName)*2 + 2);
      return EFI_SUCCESS;
    }
  }

  CopyMem(GpuName, L"UNKNOW", 14);
  CopyMem(VendorName, L"UNKNOW", 14);

  return EFI_SUCCESS;
}

/**
  Show System Infomation, ie BIOS Version, BIOS Release Date,
  CPU Type, and Memory Speed.

  @retval EFI_SUCCESS       Success to show system information

**/
EFI_STATUS
ShowSystemInfo (
  VOID
  )
{
  EFI_STATUS                         Status;
  EFI_SMBIOS_PROTOCOL                *Smbios;
  CHAR16                             *ItemStr;
  EFI_SMBIOS_HANDLE                  SmbiosHandle;
  EFI_SMBIOS_TYPE                    SmbiosType;
  EFI_SMBIOS_TABLE_HEADER            *SmbiosRecord;
  UINTN                              PrintLine;
  SMBIOS_TABLE_TYPE0                 *Type0Record;
  SMBIOS_TABLE_TYPE4                 *Type4Record;
  SMBIOS_TABLE_TYPE17                *Type17Record;
  UINT8                              StrIndex;
  CHAR16                             *UnicodeStr;
  UINTN                              BufferSize;
  CHAR16                             *BiosVerStr;
  CHAR16                             *BiosBuiltStr;
  CHAR16                             *PackageBiosVerStr;
  CHAR16                             *PackageBiosBuiltStr;
  CHAR16                             *ProcessorVerStr;
  CHAR16                             *PackageProcessorVerStr;
  CHAR16                             *PackageProcessorFreqStr;
  CHAR16                             *PackageProcessorCoresStr;
  CHAR16                             *PackageModelNameStr;
  CHAR16                             *PackageHddCapStr;
  UINT8                              CpuNum;
  CHAR16                             *DimmStr;
  CHAR16                             *Manufacture;
  UINT32                             MemSize;
  CHAR16                             DramTypeStr[8];
  UINT16                             MemSpeed;
  DEVICE_LINK_DATA_PROTOCOL          *DeviceLinkDataProtocol;
  LIST_ENTRY                         *Entry;
  SATA_CONFIG_FORM_ENTRY             *SataConfigFormEntry;
  NVME_CONFIG_FORM_ENTRY             *NvmeConfigFormEntry;
  NIC_CONFIG_FORM_ENTRY              *NicConfigFormEntry;
  PCI_CONFIG_FORM_ENTRY              *PciConfigFormEntry;
  CHAR16                             GpuName[100];
  CHAR16                             VendorName[40];

  SmbiosType = SMBIOS_OEM_END;
  BufferSize = 0x100;
  CpuNum = 0;
  MemSize = 0;
  MemSpeed = 0;

  Status = gBS->LocateProtocol (
                  &gEfiSmbiosProtocolGuid,
                  NULL,
                  (VOID **) &Smbios
                  );
  ASSERT_EFI_ERROR (Status);
  if (EFI_ERROR(Status)) {
    return EFI_UNSUPPORTED;
  }

  if (gPlatformBmStringPackHandle == NULL) {
    gPlatformBmStringPackHandle = HiiAddPackages (
                                    &mPlatformBmStringPackGuid,
                                    gImageHandle,
                                    PlatformBmLibStrings,
                                    NULL
                                    );
  }
  ASSERT (gPlatformBmStringPackHandle != NULL);

  PrintLine = 1;
  gST->ConOut->EnableCursor (gST->ConOut, FALSE);
  gST->ConOut->SetCursorPosition (gST->ConOut, 1,PrintLine);

  //
  // BIOS Info
  //
  SmbiosHandle = (SmbiosType < EFI_SMBIOS_TYPE_SYSTEM_INFORMATION) ? SmbiosHandle : SMBIOS_HANDLE_PI_RESERVED;
  SmbiosType = EFI_SMBIOS_TYPE_BIOS_INFORMATION;

  Status = Smbios->GetNext (Smbios, &SmbiosHandle, &SmbiosType, &SmbiosRecord, NULL);
  Type0Record = (SMBIOS_TABLE_TYPE0 *) SmbiosRecord;

  ItemStr = KlGetStringById (STRING_TOKEN(STR_BIOS_INFO_STRING));
  if (ItemStr == NULL) {
    return EFI_UNSUPPORTED;
  }
  BdsLibOutputStrings (gST->ConOut, L"\n\r ", ItemStr, L":", NULL);
  PrintLine++;
  gBS->FreePool (ItemStr);


  PackageBiosVerStr = KlGetStringById (STRING_TOKEN(STR_BIOS_VERSION_STRING));
  if (PackageBiosVerStr == NULL) {
    return EFI_UNSUPPORTED;
  }

  PackageBiosBuiltStr = KlGetStringById (STRING_TOKEN(STR_BIOS_BUILD_DATE));
  if (PackageBiosBuiltStr == NULL) {
    return EFI_UNSUPPORTED;
  }

  StrIndex = Type0Record->BiosVersion;
  Status = BdsLibGetOptionalStringByIndex ((CHAR8*)((UINT8*)Type0Record + Type0Record->Hdr.Length), StrIndex, &BiosVerStr);

  StrIndex = Type0Record->BiosReleaseDate;
  Status = BdsLibGetOptionalStringByIndex ((CHAR8*)((UINT8*)Type0Record + Type0Record->Hdr.Length), StrIndex, &BiosBuiltStr);

  UnicodeStr = AllocateZeroPool (BufferSize);
  UnicodeSPrint(UnicodeStr,BufferSize,L"%s:%s   %s:%s",PackageBiosVerStr, BiosVerStr, PackageBiosBuiltStr, BiosBuiltStr);

  BdsLibOutputStrings (gST->ConOut, L"\n\r ", UnicodeStr, NULL);
  PrintLine++;
  gBS->FreePool (PackageBiosVerStr);
  gBS->FreePool (PackageBiosBuiltStr);
  gBS->FreePool (BiosVerStr);
  gBS->FreePool (BiosBuiltStr);
  gBS->FreePool (UnicodeStr);


  //
  // Processor Information
  //
  ItemStr = KlGetStringById (STRING_TOKEN(STR_CPU_INFO_STRING));
  if (ItemStr == NULL) {
    return EFI_UNSUPPORTED;
  }
  BdsLibOutputStrings (gST->ConOut, L"\n\r ", ItemStr, L":", NULL);
  PrintLine++;
  gBS->FreePool (ItemStr);


  SmbiosHandle = (SmbiosType < EFI_SMBIOS_TYPE_SYSTEM_INFORMATION) ? SmbiosHandle : SMBIOS_HANDLE_PI_RESERVED;
  SmbiosType = EFI_SMBIOS_TYPE_PROCESSOR_INFORMATION;

  Status = Smbios->GetNext (Smbios, &SmbiosHandle, &SmbiosType, &SmbiosRecord, NULL);
  Type4Record = (SMBIOS_TABLE_TYPE4 *) SmbiosRecord;
  CpuNum++;


  PackageProcessorVerStr = KlGetStringById (STRING_TOKEN(STR_CPU_TYPE_STRING));
  if (PackageProcessorVerStr == NULL) {
    return EFI_UNSUPPORTED;
  }

  PackageProcessorFreqStr = KlGetStringById (STRING_TOKEN(STR_CPU_FREQ_STRING));
  if (PackageProcessorFreqStr == NULL) {
    return EFI_UNSUPPORTED;
  }

  PackageProcessorCoresStr = KlGetStringById (STRING_TOKEN(STR_CPU_CORE_STRING));
  if (PackageProcessorCoresStr == NULL) {
    return EFI_UNSUPPORTED;
  }

  StrIndex = Type4Record->ProcessorVersion;
  Status = BdsLibGetOptionalStringByIndex ((CHAR8*)((UINT8*)Type4Record + Type4Record->Hdr.Length), StrIndex, &ProcessorVerStr);

  while (TRUE) {
    Status = Smbios->GetNext (Smbios, &SmbiosHandle, &SmbiosType, &SmbiosRecord, NULL);
    if (EFI_ERROR(Status)){
      break;
    }
    CpuNum++;
  }

  UnicodeStr = AllocateZeroPool (BufferSize);
  UnicodeSPrint (UnicodeStr, BufferSize, L"%s:%s  %s:%d MHz  %s:%d * %d Cores", PackageProcessorVerStr, ProcessorVerStr, PackageProcessorFreqStr, Type4Record->CurrentSpeed, PackageProcessorCoresStr, CpuNum, Type4Record->CoreCount);
  BdsLibOutputStrings (gST->ConOut, L"\n\r ", UnicodeStr, NULL);
  PrintLine++;
  gBS->FreePool (PackageProcessorVerStr);
  gBS->FreePool (PackageProcessorFreqStr);
  gBS->FreePool (PackageProcessorCoresStr);
  gBS->FreePool (ProcessorVerStr);
  gBS->FreePool (UnicodeStr);


  //
  // Memory Information
  //
  ItemStr = KlGetStringById (STRING_TOKEN(STR_MEMORY_INFO_STRING));
  if (ItemStr == NULL) {
    return EFI_UNSUPPORTED;
  }
  BdsLibOutputStrings (gST->ConOut, L"\n\r ", ItemStr, L":", NULL);
  PrintLine++;
  gBS->FreePool (ItemStr);

  SmbiosHandle = SMBIOS_HANDLE_PI_RESERVED;
  SmbiosType = EFI_SMBIOS_TYPE_MEMORY_DEVICE;
  do {
    Status = Smbios->GetNext (Smbios, &SmbiosHandle, &SmbiosType, &SmbiosRecord, NULL);
    if (!EFI_ERROR(Status)) {
      Type17Record = (SMBIOS_TABLE_TYPE17 *) SmbiosRecord;
      if (Type17Record->Speed > 0) {
        UnicodeStr = AllocateZeroPool (BufferSize);
        StrIndex = Type17Record->DeviceLocator;
        Status = BdsLibGetOptionalStringByIndex ((CHAR8*)((UINT8*)Type17Record + Type17Record->Hdr.Length), StrIndex, &DimmStr);

        StrIndex = Type17Record->Manufacturer;
        Status = BdsLibGetOptionalStringByIndex ((CHAR8*)((UINT8*)Type17Record + Type17Record->Hdr.Length), StrIndex, &Manufacture);

        MemSpeed = Type17Record->Speed;

        ZeroMem(DramTypeStr, sizeof(DramTypeStr));
        if (SMBIOS_MEMORY_DEV_TYPE_DDR4 == Type17Record->MemoryType) {
          StrCpyS(DramTypeStr, 8*sizeof(UINT16), L"DDR4");
        } else if (SMBIOS_MEMORY_DEV_TYPE_DDR3 == Type17Record->MemoryType) {
          StrCpyS(DramTypeStr, 8*sizeof(UINT16), L"DDR3");
        }

        if(SMBIOS_MEMORY_DEV_SIZE_EXTENDED == Type17Record->Size){
          MemSize = Type17Record->ExtendedSize;
        }else{
          MemSize = Type17Record->Size;
        }

        UnicodeSPrint (UnicodeStr, BufferSize, L"%s : %s %s %dMHz %dGB",
                        DimmStr,
                        Manufacture,
                        DramTypeStr,
                        MemSpeed,
                        MemSize/1024);

        BdsLibOutputStrings (gST->ConOut, L"\n\r ", UnicodeStr, NULL);
        PrintLine++;
        gBS->FreePool (UnicodeStr);
      }
    } else {
      break;
    }

  } while (TRUE);


  //
  //HDD Infomation
  //
  ItemStr = KlGetStringById (STRING_TOKEN(STR_HDD_INFORMATION_STRING));
  if (ItemStr == NULL) {
    return EFI_UNSUPPORTED;
  }
  BdsLibOutputStrings (gST->ConOut, L"\n\r ", ItemStr, L":", NULL);
  PrintLine++;
  gBS->FreePool (ItemStr);

  PackageModelNameStr = KlGetStringById (STRING_TOKEN(STR_HDD_MODELNAME_STRING));
  if (PackageModelNameStr == NULL) {
    return EFI_UNSUPPORTED;
  }

  PackageHddCapStr = KlGetStringById (STRING_TOKEN(STR_HDD_CAPABITITY_STRING));
  if (PackageHddCapStr == NULL) {
    return EFI_UNSUPPORTED;
  }

  Status = DeviceLinkDataInit(&DeviceLinkDataProtocol);
  if (DeviceLinkDataProtocol->SataLinkHead != NULL) {
    BASE_LIST_FOR_EACH (Entry, DeviceLinkDataProtocol->SataLinkHead) {
      SataConfigFormEntry = BASE_CR (Entry, SATA_CONFIG_FORM_ENTRY, Link);
      UnicodeStr = AllocateZeroPool (BufferSize);
      UnicodeSPrint(UnicodeStr,BufferSize,L"%s:%a  %s:%dGB",PackageModelNameStr,SataConfigFormEntry->SataData.Common.ModeName,PackageHddCapStr,SataConfigFormEntry->SataData.SizeInByte/1000/1000/1000);
      BdsLibOutputStrings (gST->ConOut, L"\n\r ", UnicodeStr, NULL);
      PrintLine++;
      gBS->FreePool (UnicodeStr);
    }
  }

  if (DeviceLinkDataProtocol->NvmeLinkHead != NULL) {
    BASE_LIST_FOR_EACH (Entry, DeviceLinkDataProtocol->NvmeLinkHead) {
      NvmeConfigFormEntry = BASE_CR (Entry, NVME_CONFIG_FORM_ENTRY, Link);
      UnicodeStr = AllocateZeroPool (BufferSize);
      UnicodeSPrint(UnicodeStr,BufferSize,L"%s:%a  %s:%dGB",PackageModelNameStr, NvmeConfigFormEntry->NvmeData.Mn,PackageHddCapStr,NvmeConfigFormEntry->NvmeData.SizeInByte/1000/1000/1000);
      BdsLibOutputStrings (gST->ConOut, L"\n\r ", UnicodeStr, NULL);
      PrintLine++;
      gBS->FreePool (UnicodeStr);
    }
  }
  gBS->FreePool (PackageModelNameStr);
  gBS->FreePool (PackageHddCapStr);

  //NIC Information
  ItemStr = KlGetStringById (STRING_TOKEN(STR_NIC_INFO_STRING));
  if (ItemStr == NULL) {
    return EFI_UNSUPPORTED;
  }
  BdsLibOutputStrings (gST->ConOut, L"\n\r ", ItemStr, L":", NULL);
  PrintLine++;
  gBS->FreePool (ItemStr);

  if (DeviceLinkDataProtocol->NicLinkHead != NULL) {
    BASE_LIST_FOR_EACH (Entry, DeviceLinkDataProtocol->NicLinkHead) {
      NicConfigFormEntry = BASE_CR (Entry, NIC_CONFIG_FORM_ENTRY, Link);
      UnicodeStr = AllocateZeroPool (BufferSize);
      UnicodeSPrint(UnicodeStr,BufferSize,L"%s: MAC:%02X-%02X-%02X-%02X-%02X-%02X",
                    NicConfigFormEntry->NicString,
                    NicConfigFormEntry->NicData.MacAddress[0],
                    NicConfigFormEntry->NicData.MacAddress[1],
                    NicConfigFormEntry->NicData.MacAddress[2],
                    NicConfigFormEntry->NicData.MacAddress[3],
                    NicConfigFormEntry->NicData.MacAddress[4],
                    NicConfigFormEntry->NicData.MacAddress[5]
                    );
      BdsLibOutputStrings (gST->ConOut, L"\n\r ", UnicodeStr, NULL);
      PrintLine++;
      gBS->FreePool (UnicodeStr);
    }
  }

  //GPU Information
  ItemStr = KlGetStringById (STRING_TOKEN(STR_GPU_INFO_STRING));
  if (ItemStr == NULL) {
    return EFI_UNSUPPORTED;
  }
  BdsLibOutputStrings (gST->ConOut, L"\n\r ", ItemStr, L":", NULL);
  PrintLine++;
  gBS->FreePool (ItemStr);

  if (DeviceLinkDataProtocol->PciLinkHead != NULL) {
    BASE_LIST_FOR_EACH (Entry, DeviceLinkDataProtocol->PciLinkHead) {
      PciConfigFormEntry = BASE_CR (Entry, PCI_CONFIG_FORM_ENTRY, Link);
      if (PciConfigFormEntry->IfrData.PciInfo.Common.ClassCode[2] != PCI_CLASS_DISPLAY) {
        continue;
      }

      GetGpuDescription(PciConfigFormEntry->PciioInstance, GpuName, VendorName);

      UnicodeStr = AllocateZeroPool (BufferSize);
      UnicodeSPrint(UnicodeStr,BufferSize,L"%s: %s", VendorName, GpuName);
      BdsLibOutputStrings (gST->ConOut, L"\n\r ", UnicodeStr, NULL);
      PrintLine++;
      gBS->FreePool (UnicodeStr);
    }
  }

  return EFI_SUCCESS;
}

/**
  Display Post Message.

  @retval EFI_SUCCESS       Display Post message successfully.

**/
EFI_STATUS
DisplayPostMessage (
  IN SYSTEM_SETUP_CONFIGURATION  *Configuration
  )
{
  EFI_STATUS                      Status;
  EFI_INPUT_KEY                   Key;
  UINTN                           EventIndex;
  UINT8                           NumberOfPressTab;
  EFI_EVENT                       WaitList[2];
  EFI_EVENT                       TimerEvent;
//  SYSTEM_SETUP_CONFIGURATION*     SetupVar = NULL;
  UINT8                           WaitTimme;

#if 0
  Status = KlSetupVarRead(&SetupVar);
  if (!EFI_ERROR(Status)) {
    WaitTimme = SetupVar->SetupWaitTimeOut;
  } else {
    WaitTimme = FIVE_SECOND;
  }
#endif

  NumberOfPressTab = 0;

  gBS->CreateEvent (EVT_TIMER, 0, NULL, NULL, &TimerEvent);

  if (Configuration == NULL) {
    WaitTimme = FIVE_SECOND;
    gBS->SetTimer (TimerEvent, TimerRelative, ONE_SECOND*3);
  } else {
    WaitTimme = Configuration->SetupWaitTimeOut;
  
    gBS->SetTimer (TimerEvent, TimerRelative, ONE_SECOND*3);
  }

  WaitList[0] = gST->ConIn->WaitForKey;
  WaitList[1] = TimerEvent;
  Status = gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);//clear key
  while (TRUE) {
    Status = gBS->WaitForEvent (2, WaitList, &EventIndex);
    if(EFI_ERROR(Status) || (EventIndex == 1)){
      gST->ConOut->ClearScreen (gST->ConOut);
      gBS->CloseEvent (TimerEvent);
      break;
    }
    //
    // If the timer expired, change the return to timed out
    //
    Status = gST->ConIn->ReadKeyStroke (gST->ConIn, &Key);//clear key
    if (!EFI_ERROR (Status)) {
      if (Key.UnicodeChar == CHAR_TAB) {
        NumberOfPressTab ++;
      } else {
        continue;
      }
      if ((NumberOfPressTab%2) != 0) {
        gST->ConOut->ClearScreen (gST->ConOut);
        ShowSystemInfo();
        while(WaitTimme>0){
          gBS->Stall(1000000);
          WaitTimme--;
        }
      } else {
        gST->ConOut->ClearScreen (gST->ConOut);
        Status = EnableQuietBoot (PcdGetPtr(PcdLogoFile));
      }
    } else {
      break;
    }
  }
 // if(SetupVar != NULL) FreePool(SetupVar);
  return EFI_SUCCESS;
}
