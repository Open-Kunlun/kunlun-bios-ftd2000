/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef _MAIN_CALLBACK_H_
#define _MAIN_CALLBACK_H_

//#include <SetupHiiData.h>
#include "SetupConfig.h"
#include <Library/SetupLib.h>
#include "SetupLibCommon.h"
#endif
