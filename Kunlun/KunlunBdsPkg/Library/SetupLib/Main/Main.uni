/** @file
Copyright (c) 2006-2022, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
All Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name: Main.uni

Abstract:

Revision History:

**/

/=#

#langdef   en-US "English"
#langdef   zh-CN "\wide中文\narrow"

//Main
#string STR_MAIN                            #language en-US "Main"
                                            #language zh-CN "\wide主页\narrow"

#string STR_MAIN_HELP                       #language en-US "System Overview."
                                            #language zh-CN "\wide系统概述。\narrow"

#string STR_LANGUAGE_HELP                   #language en-US "Select the default language of the BIOS Setup Utility."
                                            #language zh-CN "\wide选择配置工具默认语言。\narrow"

#string STR_LANGUAGE_PROMPT                 #language en-US "System Language"
                                            #language zh-CN "\wide系统语言\narrow"

#string STR_BIOS                            #language en-US "BIOS Information"
                                            #language zh-CN "BIOS\wide信息\narrow"

#string STR_BIOS_VENDOR_NAME                #language en-US "BIOS Vendor"
                                            #language zh-CN "BIOS\wide厂商\narrow"

#string STR_BIOS_VENDOR_VALUE               #language en-US "KunlunTech"
                                            #language zh-CN "\wide昆仑太科\narrow"

#string STR_BIOS_CORE_VERSION_NAME          #language en-US "Core Version"
                                            #language zh-CN "\wide内核版本\narrow"

#string STR_BIOS_CORE_VERSION_VALUE         #language en-US "N/A" //dynamically updated
                                            #language zh-CN "N/A"

#string STR_BIOS_COMPLIANCY_NAME            #language en-US "Spec Compliance"
                                            #language zh-CN "\wide遵循规范\narrow"

#string STR_BIOS_COMPLIANCY_VALUE           #language en-US "N/A" //dynamically updated
                                            #language zh-CN "N/A"

#string STR_BIOS_VERSION_NAME               #language en-US "BIOS Version"
                                            #language zh-CN "BIOS\wide版本\narrow"

#string STR_BIOS_VERSION_VALUE              #language en-US "KunLun BIOS V4.0"
                                            #language zh-CN "\wide昆仑\narrowBIOS V4.0"

#string STR_BIOS_DATE_NAME                  #language en-US "Build Date and Time"
                                            #language zh-CN "\wide建立日期\narrow"

#string STR_BIOS_DATE_VALUE                 #language en-US "N/A" //dynamically updated
                                            #language zh-CN "N/A"

#string STR_BIOS_GUID_NAME                  #language en-US "BIOS GUID"
                                            #language zh-CN "BIOS\wide序列号\narrow"

#string STR_BIOS_GUID_VALUE                 #language en-US "N/A" //dynamically updated
                                            #language zh-CN "N/A"

#string STR_BIOS_TAG_NAME                   #language en-US "BIOS Tag"
                                            #language zh-CN "BIOS\wide标识\narrow"

#string STR_BIOS_TAG_VALUE                  #language en-US "N/A"
                                            #language zh-CN "N/A"

#string STR_EC_INFO_SUBTITLE                #language en-US "EC Information"
                                            #language zh-CN "EC\wide信息\narrow"

#string STR_BOARD_CHIP_EC_REV_HELP          #language en-US "EC Version."
                                            #language zh-CN "EC\wide版本。\narrow"

#string STR_BOARD_CHIP_EC_REV_NAME          #language en-US "EC Version"
                                            #language zh-CN "EC\wide版本\narrow"

#string STR_BOARD_CHIP_EC_REV_VALUE         #language en-US "N/A"
                                            #language zh-CN "N/A"
//[jckuang-0004]
#string STR_X100_CHIP_VERSION_NAME          #language en-US  "X100 Firmware Version"
                                            #language zh-CN  "X100\wide固件版本\narrow"

#string STR_X100_CHIP_VERSION_VALUE         #language en-US  "N/A"
                                            #language zh-CN  "N/A"
//[jckuang-0004]

#string STR_PBF_INFO_SUBTITLE               #language en-US  "PBF Information"
                                            #language zh-CN  "PBF\wide信息\narrow"

#string STR_BOARD_CHIP_PBF_REV_HELP         #language en-US  "PBF Version."
                                            #language zh-CN  "PBF \wide版本。\narrow"

#string STR_BOARD_CHIP_PBF_REV_NAME         #language en-US  "PBF Version"
                                            #language zh-CN  "PBF\wide版本\narrow"

#string STR_BOARD_CHIP_PBF_REV_VALUE        #language en-US  "N/A"
                                            #language zh-CN  "N/A"

#string STR_BOARD_CHIP_PBF_SET_HELP         #language en-US  "Set the PBF Debug Level,Level0 has the most debug log, "
                                                             "Level7 has the least debug log."
                                            #language zh-CN  "PBF\wide调试打印等级设置。Level 0 打印最多，\n"
                                                             "Level 7 打印最少。\narrow"

#string STR_BOARD_CHIP_PBF_SET_NAME         #language en-US  "PBF Debug"
                                            #language zh-CN  "PBF\wide调试\narrow"

#string STR_BOARD_CHIP_PBF_SET_VALUE        #language en-US  "N/A"
                                            #language zh-CN  "N/A"

#string STR_BOARD_CHIP_PBF_SET_LEVEL_0      #language en-US  "LEVEL 0"
                                            #language zh-CN  "LEVEL 0"

#string STR_BOARD_CHIP_PBF_SET_LEVEL_1      #language en-US  "LEVEL 1"
                                            #language zh-CN  "LEVEL 1"

#string STR_BOARD_CHIP_PBF_SET_LEVEL_2      #language en-US  "LEVEL 2"
                                            #language zh-CN  "LEVEL 2"

#string STR_BOARD_CHIP_PBF_SET_LEVEL_3      #language en-US  "LEVEL 3"
                                            #language zh-CN  "LEVEL 3"

#string STR_BOARD_CHIP_PBF_SET_LEVEL_4      #language en-US  "LEVEL 4"
                                            #language zh-CN  "LEVEL 4"

#string STR_BOARD_CHIP_PBF_SET_LEVEL_5      #language en-US  "LEVEL 5"
                                            #language zh-CN  "LEVEL 5"

#string STR_BOARD_CHIP_PBF_SET_LEVEL_6      #language en-US  "LEVEL 6"
                                            #language zh-CN  "LEVEL 6"

#string STR_BOARD_CHIP_PBF_SET_LEVEL_7      #language en-US  "LEVEL 7"
                                            #language zh-CN  "LEVEL 7"
//
//Motherboard Information
//
#string STR_MOTHERBOARD_INFO_SUBTITLE       #language en-US "Motherboard Information"
                                            #language zh-CN "\wide主板信息\narrow"

#string STR_MOTHERBOARD_NAME_HELP           #language en-US "Motherboard Information."
                                            #language zh-CN "\wide主板信息。\narrow"

#string STR_MOTHERBOARD_NAME_STRING         #language en-US "Motherboard model"
                                            #language zh-CN "\wide主板型号\narrow"

#string STR_MOTHERBOARD_NAME_VALUE          #language en-US "N/A"
                                            #language zh-CN "N/A"

#string STR_MOTHERBOARD_NUMBER_HELP         #language en-US  "The serial number in smbios type 2."
                                            #language zh-CN  "\wide主板序列号。\narrow"

#string STR_MOTHERBOARD_NUMBER_STRING       #language en-US  "Serial Number"
                                            #language zh-CN  "\wide主板序列号\narrow"

#string STR_MOTHERBOARD_NUMBER_VALUE        #language en-US  "N/A"
                                            #language zh-CN  "N/A"

#string STR_MOTHERBOARD_VERSION_HELP        #language en-US  "The motherboard version in smbios type 2."
                                            #language zh-CN  "\wide主板版本信息。\narrow"

#string STR_MOTHERBOARD_VERSION_STRING      #language en-US  "Version"
                                            #language zh-CN  "\wide版本信息\narrow"

#string STR_MOTHERBOARD_VERSION_VALUE       #language en-US  "N/A"
                                            #language zh-CN  "N/A"
//add-by-fc-start-001
//
//System Information
//
#string STR_SYSTEM_INFO_SUBTITLE            #language en-US  "System Information"
                                            #language zh-CN  "\wide主机信息\narrow"

#string STR_SYSTEM_NAME_HELP                #language en-US  "The system product name in smbios type 1."
                                            #language zh-CN  "\wide主机产品名称。\narrow"

#string STR_SYSTEM_NAME_STRING              #language en-US  "Product Name"
                                            #language zh-CN  "\wide型号\narrow"

#string STR_SYSTEM_NAME_VALUE               #language en-US  "N/A"
                                            #language zh-CN  "N/A"

#string STR_SYSTEM_NUMBER_HELP              #language en-US  "The serial number in smbios type 1."
                                            #language zh-CN  "\wide主机产品编号。\narrow"

#string STR_SYSTEM_NUMBER_STRING            #language en-US  "Serial Number"
                                            #language zh-CN  "\wide序列号\narrow"

#string STR_SYSTEM_NUMBER_VALUE             #language en-US  "N/A"
                                            #language zh-CN  "N/A"

#string STR_SYSTEM_UUID_HELP                #language en-US  "The system uuid in smbios type 1."
                                            #language zh-CN  "\wide主机产品\narrowUUID\wide。\narrow"

#string STR_SYSTEM_UUID_STRING              #language en-US  "UUID"
                                            #language zh-CN  "UUID"

#string STR_SYSTEM_UUID_VALUE               #language en-US  "N/A"
                                            #language zh-CN  "N/A"
//add-by-fc-end-001
//
//PROCESSOR INFORMATION
//
#string STR_BOARD_CPU_FORM_SUBTITLE         #language en-US  "CPU Information"
                                            #language zh-CN "\wide处理器信息\narrow"

#string STR_BOARD_PROCESSOR_VENDOR_HELP     #language en-US  "The manufacturer of the CPU."
                                            #language zh-CN  "\wide处理器厂商。\narrow"

#string STR_BOARD_PROCESSOR_VENDOR_STRING   #language en-US  "Manufacturer"
                                            #language zh-CN  "\wide厂商\narrow"

#string STR_BOARD_PROCESSOR_VENDOR_VALUE    #language en-US  "N/A"
                                            #language zh-CN  "N/A"

#string STR_BOARD_PROCESSOR_HELP            #language en-US  "The CPU type and speed."
                                            #language zh-CN  "\wide处理器型号和工作频率。\narrow"

#string STR_BOARD_PROCESSOR_STRING          #language en-US  "CPU Type and Speed"
                                            #language zh-CN  "\wide型号与频率\narrow"

#string STR_BOARD_PROCESSOR_VALUE           #language en-US "N/A"
                                            #language zh-CN "N/A"

#string STR_BOARD_PROCESSOR_SPEED_HELP      #language en-US  "Displays the Processor Frequency."
                                            #language zh-CN  "\wide显示处理器频率。\narrow"

#string STR_BOARD_PROCESSOR_SPEED_STRING    #language en-US "Frequency"
                                            #language zh-CN "\wide频率\narrow"

#string STR_BOARD_PROCESSOR_SPEED_VALUE     #language en-US "N/A"
                                            #language zh-CN "N/A"

#string STR_BOARD_PROCESSOR_CORE_HELP       #language en-US  "The number of cores of one CPU multiple the CPU count detected in the system."
                                            #language zh-CN  "\wide处理器核数和颗数。\narrow"

#string STR_BOARD_PROCESSOR_CORE_STRING     #language en-US  "Core Count"
                                            #language zh-CN  "\wide核数\narrow"

#string STR_BOARD_PROCESSOR_CORE_VALUE      #language en-US  "N/A"
                                            #language zh-CN  "N/A"
//
//MEM Information
//
#string STR_BOARD_MEMORY_FORM_SUBTITLE      #language en-US  "Memory Information"
                                            #language zh-CN  "\wide内存信息\narrow"

#string STR_BOARD_MEMORY_INFO_HELP          #language en-US  "The total size and speed of memory installed in the system."
                                            #language zh-CN  "\wide系统安装总内存及工作频率。\narrow"

#string STR_BOARD_MEMORY_INFO_NAME          #language en-US "Total Memory"
                                            #language zh-CN "\wide总内存\narrow"

#string STR_BOARD_MEMORY_INFO_VALUE         #language en-US "N/A"
                                            #language zh-CN "N/A"

#string STR_MAIN_DATE_AND_TIME_FORM         #language en-US "System Date and Time"
                                            #language zh-CN "\wide系统日期和时间\narrow"

#string STR_MAIN_DATE_AND_TIME_FORM_HELP    #language en-US "Select this option to configure the system's date and time."
                                            #language zh-CN "\wide选择此选项可配置系统日期和时间。\narrow"

#string STR_MAIN_DATE_AND_TIME_FORM_SUB     #language en-US "System Date/System Time"
                                            #language zh-CN "\wide系统日期\narrow/\wide系统时间\narrow"

#string STR_DATE                            #language en-US "System Date"
                                            #language zh-CN "\wide系统日期\narrow"

#string STR_DATE_HELP                       #language en-US "Set the Date following the month/day/year format. \n"
                                                            "Use Tab key or "←/→"key to switch between Date fields. \n"
                                                            "Use "+/-"key to change the value."
                                            #language zh-CN  "\wide设置日期\narrow(\wide月\narrow/\wide日\narrow/\wide年\narrow)\wide。\n"
                                                             "使用\narrowTable\wide键或\narrow"←/→"\wide键可以在日期区域切换，\n"
                                                             "使用\narrow"+/-"\wide键进行数值调整\narrow(MM/DD/YYYY)。\narrow"

#string STR_TIME                            #language en-US "System Time"
                                            #language zh-CN "\wide系统时间\narrow"

#string STR_TIME_HELP                       #language en-US "Set the Time following the hour:minute:second format. \n"
                                                            "Use Tab key or "←/→"key to switch between Timefields. \n"
                                                            "Use "+/-"key to change the value."
                                            #language zh-CN "\wide设置时间\narrow(\wide时\narrow:\wide分\narrow:\wide秒\narrow)\wide。\n"
                                                            "使用\narrowTable\wide键或\narrow"←/→"\wide键可以在时间区域切换，\n"
                                                            "使用\narrow"+/-"\wide键进行数值调整\narrow(HH:MM:SS)\wide。\narrow"

#string STR_ACCESS_LEVEL                    #language en-US "Access Level"
                                            #language zh-CN "\wide访问权限\narrow"

#string STR_ACCESS_USER                     #language en-US "User"
                                            #language zh-CN "\wide用户\narrow"

#string STR_ACCESS_ADMIN                    #language en-US "Administrator"
                                            #language zh-CN "\wide管理员\narrow"

