/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/
#ifndef _INTER_SETUP_LIB_H_
#define _INTER_SETUP_LIB_H_

#include <Uefi.h>
//start-klk-lyang-P000A-modify//
//#include <FrameworkDxe.h>
#include <PiDxe.h>
#include <IndustryStandard/SmBios.h>
//end-klk-lyang-P000A-modify//
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/LoadFile.h>

#include <Protocol/FirmwareVolume2.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/LoadFile.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/Smbios.h>
#include <Protocol/GraphicsOutput.h>

#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiLib.h>
#include <Library/DevicePathLib.h>
#include <Library/MemoryAllocationLib.h>
//start-klk-lyang-P000A-add//
#include <Library/PrintLib.h>
#include <Library/HiiLib.h>
//end-klk-lyang-P000A-add//
#include <Library/SetupLib.h>
#include <Library/DebugLib.h>

#endif
