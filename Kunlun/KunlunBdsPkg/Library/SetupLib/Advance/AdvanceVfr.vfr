/** @file
Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
All Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name: Advance.vfr

Abstract:

Revision History:

**/

#include "SetupConfig.h"
#include "Configuration.h"

#define DEFAULT_CORE_SETTING_ENABLE 1
//#define DEFAULT_PCIE_SETTING_SPLIT_MODE  3
#if (FixedPcdGet32(PcdTheCpuType) == 0)
  #define DEFAULT_PCIE0_SETTING_SPLIT_MODE  1
  #define DEFAULT_PCIE1_SETTING_SPLIT_MODE  3
#elif (FixedPcdGet32(PcdTheCpuType) == 1)
  #define DEFAULT_PCIE0_SETTING_SPLIT_MODE  1
  #define DEFAULT_PCIE1_SETTING_SPLIT_MODE  3
#endif
//[gliu-0018]
//#define DEFAULT_PCIE_SETTING_CORE_ENABLE 1
#define DEFAULT_PCIE_SETTING_CORE_ENABLE 0
//[gliu-0018]
#define DEFAULT_PCIE_SETTING_CORE_SPEED  0
#define DEFAULT_PCIE_SETTING_CORE_EQ     7





#define TITLE_CORE0 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE0_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE0_SETTING_HELP),

#define TITLE_CORE1 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE1_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE1_SETTING_HELP),

#define TITLE_CORE2 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE2_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE2_SETTING_HELP),
#define TITLE_CORE3 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE3_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE3_SETTING_HELP),
#define TITLE_CORE4 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE4_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE4_SETTING_HELP),
#define TITLE_CORE5 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE5_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE5_SETTING_HELP),
#define TITLE_CORE6 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE6_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE6_SETTING_HELP),
#define TITLE_CORE7 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE7_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE7_SETTING_HELP),

#define CORE_ONEOF_SETTINGENABLE(CORE_NUM,DEFAULT_CORE_SETTING_ENABLE) \
        oneof varid  = SystemSetupConfig.CoreSettingEnable[CORE_NUM],\
            TITLE_CORE##CORE_NUM \
      option text  = STRING_TOKEN (STR_ENABLED),  value = 1, flags = RESET_REQUIRED;\
            option text  = STRING_TOKEN (STR_DISABLED), value = 0, flags = RESET_REQUIRED;\
            default = DEFAULT_CORE_SETTING_ENABLE, \
        endoneof


#define TITLE_PEU0_C0 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C0_ENABLE_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C0_ENABLE_SETTING_HELP),
#define TITLE_PEU0_C1 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C1_ENABLE_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C1_ENABLE_SETTING_HELP),
#define TITLE_PEU0_C2 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C2_ENABLE_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C2_ENABLE_SETTING_HELP),
#define TITLE_PEU1_C0 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C0_ENABLE_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C0_ENABLE_SETTING_HELP),
#define TITLE_PEU1_C1 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C1_ENABLE_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C1_ENABLE_SETTING_HELP),
#define TITLE_PEU1_C2 \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C2_ENABLE_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C2_ENABLE_SETTING_HELP),
#define PCIE_ONEOF_SETTINGCOREENABLE(PEU_NUM, CORE_NUM,DEFAULT_PCIE_SETTING_CORE_ENABLE) \
        oneof varid  = SystemSetupConfig.PcieSettingPeu##PEU_NUM##Enable[CORE_NUM],\
            TITLE_PEU##PEU_NUM##_C##CORE_NUM \
            option text  = STRING_TOKEN (STR_ENABLED),  value = 0, flags = RESET_REQUIRED;\
            option text  = STRING_TOKEN (STR_DISABLED), value = 1, flags = RESET_REQUIRED;\
            default = DEFAULT_PCIE_SETTING_CORE_ENABLE, \
        endoneof

#define TITLE_PEU0_C0_SPEED \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C0_SPEED_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C0_SPEED_SETTING_HELP),
#define TITLE_PEU0_C1_SPEED \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C1_SPEED_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C1_SPEED_SETTING_HELP),
#define TITLE_PEU0_C2_SPEED \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C2_SPEED_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C2_SPEED_SETTING_HELP),
#define TITLE_PEU1_C0_SPEED \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C0_SPEED_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C0_SPEED_SETTING_HELP),
#define TITLE_PEU1_C1_SPEED \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C1_SPEED_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C1_SPEED_SETTING_HELP),
#define TITLE_PEU1_C2_SPEED \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C2_SPEED_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C2_SPEED_SETTING_HELP),
#define PCIE_ONEOF_SETTINGCORESPEED(PEU_NUM, CORE_NUM, DEFAULT_PCIE_SETTING_CORE_SPEED) \
        oneof varid  = SystemSetupConfig.PcieSettingPeu##PEU_NUM##Speed[CORE_NUM],\
            TITLE_PEU##PEU_NUM##_C##CORE_NUM##_SPEED \
            option text  = STRING_TOKEN (STR_AUTO), value = 0, flags = RESET_REQUIRED;\
            option text  = STRING_TOKEN (STR_GEN1), value = 1, flags = RESET_REQUIRED;\
            option text  = STRING_TOKEN (STR_GEN2), value = 2, flags = RESET_REQUIRED;\
            option text  = STRING_TOKEN (STR_GEN3), value = 3, flags = RESET_REQUIRED;\
            default = DEFAULT_PCIE_SETTING_CORE_SPEED, \
        endoneof

#define TITLE_PEU0_C0_EQ \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C0_EQ_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C0_EQ_SETTING_HELP),
#define TITLE_PEU0_C1_EQ \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C1_EQ_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C1_EQ_SETTING_HELP),
#define TITLE_PEU0_C2_EQ \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C2_EQ_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_C2_EQ_SETTING_HELP),
#define TITLE_PEU1_C0_EQ \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C0_EQ_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C0_EQ_SETTING_HELP),
#define TITLE_PEU1_C1_EQ \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C1_EQ_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C1_EQ_SETTING_HELP),
#define TITLE_PEU1_C2_EQ \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C2_EQ_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_C2_EQ_SETTING_HELP),
#define PCIE_NUMERIC_SETTINGCOREEQ(PEU_NUM, CORE_NUM, DEFAULT_PCIE_SETTING_CORE_EQ) \
        numeric varid  = SystemSetupConfig.PcieSettingPeu##PEU_NUM##Eq[CORE_NUM],\
            TITLE_PEU##PEU_NUM##_C##CORE_NUM##_EQ \
            flags   = RESET_REQUIRED, \
            minimum = 0, \
            maximum = 10, \
            step    = 1, \
            default = DEFAULT_PCIE_SETTING_CORE_EQ, \
        endnumeric
#define TITLE_PEU0_SPLIT_MODE \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_SPLIT_MODE_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU0_SPLIT_MODE_SETTING_HELP),
#define TITLE_PEU1_SPLIT_MODE \
            prompt   = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_SPLIT_MODE_SETTING_STRING),\
            help     = STRING_TOKEN(STR_BOARD_PROCESSOR_PEU1_SPLIT_MODE_SETTING_HELP),
#define PCIE_ONEOF_SETTINGSPLITMODE(PEU_NUM, DEFAULT_PCIE_SETTING_SPLIT_MODE) \
        oneof name = PeuSplitMode##PEU_NUM,\
            varid  = SystemSetupConfig.PcieSettingSplitMode[PEU_NUM],\
            TITLE_PEU##PEU_NUM##_SPLIT_MODE \
            option text  = STRING_TOKEN (STR_X16),  value = 1, flags = RESET_REQUIRED;\
            option text  = STRING_TOKEN (STR_X8X8), value = 3, flags = RESET_REQUIRED;\
            default = DEFAULT_PCIE_SETTING_SPLIT_MODE, \
        endoneof

#define PCIE_SETTING(PEU_NUM,CORE_NUM,DEFAULT_PCIE_SETTING_CORE_ENABLE,DEFAULT_PCIE_SETTING_CORE_SPEED,DEFAULT_PCIE_SETTING_CORE_EQ) \
       PCIE_ONEOF_SETTINGCOREENABLE(PEU_NUM, CORE_NUM, DEFAULT_PCIE_SETTING_CORE_ENABLE);\
       PCIE_ONEOF_SETTINGCORESPEED(PEU_NUM, CORE_NUM, DEFAULT_PCIE_SETTING_CORE_SPEED);\
       PCIE_NUMERIC_SETTINGCOREEQ(PEU_NUM, CORE_NUM, DEFAULT_PCIE_SETTING_CORE_EQ)

formset
  guid  = FORMSET_ID_GUID_ADVANCE,
  title     = STRING_TOKEN(STR_ADVANCED),
  help      = STRING_TOKEN(STR_ADVANCED_HELP),
  classguid = gEfiIfrFrontPageGuid,

  varstore SYSTEM_SETUP_CONFIGURATION,            // This is the data structure type
    varid = CONFIGURATION_VARSTORE_ID,      // Optional VarStore ID
    name  = SystemSetupConfig,                    // Define referenced name in vfr
    guid  = SYSTEM_CONFIGURATION_GUID;      // GUID of this buffer storage

  varstore SYSTEM_ACCESS,            // This is the data structure type
    name  = SystemAccess,                    // Define referenced name in vfr
    guid  = SYSTEM_ACCESS_GUID;      // GUID of this buffer storage

  form
    formid = ROOT_FORM_ID,
    title = STRING_TOKEN(STR_ADVANCED);

    goto CPU_CONFIG_FORM_ID,
      prompt  = STRING_TOKEN(STR_CPU_CONFIGURATION),
      help    = STRING_TOKEN(STR_CPU_CONFIGURATION_HELP);

    goto MEMORY_CONFIG_FORM_ID,
      prompt  = STRING_TOKEN(STR_MEMORY_CONFIGURATION),
      help    = STRING_TOKEN(STR_MEMORY_CONFIGURATION_HELP);


    label LABEL_THIRD_PARTY_DRIVER_INFORMATION;
    //
    // This is where we will dynamically add a Action type op-code to show
    // the platform information.
    //
    label LABEL_END;
  
  endform;


   
  form formid = CPU_CONFIG_FORM_ID,
      title   = STRING_TOKEN (STR_CPU_CONFIGURATION);          
      SUBTITLE(STRING_TOKEN(STR_CPU_CONFIGURATION))
          
      text 
        help   = STRING_TOKEN(STR_BOARD_PROCESSOR_VENDOR_HELP),
        text   = STRING_TOKEN(STR_BOARD_PROCESSOR_VENDOR_STRING),
        text   = STRING_TOKEN(STR_BOARD_PROCESSOR_VENDOR_VALUE),
        flags  = 0, key = 0;
       
      text 
        help   = STRING_TOKEN(STR_BOARD_PROCESSOR_TYPE_HELP),
        text   = STRING_TOKEN(STR_BOARD_PROCESSOR_TYPE_STRING),
        text   = STRING_TOKEN(STR_BOARD_PROCESSOR_VALUE),
        flags  = 0, key = 0;
  
      text 
        help   = STRING_TOKEN(STR_BOARD_PROCESSOR_SPEED_HELP),
        text   = STRING_TOKEN(STR_BOARD_PROCESSOR_SPEED_STRING),
        text   = STRING_TOKEN(STR_BOARD_PROCESSOR_SPEED_VALUE),
        flags  = 0, key = 0;

    text 
      help   = STRING_TOKEN(STR_BOARD_PROCESSOR_COUNT_HELP),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_COUNT_STRING),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_COUNT_VALUE),
      flags  = 0, key = 0;
         
    text 
      help   = STRING_TOKEN(STR_BOARD_PROCESSOR_PRID_HELP),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_PRID_STRING),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_PRID_VALUE),
      flags  = 0, key = 0;

    text 
      help   = STRING_TOKEN(STR_BOARD_PROCESSOR_L1_CODE_CACHE_HELP),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_L1_CODE_CACHE_STRING),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_L1CACHE_VALUE),
      flags  = 0, key = 0;
    text 
      help   = STRING_TOKEN(STR_BOARD_PROCESSOR_L1_DATA_CACHE_HELP),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_L1_DATA_CACHE_STRING),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_L1CACHE_VALUE),
      flags  = 0, key = 0;
    text 
      help   = STRING_TOKEN(STR_BOARD_PROCESSOR_L2CACHE_HELP),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_L2CACHE_STRING),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_L2CACHE_VALUE),
      flags  = 0, key = 0;
    text 
      help   = STRING_TOKEN(STR_BOARD_PROCESSOR_L3CACHE_HELP),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_L3CACHE_STRING),
      text   = STRING_TOKEN(STR_BOARD_PROCESSOR_L3CACHE_VALUE),
      flags  = 0, key = 0;
    grayoutif NOT ideqval SystemAccess.Access == SYSTEM_PASSWORD_ADMIN;
    numeric varid = SystemSetupConfig.CoreSpeed,
        prompt  = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE_SPEED_STRING),
#if (FixedPcdGet32(PcdTheCpuType) == 0)//[zhyli-0006]-add-begin
        help    = STRING_TOKEN(STR_FT2004C_BOARD_PROCESSOR_CORE_SPEED_HELP),
        flags   = RESET_REQUIRED,
        minimum = 1600,
        maximum = 2300,
#else
#if (FixedPcdGet32(PcdTheCpuType) == 1)
        help    = STRING_TOKEN(STR_D2000_BOARD_PROCESSOR_CORE_SPEED_HELP),
        flags   = RESET_REQUIRED,
        minimum = 750,
        maximum = 2300,
#else
        help    = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE_SPEED_HELP),
        flags   = RESET_REQUIRED,
        minimum = 250,
        maximum = 2300,
#endif
#endif//[zhyli-0006]-add-end
        step    = 50,
        inconsistentif prompt = STRING_TOKEN(STR_ERROR_POPUP),
            pushthis % 50 != 0
        endif;

        default = 2300,
    endnumeric;
    SEPARATOR
    subtitle text = STRING_TOKEN(STR_BOARD_PROCESSOR_CORE_SETTING_TITLE);
    grayoutif TRUE;
    CORE_ONEOF_SETTINGENABLE(0, DEFAULT_CORE_SETTING_ENABLE);
    endif;
    CORE_ONEOF_SETTINGENABLE(1, DEFAULT_CORE_SETTING_ENABLE);
    CORE_ONEOF_SETTINGENABLE(2, DEFAULT_CORE_SETTING_ENABLE);
    CORE_ONEOF_SETTINGENABLE(3, DEFAULT_CORE_SETTING_ENABLE);
    suppressif (FixedPcdGet32(PcdTheCpuType) == 0);//[zhyli-0006]-add
    CORE_ONEOF_SETTINGENABLE(4, DEFAULT_CORE_SETTING_ENABLE);
    CORE_ONEOF_SETTINGENABLE(5, DEFAULT_CORE_SETTING_ENABLE);
    CORE_ONEOF_SETTINGENABLE(6, DEFAULT_CORE_SETTING_ENABLE);
    CORE_ONEOF_SETTINGENABLE(7, DEFAULT_CORE_SETTING_ENABLE);
    endif;  //[zhyli-0006]-add
    SEPARATOR
    subtitle text = STRING_TOKEN(STR_BOARD_PROCESSOR_PCIE_SETTING_TITLE);
    PCIE_ONEOF_SETTINGSPLITMODE(0, DEFAULT_PCIE0_SETTING_SPLIT_MODE);
    PCIE_SETTING(0,0,DEFAULT_PCIE_SETTING_CORE_ENABLE,DEFAULT_PCIE_SETTING_CORE_SPEED,DEFAULT_PCIE_SETTING_CORE_EQ); 
    suppressif questionref (PeuSplitMode0) == 1;
    PCIE_SETTING(0,1,DEFAULT_PCIE_SETTING_CORE_ENABLE,DEFAULT_PCIE_SETTING_CORE_SPEED,DEFAULT_PCIE_SETTING_CORE_EQ); 
    endif; 
    PCIE_SETTING(0,2,DEFAULT_PCIE_SETTING_CORE_ENABLE,DEFAULT_PCIE_SETTING_CORE_SPEED,DEFAULT_PCIE_SETTING_CORE_EQ); 
    PCIE_ONEOF_SETTINGSPLITMODE(1, DEFAULT_PCIE1_SETTING_SPLIT_MODE);
    PCIE_SETTING(1,0,DEFAULT_PCIE_SETTING_CORE_ENABLE,DEFAULT_PCIE_SETTING_CORE_SPEED,DEFAULT_PCIE_SETTING_CORE_EQ); 
    suppressif questionref (PeuSplitMode1) == 1;
    PCIE_SETTING(1,1,DEFAULT_PCIE_SETTING_CORE_ENABLE,DEFAULT_PCIE_SETTING_CORE_SPEED,DEFAULT_PCIE_SETTING_CORE_EQ); 
    endif;
    PCIE_SETTING(1,2,DEFAULT_PCIE_SETTING_CORE_ENABLE,DEFAULT_PCIE_SETTING_CORE_SPEED,DEFAULT_PCIE_SETTING_CORE_EQ); 
    endif;//End of grayoutif
  endform; // End of CPU_CONFIG_FORM_ID
  form formid = MEMORY_CONFIG_FORM_ID,
    title   = STRING_TOKEN (STR_MEMORY_CONFIGURATION);
    SUBTITLE(STRING_TOKEN(STR_MEMORY_CONFIGURATION))
  
    text 
      help   = STRING_TOKEN(STR_BOARD_MEMORY_SIZE_HELP),
      text   = STRING_TOKEN(STR_BOARD_MEMORY_SIZE_NAME),
      text   = STRING_TOKEN(STR_BOARD_MEMORY_SIZE_VALUE),
      flags  = 0,
      key    = 0;

    grayoutif ideqval SystemAccess.Access == SYSTEM_PASSWORD_USER;
    oneof varid  = SystemSetupConfig.LmuSpeed,
        prompt   = STRING_TOKEN(STR_BOARD_MEMORY_FREQ_NAME),
        help     = STRING_TOKEN(STR_BOARD_MEMORY_FREQ_HELP),
             option text  = STRING_TOKEN (STR_DDR_FREQ_AUTO), value = 0, flags = RESET_REQUIRED;
                option text  = STRING_TOKEN (STR_DDR_FREQ_1600), value = 400, flags = RESET_REQUIRED;
                option text  = STRING_TOKEN (STR_DDR_FREQ_1866), value = 467, flags = RESET_REQUIRED;
                option text  = STRING_TOKEN (STR_DDR_FREQ_2133), value = 533, flags = RESET_REQUIRED;
                option text  = STRING_TOKEN (STR_DDR_FREQ_2400), value = 600, flags = RESET_REQUIRED;
                option text  = STRING_TOKEN (STR_DDR_FREQ_2666), value = 667, flags = RESET_REQUIRED;
              //option text  = STRING_TOKEN (STR_DDR_FREQ_2933), value = 733, flags = RESET_REQUIRED;
              //option text  = STRING_TOKEN (STR_DDR_FREQ_3200), value = 800, flags = RESET_REQUIRED;
        default = 0, 
    endoneof;
    endif;
  //[jiangfengzhang-0001]

    //[gliu-0027]add-start 
    suppressif (FixedPcdGet32(PcdTheCpuType) == 0);
    grayoutif ideqval SystemAccess.Access == SYSTEM_PASSWORD_USER;
    oneof varid  = SystemSetupConfig.FastTrainEnable,
        questionid = FORM_MEMORY_FAST_TRAIN_SET,
        prompt   = STRING_TOKEN(STR_BOARD_MEMORY_FAST_TRAIN_NAME),
        help     = STRING_TOKEN(STR_BOARD_MEMORY_FAST_TRAIN_HELP),
        flags = INTERACTIVE | RESET_REQUIRED,
                option text  = STRING_TOKEN (STR_DDR_FAST_TRAIN_DISABLE), value = 0, flags = DEFAULT;
                option text  = STRING_TOKEN (STR_DDR_FAST_TRAIN_ENABLE), value = 1, flags = 0;
    endoneof;
    endif;
    endif;
    //[gliu-0027]add-end


    SEPARATOR
    label MEM_DEVICE_ENTRY_LABEL;
    label MEM_DEVICE_LABEL_END;


  endform; // End of MEMORY_CONFIG_FORM_ID


endformset;
