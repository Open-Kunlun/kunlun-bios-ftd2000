/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef _ADVANCE_CALLBACK_H_
#define _ADVANCE_CALLBACK_H_

#include <Library/SetupLib.h>
#include "SetupLibCommon.h"
#include "SetupConfig.h"

#define SMBIOS_MEMORY_DEV_SIZE_UNINSTALL    0x0
#define SMBIOS_MEMORY_DEV_SIZE_UNKNOWN      0xFFFF
#define SMBIOS_MEMORY_DEV_SIZE_EXTENDED     0x7FFF      //Mb
#define SMBIOS_MEMORY_DEV_SIZE_MAX_VALUE    0x3FFFFFFF  //Mb

#define SMBIOS_MEMORY_DEV_TYPE_UNKNOWN      0x02
#define SMBIOS_MEMORY_DEV_TYPE_DDR          0x12
#define SMBIOS_MEMORY_DEV_TYPE_DDR2         0x13
#define SMBIOS_MEMORY_DEV_TYPE_DDR3         0x18
#define SMBIOS_MEMORY_DEV_TYPE_DDR4         0x1A

#define SMBIOS_CACHE_LEVEL_OFFSET             0x0
#define SMBIOS_CACHE_LEVEL_MASK               0x7
#define SMBIOS_CACHE_SIZE_GRANULARITY_OFFSET  0xF
#define SMBIOS_CACHE_SIZE_GRANULARITY_MASK    0x1
#define SMBIOS_CACHE_SIZE_GRANULARITY_1K      1       //Kb
#define SMBIOS_CACHE_SIZE_GRANULARITY_64K     64      //Kb
#define SMBIOS_CACHE_SIZE_OFFSET              0x0
#define SMBIOS_CACHE_SIZE_MASK                0x7FFF

VOID
UpdateCpuInformation (VOID);
VOID
UpdateMemoryInformation (VOID);
VOID
UiCreateGraphicsResolutionMenu (
EFI_HII_HANDLE  HiiHandle
);
#endif
