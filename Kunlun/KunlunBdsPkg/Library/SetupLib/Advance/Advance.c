/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#include <Advance.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HobLib.h>
#include <Library/FlashLib.h>
#include <Library/IoLib.h>

STATIC EFI_CALLBACK_INFO                            *mAdvanceCallBackInfo;
EFI_HII_HANDLE                                      gAdvHiiHandle = NULL;
EFI_GUID                                            FormsetGuid = FORMSET_ID_GUID_ADVANCE;
EFI_GUID                                            VarStoreGuid = SYSTEM_CONFIGURATION_GUID;


/**
  Brief description of InitAdvancedMenu.

  @param  HiiHandle

  @retval EFI_SUCCESS   Function successful returned.
**/
EFI_STATUS
InitAdvancedMenu (
  IN EFI_HII_HANDLE                         HiiHandle
  )
{
  if (!CheckSmBiosProtocol()) {
    return EFI_UNSUPPORTED;
  }
  UpdateCpuInformation();
  UpdateMemoryInformation();
  return EFI_SUCCESS;
}



/**
  Brief description of AdvanceCallbackRoutine.

  @param  This
  @param  Action
  @param  QuestionId
  @param  Type
  @param  Value
  @param  ActionRequest

  @retval EFI_SUCCESS   Function successful returned.
**/
EFI_STATUS
EFIAPI
AdvanceCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  EFI_BROWSER_ACTION                     Action,
  IN  EFI_QUESTION_ID                        QuestionId,
  IN  UINT8                                  Type,
  IN  EFI_IFR_TYPE_VALUE                     *Value,
  OUT EFI_BROWSER_ACTION_REQUEST             *ActionRequest
  )
{
  EFI_STATUS                            Status;
  EFI_CALLBACK_INFO                     *CallbackInfo;
  UINTN                                 BufferSize;

  if (Action != EFI_BROWSER_ACTION_CHANGED) {
    return EFI_UNSUPPORTED;
  }
  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;
  CallbackInfo = EFI_CALLBACK_INFO_FROM_THIS (This);
  CheckSetupHiiDataManager();
  //
  //The callback is called before/after setupbrowser, so need save/restore setup browser data to gSetupDataManagment SCBuffer
  //
  if (QuestionId == GET_SETUP_CONFIG || QuestionId == SET_SETUP_CONFIG) {
  BufferSize = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemSetupConfig");
  Status = SetupVariableConfig (
         &VarStoreGuid,
         SETUP_VARIABLE_NAME,
         BufferSize,
         (UINT8 *) gSetupDataManagment->SCBuffer,
         (BOOLEAN)(QuestionId == GET_SETUP_CONFIG)
         );

  return Status;
  }

  BufferSize = GetVarStoreSize (CallbackInfo->HiiHandle, &CallbackInfo->FormsetGuid, &VarStoreGuid, "SystemSetupConfig");
  SetupVariableConfig (
    &VarStoreGuid,
    SETUP_VARIABLE_NAME,
    BufferSize,
    (UINT8 *) gSetupDataManagment->SCBuffer,
    TRUE
    );

  return EFI_SUCCESS;
}

/**
  Brief description of InstallAdvanceCallbackRoutine.

  @param  DriverHandle
  @param  HiiHandle

  @retval EFI_SUCCESS   Function successful returned.
**/
EFI_STATUS
InstallAdvanceCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  )
{
  EFI_STATUS                                Status;

  mAdvanceCallBackInfo = AllocatePool (sizeof (EFI_CALLBACK_INFO));
  if (mAdvanceCallBackInfo == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  mAdvanceCallBackInfo->Signature                    = EFI_CALLBACK_INFO_SIGNATURE;
  mAdvanceCallBackInfo->DriverCallback.ExtractConfig = GenericExtractConfig;
  mAdvanceCallBackInfo->DriverCallback.RouteConfig   = GenericRouteConfig;
  mAdvanceCallBackInfo->DriverCallback.Callback      = AdvanceCallbackRoutine;
  mAdvanceCallBackInfo->HiiHandle                    = HiiHandle;
  gAdvHiiHandle                                      = HiiHandle;

  CopyGuid (&mAdvanceCallBackInfo->FormsetGuid, &FormsetGuid);

   //
  // Install protocol interface
  //
  Status = gBS->InstallProtocolInterface (
                  &DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &mAdvanceCallBackInfo->DriverCallback
                  );
  ASSERT_EFI_ERROR (Status);

  InitAdvancedMenu(HiiHandle);
  return Status;

}
