/** @file
Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
All Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name: PowerManage.vfr

Abstract:

Revision History:

**/

#include "SetupConfig.h"
#include <Guid/HiiPlatformSetupFormset.h>

/*
UINT8     RtcWakeUpSelect;   //0:关闭1:单次  2:每天    3:每周
UINT8     RtcWakeUpWeek;
UINT8     RtcWakeUpHour;
UINT8     RtcWakeUpMin;
*/

#define RTC_WAKEUP_ONEOF_SELECT \
  grayoutif ideqval SystemAccess.Access == SYSTEM_PASSWORD_USER;\
  oneof varid = SystemSetupConfig.RtcWakeUpPolicy,\
    questionid = POWER_MANAGE_RTC_WAKEUP_ID,\
    prompt = STRING_TOKEN(STR_RTC_WAKE_UP_POLICY),\
    help = STRING_TOKEN(STR_RTC_WAKE_UP_POLICY_HELP),\
    flags = INTERACTIVE | RESET_REQUIRED,\
    option text  = STRING_TOKEN (STR_RTC_WAKE_UP_CTRL_OFF),  value = 0,flags = RESET_REQUIRED;\
    option text  = STRING_TOKEN (STR_RTC_WAKE_UP_MODE_ONCE),  value = 1, flags = RESET_REQUIRED;\
    option text  = STRING_TOKEN (STR_RTC_WAKE_UP_MODE_EVERY_DAY),  value = 2, flags = RESET_REQUIRED;\
    option text  = STRING_TOKEN (STR_RTC_WAKE_UP_MODE_EVERY_WEEK),  value = 3, flags = RESET_REQUIRED;\
    default = 0, \
  endoneof;\
  endif;


formset guid  = FORMSET_ID_GUID_POWER_MANAGE,
  title     = STRING_TOKEN(STR_POWER_MANAGER_TITLE),
  help      = STRING_TOKEN(STR_POWER_MANAGER_TITLE),
  classguid = gEfiIfrFrontPageGuid,

  varstore SYSTEM_SETUP_CONFIGURATION,            // This is the data structure type
    varid = CONFIGURATION_VARSTORE_ID,      // Optional VarStore ID
    name  = SystemSetupConfig,                    // Define referenced name in vfr
    guid  = SYSTEM_CONFIGURATION_GUID;      // GUID of this buffer storage
    
  varstore SYSTEM_ACCESS,            // This is the data structure type
    name  = SystemAccess,                    // Define referenced name in vfr
    guid  = SYSTEM_ACCESS_GUID;      // GUID of this buffer storage 
    
  form
    formid = ROOT_FORM_ID,
    title = STRING_TOKEN(STR_POWER_MANAGER_TITLE);

    grayoutif ideqval SystemAccess.Access == SYSTEM_PASSWORD_USER;
    oneof varid = SystemSetupConfig.NicWakeUpPolicy,
      prompt = STRING_TOKEN(STR_NIC_WAKE_UP_POLICY),
      help = STRING_TOKEN(STR_NIC_WAKE_UP_POLICY_HELP),
      option text  = STRING_TOKEN (STR_POWER_M_DISABLE), value = 0, flags = DEFAULT | RESET_REQUIRED;
      option text  = STRING_TOKEN (STR_POWER_M_ENABLE),  value = 1, flags = RESET_REQUIRED;
      
    endoneof;
    endif;

  goto RTC_WAKEUP_FORM_ID,
    prompt = STRING_TOKEN(STR_RTC_WAKE_UP_TITLE),
    help = STRING_TOKEN(STR_RTC_WAKE_UP_TITLE_HELP);
    
  endform;


  form formid = RTC_WAKEUP_FORM_ID,
    title = STRING_TOKEN(STR_RTC_WAKE_UP_TITLE);
    SEPARATOR
    subtitle text = STRING_TOKEN(STR_RTC_WAKE_UP_TITLE);  
      RTC_WAKEUP_ONEOF_SELECT
    suppressif ideqval SystemSetupConfig.RtcWakeUpPolicy == 0;

      time
        varid   = SystemSetupConfig.WakeUpTime,
        prompt  = STRING_TOKEN(STR_WAKEUP_TIME),
        help    = STRING_TOKEN(STR_WAKEUP_TIME_HELP),
        flags   = STORAGE_NORMAL,
        default = 8:0:0,
      endtime;

      suppressif NOT ideqval SystemSetupConfig.RtcWakeUpPolicy == 1;
        date
          varid  = SystemSetupConfig.WakeUpDate,
          prompt  = STRING_TOKEN(STR_WAKEUP_DATE),
          help    = STRING_TOKEN(STR_WAKEUP_DATE_HELP),
          flags = STORAGE_NORMAL,
        default = 2008/4/23,
        enddate;
      endif;

    suppressif NOT ideqval SystemSetupConfig.RtcWakeUpPolicy == 3;
    oneof
        varid       = SystemSetupConfig.WakeUpWeek,
        prompt      = STRING_TOKEN(STR_WAKEUP_WEEK),
        help        = STRING_TOKEN(STR_WAKEUP_WEEK_HELP),
        option text = STRING_TOKEN(STR_WAKEUP_WEEK_1), value = 1, flags = DEFAULT;
        option text = STRING_TOKEN(STR_WAKEUP_WEEK_2), value = 2, flags = 0;
        option text = STRING_TOKEN(STR_WAKEUP_WEEK_3), value = 3, flags = 0;
        option text = STRING_TOKEN(STR_WAKEUP_WEEK_4), value = 4, flags = 0;
        option text = STRING_TOKEN(STR_WAKEUP_WEEK_5), value = 5, flags = 0;
        option text = STRING_TOKEN(STR_WAKEUP_WEEK_6), value = 6, flags = 0;
        option text = STRING_TOKEN(STR_WAKEUP_WEEK_7), value = 7, flags = 0;
    endoneof;
    endif;

    endif; 

  endform;

endformset;

