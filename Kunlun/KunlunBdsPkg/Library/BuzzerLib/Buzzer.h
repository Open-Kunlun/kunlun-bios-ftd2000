/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef _BUZZER_H
#define _BUZZER_H

#include <Library/DebugLib.h>
#include <Library/ParameterTable.h>
#include <Library/IoLib.h>
#include <Library/TimerLib.h>
#include <Library/PhytiumPowerControlLib.h>
#include <Library/ArmPlatformLib.h>
#include <Library/PcdLib.h>
#include <Library/DebugLib.h>
#include <Library/I2CLib.h>
#include <Library/SeLib.h>
#include <SmbiosData.h>
#include <Library/HobLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <IndustryStandard/SmBios.h>
#include <Library/BuzzerLib.h>
#include <Library/UefiBootManagerLib.h>



#define PWR_CTR0_REG      0x28180480
#define PWR_CTR1_REG      0x28180484



#endif
