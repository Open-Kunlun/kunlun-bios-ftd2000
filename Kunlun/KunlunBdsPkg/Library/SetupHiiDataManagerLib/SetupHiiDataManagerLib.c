/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/
#include <Library/SetupHiiDataManagerLib.h>

SETUP_HII_DATA_MANAGMENT_PROTOCOL            *gSetupDataManagment = NULL;
extern EFI_BOOT_SERVICES  *gBS;

/**
  Brief description of CheckSetupHiiDataManager.

**/
BOOLEAN
CheckSetupHiiDataManager (
  VOID
  )
{
  EFI_STATUS    Status;

  //
  // There should be only one instance in the system
  // of the SetupDataManagment protocol, installed by
  // the BDS before sending the SetupUtilutyBrowser form
  //
  if (gSetupDataManagment != NULL) {
    return TRUE;
  }
  Status = gBS->LocateProtocol (
                  &gSetupHiiDataManagmentProtocolGuid,
                  NULL,
                  (VOID**)&gSetupDataManagment
                  );

  if (EFI_ERROR (Status)) {
    gSetupDataManagment = NULL;
    return FALSE;
  }

  return TRUE;
}

//[gliu-0060]add-begin
/**
  Brief description of RepaintFrameLine.

**/

VOID
RepaintFrameLine (
  VOID
)
{
  gSetupDataManagment->LayoutIsChanged = TRUE;
  gSetupDataManagment->RepaintFrameLine = TRUE;
}
//[gliu-0060]add-end
