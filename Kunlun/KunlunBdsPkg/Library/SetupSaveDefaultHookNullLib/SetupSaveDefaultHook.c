/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#include <Base.h>
#include <Library/SetupSaveDefaultHookLib.h>
#include <Library/DxeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/HiiLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Protocol/BIosEventLogStorage.h>
#include <Protocol/BiosPasswordService.h>

EFI_STATUS
SetupDefaultCallBack (
  VOID
  )
{
  EFI_BIOS_EVENT_LOG_PROTOCOL    *EventLogProtocol;
  EFI_STATUS                     Status;
  UINT8                          Type;
  EFI_TIME                       EfiTime;

  DEBUG ((EFI_D_ERROR, "enter SetupDefaultCallBack.\n"));

  Status = gBS->LocateProtocol (&gEfiBiosEventLogProtocolGuid, NULL, (VOID **) &EventLogProtocol);
  if (EFI_ERROR(Status)) {
    DEBUG ((EFI_D_ERROR, "not found event log protocol in SetupDefaultCallBack.\n" ));
    return EFI_UNSUPPORTED;
  }

  gRT->GetTime (&EfiTime, NULL);

  Type = EfiEventLogTypeSystemConfigurationInformation;
  Status = EventLogProtocol->Write (
                EventLogProtocol,
                Type,
                0,
                0,
                0,
                NULL,
                &EfiTime
                );

  return EFI_UNSUPPORTED;
}

EFI_STATUS
SetupSubmitCallBack (
  VOID
  )

{
  EFI_BIOS_EVENT_LOG_PROTOCOL    *EventLogProtocol;
  EFI_STATUS                     Status;
  UINT8                          Type;
  EFI_TIME                       EfiTime;

  DEBUG ((EFI_D_INFO, "enter SetupSaveCallBack.\n"));

  Status = gBS->LocateProtocol (&gEfiBiosEventLogProtocolGuid, NULL, (VOID **) &EventLogProtocol);
  if (EFI_ERROR(Status)) {
    DEBUG ((EFI_D_ERROR, "not found event log protocol in SetupSaveCallBack.\n" ));
    return EFI_UNSUPPORTED;
  }

  gRT->GetTime (&EfiTime, NULL);

  Type = EfiEventLogTypeSystemReconfigured;
  Status = EventLogProtocol->Write (
                EventLogProtocol,
                Type,
                0,
                0,
                0,
                NULL,
                &EfiTime
                );

  return EFI_SUCCESS;
}
EFI_STATUS
SetupDefaultAuth (
  VOID
  )
{
   return EFI_SUCCESS;
}

EFI_STATUS
SetupSubmitAuth (
  VOID
  )
{
   return EFI_SUCCESS;
}
