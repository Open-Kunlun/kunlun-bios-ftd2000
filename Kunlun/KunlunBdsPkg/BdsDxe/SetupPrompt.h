/** @file
Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
All Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name: SetupPromptString.uni

Abstract:

Revision History:

**/

#ifndef	SETUPPROMPT_H_
#define	SETUPPROMPT_H_

#include <Uefi/UefiBaseType.h>
#include <Uefi/UefiSpec.h>
#include <Library/DebugLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseLib.h>
#include <Library/PcdLib.h>
#include <Library/UefiBootManagerLib.h>
#include <Library/DevicePathLib.h>
#include <Library/UefiLib.h>
#include <Library/HiiLib.h>
#include <Library/PrintLib.h>
#include <Library/KlSetupVarRWLib.h>
#include <SetupConfig.h>
#include <Library/KlBootLogoLib.h>

extern unsigned char BdsDxeStrings[];

VOID
SetupPromptWait (
  IN EFI_EVENT      HotkeyTriggered
);


#endif // _SETUPPROMPT_H_
