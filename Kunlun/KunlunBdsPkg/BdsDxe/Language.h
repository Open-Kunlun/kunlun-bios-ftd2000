/** @file
  Language setting

Copyright (c) 2004 - 2015, Intel Corporation. All rights reserved.<BR>
Copyright (c) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.
SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _LANGUAGE_H_
#define _LANGUAGE_H_

/**
  Determine the current language that will be used
  based on language related EFI Variables.

  @param LangCodesSettingRequired If required to set LangCode variable

**/
VOID
InitializeLanguage (
  BOOLEAN LangCodesSettingRequired
  );

#endif // _LANGUAGE_H_
