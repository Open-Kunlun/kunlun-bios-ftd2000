/** @file
Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
All Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name: SetupPromptString.uni

Abstract:

Revision History:

**/
#include "SetupPrompt.h"

STATIC EFI_HII_HANDLE gStringPackHandle;
#define PLATFORM_MONITOR_HDD_CHANGED      BIT0
#define PLATFORM_MONITOR_RAM_CHANGED      BIT1

STATIC EFI_GUID mUiStringPackGuid = {
  0x35FE14C0, 0x8027, 0x80F2, {0x9B, 0x5E, 0x9D, 0x67, 0xA7, 0x20, 0xE0, 0x56}
};

/**

  Initialize the string.

  VOID

**/
STATIC
VOID
InitializeStringSupport (
  VOID
  )
{
  gStringPackHandle = HiiAddPackages (
                         &mUiStringPackGuid,
                         gImageHandle,
                         BdsDxeStrings,
                         NULL
                         );
  ASSERT (gStringPackHandle != NULL);
}

/**

  Uninitialize the string.

  VOID

**/
STATIC
VOID
UninitializeStringSupport (
  VOID
  )
{
  HiiRemovePackages (gStringPackHandle);
}


/**

  Display the hhd and memory change on the setup.

  VOID

**/
STATIC
VOID
EFIAPI
SetupHhdAndMemChangeDisplay(
  VOID
)
{
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL Black;
  EFI_GRAPHICS_OUTPUT_BLT_PIXEL White;
  EFI_STRING                    String;
  UINT16                        MemAndHdChangedFlag;
  UINTN                         BaseRow;
  UINTN                         RowOffset;

  RowOffset=40;
  BaseRow = PcdGet32(PcdVideoVerticalResolution);
  MemAndHdChangedFlag = PcdGet16(PcdMemAndHdChangedFlag);

  if (gST->ConOut == NULL) {
    return;
  }

  Black.Blue = 0;
  Black.Green = 0;
  Black.Red = 0;
  Black.Reserved = 0;

  White.Blue = 0xFF;
  White.Green = 0xFF;
  White.Red = 0xFF;
  White.Reserved = 0xFF;

  gST->ConOut->EnableCursor(gST->ConOut, FALSE);
  gST->ConOut->ClearScreen(gST->ConOut);
  EnableQuietBoot (PcdGetPtr(PcdLogoFile));

  if(MemAndHdChangedFlag&PLATFORM_MONITOR_HDD_CHANGED){
    String = HiiGetString (gStringPackHandle, STRING_TOKEN (STR_POST_MONITOR_HDD_WARNING), NULL);
    ASSERT (String != NULL);
    PrintXY(20, BaseRow-RowOffset, &White, &Black, String);
    FreePool (String);
    RowOffset += 20;
  }
  if(MemAndHdChangedFlag&PLATFORM_MONITOR_RAM_CHANGED){
    String = HiiGetString (gStringPackHandle, STRING_TOKEN (STR_POST_MONITOR_RAM_WARNING), NULL);
    ASSERT (String != NULL);
    PrintXY(20, BaseRow-RowOffset, &White, &Black, String);
    FreePool (String);
  }
#ifdef KEY_HELP_INORMATION_OPEN

    String = HiiGetString (gStringPackHandle, STRING_TOKEN (STR_SETUP_PROMPT_TAB), NULL);
    ASSERT (String != NULL);
    PrintXY(20, 10, &White, &Black, String);
    FreePool (String);

    String = HiiGetString (gStringPackHandle, STRING_TOKEN (STR_SETUP_PROMPT_F2), NULL);
    ASSERT (String != NULL);
    PrintXY(20, 30, &White, &Black, String);
    FreePool (String);
    String = HiiGetString (gStringPackHandle, STRING_TOKEN (STR_SETUP_PROMPT_F7), NULL);
    ASSERT (String != NULL);
    PrintXY(20, 50, &White, &Black, String);
    FreePool (String);

#endif


}


/**

  To prompt setup wait.

  @param HotkeyTriggered

**/
VOID
SetupPromptWait(
  IN EFI_EVENT HotkeyTriggered
)
{

  InitializeStringSupport();
  DEBUG((EFI_D_INFO, "====----------------bds setup wait start-----------====\n"));
  SetupHhdAndMemChangeDisplay();


  UninitializeStringSupport();
  DEBUG((EFI_D_INFO, "====----------------bds setup wait end------------====\n"));
}
