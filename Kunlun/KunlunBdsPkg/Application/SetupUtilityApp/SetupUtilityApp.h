/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef _SETUP_UTILITY_APPLICATION_H_
#define _SETUP_UTILITY_APPLICATION_H_

#include <Uefi.h>

#include <Protocol/HiiDatabase.h>
#include <Protocol/FormBrowser2.h>
#include <Protocol/FormBrowserEx2.h>
#include <Protocol/SetupUtilityApplication.h>

#include <Guid/MdeModuleHii.h>

#include <Library/UefiApplicationEntryPoint.h>
#include <Library/UefiLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/HiiLib.h>
#include <Library/PcdLib.h>
#include <Library/SetupLib.h>
#include <Library/SetupHiiDataManagerLib.h>

#define SETUP_FORMSET_GUID \
  { \
    0x3336B68B, 0x9F08, 0x47DA, { 0xBC, 0x4F, 0xC9, 0x95, 0xC0, 0xA4, 0x7C, 0x4C} \
  }

#endif

