/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#include <Security.h>
#include <Library/BaseMemoryLib.h>
//add-klk-lyang-P000A-start//
#include <Protocol/BiosPasswordService.h>
//add-klk-lyang-P000A-end//

STATIC EFI_CALLBACK_INFO                            *mSecurityCallBackInfo;
//add-klk-lyang-P000A-start//


/**
  Brief description of SecurityCallbackRoutine.

  @param  This
  @param  Action
  @param  QuestionId
  @param  Type
  @param  Value
  @param  ActionRequest

  @retval EFI_SUCCESS   Function successful returned.
**/
EFI_STATUS
EFIAPI
SecurityCallbackRoutine (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  EFI_BROWSER_ACTION                     Action,
  IN  EFI_QUESTION_ID                        QuestionId,
  IN  UINT8                                  Type,
  IN  EFI_IFR_TYPE_VALUE                     *Value,
  OUT EFI_BROWSER_ACTION_REQUEST             *ActionRequest
  )
{
  EFI_STATUS                            Status;
  SYSTEM_ACCESS                          SystemAccess;
  EFI_GUID                               SysAccessGuid = SYSTEM_ACCESS_GUID;
  UINTN                                  BufferSize;
//add-klk-lyang-P000A-end//
  if (Action == EFI_BROWSER_ACTION_CHANGING) {
    Action = EFI_BROWSER_ACTION_CHANGED;
  }
  if (Action != EFI_BROWSER_ACTION_CHANGED) {
    return EFI_UNSUPPORTED;
  }
  *ActionRequest = EFI_BROWSER_ACTION_REQUEST_NONE;


  
  BufferSize = sizeof(SYSTEM_ACCESS);
  Status = gRT->GetVariable (L"SystemAccess", &SysAccessGuid, NULL, &BufferSize, &SystemAccess);
  if (EFI_ERROR (Status)) {
    return Status;
  }
  Status = HiiSetBrowserData (&SysAccessGuid, L"SystemAccess", BufferSize, (UINT8*)&SystemAccess, NULL);
  SetMem(gSetupDataManagment->SUCInfo->InputString, PASSWORD_MAX_SIZE, 0x00); //信创标准，密码使用后必须清除对应内存

  return EFI_SUCCESS;
}


/**
  Brief description of InstallSecurityCallbackRoutine.

  @param  DriverHandle
  @param  HiiHandle

  @retval EFI_SUCCESS   Function successful returned.
**/
EFI_STATUS
InstallSecurityCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  )
{
  EFI_STATUS                                Status;
  EFI_GUID                                  FormsetGuid = FORMSET_ID_GUID_SECURITY;

  mSecurityCallBackInfo = AllocatePool (sizeof (EFI_CALLBACK_INFO));
  if (mSecurityCallBackInfo == NULL) {
    return EFI_OUT_OF_RESOURCES;
  }

  mSecurityCallBackInfo->Signature                    = EFI_CALLBACK_INFO_SIGNATURE;
  mSecurityCallBackInfo->DriverCallback.ExtractConfig = GenericExtractConfig;
  mSecurityCallBackInfo->DriverCallback.RouteConfig   = GenericRouteConfig;
  mSecurityCallBackInfo->DriverCallback.Callback      = SecurityCallbackRoutine;
  mSecurityCallBackInfo->HiiHandle                    = HiiHandle;
  CopyGuid (&mSecurityCallBackInfo->FormsetGuid, &FormsetGuid);

   //
  // Install protocol interface
  //
  Status = gBS->InstallProtocolInterface (
                  &DriverHandle,
                  &gEfiHiiConfigAccessProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &mSecurityCallBackInfo->DriverCallback
                  );
  ASSERT_EFI_ERROR (Status);
 
  return Status;

}
