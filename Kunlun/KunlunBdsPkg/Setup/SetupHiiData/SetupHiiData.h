/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef _UEFI_SETUP_UTILITY_H_
#define _UEFI_SETUP_UTILITY_H_

#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DevicePathLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HiiLib.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/SetupLib.h>
#include <Library/PrintLib.h>
//add-klk-lyang-P000A-start//
#include <Library/SetupHiiDataManagerLib.h>
#include <Library/UefiLib.h>
//add-klk-lyang-P000A-end//
#include <Library/RetainDataLib.h>

#include <Protocol/HiiConfigAccess.h>
#include <Protocol/SetupHiiDataMgr.h>
#include <Protocol/HiiConfigRouting.h>
#include <Protocol/DevicePath.h>
#include <Protocol/HiiString.h>
#include <Protocol/SetupUtilityApplication.h>
#include <Protocol/HiiDatabase.h>
#include <Protocol/BiosPasswordService.h>
#include <Guid/MdeModuleHii.h>

#include <FlashRetainDefinitions.h>
#include <SetupConfig.h>

#define MAX_HII_HANDLES 10


#ifndef EFI_SIGNATURE_16
#define EFI_SIGNATURE_16(A, B)             ((A) | (B << 8))
#endif
#ifndef EFI_SIGNATURE_32
#define EFI_SIGNATURE_32(A, B, C, D)       (EFI_SIGNATURE_16 (A, B) | (EFI_SIGNATURE_16 (C, D) << 16))
#endif



extern UINT8 SecurityVfrBin[];
extern UINT8 BootVfrBin[];
extern unsigned char SetupHiiDataStrings[];
extern EFI_GUID  mFormSetGuid;
extern CHAR16    mVariableName[];


typedef
EFI_STATUS
(EFIAPI *USER_INSTALL_CALLBACK_ROUTINE) (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  );

typedef struct _NEW_PACKAGE_INFO {
  USER_INSTALL_CALLBACK_ROUTINE         CallbackRoutine;
  UINT8                                 *IfrPack;
  UINT8                                 *StringPack;
  UINT8                                 *ImagePack;
  EFI_GUID                              FormSetGuid;
} NEW_PACKAGE_INFO;

typedef struct {
  UINTN                                 Signature;
  EFI_HANDLE                            Handle;
  SETUP_HII_DATA_MANAGMENT_PROTOCOL     Interface;
  EFI_HII_DATABASE_PROTOCOL             *HiiDatabase;
  EFI_HII_STRING_PROTOCOL               *HiiString;
  EFI_HII_CONFIG_ROUTING_PROTOCOL       *HiiConfigRouting;
  EFI_FORM_BROWSER2_PROTOCOL            *Browser2;
  EFI_HII_ACCESS_EXTRACT_CONFIG         ExtractConfig;
  EFI_HII_ACCESS_ROUTE_CONFIG           RouteConfig;
  SYSTEM_SETUP_CONFIGURATION            Configuration;
  UINT8                                 *SCBuffer;
} SETUP_HII_PRIVATE_DATA;

#define EFI_SETUP_HII_PRIVATE_DATA_SIGNATURE SIGNATURE_32('S','H','P','V')
#define EFI_SETUP_HII_PRIVATE_DATA_FROM_THIS(a) CR(a, SETUP_HII_PRIVATE_DATA, Interface, EFI_SETUP_HII_PRIVATE_DATA_SIGNATURE)

EFI_STATUS
EFIAPI
InstallSecurityCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  );


EFI_STATUS
EFIAPI
InstallBootCallbackRoutine (
  IN EFI_HANDLE                             DriverHandle,
  IN EFI_HII_HANDLE                         HiiHandle
  );

EFI_STATUS
EFIAPI
GenericExtractConfig (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  CONST EFI_STRING                       Request,
  OUT EFI_STRING                             *Progress,
  OUT EFI_STRING                             *Results
  );

EFI_STATUS
EFIAPI
GenericRouteConfig (
  IN  CONST EFI_HII_CONFIG_ACCESS_PROTOCOL   *This,
  IN  CONST EFI_STRING                       Configuration,
  OUT EFI_STRING                             *Progress
  );

extern SETUP_HII_PRIVATE_DATA   *gHiiData;
#endif
