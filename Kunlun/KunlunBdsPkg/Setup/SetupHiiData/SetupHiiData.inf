## @file
# Brief description of the file's purpose.
#
# Copyright (c) 2006-2022, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
# Rights Reserved.
#
# You may not reproduce, distribute, publish, display, perform, modify, adapt,
# transmit, broadcast, present, recite, release, license or otherwise exploit
# any part of this publication in any form, by any means, without the prior
# written permission of Kunlun Technology (Beijing) Co., Ltd..
#
#
##

[Defines]
  INF_VERSION                    = 0x00010005
  BASE_NAME                      = SetupHiiData
  MODULE_UNI_FILE                = Setup.uni
  FILE_GUID                      = AB38D223-9B18-42FB-A38C-4A898725D65D
  MODULE_TYPE                    = DXE_DRIVER
  VERSION_STRING                 = 1.0

  ENTRY_POINT                    = SetupHiiDataInit

[Sources]
  Boot/BootVfr.vfr
  Setup.uni
  Security/SecurityVfr.vfr

  Security/Security.c
  Boot/Boot.c
  SetupHiiData.c

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  KunlunBdsPkg/KunlunBdsPkg.dec
  KunlunModulePkg/KunlunModulePkg.dec
  #[gliu-0005]
  PhytiumPkg/PhytiumGeneral/PhytiumGeneral.dec
  #[gliu-0005]

[LibraryClasses]
  BaseLib
  UefiDriverEntryPoint
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  MemoryAllocationLib
  UefiLib
  BaseMemoryLib
  HiiLib
  DxeServicesTableLib
  SetupLib
  PrintLib
  DebugLib
  SetupHiiDataManagerLib
  #DeviceInfoUiLib
  KlSetupVarRWLib
  KlDevicePathOrderLib
  #MonitorUiLib
  SaveDefaultHookLib
  RetainDataLib

[Guids]
  gEfiIfrFrontPageGuid
  gSystemConfigurationGuid
  gEfiIfrTianoGuid
  gEfiEndOfDxeEventGroupGuid
  #[gliu-0005]
  gOemDataSetupGuid
  #[gliu-0005]
#[-start-klk-lyang-010-20221202-add]#
  gKunlunHideHotkeyGuid
#[-end-klk-lyang-010-20221202-add]#
[Protocols]
  gEfiSetupUtilityApplicationProtocolGuid
  gEfiHiiConfigAccessProtocolGuid
  gSetupHiiDataManagmentProtocolGuid
  gEfiHiiDatabaseProtocolGuid
  gEfiHiiConfigRoutingProtocolGuid
  gEfiDevicePathToTextProtocolGuid
  gEfiSimpleFileSystemProtocolGuid
  gEfiLoadFileProtocolGuid
  gEfiFirmwareVolume2ProtocolGuid
  gEfiLoadedImageProtocolGuid
  gEfiBiosPasswordServiceProtocolGuid

[Pcd]

[FixedPcd]
  gEfiKunlunBdsPkgTokenSpaceGuid.PcdSetupPopUpHistory
  gEfiKunlunBdsPkgTokenSpaceGuid.PcdSetupMouseVirkKeyDefault
  gEfiKunlunBdsPkgTokenSpaceGuid.PcdSetupBootOptionXkeyEnable
#[-start-klk-lyang-010-20221202-add]#
  gEfiKunlunBdsPkgTokenSpaceGuid.PcdHideHotKeyPressEnable
  gEfiKunlunBdsPkgTokenSpaceGuid.PcdRefreshSetupKeyPressEnable
#[-end-klk-lyang-010-20221202-add]#
#[-start-klk-lyang-014-20221202-add]#
  gEfiKunlunBdsPkgTokenSpaceGuid.PcdGrayoutAllUserOpcodeEnable
#[-end-klk-lyang-014-20221202-add]#

[FeaturePcd]

[Depex]
  gEfiSimpleTextOutProtocolGuid AND gEfiHiiDatabaseProtocolGuid AND gEfiVariableArchProtocolGuid AND gEfiVariableWriteArchProtocolGuid AND gEfiBiosPasswordServiceProtocolGuid
