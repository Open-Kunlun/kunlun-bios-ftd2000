/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef _BOOT_CALLBACK_H_
#define _BOOT_CALLBACK_H_
#include <Library/DebugLib.h>
#include <SetupHiiData.h>
#include "SetupConfig.h"

#include <Guid/FileSystemVolumeLabelInfo.h>

#include <Protocol/LoadFile.h>
#include <Protocol/SimpleFileSystem.h>
#include <Protocol/DevicePathToText.h>

#include <Library/DevicePathLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiLib.h>

//#define MAX_MENU_NUMBER 100

#define BM_LOAD_CONTEXT_SELECT        0x0
#define BM_CONSOLE_CONTEXT_SELECT     0x1
#define BM_FILE_CONTEXT_SELECT        0x2
#define BM_HANDLE_CONTEXT_SELECT      0x3
#define BM_TERMINAL_CONTEXT_SELECT    0x5

//
// Namespace of callback keys used in display and file system navigation
//
//#define CONFIG_OPTION_OFFSET    0x2200


#define BM_MENU_OPTION_SIGNATURE      SIGNATURE_32 ('m', 'e', 'n', 'u')
#define BM_MENU_ENTRY_SIGNATURE       SIGNATURE_32 ('e', 'n', 't', 'r')

#define VAR_OFFSET(Field)              ((UINT16) ((UINTN) &(((SYSTEM_SETUP_CONFIGURATION *) 0)->Field)))

//
// Question Id of Zero is invalid, so add an offset to it
//
//#define QUESTION_ID(Field)             (VAR_OFFSET (Field) + CONFIG_OPTION_OFFSET)

#define BOOT_OPTION_ORDER_VAR_OFFSET    VAR_OFFSET (BootOptionOrder)



typedef UINT16                 STRING_REF;

typedef struct {
  UINTN           Signature;
  LIST_ENTRY      Link;
  UINTN           OptionNumber;
  UINT16          *DisplayString;
  UINT16          *HelpString;
  EFI_STRING_ID   DisplayStringToken;
  EFI_STRING_ID   HelpStringToken;
  UINTN           ContextSelection;
  VOID            *VariableContext;
} BM_MENU_ENTRY;

typedef struct {
  BOOLEAN                   IsBootNext;
  BOOLEAN                   Deleted;

  BOOLEAN                   IsLegacy;

  UINT32                    Attributes;
  UINT16                    FilePathListLength;
  UINT16                    *Description;
  EFI_DEVICE_PATH_PROTOCOL  *FilePathList;
  UINT8                     *OptionalData;
} BM_LOAD_CONTEXT;

typedef struct {
  EFI_HANDLE                        Handle;
  EFI_DEVICE_PATH_PROTOCOL          *DevicePath;
  EFI_FILE_HANDLE                   FHandle;
  UINT16                            *FileName;
  EFI_FILE_SYSTEM_VOLUME_LABEL      *Info;

  BOOLEAN                           IsRoot;
  BOOLEAN                           IsDir;
  BOOLEAN                           IsRemovableMedia;
  BOOLEAN                           IsLoadFile;
  BOOLEAN                           IsBootLegacy;
} BM_FILE_CONTEXT;

typedef struct {
  UINTN           Signature;
  LIST_ENTRY      Head;
  UINTN           MenuNumber;
} BM_MENU_OPTION;

typedef struct _STRING_PTR {
  UINT16                                    EfiBootDevFlag;
  UINT16                                    BootOrderIndex;
  CHAR16                                    *pString;
  EFI_DEVICE_PATH_PROTOCOL                  *DevicePath;
} STRING_PTR;

typedef struct {
  UINT16               BootOptionNum;
  BOOLEAN              IsEfiBootDev;
  UINT16               DevType;

  //
  // Settings in BOOT_CONFIGURATION
  //
  BOOLEAN              IsActive;
  STRING_PTR           BootDeviceString;
} BOOT_DEV_INFO;

#endif
