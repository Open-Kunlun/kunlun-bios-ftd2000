## @file
# Kunlun BDS package project build description file.
#
#;*************************************************************************************************
#;*  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
#;*  Rights Reserved.
#;*
#;*  You may not reproduce, distribute, publish, display, perform, modify, adapt,
#;*  transmit, broadcast, present, recite, release, license or otherwise exploit
#;*  any part of this publication in any form, by any means, without the prior
#;*  written permission of Kunlun Technology (Beijing) Co., Ltd..
#;*
#;*************************************************************************************************

################################################################################
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]
  DEFINE UI_BINARY = TRUE
  #DEFINE UI_BINARY = FALSE

################################################################################
#
# Library Class section - list of all Library Classes needed by this Platform.
#
################################################################################
[LibraryClasses]
  PlatformBootManagerLib|KunlunBdsPkg/Library/PlatformBmLib/PlatformBmLib.inf
  SetupLib|KunlunBdsPkg/Library/SetupLib/SetupLib.inf
  DynamicFormSupportLib|KunlunBdsPkg/Library/DynamicFormSupportLib/DynamicFormSupportLib.inf
  PeiKlSetupVarRWLib|KunlunBdsPkg/Library/KlSetupVarRWLib/PeiKlSetupVarRWLib.inf
  KlSetupVarRWLib|KunlunBdsPkg/Library/KlSetupVarRWLib/KlSetupVarRWLib.inf
  BootLogoLib|KunlunBdsPkg/Library/BootLogoLib/BootLogoLib.inf
  UefiBootManagerLib|KunlunBdsPkg/Library/UefiBootManagerLib/UefiBootManagerLib.inf
  SetupHiiDataManagerLib|KunlunBdsPkg/Library/SetupHiiDataManagerLib/SetupHiiDataManagerLib.inf
  KlDevicePathOrderLib| KunlunBdsPkg/Library/KlDevicePathOrderLib/KlDevicePathOrderLib.inf
  DeviceLinkDataLib| KunlunBdsPkg/Library/DeviceLinkDataLib/DeviceLinkDataLib.inf
  HiiLib|KunlunBdsPkg/Library/UefiHiiLib/UefiHiiLib.inf
  BuzzerLib| KunlunBdsPkg/Library/BuzzerLib/BuzzerLib.inf
  SaveDefaultHookLib|KunlunBdsPkg/Library/SetupSaveDefaultHookNullLib/SetupSaveDefaultHookNull.inf
  KlCpuAndDdrSettingLib|KunlunBdsPkg/Library/KlCpuAndDdrSettingLib/KlCpuAndDdrSettingLib.inf

[LibraryClasses.common]

[LibraryClasses.common.PEIM]


[LibraryClasses.common.DXE_DRIVER]


[LibraryClasses.common.DXE_SMM_DRIVER]


[LibraryClasses.common.COMBINED_SMM_DXE]

[LibraryClasses.common.DXE_RUNTIME_DRIVER]

[LibraryClasses.common.UEFI_DRIVER]

[LibraryClasses.common.UEFI_APPLICATION]

################################################################################
#
# Pcd Section - list of all EDK II PCD Entries defined by this Platform
#
################################################################################
[PcdsFeatureFlag]
  gEfiMdeModulePkgTokenSpaceGuid.PcdBrowerGrayOutReadOnlyMenu|TRUE

[PcdsFixedAtBuild]


[Components.$(PEI_ARCH)]


[Components.common]
    KunlunBdsPkg/BdsDxe/BdsDxe.inf{
      <LibraryClasses>
  !if $(CAPSULE_ENABLE)
        FmpAuthenticationLib|SecurityPkg/Library/FmpAuthenticationLibPkcs7/FmpAuthenticationLibPkcs7.inf
  !else
        FmpAuthenticationLib|MdeModulePkg/Library/FmpAuthenticationLibNull/FmpAuthenticationLibNull.inf
!endif
  }
  KunlunBdsPkg/Setup/HiiDatabaseDxe/HiiDatabaseDxe.inf
  KunlunBdsPkg/Setup/SetupHiiData/SetupHiiData.inf
  KunlunBdsPkg/Drivers/BiosPasswordDxe/BiosPasswordDxe.inf

  KunlunBdsPkg/Drivers/GraphicsConsoleDxe/GraphicsConsoleDxe.inf
  KunlunBdsPkg/Drivers/KunlunFontDxe/KunlunFontDxe.inf

  !if $(UI_BINARY)
    KunlunBdsPkg/Setup/DisplayEngineDxe/DisplayEngineDxe.inf
    KunlunBdsPkg/Setup/SetupBrowserDxe/SetupBrowserDxe.inf
  !else
    KunlunBdsPkg/Setup/DisplayEngineDxeSrc/DisplayEngineDxe.inf{
     <LibraryClasses>
       CustomizedDisplayLib|KunlunBdsPkg/Library/CustomizedDisplayLibSrc/CustomizedDisplayLib.inf
     }
    KunlunBdsPkg/Setup/SetupBrowserDxeSrc/SetupBrowserDxe.inf
  !endif

  KunlunBdsPkg/Application/SetupUtilityApp/SetupUtilityApp.inf
  KunlunBdsPkg/Application/BootManagerMenuApp/BootManagerMenuApp.inf


