## @file
#  Brief description of BiosPassword Module.
#
#  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
#  Rights Reserved.
#
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution.  The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#
##

[Defines]
INF_VERSION          = 0x00010005
BASE_NAME            = BiosPassword
FILE_GUID            = ba269b81-721f-4fcc-a117-5251fde053a8
MODULE_TYPE          = DXE_DRIVER
VERSION_STRING       = 1.0
ENTRY_POINT          = InitializeBiosPasswordDriver

[Sources]
  #BiosPasswordDxe.c
  #BiosPasswordDxe.h
  #BiosPasswordLib.c
  #BiosPasswordLib.h
  #BiosPasswordStrings.uni

[Binaries.AARCH64]
  PE32|BiosPassword.efi|*
  DXE_DEPEX|BiosPassword.depex

[Packages]
  MdePkg/MdePkg.dec
  MdeModulePkg/MdeModulePkg.dec
  KunlunBdsPkg/KunlunBdsPkg.dec
  KunlunModulePkg/KunlunModulePkg.dec
  CryptoPkg/CryptoPkg.dec
  KunlunCommonPkg/KunlunCommonPkg.dec

[LibraryClasses]
  BaseLib
  DebugLib
  UefiBootServicesTableLib
  UefiRuntimeServicesTableLib
  UefiDriverEntryPoint
  MemoryAllocationLib
  HiiLib
  PcdLib
  OpensslLib
  RetainDataLib
  KlPlatformConfigLib
  PrintLib
  DisplayPictureLib
  UefiBootManagerLib

[Guids]

[Protocols]
  gEfiBiosPasswordServiceProtocolGuid
  gEfiHiiPopupProtocolGuid
  gEfiGetSetRtcProtocolGuid
  gEfiSimpleTextInProtocolGuid
  gEfiUsbIoProtocolGuid
  gEfiWatchdogTimerArchProtocolGuid
  gEfiBiosEventLogProtocolGuid

[Pcd]
  gPhytiumPlatformTokenSpaceGuid.PcdForceInSetup
  gEfiKunlunBdsPkgTokenSpaceGuid.PcdEncryptType
  gEfiKunlunBdsPkgTokenSpaceGuid.PcdEncryptIterationCount

[Depex]
  gEfiVariableWriteArchProtocolGuid
