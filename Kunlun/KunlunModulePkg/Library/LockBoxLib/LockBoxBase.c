/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#include <Uefi.h>
#include "PiPei.h"
#include <LockBoxLib.h>

#include <Library/DebugLib.h>
#include <Library/PeiServicesLib.h>

#include <Ppi/ReadOnlyVariable2.h>

#include <Library/PcdLib.h>

/**
  Allocates a buffer of type EfiACPIMemoryNVS.

  Allocates the number bytes specified by AllocationSize of type
  EfiACPIMemoryNVS and returns a pointer to the allocated buffer.
  If AllocationSize is 0, then a valid buffer of 0 size is
  returned.  If there is not enough memory remaining to satisfy
  the request, then NULL is returned.

  @param  AllocationSize        The number of bytes to allocate.

  @return A pointer to the allocated buffer or NULL if allocation fails.

**/
VOID *
EFIAPI
AllocateAcpiNvsPool (
  IN UINTN  AllocationSize
  )
{
  ASSERT_EFI_ERROR (RETURN_UNSUPPORTED);
  return NULL;
}

/**
  LockBoxBase entry.

  @retval EFI_SUCCESS           The operation succeeds.

**/
RETURN_STATUS
EFIAPI
LockBoxPeiLibInitialize (
  VOID
  )
{
  EFI_STATUS                      Status;
  UINTN                           VarSize;
  EFI_PEI_READ_ONLY_VARIABLE2_PPI *ReadOnlyVariable;
  UINT64                          LockBoxVariable;
  UINT32                          VarAttrib;
  EFI_BOOT_MODE                   BootMode;

  Status = PeiServicesGetBootMode (&BootMode);
  if ((EFI_ERROR (Status)) || (BootMode != BOOT_ON_S3_RESUME)) {
    return EFI_SUCCESS;
  }

  Status = PeiServicesLocatePpi (
             &gEfiPeiReadOnlyVariable2PpiGuid,
             0,
             NULL,
             (VOID **)&ReadOnlyVariable
             );
  if (!EFI_ERROR (Status)) {
    VarSize = sizeof (UINT64);
    LockBoxVariable = 0;
    Status = ReadOnlyVariable->GetVariable (
                    ReadOnlyVariable,
                    LOCK_BOX_GLOBAL_VARIABLE,
                    &gEfiLockBoxGlobalGuid,
                    &VarAttrib,
                    &VarSize,
                    &LockBoxVariable
                    );
    ASSERT_EFI_ERROR (Status);
    //[gliu-0035]add-start
    Status = PcdSetEx64S (&gKlModulePkgTokenSpaceGuid,PcdLockBoxStorageBase, LockBoxVariable);
    //[gliu-0035]add-end
    DEBUG ((DEBUG_INFO, "Pei GetVariable %r: LockBoxVariable:%x PcdGet64 (PcdLockBoxStorageBase):%x\n", Status, LockBoxVariable, PcdGetEx64 (&gKlModulePkgTokenSpaceGuid,PcdLockBoxStorageBase)));
    if (PcdGetEx64 (&gKlModulePkgTokenSpaceGuid,PcdLockBoxStorageBase) == 0) {
      return EFI_SUCCESS;
    }
  }

  return LockBoxLibInitialize();
}

