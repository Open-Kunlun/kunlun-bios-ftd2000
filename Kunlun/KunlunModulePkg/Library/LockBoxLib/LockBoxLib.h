/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef LOCK_BOX_LIB_IMPL_H_
#define LOCK_BOX_LIB_IMPL_H_

#define LOCK_BOX_GLOBAL_VARIABLE  L"LockBoxGlobalVar"

#pragma pack(1)

typedef struct {
  UINT32               Signature;
  UINT32               SubPageBuffer;
  UINT32               SubPageRemaining;
} LOCK_BOX_GLOBAL;

#define LOCK_BOX_GLOBAL_SIGNATURE SIGNATURE_32('L', 'B', 'G', 'S')

extern LOCK_BOX_GLOBAL *mLockBoxGlobal;

#pragma pack()

/**
  Allocates a buffer of type EfiACPIMemoryNVS.

  Allocates the number bytes specified by AllocationSize of type
  EfiACPIMemoryNVS and returns a pointer to the allocated buffer.
  If AllocationSize is 0, then a valid buffer of 0 size is
  returned.  If there is not enough memory remaining to satisfy
  the request, then NULL is returned.

  @param  AllocationSize        The number of bytes to allocate.

  @return A pointer to the allocated buffer or NULL if allocation fails.

**/
VOID *
EFIAPI
AllocateAcpiNvsPool (
  IN UINTN  AllocationSize
  );


RETURN_STATUS
EFIAPI
LockBoxLibInitialize (
  VOID
  );


#endif
