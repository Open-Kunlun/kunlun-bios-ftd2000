/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:GenericWatchdogLib.c


Abstract:


Revision History:

**/

#include <Library/ArmWatchdogLib.h>
#include <Library/GenericWatchdogLib.h>
//#include <SetupConfig.h>
//#include <Library/KlSetupVarRWLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/IoLib.h>
#include <Library/DebugLib.h>

#if EFI_SPECIFICATION_VERSION>0x20000
#include <Protocol/HiiDatabase.h>
#include <Protocol/HiiString.h>
#define LANGUAGE_CODE_ENGLISH    "en-US"
#else
#include <Protocol/Hii.h>
#endif


/**
  Enable the watch dog.

  @param[out]  BootTimeOut      a pointer to UINT16.

  @retval EFI_SUCCESS           The operation succeeds.

**/
EFI_STATUS
StartWatchdog(
  UINT8  Enable,
  UINT8  WatchdogTimeOut,
  UINT16 *BootTimeOut
)
{
  //EFI_STATUS              Status;
  //SYSTEM_SETUP_CONFIGURATION        *SetupVar;
  //WATCHDOG_CONFIG                   *pWatchdogData;
  UINT16                             TimeOut;

  //SetupVar = NULL;
  TimeOut = 0;
  //Status = KlSetupVarRead(&SetupVar);
  //if(EFI_ERROR(Status)){
  //    return Status;
  //}
  //pWatchdogData = &SetupVar->watchdog;
  if(Enable==1){
    DEBUG((EFI_D_INFO, "enable the Watchdog Timer. \n"));
    //[gliu-0023]add-begin
    TimeOut = TIME_OUT_30S * (WatchdogTimeOut+1);
    //[gliu-0023]add-end
    DEBUG((EFI_D_INFO, "BIOS: In StartWatchdog(), line %d. TimeOut=%d\n", __LINE__,TimeOut));
    *BootTimeOut = TimeOut;
    //
    //WatchdogTimerDxe
    //
    //enable the Watchdog Timer.
    gBS->SetWatchdogTimer (TimeOut, 0x0000, 0x00, NULL);

  }
  //if(SetupVar != NULL) {
  //  FreePool(SetupVar);
  //}
  return EFI_SUCCESS;

}

/**
  Feed the watch dog.

**/
VOID
FeedWatchdog (
  IN VOID
)
{
  WatchdogWriteRefreshRegister(0);
}

/**
  Disable the watch dog.

**/
VOID
StopWatchdog(
  IN VOID
)
{

  gBS->SetWatchdogTimer (0x0000, 0x0000, 0x0000, NULL);
}

//[gliu-0064]add-begin
/**
  Get the watch dog policy:0-reset,1-poweroff.

**/
#if 0
UINT8
GetWatchdogPolicy(
  VOID
)
{
  EFI_STATUS                        Status;
  SYSTEM_SETUP_CONFIGURATION        *SetupVar;
  WATCHDOG_CONFIG                   *pWatchdogData;
  UINT8                             Policy;

  SetupVar = NULL;
  Status = KlSetupVarRead(&SetupVar);
  if(EFI_ERROR(Status)){
      return 0;
  }
  pWatchdogData = &SetupVar->watchdog;
  Policy =  pWatchdogData->Policy;
  if(SetupVar != NULL) {
    FreePool(SetupVar);
  }
  return Policy;
}
#endif
//[gliu-0064]add-end

