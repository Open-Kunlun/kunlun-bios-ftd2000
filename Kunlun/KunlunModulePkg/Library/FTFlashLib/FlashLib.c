/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/


#include <Uefi.h>
#include <Base.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/FlashLib.h>
#include <Library/PhytiumSpiNorFlashLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>

STATIC UINTN                     mNorFlashControlBase;

STATIC
UINT32
ReadId(
  VOID
  )
{
  UINT32 Reg;

  mNorFlashControlBase = FixedPcdGet64 (PcdSpiControllerBase);
  MmioWrite32(mNorFlashControlBase + REG_CMD_PORT, 0x9F002040);
  Reg = MmioRead32(mNorFlashControlBase + REG_LD_PORT);
  return Reg;
}

EFI_STATUS
EFIAPI
FlashGetInfo (
  FLASH_INFO  *FlashInfo
)
{
  UINT8     i;
  UINT32    JedecId;
  UINT32    Capacity;

  Capacity = 1;
  JedecId = ReadId();

  for (i = 0; i < (UINT8)((JedecId >> 16) & 0xFF); i++) {
    Capacity = Capacity * 2;
  }

  FlashInfo->Lock.Tpl = 0;
  FlashInfo->Lock.OwnerTpl = 0;
  FlashInfo->Lock.Lock = 0;

  FlashInfo->Layout.Size = Capacity;
  FlashInfo->Layout.NumOfSector = 4 * 1024;
#ifdef SPIMODE
  FlashInfo->Layout.NumOfBlock = 64 * 1024;
#else
  FlashInfo->Layout.NumOfBlock = 32 * 1024;
#endif

  FlashInfo->Id.VendorId = 0;
  FlashInfo->Id.DeviceId = 0;

  FlashInfo->ErasePolarity = 0;

  return 0;
}

EFI_STATUS
EFIAPI
FlashErase (
  IN  UINTN  Lba
  )
{
   EFI_STATUS Status;

   Status = NorFlashPlatformEraseSingleBlock (Lba);
   return Status;
}

EFI_STATUS
EFIAPI
FlashWrite (
  UINT8 *SrcAddr,
  UINT8 *DestAddr,
  UINTN Counter
  )
{
 EFI_STATUS Status;
 Status = NorFlashPlatformWrite ((UINTN)DestAddr,(VOID*)SrcAddr,(UINT32)(Counter*4));
 return Status;
}

UINT64
EFIAPI
FlashReadId (
 VOID
  )
{
   return 0;
}

