/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#include <Library/DebugLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/ArmWatchdogLib.h>

/**
  Write the offset register of the watch dog.

  @param[in]  Value      The value to be write.

**/
VOID
WatchdogWriteOffsetRegister (
  UINT32  Value
  )
{
  MmioWrite32 (GENERIC_WDOG_OFFSET_REG, Value);
}

/**
  Write the compare register of the watch dog.

  @param[in]  Value      The value to be write.

**/
VOID
WatchdogWriteCompareRegister (
  UINT64  Value
  )
{
  //DEBUG((EFI_D_INFO, "timeout value:%llu. \n",Value));
  MmioWrite32 (GENERIC_WDOG_COMPARE_VALUE_REG_HIGH, (Value >> 32) & MAX_UINT32);
  MmioWrite32 (GENERIC_WDOG_COMPARE_VALUE_REG_LOW, Value & MAX_UINT32);
}

/**
  Enable the watch dog.

**/
VOID
WatchdogEnable (
  VOID
  )
{
  MmioWrite32 (GENERIC_WDOG_CONTROL_STATUS_REG, GENERIC_WDOG_ENABLED);
}

/**
  Write the refresh register of the watch dog.

  @param[in]  Value      The value to be write.

**/
VOID
WatchdogWriteRefreshRegister (
 UINT32  Value
)
{
  MmioWrite32 (GENERIC_WDOG_REFRESH_REG, Value);
}

/**
  Disable the watch dog.

**/
VOID
WatchdogDisable (
  VOID
  )
{
  MmioWrite32 (GENERIC_WDOG_CONTROL_STATUS_REG, GENERIC_WDOG_DISABLED);
}

