/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/
#ifndef ZF_SETUP_CONFIG_H_
#define ZF_SETUP_CONFIG_H_


// {3e7ae5d2-0420-462d-8d85-51282c7397d8}
#define FORMSET_ID_GUID_ZFSECURITY \
  { \
    0x3e7ae5d2, 0x0420, 0x462d, {0x8d, 0x85, 0x51, 0x28, 0x2c, 0x73, 0x97, 0xd8} \
  }

// {e6f81cf5-14a2-43b9-ac7f-557d7535c8e8}
#define FORMSET_ID_GUID_MEASUREMENT \
  { \
    0xe6f81cf5, 0x14a2, 0x43b9, {0xac, 0x7f, 0x55, 0x7d, 0x75, 0x35, 0xc8, 0xe8} \
  }

#define USER_ACCESS_GUID \
  { \
    0x1aa1c86f, 0x3bca, 0x48b0, {0x9e, 0xab, 0x88, 0x47, 0x7c, 0x76, 0xb3, 0x69} \
  }

#define FORMSET_ID_GUID_EVENTLOG \
  { \
    0xB500C982, 0xA7D1, 0xB32D, {0x10, 0xBB, 0x29, 0x2B, 0xA4, 0xE5, 0x8D, 0xA3} \
  }

#define USER_ACCESS_NAME                L"SystemAccess"
#define COMMON_USER_NAME                L"CommonUserName"
#define CURRENT_USER_NAME               L"UserName"

#define MAX_COMMON_USER_NUM             7
//setup form ID define
#define USER_INFO_MNG_FORM_ID                   0x3000
#define ADM_INFO_MNG_FORM_ID                    0x3001
#define COM_USER_MANAGE_FORM_ID                 0x3002
#define MEASUREMENT_MNG_FORM_ID                 0x3003
#define HW_MEASUREMENT_MNG_FORM_ID              0x3004
#define SW_MEASUREMENT_MNG_FORM_ID              0x3005
#define SATA_MEASUREMENT_MNG_FORM_ID            0x3006
#define PCI_MEASUREMENT_MNG_FORM_ID             0x3007
#define USB_MEASUREMENT_MNG_FORM_ID             0x3008
#define BIOS_MEASUREMENT_MNG_FORM_ID            0x3009
#define OS_BOOT_FILES_MEASUREMENT_MNG_FORM_ID   0x300A
#define OS_KERNEL_FILES_MEASUREMENT_MNG_FORM_ID 0x300B
#define OS_LIST_FILES_MEASUREMENT_MNG_FORM_ID   0x300C
#define OS_USER_FILES_MEASUREMENT_MNG_FORM_ID   0x300D
#define EVENT_LOG_FORM_ID                       0x300E
//0x4000-0x40FF reserved for Common user manage form
#define COMMON_USER_BIO_MNG_FORM_ID             0x4000
#define COMMON_USER_BIO_MNG_FORM_ID_MAX         0x40FF

//Setup callback question ID define
#define INIT_ZF_STRING_ID              0x1000
#define CHANGE_PASSWORD_ID             0x1001
#define RESET_PASSWORD_ID              0x1002
#define ADD_COMMON_USER_ID             0x1003
#define REMOVE_COMMON_USER_ID          0x1004
#define UPDATE_HW_EXPECTED             0x1005
#define UPDATE_SW_EXPECTED             0x1006
#define UPDATE_SATA_EXPECTED           0x1007
#define UPDATE_PCI_EXPECTED            0x1008
#define CLEAR_EVENT_LOG_ID             0x1009
//0x1010-0x110F reserved for Common user manage form callback
#define COMMON_USER_MODIFY             0x1010
#define COMMON_USER_MODIFY_MAX         0x110F
//0x1110-0x120F reserved for common user finger modify callback question id
#define COMMON_USER_FINGER_MODIFY      0x1110
#define COMMON_USER_FINGER_MODIFY_MAX  0x120F
//0x120F-0x130F reserved for common user finger remove callback question id
#define COMMON_USER_FINGER_REMOVE      0x1210
#define COMMON_USER_FINGER_REMOVE_MAX  0x130F
#define MODIFY_CURRENT_USER_FP         0x1310
#define REMOVE_CURRENT_USER_FP         0x1311
#define UPDATE_USB_EXPECTED            0x1312
#define UPDATE_BIOS_EXPECTED           0x1313
#define UPDATE_OS_BOOT_FILES_EXPECTED  0x1314
#define UPDATE_OS_KERNEL_FILES_EXPECTED 0x1315
#define UPDATE_OS_LIST_FILES_EXPECTED  0x1316
#define UPDATE_OS_USER_FILES_EXPECTED  0x1317
//Dynamic update vfr lable define
#define LABEL_DEL_COMMON_USER          0x1000
#define LABEL_SATA_HASH                0x1001
#define LABEL_PCI_HASH                 0x1002
#define LABEL_COMMON_USER_BIO_MNG      0x1003
#define LABEL_CURRENT_USER_BIO_MNG     0x1004
#define LABEL_EVENT_LOG                0x1005
#define LABEL_USB_HASH                 0x1006
#define LABEL_BIOS_HASH                0x1007
#define LABEL_OS_BOOT_FILES_HASH       0x1008
#define LABEL_OS_KERNEL_FILES_HASH     0x1009
#define LABEL_OS_LIST_FILES_HASH       0x100A
#define LABEL_OS_USER_FILES_HASH       0x100B

#endif
