/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef BIOS_ID_LIB_H_
#define BIOS_ID_LIB_H_

//
// BIOS ID string format:
//
// $(BOARD_ID)$(BOARD_REV).$(OEM_ID).$(VERSION_MAJOR).$(BUILD_TYPE)$(VERSION_MINOR).YYYYMMDDHHMMSS
//
// Example: "TRFTCRB1.86C.0008.D03.20180131125529"
//
#pragma pack(1)

typedef struct {
  CHAR8      BoardId[10];              // "TRFTCRB"
  CHAR8      BoardRev;                 // "1"
  CHAR8      Dot1;                     // "."
  CHAR8      BiosOemId[4];             // "86C"
  CHAR8      Dot2;                     // "."
  CHAR8      VersionMajor[4];          // "0008"
  CHAR8      Dot3;                     // "."
  CHAR8      BuildType;                // "D"
  CHAR8      VersionMinor[2];          // "03"
  CHAR8      Dot4;                     // "."
  CHAR8      TimeStamp[14];            // "YYYYMMDDHHMMSS"
  CHAR8      NullTerminator;           // 0x0000
} BIOS_ID_STRING;


typedef struct {
  BOOLEAN    BootGrub;
  CHAR8      InitrdName[40];           // "initrd.img"
  CHAR8      KernelName[40];           // "vmlinuz"
  CHAR8      KernelCfg[200];           // "console=tty root=..."
} OS_BOOT_CONFIG;


//
// A signature precedes the BIOS ID string in the FV to enable search by external tools.
//
typedef struct {
  UINT8           Signature[7];             // "$KLBIOS"
  UINT8           StructVersion;            // Version of the BIOS_ID_STRING Struct
  UINT16          Size;                     // Size of this structure
  CHAR8           BiosTag[32];              // BIOS Tag
  CHAR16          BiosGuid[38];
  CHAR8           BiosReleaseData[20];      //xx/xx/xxxx xx/xx/xx
  CHAR8           ChipName[10];             // FT2000/4 or D2000
  UINT8           Reserved[200];
} BIOS_ID_IMAGE;

#pragma pack()

#define BOOT_FROM_USB    1
#define BOOT_FROM_CD     2
#define BOOT_FROM_HD     3


#endif

