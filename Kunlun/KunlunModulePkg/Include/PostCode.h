/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:


Abstract:


Revision History:

**/

#ifndef _POSTCODE_H_
#define _POSTCODE_H_

#include <Library/PostCodeLib.h>

#define POST_CODE_EX(CodeType,Value)

#define PROGRESS_CODE(Code)\
        REPORT_STATUS_CODE (EFI_PROGRESS_CODE, Code)

#define ERROR_CODE(Code, Severity)\
        REPORT_STATUS_CODE (EFI_ERROR_CODE | Severity, Code)
//kunlun defined
//pei
#define EFI_SW_PEI_SWITCH_STACK               (EFI_OEM_SPECIFIC | 0x00000001)
#define EFI_SW_PEI_MEMORY_CALLBACK            (EFI_OEM_SPECIFIC | 0x00000002)
#define EFI_PEI_SIO_INIT                      (EFI_OEM_SPECIFIC | 0x00000003)
#define EFI_PEI_BMC_INIT                      (EFI_OEM_SPECIFIC | 0x00000004)

#define EFI_PEI_PCIE_TRAINING                 (EFI_OEM_SPECIFIC | 0x00000005)
#define EFI_PEI_TPM_INIT                      (EFI_OEM_SPECIFIC | 0x00000006)
#define EFI_PEI_SMBUS_INIT                    (EFI_OEM_SPECIFIC | 0x00000007)
#define EFI_PEI_IGD_EARLY_INITIAL             (EFI_OEM_SPECIFIC | 0x00000008)
#define EFI_PEI_WATCHDOG_INIT                 (EFI_OEM_SPECIFIC | 0x00000009)

#define EFI_SW_PEI_FINDING_DXE_CORE           (EFI_OEM_SPECIFIC | 0x0000000A)
#define EFI_SW_PEI_GO_TO_DXE_CORE             (EFI_OEM_SPECIFIC | 0x0000000B)

#define EFI_PEI_RECOVERY_LOAD_FILE_DONE       (EFI_OEM_SPECIFIC | 0x0000000C)
#define EFI_PEI_RECOVERY_START_FLASH          (EFI_OEM_SPECIFIC | 0x0000000D)
//dxe
#define EFI_S3_RESTORE_MEMORY_CONTROLLER      (EFI_OEM_SPECIFIC | 0x0000000E)
#define EFI_S3_INSTALL_S3_MEMORY              (EFI_OEM_SPECIFIC | 0x0000000F)
#define EFI_S3_SWITCH_STACK                   (EFI_OEM_SPECIFIC | 0x00000010)
#define EFI_S3_MEMORY_CALLBACK                (EFI_OEM_SPECIFIC | 0x00000011)
#define EFI_S3_ENTER_S3_RESUME_PEIM           (EFI_OEM_SPECIFIC | 0x00000012)
#define EFI_S3_BEFORE_ACPI_BOOT_SCRIPT        (EFI_OEM_SPECIFIC | 0x00000013)
#define EFI_S3_BEFORE_RUNTIME_BOOT_SCRIPT     (EFI_OEM_SPECIFIC | 0x00000014)
#define EFI_S3_BEFORE_RELOCATE_SMM_BASE       (EFI_OEM_SPECIFIC | 0x00000015)
#define EFI_S3_BEFORE_MP_INIT                 (EFI_OEM_SPECIFIC | 0x00000016)
#define EFI_S3_BEFORE_RESTORE_ACPI_CALLBACK   (EFI_OEM_SPECIFIC | 0x00000017)
#define EFI_S3_AFTER_RESTORE_ACPI_CALLBACK    (EFI_OEM_SPECIFIC | 0x00000018)
#define EFI_S3_GO_TO_FACS_WAKING_VECTOR       (EFI_OEM_SPECIFIC | 0x00000019)
#define EFI_DXE_TCGDXE                        (EFI_OEM_SPECIFIC | 0x0000001A)
#define EFI_DXE_SB_SPI_INIT                   (EFI_OEM_SPECIFIC | 0x0000001B)
#define EFI_DXE_SIO_INIT                      (EFI_OEM_SPECIFIC | 0x0000001C)
#define EFI_DXE_BMC_INIT                      (EFI_OEM_SPECIFIC | 0x0000001D)
#define EFI_DXE_FTW_INIT                      (EFI_OEM_SPECIFIC | 0x0000001E)
#define EFI_DXE_VARIABLE_INIT                 (EFI_OEM_SPECIFIC | 0x0000001F)
#define EFI_DXE_MTC_INIT                      (EFI_OEM_SPECIFIC | 0x00000020)
#define EFI_DXE_CPU_INIT                      (EFI_OEM_SPECIFIC | 0x00000021)
#define EFI_DXE_SMBUS_INIT                    (EFI_OEM_SPECIFIC | 0x00000022)
#define EFI_DXE_PCRTC_INIT                    (EFI_OEM_SPECIFIC | 0x00000023)
#define EFI_DXE_SMMACCESS                     (EFI_OEM_SPECIFIC | 0x00000024)
#define EFI_DXE_SMM_CONTROLER_INIT            (EFI_OEM_SPECIFIC | 0x00000025)
#define EFI_DXE_RELOCATE_SMBASE               (EFI_OEM_SPECIFIC | 0x00000026)
#define EFI_DXE_FIRST_SMI                     (EFI_OEM_SPECIFIC | 0x00000027)
#define EFI_BDS_BEFORE_PCIIO_INSTALL          (EFI_OEM_SPECIFIC | 0x00000028)
#define EFI_BDS_PCI_ENUMERATION_END           (EFI_OEM_SPECIFIC | 0x00000029)
#define EFI_BDS_CONNECT_STD_ERR               (EFI_OEM_SPECIFIC | 0x0000002A)
#define EFI_BDS_INSTALL_HOTKEY                (EFI_OEM_SPECIFIC | 0x0000002B)
#define EFI_BDS_CONNECT_USB_BUS               (EFI_OEM_SPECIFIC | 0x0000002C)
#define EFI_BDS_CONNECT_USB_DEVICE            (EFI_OEM_SPECIFIC | 0x0000002D)
#define EFI_BDS_START_AHCI_BUS                (EFI_OEM_SPECIFIC | 0x0000002E)
#define EFI_BDS_START_SDHC_BUS                (EFI_OEM_SPECIFIC | 0x0000002F)
#define EFI_BDS_START_ISA_ACPI_CONTROLLER     (EFI_OEM_SPECIFIC | 0x00000030)
#define EFI_BDS_START_ISA_BUS                 (EFI_OEM_SPECIFIC | 0x00000031)
#define EFI_BDS_START_ISA_SEIRAL              (EFI_OEM_SPECIFIC | 0x00000032)
#define EFI_DXE_LOAD_ACPI_TABLE               (EFI_OEM_SPECIFIC | 0x00000033)
#define EFI_DXE_VARIABLE_RECLAIM              (EFI_OEM_SPECIFIC | 0x00000034)
#define EFI_DXE_BEFORE_CSM16_INIT             (EFI_OEM_SPECIFIC | 0x00000035)
#define EFI_BDS_ENUMERATE_ALL_BOOT_OPTION     (EFI_OEM_SPECIFIC | 0x00000036)
#define EFI_BDS_DISPLAY_LOGO_SYSTEM_INFO      (EFI_OEM_SPECIFIC | 0x00000037)
#define EFI_BDS_END_OF_BOOT_SELECTION         (EFI_OEM_SPECIFIC | 0x00000038)
#define EFI_BDS_ENTER_BOOT_MANAGER            (EFI_OEM_SPECIFIC | 0x00000039)
#define EFI_BDS_BOOT_DEVICE_SELECT            (EFI_OEM_SPECIFIC | 0x0000003A)
#define EFI_BDS_ACPI_S3SAVE                   (EFI_OEM_SPECIFIC | 0x0000003B)
#define EFI_BDS_GO_UEFI_BOOT                  (EFI_OEM_SPECIFIC | 0x0000003C)
#define EFI_POST_BDS_START_IMAGE              (EFI_OEM_SPECIFIC | 0x0000003D)
#define EFI_BDS_RECOVERY_START_FLASH          (EFI_OEM_SPECIFIC | 0x0000003E)
#define EFI_DXE_AFTER_CSM16_INIT              (EFI_OEM_SPECIFIC | 0x0000003F)
#define EFI_BDS_LEGACY16_PREPARE_TO_BOOT      (EFI_OEM_SPECIFIC | 0x00000040)
#define EFI_BDS_LEGACY_BOOT_EVENT             (EFI_OEM_SPECIFIC | 0x00000041)
#define EFI_SMM_SMM_PLATFORM_INIT             (EFI_OEM_SPECIFIC | 0x00000042)
#define EFI_SMM_IDENTIFY_FLASH_DEVICE         (EFI_OEM_SPECIFIC | 0x00000043)
#define EFI_SMM_ACPI_ENABLE_START             (EFI_OEM_SPECIFIC | 0x00000044)
#define EFI_SMM_ACPI_ENABLE_END               (EFI_OEM_SPECIFIC | 0x00000045)
#define EFI_SMM_S1_SLEEP_CALLBACK             (EFI_OEM_SPECIFIC | 0x00000046)
#define EFI_SMM_S3_SLEEP_CALLBACK             (EFI_OEM_SPECIFIC | 0x00000047)
#define EFI_SMM_S4_SLEEP_CALLBACK             (EFI_OEM_SPECIFIC | 0x00000048)
#define EFI_SMM_S5_SLEEP_CALLBACK             (EFI_OEM_SPECIFIC | 0x00000049)
#define EFI_SMM_ACPI_DISABLE_START            (EFI_OEM_SPECIFIC | 0x0000004A)
#define EFI_SMM_ACPI_DISABLE_END              (EFI_OEM_SPECIFIC | 0x0000004B)
#define EFI_ASL_ENTER_S1                      (EFI_OEM_SPECIFIC | 0x0000004C)
#define EFI_ASL_ENTER_S3                      (EFI_OEM_SPECIFIC | 0x0000004D)
#define EFI_ASL_ENTER_S4                      (EFI_OEM_SPECIFIC | 0x0000004E)
#define EFI_ASL_ENTER_S5                      (EFI_OEM_SPECIFIC | 0x0000004F)
#define EFI_ASL_WAKEUP_S1                     (EFI_OEM_SPECIFIC | 0x00000050)
#define EFI_ASL_WAKEUP_S3                     (EFI_OEM_SPECIFIC | 0x00000051)
#define EFI_ASL_WAKEUP_S4                     (EFI_OEM_SPECIFIC | 0x00000052)
#define EFI_ASL_WAKEUP_S5                     (EFI_OEM_SPECIFIC | 0x00000053)
#define EFI_PEI_RECOVERY_MEDIA_NOT_FOUND      (EFI_OEM_SPECIFIC | 0x00000054)
#define EFI_BDS_NO_CONSOLE_ACTION             (EFI_OEM_SPECIFIC | 0x00000055)
#define EFI_DXE_VARIABLE_INIT_FAIL            (EFI_OEM_SPECIFIC | 0x00000056)
#define EFI_DXE_FLASH_PART_NONSUPPORT         (EFI_OEM_SPECIFIC | 0x00000057)
#define EFI_POST_BDS_NO_BOOT_DEVICE           (EFI_OEM_SPECIFIC | 0x00000058)
#define EFI_PEI_S3_STARTED                    (EFI_OEM_SPECIFIC | 0x00000059)
#define EFI_BDS_END_CONNECT_ALL               (EFI_OEM_SPECIFIC | 0x0000005A)
#define EFI_BDS_SETUP_VERIFYING_PASSWORD      (EFI_OEM_SPECIFIC | 0x0000005B)

#define PEI_SWITCH_STACK                      (EFI_SOFTWARE_PEI_SERVICE  | EFI_SW_PEI_SWITCH_STACK)
#define PEI_MEMORY_CALLBACK                   (EFI_SOFTWARE_PEI_SERVICE  | EFI_PEI_MEMORY_CALLBACK)

#define PEI_SIO_INIT                          (EFI_IO_BUS_LPC | EFI_PEI_SIO_INIT)
#define PEI_BMC_INIT                          (EFI_IO_BUS_LPC | EFI_PEI_BMC_INIT)
#define PEI_PCIE_TRAINING                     (EFI_IO_BUS_PCI | EFI_PEI_PCIE_TRAINING)
#define PEI_TPM_INIT                          (EFI_IO_BUS_LPC | EFI_PEI_TPM_INIT)
#define PEI_SMBUS_INIT                        (EFI_IO_BUS_SMBUS | EFI_PEI_SMBUS_INIT)
#define PEI_IGD_EARLY_INITIAL                 (EFI_IO_BUS_PCI | EFI_PEI_IGD_EARLY_INITIAL)
#define PEI_WATCHDOG_INIT                     (EFI_IO_BUS_LPC | EFI_PEI_WATCHDOG_INIT)
#define PEI_FINDING_DXE_CORE                  (EFI_SOFTWARE_PEI_CORE | EFI_SW_PEI_FINDING_DXE_CORE)
#define PEI_GO_TO_DXE_CORE                    (EFI_SOFTWARE_PEI_CORE | EFI_SW_PEI_GO_TO_DXE_CORE)

#define PEI_RECOVERY_LOAD_FILE_DONE           (EFI_SOFTWARE_PEI_MODULE | EFI_PEI_RECOVERY_LOAD_FILE_DONE)
#define PEI_RECOVERY_START_FLASH              (EFI_SOFTWARE_PEI_MODULE | EFI_PEI_RECOVERY_START_FLASH)
#define PEI_S3_STARTED                        (EFI_SOFTWARE_PEI_MODULE | EFI_PEI_S3_STARTED)

#define S3_RESTORE_MEMORY_CONTROLLER          (EFI_COMPUTING_UNIT_MEMORY | EFI_S3_RESTORE_MEMORY_CONTROLLER)
#define S3_INSTALL_S3_MEMORY                  (EFI_COMPUTING_UNIT_MEMORY | EFI_S3_INSTALL_S3_MEMORY)
#define S3_SWITCH_STACK                       (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_SWITCH_STACK)
#define S3_MEMORY_CALLBACK                    (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_MEMORY_CALLBACK)
#define S3_ENTER_S3_RESUME_PEIM               (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_ENTER_S3_RESUME_PEIM )
#define S3_BEFORE_ACPI_BOOT_SCRIPT            (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_BEFORE_ACPI_BOOT_SCRIPT)
#define S3_BEFORE_RUNTIME_BOOT_SCRIPT         (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_BEFORE_RUNTIME_BOOT_SCRIPT)
#define S3_BEFORE_RELOCATE_SMM_BASE           (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_BEFORE_RELOCATE_SMM_BASE)
#define S3_BEFORE_MP_INIT                     (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_BEFORE_MP_INIT)
#define S3_BEFORE_RESTORE_ACPI_CALLBACK       (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_BEFORE_RESTORE_ACPI_CALLBACK)
#define S3_AFTER_RESTORE_ACPI_CALLBACK        (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_AFTER_RESTORE_ACPI_CALLBACK)
#define S3_GO_TO_FACS_WAKING_VECTOR           (EFI_SOFTWARE_PEI_SERVICE | EFI_S3_GO_TO_FACS_WAKING_VECTOR)
#define DXE_TCGDXE                            (EFI_IO_BUS_LPC | EFI_DXE_TCGDXE)
#define DXE_SB_SPI_INIT                       (EFI_IO_BUS_UNSPECIFIED | EFI_DXE_SB_SPI_INIT)
// #define DXE_SIO_INIT                          (EFI_IO_BUS_LPC | EFI_DXE_SIO_INIT)
#define DXE_BMC_INIT                          (EFI_IO_BUS_LPC | EFI_DXE_BMC_INIT)
#define DXE_FTW_INIT                          (EFI_SOFTWARE_DXE_RT_DRIVER | EFI_DXE_FTW_INIT)
#define DXE_VARIABLE_INIT                     (EFI_SOFTWARE_DXE_RT_DRIVER | EFI_DXE_VARIABLE_INIT)
#define DXE_MTC_INIT                          (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_DXE_MTC_INIT)
#define DXE_CPU_INIT                          (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_DXE_CPU_INIT)
#define DXE_SMBUS_INIT                        (EFI_IO_BUS_SMBUS | EFI_DXE_SMBUS_INIT)
#define DXE_PCRTC_INIT                        (EFI_IO_BUS_I2C | EFI_DXE_PCRTC_INIT)
#define DXE_SMMACCESS                         (EFI_SOFTWARE_SMM_DRIVER | EFI_DXE_SMMACCESS)
#define DXE_SMM_CONTROLER_INIT                (EFI_SOFTWARE_SMM_DRIVER | EFI_DXE_SMM_CONTROLER_INIT)
#define DXE_RELOCATE_SMBASE                   (EFI_SOFTWARE_SMM_DRIVER | EFI_DXE_RELOCATE_SMBASE)
#define DXE_FIRST_SMI                         (EFI_SOFTWARE_SMM_DRIVER | EFI_DXE_FIRST_SMI)
#define BDS_BEFORE_PCIIO_INSTALL              (EFI_IO_BUS_PCI | EFI_BDS_BEFORE_PCIIO_INSTALL)
#define BDS_PCI_ENUMERATION_END               (EFI_IO_BUS_PCI | EFI_BDS_PCI_ENUMERATION_END)
#define BDS_CONNECT_STD_ERR                   (EFI_IO_BUS_LPC | EFI_BDS_CONNECT_STD_ERR)
#define BDS_INSTALL_HOTKEY                    (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_INSTALL_HOTKEY)
#define BDS_CONNECT_USB_BUS                   (EFI_IO_BUS_USB | EFI_BDS_CONNECT_USB_BUS)
#define BDS_CONNECT_USB_DEVICE                (EFI_IO_BUS_USB | EFI_BDS_CONNECT_USB_DEVICE)
#define BDS_START_AHCI_BUS                    (EFI_IO_BUS_ATA_ATAPI | EFI_BDS_START_AHCI_BUS)
#define BDS_START_SDHC_BUS                    (EFI_IO_BUS_PC_CARD | EFI_BDS_START_SDHC_BUS)
#define BDS_START_ISA_ACPI_CONTROLLER         (EFI_IO_BUS_LPC | EFI_BDS_START_ISA_ACPI_CONTROLLER)
#define BDS_START_ISA_BUS                     (EFI_IO_BUS_LPC | EFI_BDS_START_ISA_BUS)
#define BDS_START_ISA_SEIRAL                  (EFI_IO_BUS_LPC | EFI_BDS_START_ISA_SEIRAL)
#define DXE_LOAD_ACPI_TABLE                   (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_DXE_LOAD_ACPI_TABLE)
#define DXE_VARIABLE_RECLAIM                  (EFI_SOFTWARE_DXE_RT_DRIVER | EFI_DXE_VARIABLE_RECLAIM)
#define DXE_BEFORE_CSM16_INIT                 (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_DXE_BEFORE_CSM16_INIT)
#define BDS_ENUMERATE_ALL_BOOT_OPTION         (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_ENUMERATE_ALL_BOOT_OPTION)
#define BDS_DISPLAY_LOGO_SYSTEM_INFO          (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_DISPLAY_LOGO_SYSTEM_INFO)
#define BDS_END_OF_BOOT_SELECTION             (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_END_OF_BOOT_SELECTION)
#define BDS_ENTER_BOOT_MANAGER                (EFI_SOFTWARE_EFI_APPLICATION | EFI_BDS_ENTER_BOOT_MANAGER)
#define BDS_BOOT_DEVICE_SELECT                (EFI_SOFTWARE_EFI_APPLICATION | EFI_BDS_BOOT_DEVICE_SELECT)
#define BDS_ACPI_S3SAVE                       (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_ACPI_S3SAVE)
#define BDS_GO_UEFI_BOOT                      (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_GO_UEFI_BOOT)
#define POST_BDS_START_IMAGE                  (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_POST_BDS_START_IMAGE)
#define BDS_RECOVERY_START_FLASH              (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_RECOVERY_START_FLASH)
#define DXE_AFTER_CSM16_INIT                  (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_DXE_AFTER_CSM16_INIT)
#define BDS_LEGACY16_PREPARE_TO_BOOT          (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_LEGACY16_PREPARE_TO_BOOT)
#define BDS_LEGACY_BOOT_EVENT                 (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_LEGACY_BOOT_EVENT)
#define SMM_SMM_PLATFORM_INIT                 (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_SMM_PLATFORM_INIT)
#define SMM_IDENTIFY_FLASH_DEVICE             (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_IDENTIFY_FLASH_DEVICE)
#define SMM_ACPI_ENABLE_START                 (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_ACPI_ENABLE_START)
#define SMM_ACPI_ENABLE_END                   (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_ACPI_ENABLE_END)
#define SMM_S1_SLEEP_CALLBACK                 (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_S1_SLEEP_CALLBACK)
#define SMM_S3_SLEEP_CALLBACK                 (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_S3_SLEEP_CALLBACK)
#define SMM_S4_SLEEP_CALLBACK                 (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_S4_SLEEP_CALLBACK)
#define SMM_S5_SLEEP_CALLBACK                 (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_S5_SLEEP_CALLBACK)
#define SMM_ACPI_DISABLE_START                (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_ACPI_DISABLE_START)
#define SMM_ACPI_DISABLE_END                  (EFI_SOFTWARE_SMM_DRIVER | EFI_SMM_ACPI_DISABLE_END)
#define ASL_ENTER_S1                          (EFI_SOFTWARE_UNSPECIFIED | EFI_ASL_ENTER_S1)
#define ASL_ENTER_S3                          (EFI_SOFTWARE_UNSPECIFIED | EFI_ASL_ENTER_S3)
#define ASL_ENTER_S4                          (EFI_SOFTWARE_UNSPECIFIED | EFI_ASL_ENTER_S4)
#define ASL_ENTER_S5                          (EFI_SOFTWARE_UNSPECIFIED | EFI_ASL_ENTER_S5)
#define ASL_WAKEUP_S1                         (EFI_SOFTWARE_UNSPECIFIED | EFI_ASL_WAKEUP_S1)
#define ASL_WAKEUP_S3                         (EFI_SOFTWARE_UNSPECIFIED | EFI_ASL_WAKEUP_S3)
#define ASL_WAKEUP_S4                         (EFI_SOFTWARE_UNSPECIFIED | EFI_ASL_WAKEUP_S4)
#define ASL_WAKEUP_S5                         (EFI_SOFTWARE_UNSPECIFIED | EFI_ASL_WAKEUP_S5)
#define PEI_RECOVERY_MEDIA_NOT_FOUND          (EFI_SOFTWARE_PEI_MODULE | EFI_PEI_RECOVERY_MEDIA_NOT_FOUND)
#define BDS_NO_CONSOLE_ACTION                 (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_NO_CONSOLE_ACTION)
#define DXE_VARIABLE_INIT_FAIL                (EFI_SOFTWARE_DXE_RT_DRIVER | EFI_DXE_VARIABLE_INIT_FAIL)
#define DXE_FLASH_PART_NONSUPPORT             (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_DXE_FLASH_PART_NONSUPPORT)
#define BDS_NO_BOOT_DEVICE                    (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_POST_BDS_NO_BOOT_DEVICE)
#define BDS_END_CONNECT_ALL                   (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_END_CONNECT_ALL)
#define BDS_SETUP_VERIFYING_PASSWORD          (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_BDS_SETUP_VERIFYING_PASSWORD)

//Progress/Error codes
#define SEC_CORE_STARTED                      (EFI_SOFTWARE_SEC | EFI_SW_SEC_PC_ENTRY_POINT)
#define SEC_GO_TO_PEICORE                     (EFI_SOFTWARE_SEC | EFI_SW_SEC_PC_HANDOFF_TO_NEXT)
#define PEI_CORE_STARTED                      (EFI_SOFTWARE_PEI_CORE | EFI_SW_PEI_CORE_PC_ENTRY_POINT)
#define PEI_RESET_NOT_AVAILABLE               (EFI_SOFTWARE_PEI_CORE | EFI_SW_PS_EC_RESET_NOT_AVAILABLE)
#define PEI_DXEIPL_NOT_FOUND                  (EFI_SOFTWARE_PEI_CORE | EFI_SW_PEI_CORE_EC_DXEIPL_NOT_FOUND)
#define PEI_DXE_CORE_NOT_FOUND                (EFI_SOFTWARE_PEI_CORE | EFI_SW_PEI_CORE_EC_DXE_CORRUPT)
#define PEI_S3_RESUME_ERROR                   (EFI_SOFTWARE_PEI_CORE | EFI_SW_PEI_EC_S3_RESUME_FAILED)
#define PEI_RECOVERY_FAILED                   (EFI_SOFTWARE_PEI_CORE | EFI_SW_PEI_EC_RECOVERY_FAILED)
#define DXE_CORE_STARTED                      (EFI_SOFTWARE_DXE_CORE | EFI_SW_DXE_CORE_PC_ENTRY_POINT)

//#define DXE_EXIT_BOOT_SERVICES_BEGIN 0xF8
#define DXE_EXIT_BOOT_SERVICES_END            (EFI_SOFTWARE_EFI_BOOT_SERVICE | EFI_SW_BS_PC_EXIT_BOOT_SERVICES)

// Reported by CPU PEIM
#define PEI_CAR_CPU_INIT                      (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_PC_POWER_ON_INIT)

// Reported by NB PEIM
//#define PEI_CAR_NB_INIT                       (EFI_COMPUTING_UNIT_CHIPSET | EFI_CU_CHIPSET_NORTH_INIT)

// Reported by SB PEIM
//#define PEI_CAR_SB_INIT                       (EFI_COMPUTING_UNIT_CHIPSET | EFI_CU_CHIPSET_PC_SOUTH_INIT)

//Reported by Memory Detection PEIM
#define PEI_MEMORY_SPD_READ                   (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_PC_SPD_READ)
#define PEI_MEMORY_PRESENCE_DETECT            (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_PC_PRESENCE_DETECT)
#define PEI_MEMORY_TIMING                     (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_PC_TIMING)
#define PEI_MEMORY_CONFIGURING                (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_PC_CONFIGURING)
#define PEI_MEMORY_OPTIMIZING                 (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_PC_OPTIMIZING)
#define PEI_MEMORY_INIT                       (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_PC_INIT)
#define PEI_MEMORY_TEST                       (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_PC_TEST)
#define PEI_MEMORY_INVALID_TYPE               (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_INVALID_TYPE)
#define PEI_MEMORY_INVALID_SPEED              (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_INVALID_SPEED)
#define PEI_MEMORY_SPD_FAIL                   (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_SPD_FAIL)
#define PEI_MEMORY_INVALID_SIZE               (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_INVALID_SIZE)
#define PEI_MEMORY_MISMATCH                   (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_MISMATCH)
#define PEI_MEMORY_S3_RESUME_FAILED           (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_S3_RESUME_FAIL)
#define PEI_MEMORY_NOT_DETECTED               (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_NONE_DETECTED)
#define PEI_MEMORY_NONE_USEFUL                (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_NONE_USEFUL)
#define PEI_MEMORY_ERROR                      (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_EC_NON_SPECIFIC)
#define PEI_MEMORY_INSTALLED                  (EFI_SOFTWARE_PEI_SERVICE  | EFI_SW_PS_PC_INSTALL_PEI_MEMORY)
#define PEI_MEMORY_NOT_INSTALLED              (EFI_SOFTWARE_PEI_SERVICE  | EFI_SW_PEI_CORE_EC_MEMORY_NOT_INSTALLED)
#define PEI_MEMORY_INSTALLED_TWICE            (EFI_SOFTWARE_PEI_SERVICE  | EFI_SW_PS_EC_MEMORY_INSTALLED_TWICE)

//Reported by CPU PEIM
#define PEI_CPU_INIT                          (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_PC_INIT_BEGIN)
#define PEI_CPU_CACHE_INIT                    (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_PC_CACHE_INIT)
#define PEI_CPU_BSP_SELECT                    (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_PC_BSP_SELECT)
#define PEI_CPU_AP_INIT                       (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_PC_AP_INIT)
#define PEI_CPU_SMM_INIT                      (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_PC_SMM_INIT)
#define PEI_CPU_INVALID_TYPE                  (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_INVALID_TYPE)
#define PEI_CPU_INVALID_SPEED                 (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_INVALID_SPEED)
#define PEI_CPU_MISMATCH                      (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_MISMATCH)
#define PEI_CPU_SELF_TEST_FAILED              (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_SELF_TEST)
#define PEI_CPU_CACHE_ERROR                   (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_CACHE)
#define PEI_CPU_MICROCODE_UPDATE_FAILED       (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_MICROCODE_UPDATE)
#define PEI_CPU_NO_MICROCODE                  (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_NO_MICROCODE_UPDATE)
//If non of the errors above apply use this one
#define PEI_CPU_INTERNAL_ERROR                (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_INTERNAL)
//Generic CPU error. It should only be used if non of the errors above apply
#define PEI_CPU_ERROR                         (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_EC_NON_SPECIFIC)

// Reported by NB PEIM
#define PEI_MEM_NB_INIT                       (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_PC_PEI_MEM_NB_INIT)
// Reported by SB PEIM
#define PEI_MEM_SB_INIT                       (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_PC_PEI_MEM_SB_INIT)

//Reported by PEIM which detected forced or auto recovery condition
#define PEI_RECOVERY_AUTO                     (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_PC_RECOVERY_AUTO)
#define PEI_RECOVERY_USER                     (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_PC_RECOVERY_USER)

//Reported by DXE IPL
#define PEI_RECOVERY_PPI_NOT_FOUND            (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_EC_RECOVERY_PPI_NOT_FOUND)
#define PEI_S3_RESUME_PPI_NOT_FOUND           (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_EC_S3_RESUME_PPI_NOT_FOUND)
#define PEI_S3_RESUME_FAILED                  (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_EC_S3_RESUME_FAILED)

//Reported by Recovery PEIM
#define PEI_RECOVERY_STARTED                  (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_PC_RECOVERY_BEGIN)
#define PEI_RECOVERY_CAPSULE_FOUND            (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_PC_CAPSULE_LOAD)
#define PEI_RECOVERY_NO_CAPSULE               (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_EC_NO_RECOVERY_CAPSULE)
#define PEI_RECOVERY_CAPSULE_LOADED           (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_PC_CAPSULE_START)
#define PEI_RECOVERY_INVALID_CAPSULE          (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_EC_INVALID_CAPSULE_DESCRIPTOR)

//Reported by S3 Resume PEIM
#define PEI_S3_BOOT_SCRIPT                    (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_PC_S3_BOOT_SCRIPT)
#define PEI_S3_OS_WAKE                        (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_PC_OS_WAKE)
#define PEI_S3_BOOT_SCRIPT_ERROR              (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_EC_S3_BOOT_SCRIPT_ERROR)
#define PEI_S3_OS_WAKE_ERROR                  (EFI_SOFTWARE_PEI_MODULE | EFI_SW_PEI_EC_S3_OS_WAKE_ERROR)

#define PEI_PEIM_STARTED                      (EFI_SOFTWARE_PEI_CORE | EFI_SW_PC_INIT_BEGIN)
#define PEI_PEIM_ENDED                        (EFI_SOFTWARE_PEI_CORE | EFI_SW_PC_INIT_END)

//Reported by DXE IPL
#define PEI_DXE_IPL_STARTED                   (EFI_SOFTWARE_PEI_CORE | EFI_SW_PEI_CORE_PC_HANDOFF_TO_NEXT)

//Reported by PEIM which installs Reset PPI
#define PEI_RESET_SYSTEM                      (EFI_SOFTWARE_PEI_SERVICE | EFI_SW_PS_PC_RESET_SYSTEM)

//Reported by the PEIM or DXE driver which detected the error
#define GENERIC_MEMORY_CORRECTABLE_ERROR      (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_CORRECTABLE)
#define GENERIC_MEMORY_UNCORRECTABLE_ERROR    (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_UNCORRECTABLE)

//Reported by Flash Update DXE driver
#define BDS_FLASH_UPDATE_FAILED               (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_UPDATE_FAIL)

//Reported by the PEIM or DXE driver which detected the error
#define GENERIC_CPU_THERMAL_ERROR             (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_THERMAL)
#define GENERIC_CPU_LOW_VOLTAGE               (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_LOW_VOLTAGE)
#define GENERIC_CPU_HIGH_VOLTAGE              (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_HIGH_VOLTAGE)
#define GENERIC_CPU_CORRECTABLE_ERROR         (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_CORRECTABLE)
#define GENERIC_CPU_UNCORRECTABLE_ERROR       (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_UNCORRECTABLE)
#define GENERIC_BAD_DATE_TIME_ERROR           (EFI_SOFTWARE_UNSPECIFIED | EFI_SW_EC_BAD_DATE_TIME)
#define GENERIC_MEMORY_SIZE_DECREASE          (EFI_COMPUTING_UNIT_MEMORY | EFI_CU_MEMORY_EC_MISMATCH)

//Reported by DXE Core
#define DXE_DRIVER_STARTED                    (EFI_SOFTWARE_EFI_DXE_SERVICE | EFI_SW_PC_INIT_BEGIN)
#define DXE_DRIVER_ENED                       (EFI_SOFTWARE_DXE_CORE | EFI_SW_PC_INIT_END)
#define DXE_ARCH_PROTOCOLS_AVAILABLE          (EFI_SOFTWARE_DXE_CORE | EFI_SW_DXE_CORE_PC_ARCH_READY)
#define DXE_DRIVER_CONNECTED                  (EFI_SOFTWARE_DXE_CORE | EFI_SW_DXE_CORE_PC_START_DRIVER)
#define DXE_ARCH_PROTOCOL_NOT_AVAILABLE       (EFI_SOFTWARE_DXE_CORE | EFI_SW_DXE_CORE_EC_NO_ARCH)

//Reported by DXE CPU driver
#define DXE_CPU_SELF_TEST_FAILED              (EFI_COMPUTING_UNIT_HOST_PROCESSOR | EFI_CU_HP_EC_SELF_TEST)

//Reported by PCI Host Bridge driver
#define DXE_NB_HB_INIT                        (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_PC_DXE_HB_INIT )

// Reported by NB Driver
#define DXE_NB_INIT                           (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_PC_DXE_NB_INIT )
#define DXE_NB_SMM_INIT                       (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_PC_DXE_NB_SMM_INIT )
#define DXE_NB_ERROR                          (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_EC_DXE_NB_ERROR )

// Reported by SB Driver(s)
#define DXE_SBRUN_INIT                        (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_PC_DXE_SB_RT_INIT )
#define DXE_SB_INIT                           (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_PC_DXE_SB_INIT )
#define DXE_SB_SMM_INIT                       (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_PC_DXE_SB_SMM_INIT )
#define DXE_SB_DEVICES_INIT                   (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_PC_DXE_SB_DEVICES_INIT )
#define DXE_SB_BAD_BATTERY                    (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_EC_BAD_BATTERY)
#define DXE_SB_ERROR                          (EFI_COMPUTING_UNIT_CHIPSET | EFI_CHIPSET_EC_DXE_SB_ERROR )

//Reported by DXE Core
#define DXE_BDS_STARTED                       (EFI_SOFTWARE_DXE_CORE | EFI_SW_DXE_CORE_PC_HANDOFF_TO_NEXT)

//Reported by BDS
#define BDS_CONNECT_DRIVERS             (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_PC_BEGIN_CONNECTING_DRIVERS)

//Reported by Boot Manager
#define BDS_READY_TO_BOOT                     (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_PC_READY_TO_BOOT_EVENT)

//Reported by DXE Core
#define BDS_EXIT_BOOT_SERVICES                (EFI_SOFTWARE_EFI_BOOT_SERVICE | EFI_SW_BS_PC_EXIT_BOOT_SERVICES)
#define BDS_EXIT_BOOT_SERVICES_EVENT          (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_PC_EXIT_BOOT_SERVICES_EVENT)

//Reported by driver that installs Runtime AP
#define RT_SET_VIRTUAL_ADDRESS_MAP_BEGIN      (EFI_SOFTWARE_EFI_RUNTIME_SERVICE | EFI_SW_RS_PC_SET_VIRTUAL_ADDRESS_MAP)
#define RT_SET_VIRTUAL_ADDRESS_MAP_END        (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_PC_VIRTUAL_ADDRESS_CHANGE_EVENT)

//Reported by CSM
#define BDS_LEGACY_OPROM_INIT                 (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_PC_LEGACY_OPROM_INIT)
#define BDS_LEGACY_BOOT                       (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_PC_LEGACY_BOOT_EVENT)
#define BDS_LEGACY_OPROM_NO_SPACE             (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_EC_LEGACY_OPROM_NO_SPACE)

//Reported by SETUP
//#define DXE_SETUP_VERIFYING_PASSWORD        (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_PC_VERIFYING_PASSWORD)
#define BDS_SETUP_START                       (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_PC_USER_SETUP)
#define BDS_SETUP_INPUT_WAIT                  (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_PC_INPUT_WAIT)
#define BDS_INVALID_PASSWORD                  (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_EC_INVALID_PASSWORD)
#define BDS_INVALID_IDE_PASSWORD              (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_EC_INVALID_IDE_PASSWORD)
#define BDS_BOOT_OPTION_LOAD_ERROR            (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_EC_BOOT_OPTION_LOAD_ERROR)
#define BDS_BOOT_OPTION_FAILED                (EFI_SOFTWARE_DXE_BS_DRIVER | EFI_SW_DXE_BS_EC_BOOT_OPTION_FAILED)

//Reported by a Driver that installs Reset AP
#define BDS_RESET_SYSTEM                      (EFI_SOFTWARE_EFI_RUNTIME_SERVICE | EFI_SW_RS_PC_RESET_SYSTEM)
#define BDS_RESET_NOT_AVAILABLE               (EFI_SOFTWARE_EFI_RUNTIME_SERVICE | EFI_SW_PS_EC_RESET_NOT_AVAILABLE)

// Reported by PCI bus driver
#define BDS_PCI_BUS_BEGIN                     (EFI_IO_BUS_PCI | EFI_IOB_PC_INIT)
#define BDS_PCI_BUS_ENUM                      (EFI_IO_BUS_PCI | EFI_IOB_PCI_BUS_ENUM)
#define BDS_PCI_BUS_HPC_INIT                  (EFI_IO_BUS_PCI | EFI_IOB_PCI_HPC_INIT)
#define BDS_PCI_BUS_REQUEST_RESOURCES         (EFI_IO_BUS_PCI | EFI_IOB_PCI_RES_ALLOC)
#define BDS_PCI_BUS_ASSIGN_RESOURCES          (EFI_IO_BUS_PCI | EFI_IOB_PC_ENABLE)
#define BDS_PCI_BUS_HOTPLUG                   (EFI_IO_BUS_PCI | EFI_IOB_PC_HOTPLUG)
#define BDS_PCI_BUS_OUT_OF_RESOURCES          (EFI_IO_BUS_PCI | EFI_IOB_EC_RESOURCE_CONFLICT)

// Reported by USB bus driver
#define BDS_USB_BEGIN                         (EFI_IO_BUS_USB | EFI_IOB_PC_INIT)
#define BDS_USB_RESET                         (EFI_IO_BUS_USB | EFI_IOB_PC_RESET)
#define BDS_USB_DETECT                        (EFI_IO_BUS_USB | EFI_IOB_PC_DETECT)
#define BDS_USB_ENABLE                        (EFI_IO_BUS_USB | EFI_IOB_PC_ENABLE)
#define BDS_USB_HOTPLUG                       (EFI_IO_BUS_USB | EFI_IOB_PC_HOTPLUG)

//Reported by IDE bus driver
#define BDS_SATA_BEGIN                         (EFI_IO_BUS_ATA_ATAPI | EFI_IOB_PC_INIT)
#define BDS_SATA_RESET                         (EFI_IO_BUS_ATA_ATAPI | EFI_IOB_PC_RESET)
#define BDS_SATA_DETECT                        (EFI_IO_BUS_ATA_ATAPI | EFI_IOB_PC_DETECT)
#define BDS_SATA_ENABLE                        (EFI_IO_BUS_ATA_ATAPI | EFI_IOB_PC_ENABLE)
#define BDS_SATA_SMART_ERROR                   (EFI_IO_BUS_ATA_ATAPI | EFI_IOB_ATA_BUS_SMART_OVERTHRESHOLD)
#define BDS_SATA_CONTROLLER_ERROR              (EFI_IO_BUS_ATA_ATAPI | EFI_IOB_EC_CONTROLLER_ERROR)
#define BDS_SATA_DEVICE_FAILURE                (EFI_IO_BUS_ATA_ATAPI | EFI_IOB_EC_INTERFACE_ERROR)

// Reported by SCSI bus driver
#define BDS_SCSI_BEGIN                        (EFI_IO_BUS_SCSI | EFI_IOB_PC_INIT)
#define BDS_SCSI_RESET                        (EFI_IO_BUS_SCSI | EFI_IOB_PC_RESET)
#define BDS_SCSI_DETECT                       (EFI_IO_BUS_SCSI | EFI_IOB_PC_DETECT)
#define BDS_SCSI_ENABLE                       (EFI_IO_BUS_SCSI | EFI_IOB_PC_ENABLE)

// Reported by Super I/O driver
#define DXE_SIO_INIT                          (EFI_IO_BUS_LPC | EFI_IOB_PC_INIT)

// Reported by Keyboard driver
#define BDS_KEYBOARD_INIT                     (EFI_PERIPHERAL_KEYBOARD | EFI_P_PC_INIT)
#define BDS_KEYBOARD_RESET                    (EFI_PERIPHERAL_KEYBOARD | EFI_P_PC_RESET)
#define BDS_KEYBOARD_DISABLE                  (EFI_PERIPHERAL_KEYBOARD | EFI_P_PC_DISABLE)
#define BDS_KEYBOARD_DETECT                   (EFI_PERIPHERAL_KEYBOARD | EFI_P_PC_PRESENCE_DETECT)
#define BDS_KEYBOARD_ENABLE                   (EFI_PERIPHERAL_KEYBOARD | EFI_P_PC_ENABLE)
#define BDS_KEYBOARD_CLEAR_BUFFER             (EFI_PERIPHERAL_KEYBOARD | EFI_P_KEYBOARD_PC_CLEAR_BUFFER)
#define BDS_KEYBOARD_SELF_TEST                (EFI_PERIPHERAL_KEYBOARD | EFI_P_KEYBOARD_PC_SELF_TEST)

// Reported by Mouse driver
#define BDS_MOUSE_INIT                        (EFI_PERIPHERAL_MOUSE | EFI_P_PC_INIT)
#define BDS_MOUSE_RESET                       (EFI_PERIPHERAL_MOUSE | EFI_P_PC_RESET)
#define BDS_MOUSE_DISABLE                     (EFI_PERIPHERAL_MOUSE | EFI_P_PC_DISABLE)
#define BDS_MOUSE_DETECT                      (EFI_PERIPHERAL_MOUSE | EFI_P_PC_PRESENCE_DETECT)
#define BDS_MOUSE_ENABLE                      (EFI_PERIPHERAL_MOUSE | EFI_P_PC_ENABLE)

// Reported by Mass Storage drivers
#define BDS_FIXED_MEDIA_INIT                  (EFI_PERIPHERAL_FIXED_MEDIA | EFI_P_PC_INIT)
#define BDS_FIXED_MEDIA_RESET                 (EFI_PERIPHERAL_FIXED_MEDIA | EFI_P_PC_RESET)
#define BDS_FIXED_MEDIA_DISABLE               (EFI_PERIPHERAL_FIXED_MEDIA | EFI_P_PC_DISABLE)
#define BDS_FIXED_MEDIA_DETECT                (EFI_PERIPHERAL_FIXED_MEDIA | EFI_P_PC_PRESENCE_DETECT)
#define BDS_FIXED_MEDIA_ENABLE                (EFI_PERIPHERAL_FIXED_MEDIA | EFI_P_PC_ENABLE)
#define BDS_REMOVABLE_MEDIA_INIT              (EFI_PERIPHERAL_REMOVABLE_MEDIA | EFI_P_PC_INIT)
#define BDS_REMOVABLE_MEDIA_RESET             (EFI_PERIPHERAL_REMOVABLE_MEDIA | EFI_P_PC_RESET)
#define BDS_REMOVABLE_MEDIA_DISABLE           (EFI_PERIPHERAL_REMOVABLE_MEDIA | EFI_P_PC_DISABLE)
#define BDS_REMOVABLE_MEDIA_DETECT            (EFI_PERIPHERAL_REMOVABLE_MEDIA | EFI_P_PC_PRESENCE_DETECT)
#define BDS_REMOVABLE_MEDIA_ENABLE            (EFI_PERIPHERAL_REMOVABLE_MEDIA | EFI_P_PC_ENABLE)


// Reported by BDS
#define BDS_CON_OUT_CONNECT                   (EFI_PERIPHERAL_LOCAL_CONSOLE | EFI_P_PC_INIT)
#define BDS_CON_IN_CONNECT                    (EFI_PERIPHERAL_KEYBOARD | EFI_P_PC_INIT)
#define BDS_NO_CON_OUT                        (EFI_PERIPHERAL_LOCAL_CONSOLE | EFI_P_EC_NOT_DETECTED)
#define BDS_NO_CON_IN                         (EFI_PERIPHERAL_KEYBOARD | EFI_P_EC_NOT_DETECTED)

//
// UEFI_DRIVER Post Code Table
//
typedef struct {
  CHAR16                  *UefiDriverName;          ///< Name for UEFI driver
  UINT32                   UefiDriverCode;           ///< UEFI dirver post code value
} POST_CODE_UEFI_DRIVER_TABLE;


//
// DXE_DRIVER Post Code Table
//
typedef struct {
  EFI_GUID                UefiGuid;
  UINT32                   DxeDriverCode;            ///< DXE dirver post code value
} POST_CODE_DXE_DRIVER_TABLE;

#if 0
//
// Use POST_CODE(Value) to display post code
//

//
// SEC Functionality
//
#define SEC_SYSTEM_POWER_ON                   0x01        // CPU power on and switch to Protected mode
#define SEC_GO_TO_SECSTARTUP                  0x02        // Setup BIOS ROM cache
#define SEC_GO_TO_PEICORE                     0x03        // Enter Boot Firmware Volume

//
// PEI Functionality
//
#define PEI_SIO_INIT                          0x60        // Super I/O initial
#define PEI_BMC_INIT                          0x61        // IPMI Early initial
#define PEI_CPU_PEI_INIT                      0x62        // CPU Early Initial
#define PEI_NB_REG_INIT                       0x63        // North Bridge Early Initial
#define PEI_SB_REG_INIT                       0x64        // South Bridge Early Initial
#define PEI_PCIE_TRAINING                     0x65        // PCIE Training
#define PEI_TPM_INIT                          0x66        // TPM Initial
#define PEI_SMBUS_INIT                        0x67        // SMBUS Early Initial
#define PEI_IGD_EARLY_INITIAL                 0x68        // Internal Graphic device early initial
#define PEI_WATCHDOG_INIT                     0x69        // Watchdog timer initial
#define PEI_MEMORY_INIT                       0x6A        // Memory Initial for Normal boot.
#define PEI_MEMORY_INSTALL                    0x6B        // Simple Memory test
#define PEI_SWITCH_STACK                      0x6C        // Start to use Memory
#define PEI_MEMORY_CALLBACK                   0x6D        // Set cache for physical memory
#define PEI_ENTER_RECOVERY_MODE               0x6E        // Recovery device initial
#define PEI_RECOVERY_MEDIA_FOUND              0x6F        // Found Recovery image
#define PEI_RECOVERY_MEDIA_NOT_FOUND          0x70        // Recovery image not found
#define PEI_RECOVERY_LOAD_FILE_DONE           0x71        // Load Recovery Image complete
#define PEI_RECOVERY_START_FLASH              0x72        // Start Flash BIOS with Recovery image
#define PEI_ENTER_DXEIPL                      0x73        // Loading BIOS image to RAM
#define PEI_FINDING_DXE_CORE                  0x74        // Loading DXE core
#define PEI_GO_TO_DXE_CORE                    0x75        // Enter DXE core

//
// DXE Functionality
//
#define DXE_TCGDXE                            0x80        // TPM initial in DXE
#define DXE_SB_SPI_INIT                       0x81        // South bridge SPI initial
#define DXE_SMMACCESS                         0x82        // Setup SMM ACCESS service
#define DXE_NB_INIT                           0x83        // North bridge Middle initial
#define DXE_SIO_INIT                          0x84        // Super I/O DXE initial
#define DXE_BMC_INIT                          0x85        // IPMI DXE initial
#define DXE_SB_INIT                           0x86        // South Bridge Middle Initial
#define DXE_FTW_INIT                          0x87        // Fault Tolerant Write verification
#define DXE_VARIABLE_INIT                     0x88        // Variable Service Initial
#define DXE_VARIABLE_INIT_FAIL                0x89        // Fail to initial Variable Service
#define DXE_MTC_INIT                          0x8A        // MTC Initial
#define DXE_CPU_INIT                          0x8B        // CPU Middle Initial
#define DXE_SMBUS_INIT                        0x8C        // SMBUS Driver Initial
#define DXE_PCRTC_INIT                        0x8D        // RTC Initial
#define DXE_SATA_INIT                         0x8E        // SATA Controller early initial
#define DXE_SMM_CONTROLER_INIT                0x8F        // Setup SMM Control service, DXE_SMMControler_INIT
#define DXE_RELOCATE_SMBASE                   0x90        // Relocate SMM BASE
#define DXE_FIRST_SMI                         0x91        // SMI test
#define DXE_LOAD_ACPI_TABLE                   0x92        // ACPI Table Initial
#define DXE_VARIABLE_RECLAIM                  0x93        // Variable store garbage collection and reclaim operation
#define DXE_FLASH_PART_NONSUPPORT             0x94        // Do not support flash part (which is defined in SpiDevice.c)
#define DXE_BEFORE_CSM16_INIT                 0x95        // Legacy BIOS initial
#define DXE_AFTER_CSM16_INIT                  0x96        // Legacy interrupt function initial
//
// BDS Functionality
//
#define BDS_ENTER_BDS                         0x15        // Enter BDS entry
#define BDS_INSTALL_HOTKEY                    0x16        // Install Hotkey service
#define BDS_PCI_ENUMERATION_START             0x17        // PCI enumeration
#define BDS_BEFORE_PCIIO_INSTALL              0x18        // PCI resource assign complete
#define BDS_PCI_ENUMERATION_END               0x19        // PCI enumeration complete
#define BDS_CONNECT_CONSOLE_IN                0x1A        // Keyboard Controller, Keyboard and Moust initial
#define BDS_CONNECT_CONSOLE_OUT               0x1B        // Video device initial
#define BDS_CONNECT_STD_ERR                   0x1C        // Error report device initial
#define BDS_CONNECT_USB_HC                    0x1D        // USB host controller initial
#define BDS_CONNECT_USB_BUS                   0x1E        // USB BUS driver initial
#define BDS_CONNECT_USB_DEVICE                0x1F        // USB device driver initial
#define BDS_NO_CONSOLE_ACTION                 0x20        // Console device initial fail
#define BDS_DISPLAY_LOGO_SYSTEM_INFO          0x21        // Display logo or system information
#define BDS_START_SATA_CONTROLLER             0x22        // SATA controller initial
#define BDS_START_ISA_ACPI_CONTROLLER         0x23        // SIO controller initial
#define BDS_START_ISA_BUS                     0x24        // ISA BUS driver initial
#define BDS_START_ISA_SEIRAL                  0x25        // Serial device initial
#define BDS_START_AHCI_BUS                    0x26        // AHCI device initial
#define BDS_ENUMERATE_ALL_BOOT_OPTION         0x27        // Get boot device information
#define BDS_END_OF_BOOT_SELECTION             0x28        // End of boot selection
#define BDS_ENTER_SETUP                       0x29        // Enter Setup Menu
#define BDS_ENTER_BOOT_MANAGER                0x30        // Enter Boot manager
#define BDS_BOOT_DEVICE_SELECT                0x31        // Try to boot system to OS
#define BDS_ACPI_S3SAVE                       0x32        // Save S3 resume required data in RAM
#define BDS_READY_TO_BOOT_EVENT               0x33        // Last Chipset initial before boot to OS
#define BDS_GO_UEFI_BOOT                      0x34        // Start to boot UEFI OS
#define BDS_EXIT_BOOT_SERVICES                0x35        // Send END of POST Message to ME via HECI
#define BDS_RECOVERY_START_FLASH              0x36        // Fast recovery start flash
#define BDS_START_SDHC_BUS                    0x37        // SDHC device initial
#define BDS_GO_LEGACY_BOOT                    0x38        // Start to boot Legacy OS
#define BDS_LEGACY16_PREPARE_TO_BOOT          0x39        // Prepare to Boot to Legacy OS
#define BDS_LEGACY_BOOT_EVENT                 0x3A        // Last Chipset initial before boot to Legacy OS.
#define BDS_ENTER_LEGACY_16_BOOT              0x3B        // Ready to Boot Legacy OS.
#define BDS_CONNECT_LEGACY_ROM                0x3C        // Dispatch option ROMs

//
// PostBDS Functionality
//
#define POST_BDS_NO_BOOT_DEVICE                0xF9        // No Boot Device, PostBDS_NO_BOOT_DEVICE

#define POST_BDS_START_IMAGE                   0xFB        // UEFI Boot Start Image, PostBDS_START_IMAGE

//
// SMM Functionality
//
#define SMM_SMM_PLATFORM_INIT                 0xA0        // SMM service initial
#define SMM_IDENTIFY_FLASH_DEVICE             0xA2        // Identify Flash device in SMM
#define SMM_ACPI_ENABLE_START                 0xA6        // OS call ACPI enable function
#define SMM_ACPI_ENABLE_END                   0xA7        // ACPI enable function complete
#define SMM_S1_SLEEP_CALLBACK                 0xA1        // Enter S1
#define SMM_S3_SLEEP_CALLBACK                 0xA3        // Enter S3
#define SMM_S4_SLEEP_CALLBACK                 0xA4        // Enter S4
#define SMM_S5_SLEEP_CALLBACK                 0xA5        // Enter S5
#define SMM_ACPI_DISABLE_START                0xA8        // OS call ACPI disable function
#define SMM_ACPI_DISABLE_END                  0xA9        // ACPI disable function complete

//
// S3 Functionality
//
#define S3_RESTORE_MEMORY_CONTROLLER          0xB0        // Memory initial for S3 resume
#define S3_INSTALL_S3_MEMORY                  0xB1        // Get S3 resume required data from memory
#define S3_SWITCH_STACK                       0xB2        // Start to use memory during S3 resume
#define S3_MEMORY_CALLBACK                    0xB3        // Set cache for physical memory during S3 resume
#define S3_ENTER_S3_RESUME_PEIM               0xB4        // Start to restore system configuration
#define S3_BEFORE_ACPI_BOOT_SCRIPT            0xB5        // Restore system configuration stage 1
#define S3_BEFORE_RUNTIME_BOOT_SCRIPT         0xB6        // Restore system configuration stage 2
#define S3_BEFORE_RELOCATE_SMM_BASE           0xB7        // Relocate SMM BASE during S3 resume
#define S3_BEFORE_MP_INIT                     0xB8        // Multi-processor initial during S3 resume
#define S3_BEFORE_RESTORE_ACPI_CALLBACK       0xB9        // Start to restore system configuration in SMM
#define S3_AFTER_RESTORE_ACPI_CALLBACK        0xBA        // Restore system configuration in SMM complete
#define S3_GO_TO_FACS_WAKING_VECTOR           0xBB        // Back to OS

//
// ASL Functionality
//
#define ASL_ENTER_S1                          0x51        // Prepare to enter S1
#define ASL_ENTER_S3                          0x53        // Prepare to enter S3
#define ASL_ENTER_S4                          0x54        // Prepare to enter S4
#define ASL_ENTER_S5                          0x55        // Prepare to enter S5
#define ASL_WAKEUP_S1                         0xE1        // System wakeup from S1
#define ASL_WAKEUP_S3                         0xE3        // System wakeup from S3
#define ASL_WAKEUP_S4                         0xE4        // System wakeup from S4
#define ASL_WAKEUP_S5                         0xE5        // System wakeup from S5
#endif
#endif
