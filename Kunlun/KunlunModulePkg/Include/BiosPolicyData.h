/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/
#ifndef BIOS_POLICY_DATA_H_
#define BIOS_POLICY_DATA_H_
#include <BiosGpnvData.h>

//BIOS data area offset
#define BIOS_POLOCY_DATA_ADDRESS   (PcdGet64(PcdBiosDataAreaOffset) - PcdGet64(PcdFdBaseAddress) + PHYTIUM_HEADFILE_SIZE)

typedef struct _SMBIOS_INFO{
  UINT8        SmbiosData[512];
}SMBIOS_INFO;

typedef struct _BIOS_POLICY_DATA{
  GPNV_HEADER  Header;
  UINT64       Version;
  SMBIOS_INFO  SmbiosInfo;
  UINT8        MfgMode;
  UINT8        ExceptionNum;
  UINT8        CentralizedMC;
  UINT8        Reserved2[13];
  CHAR8        OdmName[16];
  UINT8        AdminPsw[128];
  UINT8        UserPsw[128];
  UINT8        PopPws[128];
  UINT8        CommonUsrPws[7][32];
  UINT8        HDPsw[32];
  UINT8        Reserved3[112];
  UINT8        SecurityConfig[512];
  UINT8        Reserved4[512];
  UINT8        TCMNvRam[1024];
}BIOS_POLICY_DATA;


#endif
