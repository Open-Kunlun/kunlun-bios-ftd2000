/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef PROCESSOR_INFO_PROTOCOL_H_
#define PROCESSOR_INFO_PROTOCOL_H_


//
// String length definitions
//
#define PROCESSOR_INFO_MANUFACTURER_LEN     64
#define PROCESSOR_INFO_SN                   64
#define PROCESSOR_INFO_VERSION              64
#define PROCESSOR_INFO_PN                   64

typedef struct _EFI_PROCESSOR_INFO_PROTOCOL EFI_PROCESSOR_INFO_PROTOCOL;

typedef struct {
  UINT16 Manufacturer[PROCESSOR_INFO_MANUFACTURER_LEN];
  UINTN  ProcessorID;
  UINT16 Version[PROCESSOR_INFO_VERSION];
  UINT16 MaxSpeed;
  UINT16 CurrentSpeed;
  UINT16 L1CacheSize;
  UINT16 L2CacheSize;
  UINT16 L3CacheSize;
  UINT16 SerialNumber[PROCESSOR_INFO_SN];
  UINT16 PartNumber[PROCESSOR_INFO_PN];
}PROCESSOR_INFO;

/**
  Get the number of CPUs supported by the motherboard.

  @param  This           Protocol instance pointer.
          ProcessorNum   The currently CPUs number

  @retval EFI_SUCCESS.

**/
typedef
EFI_STATUS
(EFIAPI *EFI_PROCESSOR_INFO_GET_PROCESSOR_NUM)(
  IN  EFI_PROCESSOR_INFO_PROTOCOL  *This,
  OUT UINT8                        *ProcessorNum
  );

/**
  Get the number of Cores supported by the motherboard.

  @param  This           Protocol instance pointer.
          ProcessorNum   The currently Cores number

  @retval EFI_SUCCESS.

**/
typedef
EFI_STATUS
(EFIAPI *EFI_PROCESSOR_INFO_GET_CORE_NUM)(
  IN  EFI_PROCESSOR_INFO_PROTOCOL  *This,
  OUT UINT8                        *CoreNum
  );

/**
  Get processor Information.

  @param  This    Protocol instance pointer.

  @retval EFI_SUCCESS.

**/
typedef
EFI_STATUS
(EFIAPI *EFI_PROCESSOR_INFO_GET_PROCESSOR_INFO)(
  IN  EFI_PROCESSOR_INFO_PROTOCOL  *This,
  OUT PROCESSOR_INFO               *ProcessorInfo
  );



struct _EFI_PROCESSOR_INFO_PROTOCOL{
  EFI_PROCESSOR_INFO_GET_PROCESSOR_NUM            GetProcessorNum;
  EFI_PROCESSOR_INFO_GET_CORE_NUM                 GetCoreNum;
  EFI_PROCESSOR_INFO_GET_PROCESSOR_INFO           GetProcessorInfo;
};

extern EFI_GUID gEfiProcessorInfoProtocolGuid;

#endif
