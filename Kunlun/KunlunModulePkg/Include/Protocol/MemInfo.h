/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef MEM_INFO_PROTOCOL_H_
#define MEM_INFO_PROTOCOL_H_

#include <MemInfoDefinitions.h>

#define EFI_MEM_INFO_PROTOCOL_GUID \
  { 0x5cecdaa7, 0x56ef, 0x46e3, { 0xa1, 0x98, 0x97, 0x90, 0x61, 0x19, 0x6f, 0x49 } }

typedef struct _EFI_MEM_INFO_PROTOCOL EFI_MEM_INFO_PROTOCOL;

/**
  Get the number of CPUs supported by the motherboard.

  @param  This    Protocol instance pointer.

  @retval The currently CPUs number.

**/
typedef
UINT8
(EFIAPI *EFI_MEM_INFO_GET_CPU_NUM)(
  IN  EFI_MEM_INFO_PROTOCOL  *This
  );

/**
  Get the number of DIMMs supported by the motherboard.

  @param  This    Protocol instance pointer.

  @retval The currently DIMMs number.

**/
typedef
UINT8
(EFIAPI *EFI_MEM_INFO_GET_DIMM_NUM)(
  IN  EFI_MEM_INFO_PROTOCOL  *This
  );

/**
  Get memory frequency.

  @param  This    Protocol instance pointer.

  @retval Memory frequency.

**/
typedef
UINT32
(EFIAPI *EFI_MEM_INFO_GET_MEM_FREQ)(
  IN  EFI_MEM_INFO_PROTOCOL  *This
  );

/**
  Get total memory size of current system.

  @param  This    Protocol instance pointer.

  @retval Total memory size.

**/
typedef
UINTN
(EFIAPI *EFI_MEM_INFO_GET_TOTAL_MEM_SIZE)(
  IN  EFI_MEM_INFO_PROTOCOL  *This
  );

/**
  Get total memory size of specified CPU.

  @param  This    Protocol instance pointer.

  @retval Total memory size of specified CPU.

**/
typedef
UINTN
(EFIAPI *EFI_MEM_INFO_GET_CPU_MEM_SIZE)(
  IN  EFI_MEM_INFO_PROTOCOL  *This,
  IN  UINT8                  CpuIndex
  );

/**
  Get memory information of specified DIMM.

  @param  This          Protocol instance pointer.
  @param  DimmIndex     The DIMM index that needs to be matched.

  @retval Memory information of specified DIMM.

**/
typedef
EFI_STATUS
(EFIAPI *EFI_MEM_INFO_GET_DIMM_MEM_INFO)(
  IN  EFI_MEM_INFO_PROTOCOL   *This,
  IN  UINT8                   DimmIndex,
  OUT DIMM_MEM_INFO           *DimmMemInfo
  );

/**
  Get the all memory information of current system.

  @param  This          Protocol instance pointer.

  @retval Memory information of current system.

**/
typedef
EFI_STATUS
(EFIAPI *EFI_MEM_INFO_GET_SYSTEM_MEM_INFO)(
  IN  EFI_MEM_INFO_PROTOCOL   *This,
  OUT SYSTEM_MEM_INFO         *SystemMemInfo
  );

struct _EFI_MEM_INFO_PROTOCOL{
  EFI_MEM_INFO_GET_CPU_NUM            GetCpuNum;
  EFI_MEM_INFO_GET_DIMM_NUM           GetDimmNum;
  EFI_MEM_INFO_GET_MEM_FREQ           GetMemFreq;
  EFI_MEM_INFO_GET_TOTAL_MEM_SIZE     GetTotalMemSize;
  EFI_MEM_INFO_GET_CPU_MEM_SIZE       GetCpuMemSize;
  EFI_MEM_INFO_GET_DIMM_MEM_INFO      GetDimmMemInfo;
  EFI_MEM_INFO_GET_SYSTEM_MEM_INFO    GetSystemMemInfo;
};

extern EFI_GUID gEfiMemInfoProtocolGuid;

#endif
