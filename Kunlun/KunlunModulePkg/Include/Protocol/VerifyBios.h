/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef VERIFY_BIOS_H_
#define VERIFY_BIOS_H_
#include <BiosIdLib.h>

typedef
EFI_STATUS
(EFIAPI *VERIFY_BIOS_CHECK_BIOS_VERSION) (
  IN  VOID    *Buffer,
  IN  UINTN   FileSize
);

typedef
EFI_STATUS
(EFIAPI *VERIFY_BIOS_CHECK_BIOS_SHA256) (
  IN  VOID    *Buffer,
  IN  UINTN   FileSize
);

typedef
EFI_STATUS
(EFIAPI *VERIFY_BIOS_VERIFY_BIOS_CERT_SIGN) (
  IN  VOID    *Buffer,
  IN  UINTN   FileSize
);

typedef
EFI_STATUS
(EFIAPI *VERIFY_CERT) (
  IN  VOID    *Buffer,
  IN  UINTN   FileSize
);

typedef struct _KL_VERIFY_BIOS_PROTOCOL{
  VERIFY_BIOS_CHECK_BIOS_VERSION                  CheckBiosVersion;
  VERIFY_BIOS_CHECK_BIOS_SHA256                   CheckBiosSha256;
  VERIFY_BIOS_VERIFY_BIOS_CERT_SIGN               Verify_Bios_Cert_Sign;
  VERIFY_CERT                                     VerifyCertSign;
}KL_VERIFY_BIOS_PROTOCOL;


extern EFI_GUID gVerifyBiosProtocolGuid;

#endif
