/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef BIOS_EVENT_STORAGE_H_
#define BIOS_EVENT_STORAGE_H_

#include <Protocol/HiiConfigAccess.h>
#include <Protocol/HiiConfigRouting.h>
#include <Protocol/HiiDatabase.h>
#include <Protocol/HiiString.h>
#include <Library/HiiLib.h>

typedef struct _EFI_BIOS_EVENT_LOG_PROTOCOL EFI_BIOS_EVENT_LOG_PROTOCOL;

typedef enum {
  EfiEventLogTypeReserved1                          = 0,
  EfiEventLogTypeSingleBitEccMemoryError            = 1,
  EfiEventLogTypeMultiBitEccMemoryError             = 2,
  EfiEventLogTypeParityMemoryError                  = 3,
  EfiEventLogTypeBusTimeOut                         = 4,
  EfiEventLogTypeIoChannelCheck                     = 5,
  EfiEventLogTypeSoftwareNmi                        = 6,
  EfiEventLogTypePostMemoryResize                   = 7,
  EfiEventLogTypePostError                          = 8,
  EfiEventLogTypePciParityError                     = 9,
  EfiEventLogTypePciSystemError                     = 0xA,
  EfiEventLogTypeCpuFailure                         = 0xB,
  EfiEventLogTypeEisaFailSafeTimerTimeOut           = 0xC,
  EfiEventLogTypeCorrectableMemoryLogDisabled       = 0xD,
  EfiEventLogTypeLoggingDisabled                    = 0xE,
  EfiEventLogTypeReserved2                          = 0xF,
  EfiEventLogTypeSystemLimitExceeded                = 0x10,
  EfiEventLogTypeAsynchronousHardwareTimerExpired   = 0x11,
  EfiEventLogTypeSystemConfigurationInformation     = 0x12,
  EfiEventLogTypeHardDiskInformation                = 0x13,
  EfiEventLogTypeSystemReconfigured                 = 0x14,
  EfiEventLogTypeUncorrectableCpuComplexError       = 0x15,
  EfiEventLogTypeLogAreaResetCleared                = 0x16,
  EfiEventLogTypeSystemBoot                         = 0x17,
  EfiEventLogTypeNoVideoDevice                      = 0x80,
  EfiEventLogTypeSupervisorPasswordSet              = 0x81,
  EfiEventLogTypeSupervisorPasswordClean            = 0x82,
  EfiEventLogTypeSupervisorPasswordModify           = 0x83,
  EfiEventLogTypePowerOnPasswordSet                 = 0x84,
  EfiEventLogTypePowerOnPasswordClean               = 0x85,
  EfiEventLogTypePowerOnPasswordModify              = 0x86,
  EfiEventLogTypeBiosLock                           = 0x87,
  #ifdef XC_REQ
  EfiEventLogTypeMemInit                            = 0x88,
  #endif
  EfiEventLogTypeHardDiskChange                     = 0x89,
  EfiEventLogTypeMemoryChange                       = 0x8A,
  EfiEventLogTypeEndOfLog                           = 0xFF
} EFI_MISC_LOG_TYPE;

#define BIOS_EVENT_LOG_BASE_LENGTH                               0x08
#define BIOS_EVENT_LOG_DATA_FORMAT_POST_RESULT_BITMAP_LENGTH     0x08
#define BIOS_EVENT_LOG_DATA_FORMAT_NONE_LENGTH                   0x00


/**
  Read the BIOS Event Log.

  @param [in] This                 This PPI interface.
  @param [in] EventListAddress     Address for reading Event Log in BIOS storage.

  @retval EFI Status
**/
typedef
EFI_STATUS
(EFIAPI *EFI_BIOS_EVENT_LOG_READ_NEXT) (
  IN  EFI_BIOS_EVENT_LOG_PROTOCOL       *This,
  IN OUT VOID                           **EventListAddress
  );

/**
  Clear the BIOS Event Log.

  @param [in] This                 This PPI interface.

  @retval EFI Status
**/
typedef
EFI_STATUS
(EFIAPI *EFI_BIOS_EVENT_LOG_CLEAR) (
  IN  EFI_BIOS_EVENT_LOG_PROTOCOL       *This
  );
/**
  Write event to BIOS Storage.

  @param [in] This                 This PPI interface.
  @param [in] BiosStorageType      Event log type.
  @param [in] PostBitmap1          Post bitmap 1 which will be stored in data area of POST error type log.
  @param [in] PostBitmap2          Post bitmap 2 which will be stored in data area of POST error type log.
  @param [in] OptionDataSize       Optional data size.
  @param [in] OptionLogData        Pointer to optional data.

  @retval EFI Status
**/
typedef
EFI_STATUS
(EFIAPI *EFI_BIOS_EVENT_LOG_WRITE) (
  IN  EFI_BIOS_EVENT_LOG_PROTOCOL       *This,
  IN  UINT8                             EventLogType,
  IN  UINT32                            PostBitmap1,
  IN  UINT32                            PostBitmap2,
  IN  UINTN                             OptionDataSize,
  IN  UINT8                             *OptionLogData,
  IN  EFI_TIME                          *EfiTime
  );

struct _EFI_BIOS_EVENT_LOG_PROTOCOL {
  EFI_BIOS_EVENT_LOG_WRITE                     Write;
  EFI_BIOS_EVENT_LOG_CLEAR                     Clear;
  EFI_BIOS_EVENT_LOG_READ_NEXT                 ReadNext;
  UINT8                                        EventStorageFull; // Libo: EventStorage is full.
};

extern EFI_GUID  gEfiBiosEventLogProtocolGuid;
#endif

