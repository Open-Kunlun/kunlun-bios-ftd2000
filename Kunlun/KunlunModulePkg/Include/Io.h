/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef IO_H_
#define IO_H_

#define WRITE64(addr, data)     (*(volatile UINT64*)(addr) = (data))
#define WRITE32(addr, data)     (*(volatile UINT32*)(addr) = (data))
#define WRITE16(addr, data)     (*(volatile UINT16*)(addr) = (data))
#define WRITEL(addr, data)      (*(volatile UINT32*)(addr) = (data))
#define WRITEW(addr, data)      (*(volatile UINT16*)(addr) = (data))
#define WRITEB(addr, data)      (*(volatile UINT8*)(addr) = (data))
#define READ64(addr)            (*(volatile UINT64*)(addr))
#define READ32(addr)            (*(volatile UINT32*)(addr))
#define READ16(addr)            (*(volatile UINT16*)(addr))
#define READL(addr)             (*(volatile UINT32*)(addr))
#define READW(addr)             (*(volatile UINT16*)(addr))
#define READB(addr)             (*(volatile UINT8*)(addr))
#define IRQ_BASE                64
#define PCI_CONF_READ(a,b,c) (*(volatile char*)((HT_CONF_TYPE0_ADDR | (a << 11) | (b << 8)) + c))
#define PCI_CONF_WRITE(a,b,c,d) (*(volatile char*)((HT_CONF_TYPE0_ADDR | (a << 11) | (b << 8)) + c)  =  d)


#endif
