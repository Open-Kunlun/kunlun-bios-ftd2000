/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef POWER_MANAGEMENT_123_H_
#define POWER_MANAGEMENT_123_H_


#define AC_OFF            0x00
#define AC_ON             0x01
#define AC_LASTSTATE      0x02

#define PCIE_WAKE_OFF     0x00
#define PCIE_WAKE_ON      0x01
#define USB_WAKE_OFF      0x00
#define USB_WAKE_ON       0x01
#define WOL_OFF           0x00
#define WOL_ON            0x01
#define RTC_WAKE_OFF      0x00
#define RTC_WAKE_ON       0x01


typedef struct {
    UINT8    ACmode;
    UINT8    PcieWake;
    UINT8    UsbWake;
    UINT8    NicWake;
    UINT8    WakeOnRtc;
    UINT8    Reserved0;
    UINT8    Reserved1;
    UINT8    Reserved2;
    UINT32   AlarmYY;
    UINT32   AlarmMON;
    UINT32   AlarmDate;
    UINT32   AlarmHH;
    UINT32   AlarmMM;
    UINT32   AlarmSS;
}ACPI_SETUP_DATA;

///
/// HII specific Vendor Device Path definition.
///

#define FORMSET_ID_POWER_MANAGEMENT_GUID     { 0xdd26f555, 0xd58a, 0x4b28, { 0x82, 0xe6, 0x71, 0xd7, 0xad, 0x46, 0xf8, 0x3d } }
#define WAKE_ON_S5_GUID                      { 0x498ccd41, 0x9451, 0x4b01, { 0x9a, 0x6b, 0x9f, 0x52, 0xc7, 0x54, 0x71, 0x2d } }

#endif

