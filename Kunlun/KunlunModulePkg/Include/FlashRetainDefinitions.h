/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef FLASH_RETAIN_DEFINITIONS_H_
#define FLASH_RETAIN_DEFINITIONS_H_
#include <SetupConfig.h>

#define EFI_KUNLUN_RETAIN_SPACE_SIGNATURE        SIGNATURE_64 ('K', 'L', 'R', 'E', 'T', 'A', 'I','N')
//[jqmao-002]
#define COMPARE_PASSWORD_COUNT  5
//[jqmao-002]
//#pragma pack(1)
//
// The structure is used to save old BIOS passwords
// to variable areas for password repeatability checking.
//
typedef struct _OLD_PASSWORD_DATA
{
  UINT32  AdmCurrentIndex;
  UINT32  UserCurrentIndex;
  CHAR16  AdmPassword[COMPARE_PASSWORD_COUNT+1][PASSWORD_MAX_SIZE+1];
  UINT8   AdmSalt[COMPARE_PASSWORD_COUNT+1][PASSWORD_HASH_MAX_SIZE+1];
  UINT16  AdmIterationCount[COMPARE_PASSWORD_COUNT+1];
  UINT16  AdmEncrypt[COMPARE_PASSWORD_COUNT+1];
  CHAR16  UserPassword[COMPARE_PASSWORD_COUNT+1][PASSWORD_MAX_SIZE+1];
  UINT8   UserSalt[COMPARE_PASSWORD_COUNT+1][PASSWORD_HASH_MAX_SIZE+1];
  UINT16  UserIterationCount[COMPARE_PASSWORD_COUNT+1];
  UINT16  UserEncrypt[COMPARE_PASSWORD_COUNT+1];
} OLD_PASSWORD_DATA;

typedef struct {
  UINT64                            Signature;
  UINT16                            Version;
  UINT8                             Reserved[2];
  UINT32                            DxeFvChecksum;
  SYSTEM_ACCESS                     SystemAccess;
  OLD_PASSWORD_DATA                 PasswordData;
  UINT8                             HddMasterPassword[32];
  UINT8                             HddUserPassword[32];
} KL_FLASH_RETAIN_SPACE_DEFINE;

//#pragma pack()

#endif
