/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef MEM_POLL_H_
#define MEM_POLL_H_

typedef struct __mem_pool {
   LIST_ENTRY         Used;
   LIST_ENTRY         Free;
   UINT32             UsedBlockNum;
   UINT32             FreeBlockNum;
   UINT32             BlockSize;
   UINT32             TotalBlockNum;
}MEM_POOL;

/**
  Get the base of the device configure information.

  @retval VOID*         a pointer to the base of the device configure information.

**/
VOID *
GetMemDeviceConfigInfoBase (
  VOID
);

/**
  Init the memory pool.

  @retval EFI_SUCCESS              The operation succeeds.

**/
EFI_STATUS
InitMemPool (
  VOID
);

/**
  Allocate the memory pool.

**/
VOID *
AllocateMemPool (
  VOID
);

/**
  Free the memory pool.

  @param[in,out]  Buffer       a pointer to a memory pool.

**/
VOID
FreeMemPool (
  IN VOID * Buffer
);

/**
  Init the memory device configure information.

  @retval EFI_SUCCESS              The operation succeeds.

**/
EFI_STATUS
InitMemDeviceConfigInfo (
  VOID
);

#endif
