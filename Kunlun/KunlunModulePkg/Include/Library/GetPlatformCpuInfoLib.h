/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef GET_PLATFORM_CPU_INFO_LIB_
#define GET_PLATFORM_CPU_INFO_LIB_

UINT8
GetPlatformCpuNum (
  VOID
);

UINT8
GetPlatformCoreNum (
  VOID
);

VOID
GetPlatformCpuManufacturer (
  UINT16 * Manufacturer
);

UINTN
GetPlatformCpuID (
  VOID
);

VOID
GetPlatformCpuVersion (
  UINT16 *Version
);
UINT16
GetPlatformCpuMaxSpeed (
  VOID
);

UINT16
GetPlatformCpuCurrentSpeed (
  VOID
);
UINT16
GetPlatformCpuL1cacheSize (
  VOID
);
UINT16
GetPlatformCpuL2cacheSize (
  VOID
);
UINT16
GetPlatformCpuL3cacheSize (
  VOID
);

VOID
GetPlatformCpuSN (
  UINT16 *SerialNumber
);

VOID
GetPlatformCpuPN (
  UINT16 *PartNumber
);
#endif //GET_MEM_PARAMS_LIB__
