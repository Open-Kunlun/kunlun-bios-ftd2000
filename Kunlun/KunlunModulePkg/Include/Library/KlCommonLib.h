/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef KL_COMMON_LIB_H_
#define KL_COMMON_LIB_H_

#include <Uefi.h>

/**
  Complete hash sequence complete.

  @param Message        The source message to hash.
  @param MessageSize    The size of message.
  @param Digest         The hashed message.

  @retval EFI_SUCCESS     Hash sequence complete and is returned.
**/
EFI_STATUS
Sha256 (
  IN UINT8                      *Message,
  IN UINTN                      MessageSize,
  OUT UINT8                     *Digest
  );

UINT16
UpdateCrc16 (     
  UINT16 Crc,
  INT8 Byte
  );

UINT64
UpdateCrc32 (     
  UINT64  Crc,
  INT8 Byte
  );

UINT16
UpdateCrcCcitt (
  UINT16 Crc,
  INT8 Byte
  );

UINT16
UpdateCrcDnp (    
  UINT16 Crc,
  INT8 Byte
  );

UINT16
UpdateCrcKermit (
  UINT16 Crc,
  INT8 Byte
  );

UINT16
UpdateCrcSick (   
  UINT16 Crc,
  INT8 Byte,
  INT8 PrevByte
  );

/**
  Convert one Null-terminated ASCII string to a Null-terminated
  Unicode string.

  This function is similar to StrCpyS.

  This function converts the contents of the ASCII string Source to the Unicode
  string Destination. The function terminates the Unicode string Destination by
  appending a Null-terminator character at the end.

  The caller is responsible to make sure Destination points to a buffer with size
  equal or greater than ((AsciiStrLen (Source) + 1) * sizeof (CHAR16)) in bytes.

  If Destination is not aligned on a 16-bit boundary, then ASSERT().

  If an error is returned, then the Destination is unmodified.

  @param  AsciiString     The pointer to a Null-terminated ASCII string.
  @param  BufferSize      The maximum number of Destination Unicode
                          char, including terminating null char.
  @param  UnicodeString   The pointer to a Null-terminated Unicode string.

**/
VOID
AsciiStrToWide(
  IN CHAR8         *AsciiString,
  IN  UINTN        BufferSize,
  IN OUT CHAR16    *UnicodeString
  );

#endif
