/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef ARM_WATCHDOG_LIB_H_
#define ARM_WATCHDOG_LIB_H_

// Refresh Frame:
#define GENERIC_WDOG_REFRESH_REG              ((UINTN)FixedPcdGet64 (PcdGenericWatchdogRefreshBase) + 0x000)

// Control Frame:
#define GENERIC_WDOG_CONTROL_STATUS_REG       ((UINTN)FixedPcdGet64 (PcdGenericWatchdogControlBase) + 0x000)
#define GENERIC_WDOG_OFFSET_REG               ((UINTN)FixedPcdGet64 (PcdGenericWatchdogControlBase) + 0x008)
#define GENERIC_WDOG_COMPARE_VALUE_REG_LOW    ((UINTN)FixedPcdGet64 (PcdGenericWatchdogControlBase) + 0x010)
#define GENERIC_WDOG_COMPARE_VALUE_REG_HIGH   ((UINTN)FixedPcdGet64 (PcdGenericWatchdogControlBase) + 0x014)

// Values of bit 0 of the Control/Status Register
#define GENERIC_WDOG_ENABLED          1
#define GENERIC_WDOG_DISABLED         0

VOID
WatchdogWriteOffsetRegister (
  UINT32  Value
  );

VOID
WatchdogWriteCompareRegister (
  UINT64  Value
  );

VOID
WatchdogEnable (
  VOID
  );

VOID
WatchdogWriteRefreshRegister (
  UINT32  Value
);

VOID
WatchdogDisable (
  VOID
  );

#endif  // ARM_WATCHDOG_LIB_H_
