/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef GENERIC_WATCHDOG_H_
#define GENERIC_WATCHDOG_H_
#include <Uefi/UefiBaseType.h>


#define TIME_OUT_30S   30
#define TIME_OUT_60S   60
#define TIME_OUT_90S   90
#define TIME_OUT_120S  120
#define TIME_OUT_180S  180
#define TIME_OUT_300S  300

EFI_STATUS
StartWatchdog(
  UINT8  Enable,
  UINT8  WatchdogTimeOut,
  UINT16 *BootTimeOut
);

VOID
FeedWatchdog (
  VOID
);

VOID
StopWatchdog(
  VOID
);
#endif

