/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef GET_MEM_PARAMS_LIB_
#define GET_MEM_PARAMS_LIB_

/**

  Check whether the current platform supports SPD.

  @retval TRUE                Support SPD.
  @retval FALSE               Unsupported.

**/
BOOLEAN
EFIAPI
MemParamSupportSpd (
  VOID
  );

/**

  Get the number of CPUs supported on the current platform.

  @retval 0                Get failed.
  @retval >0               CPU number.

**/
UINT8
EFIAPI
MemParamGetCpuNum (
  VOID
  );

/**

  Get the number of DIMMs supported on the current platform.

  @retval 0                Get failed.
  @retval >0               DIMM number.

**/
UINT8
EFIAPI
MemParamGetDimmNum (
  VOID
  );

/**

  Get memory frequency.

  @retval 0                Get data failed, no data is to be get.
  @retval >0               Actual memory frequency.

**/
UINT32
EFIAPI
MemParamGetMemFreq (
  VOID
  );

/**
  Get memory device parameters that matched DimmIndex.

  @param  DimmIndex             The memory index that needs to be matched.
  @param  DimmMemParam          Return memory parameters

  @retval RETURN_SUCCESS        Get data successfully.
  @retval RETURN_NOT_FOUND      Get data failed.

**/
RETURN_STATUS
EFIAPI
MemParamGetDeviceParam (
  IN  UINT8           DimmIndex,
  OUT VOID            *DimmMemParam
  );

/**
  Get memory information that matched DimmIndex.

  @param  DimmIndex             The memory index that needs to be matched.
  @param  DimmMemInfo           Return memory information

  @retval RETURN_SUCCESS        Get data successfully.
  @retval RETURN_NOT_FOUND      Get data failed.

**/
RETURN_STATUS
EFIAPI
MemParamGetDimmMemInfo (
  IN  UINT8           DimmIndex,
  OUT VOID            *DimmMemInfo
  );
#endif //GET_MEM_PARAMS_LIB__
