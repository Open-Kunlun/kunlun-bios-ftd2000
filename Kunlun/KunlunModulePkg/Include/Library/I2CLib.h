/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef I2C_LIB_H_
#define I2C_LIB_H_

//[gliu-0009]

INT32
i2c_read(
  UINTN BusAddress,
  UINT32 Chip,
  UINT16 Addr,
  INT32 Alen,
  UINT8 *Buffer,
  INT32 Len
  );

INT32
i2c_write(
  UINTN BusAddress,
  UINT8 Chip,
  UINT64 Addr,
  INT32 Alen,
  UINT8 *Buffer,
  INT32 Len
  );

VOID
i2c_init(
  UINTN BusAddress,
  unsigned int Speed,
  INT32 SlaveAddress
  );

VOID
spd_setpage(
  UINTN BusAddress,
  UINT8 chip,
  INT32 page_num
  );

INT32
enable_i2c_as_slaver(
  UINTN BusAddress,
  UINT8 slave_address
  );
//[gliu-0009]
#endif /* I2C_LIB_H_ */
