/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef KL_I2C_LIB_H_
#define KL_I2C_LIB_H_

#include <Library/DebugLib.h>
#include <Io.h>
#define PRER_LO_REG 0x0
#define PRER_HI_REG 0x1
#define CTR_REG   0x2
#define TXR_REG   0x3
#define RXR_REG   0x3
#define CR_REG    0x4
#define SR_REG    0x4

#define CR_START  0x80
#define CR_STOP   0x40
#define CR_READ   0x20
#define CR_WRITE  0x10
#define CR_ACK    0x8
#define CR_IACK   0x1

#define SR_NOACK  0x80
#define SR_BUSY   0x40
#define SR_AL   0x20
#define SR_TIP    0x2
#define SR_IF   0x1

#define SPA0    0x6c
#define SPA1    0x6e


#define I2C0_BASE_ADDR_OFFSET      0x10000
#define I2C1_BASE_ADDR_OFFSET      0x10100
#define LS7A_I2C0_BASE_ADDR     (0x90000e0010080000 | I2C0_BASE_ADDR_OFFSET)
#define LS7A_I2C1_BASE_ADDR     (0x90000e0010080000 | I2C1_BASE_ADDR_OFFSET)

VOID
Kl_I2cInit(
  IN UINTN
  );

VOID
Kl_Ls7aI2cStop(
  UINTN
  );

UINT8
Kl_I2cReadByte(
  UINTN,
  UINT8,
  UINT8
  );

VOID
Kl_I2cWriteByte(
  UINTN  Base,
  UINT8  DevAddr,
  UINT8  Offset,
  UINT8  Data
  );

UINT8
Kl_I2cReadHighByte(
  UINTN,
  UINT8,
  UINT8
  );

VOID
Kl_SetPage(
  UINTN  Base,
  UINT16 SPA
  );

UINT8
Kl_I2cRead(
  UINTN Base,
  UINT8 DevAddr,
  UINT16 Offset
  );

UINT8
Kl_I2cReadByteFromPage(
  UINTN Base,
  UINT8 DevAddr,
  UINT8 Offset,
  UINT8 SelPage
  );
#endif

