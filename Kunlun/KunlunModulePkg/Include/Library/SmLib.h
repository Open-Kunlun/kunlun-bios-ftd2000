/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef SM_RYPT_LIB_H_
#define SM_RYPT_LIB_H_

VOID sm4_crypt_ecb_enc (
  IN  UINT8 key[16],
  IN  INT32  Length,
  IN  UINT8 *input,
  OUT UINT8 *output
);

VOID sm4_crypt_ecb_dec (
  IN  UINT8 key[16],
  IN  INT32  Length,
  IN  UINT8 *input,
  OUT UINT8 *output
);
//[gliu-0035]add-start
VOID sm3(
  UINT8 *input,
  INT32 ilen,
  UINT8 output[32]
  );
//[gliu-0035]add-end

#endif //BASE_CRYPT_LIB_H__
