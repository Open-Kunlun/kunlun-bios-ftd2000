/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef RETAIN_DATA_LIB_
#define RETAIN_DATA_LIB_

UINTN
GetRetainDataSize();


EFI_STATUS
GetRetainData (
  IN   UINTN    ByteCount,
  OUT  UINT8    *DataBuffer
);


EFI_STATUS
SetRetainData (
  IN  UINTN    ByteCount,
  IN  UINT8    *DataBuffer
);

#endif //RETAIN_DATA_LIB__
