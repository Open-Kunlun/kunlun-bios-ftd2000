/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef POST_INFO_LIB_H_
#define POST_INFO_LIB_H_

typedef struct{
CHAR8   SerialNumber[21];
CHAR8   ModeName[41];
UINT64  Capacity;
} POST_HARD_DISK_INFO;

typedef struct{
CHAR8   ClassName[24];
CHAR16  Manufacturer[40];
CHAR16  Product[40];
CHAR16  SerialNum[40];
UINT16  BcdUSB;
UINT64  VendorId;
UINT64  ProductId;
} POST_USB_DEVICE_INFO;


BOOLEAN
IsUsbKeyboard ();

EFI_STATUS
EFIAPI
GetUsbHidDeviceInfo (
  IN OUT POST_USB_DEVICE_INFO  *UsbDeviceInfo,
  IN OUT UINT32                *UsbDevNum
 );

EFI_STATUS
EFIAPI
GetSataDeviceInfo(
 POST_HARD_DISK_INFO  *HarddiskInfo,
 UINT32               *HarddiskTotalNumber
);

EFI_STATUS
EFIAPI
DisplayPostInfo ();

EFI_STATUS
GetNvmeDeviceInfo ();

VOID
SwapStringCharsdevice (
  IN CHAR8  *Destination,
  IN CHAR8  *Source,
  IN UINT32 Size
);
#endif


