/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef KL_S3_SUSPEND_UNLOCK_HDD_LIB_H_
#define KL_S3_SUSPEND_UNLOCK_HDD_LIB_H_


/**
  prepare the queue for pci bus scanning

**/
VOID *
PreparePciScan(
  VOID
);

/**
  Scan pci bus and store the strorage device path for S3 resuming.

  @param  pDevQueue        the device queue pointer.
  @param  BusNumber        start bus number.


  @retval EFI_SUCCESS      Successfully scanned  bus number.

  @note   Feature flag PcdPciBusHotplugDeviceSupport determine whether need support hotplug.

**/
EFI_STATUS
PciScanBus (
  IN VOID               *pDevQueue,
  IN UINT8               BusNumber
  );

EFI_STATUS
DumpDeviceInfo (
  VOID
);

#endif
