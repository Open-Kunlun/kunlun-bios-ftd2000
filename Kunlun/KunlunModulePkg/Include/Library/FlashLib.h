/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef FLASH_LIB_
#define FLASH_LIB_

#include <Protocol/SpiServiceDxe.h>

//Flash of Loongson Platform can be read as mem under 8M size.
#define MAX_MEM_ACCESS_FLASH_SIZE 0x800000

#define SPIMODE   // if not define SPIMODE, means the flash under QPI mode

#define WEL_CHECK     0x02
#define WIP_CHECK     0x01

#define FCR_SPCR      0x00
#define FCR_SPSR      0x01
#define FCR_SPDR      0x02
#define FCR_SPER      0x03
#define FCR_PARAM     0x04
#define FCR_SOFTCS    0x05
#define FCR_TIME      0x06


#define WREN          0x06
#define WRDI          0x04
#define RDID          0x90
#define RDSR          0x05
#define WRSR          0x01
#define READ          0x03
#define AAI_WRITE     0xad
#define BYTE_WRITE    0x02
#define EBSY          0x70
#define DBSY          0x80

#define EWSR          0x50
#define FAST_READ     0x0B
#define CHIPERASELECOFFCHIPERASET  0x4
#define BE4K          0x20  /* 4K Byte block Rrase, Sector Erase */
#define BE4KSIZE      0x1000  /* 4K Byte block Rrase, Sector Erase */
#define BE16KSIZE     0x10000 /* 16K Byte block Rrase, Sector Erase */
#define BE32K         0x52  /* 32K Byte block Rrase, Sector Erase */
#define BE32KSIZE     0x8000  /* 32K Byte block Rrase, Sector Erase */
#define BE64K         0xD8  /* 64K Byte block Rrase, Sector Erase */
#define BE64KSIZE     0x10000 /* 64K Byte block Rrase, Sector Erase */
#define CHIPERASE     0xC7  /* Chip Erase */
#define BLKERASE      BE4K
#define BLKSIZE       BE4KSIZE
#define BLKSIZE64     BE64KSIZE

#define READBLKSIZE   BE4KSIZE
#define READBLKSIZE64 BE64KSIZE


EFI_STATUS
EFIAPI
FlashGetInfo (
  FLASH_INFO  *FlashInfo
);

EFI_STATUS
EFIAPI
FlashErase (
  IN  UINTN  Lba
  );

EFI_STATUS
EFIAPI
FlashWrite (
  UINT8 *SrcAddr, UINT8 * DestAddr, UINTN Counter
  );

UINT64
EFIAPI
FlashReadId (
  VOID
  );
#endif
