/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef FLASH_LAYOUT_LIB_H_
#define FLASH_LAYOUT_LIB_H_

#ifdef __cplusplus
extern "C"{
#endif

#include <FlashLayoutStruct.h>
// {8b9c4095-6a6f-410c-8f74-8404011ac071}
#define EFI_FLASH_LAYOUT_HOB_GUID  {0x8b9c4095, 0x6a6f, 0x410c, {0x8f, 0x74, 0x84, 0x04, 0x01, 0x1a, 0xc0, 0x71}}

#define _FLASH_ROM_INFO_SUPPORT 1
#define _FLASH_ROM_INFO_SUPPORT_PEI 1
#define _FLASH_ROM_INFO_SUPPORT_DXE 1
#define FLASH_ROM_INFO_FLASH_SIZE_INFO 0x1000000 //16MB
#define FLASH_ROM_INFO_SECTION_SIZE_INFO 0x1000  //4KB //0x4000
#define EFI_FLASH_ROM_INFO_HOB_GUID  {0x8b9c4095, 0x6a6f, 0x410c, {0x8f, 0x74, 0x84, 0x04, 0x01, 0x1a, 0xc0, 0x82}}
/**

  Obtain the specified FLASH_SECTION according to Tpye.

  @param Type               FlashSection Type.
  @param Section            Return FLASH_SECTION.

  @retval  EFI_SUCCESS      Successfully found the Section.
  @retval  EFI_NOTFOUND     Not found the Section.


**/
EFI_STATUS
EFIAPI
GetFlashSectionInfo(
    IN FLASH_SECTION_TYPE Type,
    IN OUT FLASH_SECTION* Section
);
/**

  Get the FlashSection list.

  @param SectionList        Section List.
  @param SectionNum         Section Num.

  @retval  EFI_SUCCESS      Successfully found the Section List.
  @retval  EFI_NOTFOUND     Not found the Section List.

**/
EFI_STATUS
EFIAPI
GetFlashLayoutInfo(
  OUT FLASH_SECTION** SectionList,
  OUT UINTN* SectionNum
);

EFI_STATUS
EFIAPI
GetFlashRomInfoHobData(
OUT UINT32 *FlashSizeInfo_Hob,
OUT UINT32 *SectionSizeInfo_Hob
);



#ifdef __cplusplus
}
#endif
#endif
