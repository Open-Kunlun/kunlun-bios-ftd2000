/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

/**
  This function will set rtc wake register.

  @param AlarmYY      The year value.
  @param AlarmMON     The month value.
  @param AlarmDate    The date value.
  @param AlarmHH      The hour value.
  @param AlarmMM      The minute value.
  @param AlarmSS      The second value.

**/
#ifndef POWER_LOST_LIB_H_
#define POWER_LOST_LIB_H_

VOID
SetRtcWakeReg(
  UINT32   AlarmYY,
  UINT32   AlarmMON,
  UINT32   AlarmDate,
  UINT32   AlarmHH,
  UINT32   AlarmMM,
  UINT32   AlarmSS
);

/**
  This function will set power on.

**/
VOID
SetPowerLostOn (
  VOID
  );

/**
  This function will set power off.

**/
VOID
SetPowerLostOff (
  VOID
  );

#endif

