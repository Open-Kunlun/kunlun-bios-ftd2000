/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef __HEADER_VERIFY_BIOS_H__
#define __HEADER_VERIFY_BIOS_H__

/**
  Based on RSA_PSS, verify a BIOS image file:
    1) Check X509 certification in the BIOS image file
    2) Check the signature in the BIOS image file

  @param  CertFile               Certificate data
  @param  CertLen                Size of the certificate
  @param  SignBuff               Signature data
  @param  SignLen                Size of the signature
  @param  FdFile                 BIOS image data
  @param  FdLen                  Size of the BIOS image data
  @param  Cacert                 ROOT Certificate data
  @param  CaCertLen              Size of the ROOT certificate

  @return TRUE                   Verification is OK
          FALSE                  Verification failed
**/
VOID
VerifyBiosCertSign (
  IN CONST UINT8     *CertFile,
  IN INT32           CertLen,
  IN UINT8           *SignBuff,
  IN INT32           SignLen,
  IN UINT8           *FdFile,
  IN INT32           FdLen,
  IN CONST UINT8     *CaCert,
  IN INT32           CaCertLen
 );

#endif
