/** @file
  This PPI is installed by the platform PEIM to get x100 chipset informations.

  Copyright (c) 2006 - 2023, Kunlun Tech. All rights reserved.<BR>
  SPDX-License-Identifier: BSD-2-Clause-Patent

  @par Revision Reference:
  This PPI is introduced in PI Version 1.0.

**/

#ifndef __X100_INFO_PPI_H__
#define __X100_INFO_PPI_H__

#define EFI_PEI_X100_INFO_PPI_GUID \
  { \
    0xbd538a9b, 0x562a, 0xf88e, {0xa1, 0xd2, 0x2c, 0xe0, 0x6c, 0x68, 0x47, 0xc5 } \
  }
typedef struct __EFI_PEI_X100_INFO_PPI EFI_PEI_X100_INFO_PPI;
/**
  This service publishes an interface that allows PEIMs to get x100 chipset information.


  @param  PeiServices      An indirect pointer to the EFI_PEI_SERVICES table published by the PEI Foundation.
  @param  SataInfo         Indicates the sata information of x100 chipset.
  @param  SataPi           The returned sata pi

  @retval EFI_SUCCESS           The function completed successfully.
  @retval EFI_NOT_AVAILABLE_YET No progress code provider has installed an interface in the system.

**/
typedef
EFI_STATUS
(EFIAPI *EFI_PEI_GET_X100_SATAPI)(
  IN EFI_PEI_X100_INFO_PPI          *This,
  IN UINT8                          SataInfo,
  OUT UINT32                        *SataPi
  );

///
/// This PPI provides the service to get x100 chipset info.
/// There can be only one instance of this service in the system.
///
struct __EFI_PEI_X100_INFO_PPI {
  EFI_PEI_GET_X100_SATAPI  GetX100SataPi;
};

extern EFI_GUID gEfiPeiX100InfoPpiGuid;

#endif

