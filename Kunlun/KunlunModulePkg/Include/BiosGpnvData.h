/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/
#ifndef BIOS_GPNV_DATA_H_
#define BIOS_GPNV_DATA_H_

//#define PHYTIUM_HEADFILE_SIZE  0x440000
//#define PHYTIUM_HEADFILE_SIZE  0x180000
//[jwluo-0001]
#define PHYTIUM_HEADFILE_SIZE  PcdGet64(PcdFdBaseAddress)
//[jwluo-0001]

typedef struct _GPNV_HEADER{
  CHAR8        Signature[8];
  UINT64       Length;
}GPNV_HEADER;


#endif
