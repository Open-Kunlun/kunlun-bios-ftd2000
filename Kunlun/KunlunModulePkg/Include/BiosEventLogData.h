/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/
#ifndef BIOS_EVENT_LOG_DATA_H_
#define BIOS_EVENT_LOG_DATA_H_
#include <BiosGpnvData.h>

#define BIOS_EVENTLOG_DATA_ADDRESS          (PcdGet64(PcdBiosEventLogStorageBase) - PcdGet64(PcdFdBaseAddress) + PHYTIUM_HEADFILE_SIZE)

typedef struct {

  //
  //Log Header is optional, default is no header
  //
  UINT8   Type;
  UINT8   Length;

  UINT8   Year;
  UINT8   Month;
  UINT8   Day;
  UINT8   Hour;
  UINT8   Minute;
  UINT8   Second;

  UINT8   Data[8];
  //
  //Log Variable Data is optional
  //
} BIOS_EVENT_LOG_ORGANIZATION;

#endif
