/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/
#ifndef BIOS_DMI_DATA_H_
#define BIOS_DMI_DATA_H_
#include <BiosGpnvData.h>

#define BIOS_DMI_DATA_ADDRESS          (PcdGet64(PcdKlFlashDmiRegionBase) - PcdGet64(PcdFdBaseAddress) + PHYTIUM_HEADFILE_SIZE)

typedef struct _BIOS_DMI_INFO{
   UINT8    Dmi[1024];
}BIOS_DMI_INFO;

typedef struct _BIOS_DMI_DATA{
  GPNV_HEADER     Header;
  BIOS_DMI_INFO   Dmi;
}BIOS_DMI_DATA;

#endif
