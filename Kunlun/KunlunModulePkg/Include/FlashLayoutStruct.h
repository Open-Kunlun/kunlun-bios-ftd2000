/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef FLASH_LAYOUT_STRUCT_H_
#define FLASH_LAYOUT_STRUCT_H_
#ifdef __cplueplus
extern "C"
{
#endif
#include <Uefi.h>

typedef enum _FLASH_SECTION_TYPE
{
  //-------------Common--------------
  TYPE_COMMOM_START=0,
  FV_SEC,
  FV_PEI,
  FV_DXE,
  FV_RECOVERY,
  SMBIOS,
  KL_RETAIN_SPACE,
  NV_VARIABLE_STORE,
  NV_FTW_SPARE,
  NV_FTW_WORKING,
  NV_EVENT_LOG,
  KL_BIOS_SIGN,

  OS_ACTIVATION_KEY,
  LPBT_BIN,
  LPBT_BIN_ARGUMENTS,
  //
  //.. Commom add hear.
  //
  //------------Platform-------------
  TYPE_PLATFORM_START=129,
  LS_START_CODE,
  FT_HEADER,
  KL_ZF_EVENT_LOG,
  KL_ZF_BIOS_DATA_AREA,
  KL_ZF_TCM_NV_DATA,
  //
  //.. Platform add hear.
  //
  //---------------------------------
  TYPE_END = 255
} FLASH_SECTION_TYPE;

typedef enum _PROTECT_FLAG
{
    PROTECT,
    UNPROTECT
} PROTECT_FLAG;

typedef struct _FLASH_SECTION
{
  UINT32 Offset;
  UINT32 Size;
  FLASH_SECTION_TYPE Type;
  PROTECT_FLAG ProtectFlag;
} FLASH_SECTION;

typedef struct _FLASH_ROM_INFO
{
  UINT32 RomSize;
  UINT32 SectorSize;
}FLASH_ROM_INFO;

#ifdef __cplueplus
}
#endif
#endif
