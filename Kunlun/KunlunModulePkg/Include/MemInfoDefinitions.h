/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef MEM_INFO_DEFINITIONS_H_
#define MEM_INFO_DEFINITIONS_H_

//
// String length definitions
//
#define MEM_INFO_MANUFACTURER_LEN     16
#define MEM_INFO_SERIAL_NUM_LEN       16
#define MEM_INFO_PART_NUM_LEN         32
#define MEM_INFO_SCREEN_PRINTING_LEN  32

//
// DRAM type definitions
//
#define MEM_INFO_DRAM_TYPE_DDR3         0xB
#define MEM_INFO_DRAM_TYPE_DDR4         0xC
#define MEM_INFO_DRAM_TYPE_UNKNOWN      0xFF

//
// Module type definitions
//
#define MEM_INFO_MODULE_TYPE_RDIMM      0x1
#define MEM_INFO_MODULE_TYPE_UDIMM      0x2
#define MEM_INFO_MODULE_TYPE_SODIMM     0x3
#define MEM_INFO_MODULE_TYPE_UNKNOWN    0xF

//
// ECC definitions
//
#define MEM_INFO_ECC_UNSUPPORT          0
#define MEM_INFO_ECC_SUPPORT            1

#pragma pack(1)

typedef struct {
  UINT8   DRAMType;                 //Value such as: 0xB(DDR3),0xC(DDR4)
  UINT8   ModuleType;               //Value such as: 0x1(RDIMM),0x2(UDIMM)
  UINT8   SdramBankGroupsNum;       //Value such as: 0,2,4
  UINT8   SdramBanksNum;            //
  UINT8   ModuleTotalBusWidth;      //Value such as: 32bits, 64bits
  UINT8   ModulePrimaryBusWidth;    //Value such as: 32bits, 64bits
  UINT8   ModuleWithECC;            //0: No, 1: Yes
  UINT8   SdramWidth;               //Value such as: 8bits, 16bits
  UINT8   LogicalRanksNum;          //
  UINT8   CpuIndex;                 //CPU index where the memory is located
  UINT16  ModuleMinOperatingVoltage;//Minimum operating voltage for this module, in millivolts.
  UINT16  ModuleMaxOperatingVoltage;//Maximum operating voltage for this module, in millivolts.
  UINT32  SdramCapacity;            //Units: Mb
  UINT64  ModuleCapacity;           //Units: Mb
  CHAR16  ModuleManufacturer[MEM_INFO_MANUFACTURER_LEN];
  CHAR16  ModuleSerialNumber[MEM_INFO_SERIAL_NUM_LEN];
  CHAR16  ModulePartNumber[MEM_INFO_PART_NUM_LEN];
  CHAR16  ScreenPrinting[MEM_INFO_SCREEN_PRINTING_LEN];   //Screen printing of slots on the motherboard
} DIMM_MEM_INFO;

typedef struct {
  UINT8         CpuNum;             //Number of CPUs supported by the motherboard
  UINT8         DimmNum;            //Number of DIMMs supported by the motherboard
  UINT8         PresentMemNum;      //The number of memory currently present
  UINT8         Reserved;           //
  UINT32        MemFreq;            //Memory frequency
  UINT64        TotalMemSize;       //Total system memory
  UINT64        *CpuMemSize;        //The total memory of each CPU
  DIMM_MEM_INFO *DimmMemInfo;       //Memory information of each DIMM
} SYSTEM_MEM_INFO;

typedef struct {
  UINTN     ControllerBaseAddr;   //I2C controller base address
  UINT32    CpuIndex;             //CPU index where the memory device is located
  UINT32    MemDevAddr;           //Slave address of SPD
  CHAR16    ScreenPrinting[MEM_INFO_SCREEN_PRINTING_LEN]; //Screen printing of memory device on the motherboard
} MEM_DEV_PARAM;

#pragma pack()

#endif
