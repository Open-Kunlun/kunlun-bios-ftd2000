/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:DfxSendInfoToCPLDLib.c


Abstract:


Revision History:

**/


#include <Uefi.h>
#include <Uefi/UefiBaseType.h>
#include <Library/I2CLib.h>
#include  <Library/FlashLib.h>
static UINT64         I2CBaseAddress = 0x28007000;//IIC1
static UINT32         I2CSpeed = 100000;

/**
  Initialize rtc.

**/
VOID
InitialRTC(
  VOID
  )
{
  UINT32         I2CSlaveAddress;
  UINT64        I2C1BaseAddress;

  I2C1BaseAddress=0x28008000;
  I2CSlaveAddress = 0x64;
  i2c_init(I2C1BaseAddress, I2CSpeed, I2CSlaveAddress);
}

/**
  Send command to CPLD.

  @param[in]  Offset  The offset address send to CPLD.
  @param[in]  Data    The data send to CPLD

**/
VOID
SentinfoToCPLD(
  IN UINT8 Offset,
  IN UINT8 Data
  )
{
  UINT32         I2CSlaveAddress;
  UINT8          I2cStatus;

  I2CSlaveAddress = 0x28;
  i2c_init(I2CBaseAddress, I2CSpeed, I2CSlaveAddress);

  I2cStatus=i2c_write(I2CBaseAddress, I2CSlaveAddress, Offset, 1, &Data, 1);
  if(I2cStatus)
  {
    I2cStatus=i2c_write(I2CBaseAddress,I2CSlaveAddress, Offset, 1, &Data, 1);
    if(I2cStatus){
      DEBUG((EFI_D_ERROR, " : In DfxSendInfoToCPLDLib.c, line %d.i2c_write I2cStatus FAIL \n", __LINE__));
    }
  }
  InitialRTC();
}
/**
  Read data from CPLD.

  @param[in]  Offset  The offset address read from CPLD.
  @param[out]  Data    The data read from CPLD

**/
VOID
ReadinfofromCPLD(
  IN UINT8 Offset,
  OUT UINT8 *Data
  )
{
  UINT32         I2CSlaveAddress;
  UINT8          I2cStatus;

   I2CSlaveAddress = 0x28;
   i2c_init(I2CBaseAddress, I2CSpeed, I2CSlaveAddress);

  //read board ID
  I2cStatus = i2c_read(I2CBaseAddress, I2CSlaveAddress, Offset, 1, Data, 1);
  if(I2cStatus){
    DEBUG((EFI_D_ERROR, " : In DfxSendInfoToCPLDLib.c, line %d.i2c_read I2cStatus FAIL \n", __LINE__));
  }

   InitialRTC();
}

/**
  Get CPLD watch dog time and erase the flash.

**/
VOID
GetWDTTimesClearNV(
  VOID
  )
{
  UINT32                I2CSlaveAddress;
  UINT8                     Offset, Data;
  UINT64                              EraseIndex;
  UINTN                               BlockSize;
  EFI_STATUS                          Status;

  BlockSize = 0x10000;
  Status = EFI_SUCCESS;
  Offset = 0x78;
  I2CSlaveAddress = 0x28;
  i2c_init(I2CBaseAddress, I2CSpeed, I2CSlaveAddress);
  i2c_read(I2CBaseAddress, I2CSlaveAddress, Offset, 1, &Data, 1);
  DEBUG((EFI_D_ERROR, " : WDT timeout Times %d. \n", Data));
  if (Data == 3) {
    for (EraseIndex = 0; EraseIndex < 0x5; EraseIndex++) {
      Status = FlashErase((UINTN)( BlockSize * EraseIndex + 0xe00000));
    }
    }
    if (EFI_ERROR(Status)) {
      return;
  }
//CPLD_reset_ctr();
}

/**
  Turn on or off the led.

  @param[in]  StateCode  The state to set to the led.

**/
VOID
LedProcessIndicate(
  IN UINT8 StateCode
  )
{
  SentinfoToCPLD(0x1E, StateCode);
}
