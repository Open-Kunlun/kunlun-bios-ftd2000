/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name:

  KlSystemSlotConfigLib

Abstract:


Revision History:

**/

#include <Base.h>
#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PcdLib.h>

#define KEEP_PASSWORD_SUPPORT            TRUE
#define COMPARE_OLD_PASSWORD_SUPPORT     TRUE
#define KEEP_PASSWORD_COUNT              5

/**
  Check whether the password needs to be changed during the update.
**/
BOOLEAN
KeepPasswordWhenUpdate()
{
  return PcdGetBool(PcdKeepPassword);
}

/**
  Check whether comparing old passwords is supported.
**/
BOOLEAN
CompareOldPasswordSupport()
{
  return FALSE;
}

/**
  Check precise password help information is supported.
**/
BOOLEAN
PrecisePasswordHelpInformationSupport()
{
  return PcdGetBool(PcdPrecisePasswordHelpInformationSupport);
}

/**
  Get clear cmos status.
**/
BOOLEAN
GetClearCmosStatus()
{
  return FALSE;
}


/**
  description: Set Clear Cmos Status.
**/
VOID
SetClearCmosStatus()
{
  //
  //clear status;
  //
}

/**
  Set the cmos flag.
  @param Flag
**/
VOID
SetCmosFlag(BOOLEAN Flag)
{
   PcdSetBoolS(PcdCmosFlag, Flag);
}

/**
  Get the cmos flag.
**/
BOOLEAN
GetCmosFlag()
{
   return PcdGetBool(PcdCmosFlag);
}

/**
  description: Set Oem Alarm.
**/
VOID
SetOemAlarm(
  VOID
)
{
  //
  //todo
  //
}

