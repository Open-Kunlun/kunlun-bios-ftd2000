## @file
# Brief description of the file's purpose.
#
# Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
# Rights Reserved.
#
# You may not reproduce, distribute, publish, display, perform, modify, adapt,
# transmit, broadcast, present, recite, release, license or otherwise exploit
# any part of this publication in any form, by any means, without the prior
# written permission of Kunlun Technology (Beijing) Co., Ltd..
#
#
##

[Defines]

[LibraryClasses.common]
  OemS3Resumelib|KunlunCommonPkg/Library/OemS3Resumelib/OemS3Resumelib.inf
  DfxSendInfoToCPLDLib| KunlunCommonPkg/Library/DfxSendInfoToCPLDLib/DfxSendInfoToCPLDLib.inf
  KlPlatformConfigLib|KunlunCommonPkg/Library/KlPlatformConfigLib/KlPlatformConfigLib.inf
  ShellCommandLib|ShellPkg/Library/UefiShellCommandLib/UefiShellCommandLib.inf
  HandleParsingLib|ShellPkg/Library/UefiHandleParsingLib/UefiHandleParsingLib.inf

!if $(TARGET) == RELEASE
  DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
!else
  DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
!endif
  DebugPrintErrorLevelLib|MdePkg/Library/BaseDebugPrintErrorLevelLib/BaseDebugPrintErrorLevelLib.inf

  BaseLib|MdePkg/Library/BaseLib/BaseLib.inf
  SynchronizationLib|MdePkg/Library/BaseSynchronizationLib/BaseSynchronizationLib.inf
  PerformanceLib|MdePkg/Library/BasePerformanceLibNull/BasePerformanceLibNull.inf
  PrintLib|MdePkg/Library/BasePrintLib/BasePrintLib.inf
  PeCoffGetEntryPointLib|MdePkg/Library/BasePeCoffGetEntryPointLib/BasePeCoffGetEntryPointLib.inf
  PeCoffLib|MdePkg/Library/BasePeCoffLib/BasePeCoffLib.inf
  IoLib|MdePkg/Library/BaseIoLibIntrinsic/BaseIoLibIntrinsic.inf
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiDecompressLib.inf
  CpuLib|MdePkg/Library/BaseCpuLib/BaseCpuLib.inf
  !if $(BOARD) == Z706_HTDD208
  PhytiumPowerControlLib|KunlunOemInterfacePkg/Library/PhytiumPowerControlLib/PhytiumPowerControlLib.inf
  !else
  PhytiumPowerControlLib|$(PLATFORM_PACKAGE)/Library/PhytiumPowerControlLib/PhytiumPowerControlLib.inf
  !endif

  PhytiumEcLib|$(GENERAL_PACKAGE)/Library/EcLib/Ec.inf
  KlS3ResumeUnlockHddPeiLib|KunlunModulePkg/Library/KlS3ResumeUnlockHddPeiLib/KlS3ResumeUnlockHddPeiLib.inf
  KlS3SuspendUnlockHddLib|KunlunModulePkg/Library/KlS3SuspendUnlockHddLib/KlS3SuspendUnlockHddLib.inf
  UefiLib|MdePkg/Library/UefiLib/UefiLib.inf
  HobLib|MdePkg/Library/DxeHobLib/DxeHobLib.inf
  UefiRuntimeServicesTableLib|MdePkg/Library/UefiRuntimeServicesTableLib/UefiRuntimeServicesTableLib.inf
  DevicePathLib|MdePkg/Library/UefiDevicePathLib/UefiDevicePathLib.inf
  UefiBootServicesTableLib|MdePkg/Library/UefiBootServicesTableLib/UefiBootServicesTableLib.inf
  DxeServicesTableLib|MdePkg/Library/DxeServicesTableLib/DxeServicesTableLib.inf
  UefiDriverEntryPoint|MdePkg/Library/UefiDriverEntryPoint/UefiDriverEntryPoint.inf
  UefiApplicationEntryPoint|MdePkg/Library/UefiApplicationEntryPoint/UefiApplicationEntryPoint.inf
  UefiHiiServicesLib|MdeModulePkg/Library/UefiHiiServicesLib/UefiHiiServicesLib.inf
  UefiRuntimeLib|Kunlun/KunlunModulePkg/Library/UefiRuntimeLib/UefiRuntimeLib.inf
  FileExplorerLib|MdeModulePkg/Library/FileExplorerLib/FileExplorerLib.inf
  ShellCEntryLib|ShellPkg/Library/UefiShellCEntryLib/UefiShellCEntryLib.inf
!if $(HTTP_BOOT_ENABLE) == TRUE
  HttpLib|NetworkPkg/Library/DxeHttpLib/DxeHttpLib.inf
!endif

!if $(FLASH_LAYOUT_ENABLE) == TRUE
  FlashLayoutLib|KunlunModulePkg/Library/FlashLayoutLib/FlashLayoutLib.inf
!endif

  #
  # Assume everything is fixed at build
  #
  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf

  # 1/123 faster than Stm or Vstm version
  BaseMemoryLib|MdePkg/Library/BaseMemoryLib/BaseMemoryLib.inf
  ArmMmuLib|ArmPkg/Library/ArmMmuLib/ArmMmuBaseLib.inf

  # ARM Architectural Libraries
  ArmLib|ArmPkg/Library/ArmLib/ArmBaseLib.inf
  TimerLib|ArmPkg/Library/ArmArchTimerLib/ArmArchTimerLib.inf
  CacheMaintenanceLib|OverridePkg/edk2/ArmPkg/Library/ArmCacheMaintenanceLib/ArmCacheMaintenanceLib.inf
  DefaultExceptionHandlerLib|ArmPkg/Library/DefaultExceptionHandlerLib/DefaultExceptionHandlerLib.inf
  ArmDisassemblerLib|ArmPkg/Library/ArmDisassemblerLib/ArmDisassemblerLib.inf
  ArmGicLib|ArmPkg/Drivers/ArmGic/ArmGicLib.inf
  ArmGicArchLib|ArmPkg/Library/ArmGicArchLib/ArmGicArchLib.inf
  ArmSmcLib|ArmPkg/Library/ArmSmcLib/ArmSmcLib.inf

  ArmGenericTimerCounterLib|ArmPkg/Library/ArmGenericTimerPhyCounterLib/ArmGenericTimerPhyCounterLib.inf
  ArmWatchdogLib|KunlunModulePkg/Library/ArmWatchdogLib/ArmWatchdogLib.inf
  GenericWatchdogLib|KunlunModulePkg/Library/GenericWatchdogLib/GenericWatchdogLib.inf
  CpuExceptionHandlerLib|ArmPkg/Library/ArmExceptionLib/ArmExceptionLib.inf

  ArmPlatformStackLib|ArmPlatformPkg/Library/ArmPlatformStackLib/ArmPlatformStackLib.inf

  #
  # Uncomment (and comment out the next line) For RealView Debugger. The Standard IO window
  # in the debugger will show load and unload commands for symbols. You can cut and paste this
  # into the command window to load symbols. We should be able to use a script to do this, but
  # the version of RVD I have does not support scripts accessing system memory.
  #
  PeCoffExtraActionLib|MdePkg/Library/BasePeCoffExtraActionLibNull/BasePeCoffExtraActionLibNull.inf

  DebugAgentLib|MdeModulePkg/Library/DebugAgentLibNull/DebugAgentLibNull.inf
  DebugAgentTimerLib|EmbeddedPkg/Library/DebugAgentTimerLibNull/DebugAgentTimerLibNull.inf

  SemihostLib|ArmPkg/Library/SemihostLib/SemihostLib.inf


  AcpiLib|EmbeddedPkg/Library/AcpiLib/AcpiLib.inf
  DynamicAmlLib|DynamicTablesPkg/Library/Common/AmlLib/AmlLib.inf
  FdtLib|EmbeddedPkg/Library/FdtLib/FdtLib.inf

  ShellLib|ShellPkg/Library/UefiShellLib/UefiShellLib.inf
  FileHandleLib|MdePkg/Library/UefiFileHandleLib/UefiFileHandleLib.inf
  SortLib|MdeModulePkg/Library/UefiSortLib/UefiSortLib.inf

  #
  # Secure Boot dependencies
  #
!if $(SECURE_BOOT_ENABLE) == TRUE
  TpmMeasurementLib|SecurityPkg/Library/DxeTpmMeasurementLib/DxeTpmMeasurementLib.inf
  AuthVariableLib|SecurityPkg/Library/AuthVariableLib/AuthVariableLib.inf
  PlatformSecureLib|OvmfPkg/Library/PlatformSecureLib/PlatformSecureLib.inf
!else
  TpmMeasurementLib|MdeModulePkg/Library/TpmMeasurementLibNull/TpmMeasurementLibNull.inf
  AuthVariableLib|MdeModulePkg/Library/AuthVariableLibNull/AuthVariableLibNull.inf
!endif

  VarCheckLib|MdeModulePkg/Library/VarCheckLib/VarCheckLib.inf

  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibFmp/DxeCapsuleLib.inf

  # PL011 UART Driver and Dependency Libraries
  SerialPortLib|KunlunCrPkg/Library/PL011SerialPortLib/PL011SerialPortLib.inf
  PL011UartClockLib|ArmPlatformPkg/Library/PL011UartClockLib/PL011UartClockLib.inf
  PL011UartLib|ArmPlatformPkg/Library/PL011UartLib/PL011UartLib.inf

  # Scsi Requirements
  UefiScsiLib|MdePkg/Library/UefiScsiLib/UefiScsiLib.inf

  # USB Requirements
  UefiUsbLib|MdePkg/Library/UefiUsbLib/UefiUsbLib.inf

  # Networking Requirements
  NetLib|NetworkPkg/Library/DxeNetLib/DxeNetLib.inf
  DpcLib|NetworkPkg/Library/DxeDpcLib/DxeDpcLib.inf
  UdpIoLib|NetworkPkg/Library/DxeUdpIoLib/DxeUdpIoLib.inf
  IpIoLib|NetworkPkg/Library/DxeIpIoLib/DxeIpIoLib.inf

  # Phytium Platform library
  ArmPlatformLib|$(PLATFORM_PACKAGE)/Library/PhytiumPlatformLib/PhytiumPlatformLib.inf

  IntrinsicLib|CryptoPkg/Library/IntrinsicLib/IntrinsicLib.inf
  OpensslLib|CryptoPkg/Library/OpensslLib/OpensslLib.inf
  BaseCryptLib|CryptoPkg/Library/BaseCryptLib/BaseCryptLib.inf
  LogoLib|$(PLATFORM_PACKAGE)/Library/LogoLib/LogoLib.inf
  DxeServicesLib|MdePkg/Library/DxeServicesLib/DxeServicesLib.inf
  BmpSupportLib|MdeModulePkg/Library/BaseBmpSupportLib/BaseBmpSupportLib.inf
  SafeIntLib|MdePkg/Library/BaseSafeIntLib/BaseSafeIntLib.inf
!if $(KUNLUN_SETUP) == FALSE
  UefiBootManagerLib|$(GENERAL_PACKAGE)/Library/UefiBootManagerLib/UefiBootManagerLib.inf
  PlatformBootManagerLib|$(PLATFORM_PACKAGE)/Library/PlatformBootManagerLib/PlatformBootManagerLib.inf
  BootLogoLib|MdeModulePkg/Library/BootLogoLib/BootLogoLib.inf
  NorFlashBaseLib|$(PLATFORM_PACKAGE)/Library/PhytiumSpiNorFlashBaseLib/PhytiumSpiNorFlashBaseLib.inf
!endif

  # SPI Flash library
  NorFlashPlatformLib|$(PLATFORM_PACKAGE)/Library/PhytiumSpiNorFlashLib/PhytiumSpiNorFlashLib.inf
!if $(KUNLUN_SETUP) == TRUE
  FlashLib|KunlunModulePkg/Library/FTFlashLib/FlashLib.inf
!endif
  # Board Specific I2C Library

  # Board Specific I2C Library
  I2CLib|$(PLATFORM_PACKAGE)/Library/I2CLib/I2CLib.inf


  !if $(WARN_ALARM_ENABLE) == TRUE
     WarnAlarmLib|$(PLATFORM_PACKAGE)/Library/WarnAlarmLib/WarnAlarm.inf
  !endif

  # Board Specific RealTimeClock Library
  !if $(I2C_RTC_USE) == TRUE
    RealTimeClockLib|$(PLATFORM_PACKAGE)/Library/Ds1339_RtcLib/Ds1339_RtcLib.inf
  !else
    RealTimeClockLib|$(PLATFORM_PACKAGE)/Library/XgeneRealTimeClockLib/XgeneRealTimeClockLib.inf
  !endif
  TimeBaseLib|EmbeddedPkg/Library/TimeBaseLib/TimeBaseLib.inf

  MemoryAllocationLib|MdePkg/Library/UefiMemoryAllocationLib/UefiMemoryAllocationLib.inf

  DmaLib|EmbeddedPkg/Library/NonCoherentDmaLib/NonCoherentDmaLib.inf
  NonDiscoverableDeviceRegistrationLib|MdeModulePkg/Library/NonDiscoverableDeviceRegistrationLib/NonDiscoverableDeviceRegistrationLib.inf

  ParameterTableLib|$(PLATFORM_PACKAGE)/Library/ParameterTableLib/ParameterTable.inf
!if $(KUNLUN_SETUP) == TRUE
  PciExpressLib|MdePkg/Library/BasePciExpressLib/BasePciExpressLib.inf
  PciLib|MdePkg/Library/BasePciLibPciExpress/BasePciLibPciExpress.inf
  RetainDataLib| KunlunModulePkg/Library/RetainDataLib/RetainDataLib.inf
  DisplayPictureLib|KunlunModulePkg/Library/DisplayPictureLib/DisplayPictureLib.inf

  PhytiumSeLib|$(GENERAL_PACKAGE)/Library/SeLib/Se.inf
  ScpLib|$(GENERAL_PACKAGE)/Library/ScpLib/ScpLib.inf
  RegisterFilterLib|MdePkg/Library/RegisterFilterLibNull/RegisterFilterLibNull.inf
  RngLib|KunlunModulePkg/Library/BaseRngLib/BaseRngLib.inf
  OrderedCollectionLib|MdePkg/Library/BaseOrderedCollectionRedBlackTreeLib/BaseOrderedCollectionRedBlackTreeLib.inf
  VariablePolicyLib|MdeModulePkg/Library/VariablePolicyLib/VariablePolicyLib.inf
  VariablePolicyHelperLib|MdeModulePkg/Library/VariablePolicyHelperLib/VariablePolicyHelperLib.inf
  SecureBootVariableLib|SecurityPkg/Library/SecureBootVariableLib/SecureBootVariableLib.inf
  SecureBootVariableProvisionLib|SecurityPkg/Library/SecureBootVariableProvisionLib/SecureBootVariableProvisionLib.inf
!endif


  #X100 Library
  X100ConfigLib| $(GENERAL_PACKAGE)/Library/X100ConfigLib/X100ConfigLib.inf
  AcpiHelperLib | DynamicTablesPkg/Library/Common/AcpiHelperLib/AcpiHelperLib.inf
  BaseSortLib|MdeModulePkg/Library/BaseSortLib/BaseSortLib.inf
 !if $(STATUSCODE_ENABLE) == TRUE
   OemHookStatusCodeLib|MdeModulePkg/Library/OemHookStatusCodeLibNull/OemHookStatusCodeLibNull.inf
 !endif

[LibraryClasses.common.SEC]
  HobLib|MdePkg/Library/PeiHobLib/PeiHobLib.inf
  MemoryAllocationLib|MdePkg/Library/PeiMemoryAllocationLib/PeiMemoryAllocationLib.inf
  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
  PeiServicesLib|MdePkg/Library/PeiServicesLib/PeiServicesLib.inf
  PeiServicesTablePointerLib|ArmPkg/Library/PeiServicesTablePointerLib/PeiServicesTablePointerLib.inf
  SerialPortLib|KunlunCrPkg/Library/SecPL011SerialPortLib/PL011SerialPortLib.inf

[LibraryClasses.common.PEI_CORE, LibraryClasses.common.PEIM]
  BaseMemoryLib|MdePkg/Library/BaseMemoryLib/BaseMemoryLib.inf
  HobLib|MdePkg/Library/PeiHobLib/PeiHobLib.inf
  MemoryAllocationLib|MdePkg/Library/PeiMemoryAllocationLib/PeiMemoryAllocationLib.inf
  PcdLib|MdePkg/Library/PeiPcdLib/PeiPcdLib.inf
  PeiServicesLib|MdePkg/Library/PeiServicesLib/PeiServicesLib.inf
  PeiServicesTablePointerLib|ArmPkg/Library/PeiServicesTablePointerLib/PeiServicesTablePointerLib.inf
  ExtractGuidedSectionLib|MdePkg/Library/PeiExtractGuidedSectionLib/PeiExtractGuidedSectionLib.inf

[LibraryClasses.common.PEI_CORE]
  PeiCoreEntryPoint|MdePkg/Library/PeiCoreEntryPoint/PeiCoreEntryPoint.inf
  SerialPortLib|KunlunCrPkg/Library/SecPL011SerialPortLib/PL011SerialPortLib.inf
  !if $(PERFORMANCE_ENABLE) == TRUE
    PerformanceLib|MdeModulePkg/Library/PeiPerformanceLib/PeiPerformanceLib.inf
  !endif

  !if $(STATUSCODE_ENABLE) == TRUE
    ReportStatusCodeLib|MdeModulePkg/Library/PeiReportStatusCodeLib/PeiReportStatusCodeLib.inf
  !endif

[LibraryClasses.common.PEIM]
  PeimEntryPoint|MdePkg/Library/PeimEntryPoint/PeimEntryPoint.inf
  MemoryInitPeiLib|ArmPlatformPkg/MemoryInitPei/MemoryInitPeiLib.inf
!if $(KUNLUN_SETUP) == TRUE
  GenericWatchdogLib|KunlunModulePkg/Library/GenericWatchdogLib/GenericWatchdogPei.inf
!endif
  SerialPortLib|KunlunCrPkg/Library/PeiPL011SerialPortLib/PL011SerialPortLib.inf
 !if $(PERFORMANCE_ENABLE) == TRUE
   PerformanceLib|MdeModulePkg/Library/PeiPerformanceLib/PeiPerformanceLib.inf
 !endif

 !if $(STATUSCODE_ENABLE) == TRUE
    ReportStatusCodeLib|MdeModulePkg/Library/PeiReportStatusCodeLib/PeiReportStatusCodeLib.inf
 !endif

[LibraryClasses.common.DXE_CORE]
  HobLib|MdePkg/Library/DxeCoreHobLib/DxeCoreHobLib.inf
  MemoryAllocationLib|MdeModulePkg/Library/DxeCoreMemoryAllocationLib/DxeCoreMemoryAllocationLib.inf
  DxeCoreEntryPoint|MdePkg/Library/DxeCoreEntryPoint/DxeCoreEntryPoint.inf
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiDecompressLib.inf
  PerformanceLib|MdeModulePkg/Library/DxeCorePerformanceLib/DxeCorePerformanceLib.inf
  SerialPortLib|KunlunCrPkg/Library/DxeCorePL011SerialPortLib/PL011SerialPortLib.inf

[LibraryClasses.common.DXE_DRIVER]
  SecurityManagementLib|MdeModulePkg/Library/DxeSecurityManagementLib/DxeSecurityManagementLib.inf
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf
  #
  # PCI
  #
  PciSegmentLib|$(PLATFORM_PACKAGE)/Library/PciSegmentLib/PciSegmentLib.inf
  PciHostBridgeLib|$(PLATFORM_PACKAGE)/Library/PciHostBridgeLib/PciHostBridgeLib.inf

[LibraryClasses.common.UEFI_APPLICATION]
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiTianoCustomDecompressLib.inf
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf

[LibraryClasses.common.UEFI_DRIVER]
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiTianoCustomDecompressLib.inf
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibFmp/DxeRuntimeCapsuleLib.inf

!if $(SECURE_BOOT_ENABLE) == TRUE
  BaseCryptLib|CryptoPkg/Library/BaseCryptLib/RuntimeCryptLib.inf
!endif

 !if $(STATUSCODE_ENABLE) == TRUE
   ReportStatusCodeLib|MdeModulePkg/Library/RuntimeDxeReportStatusCodeLib/RuntimeDxeReportStatusCodeLib.inf
!endif

[LibraryClasses.AARCH64.DXE_RUNTIME_DRIVER]
  #
  # PSCI support in EL3 may not be available if we are not running under a PSCI
  # compliant secure firmware, but since the default VExpress EfiResetSystemLib
  # cannot be supported at runtime (due to the fact that the syscfg MMIO registers
  # cannot be runtime remapped), it is our best bet to get ResetSystem functionality
  # on these platforms.
  #
  EfiResetSystemLib|ArmPkg/Library/ArmPsciResetSystemLib/ArmPsciResetSystemLib.inf
  VariablePolicyLib|MdeModulePkg/Library/VariablePolicyLib/VariablePolicyLibRuntimeDxe.inf

[LibraryClasses.ARM, LibraryClasses.AARCH64]
  #
  # It is not possible to prevent the ARM compiler for generic intrinsic functions.
  # This library provides the instrinsic functions generate by a given compiler.
  # [LibraryClasses.ARM] and NULL mean link this library into all ARM images.
  #
  NULL|ArmPkg/Library/CompilerIntrinsicsLib/CompilerIntrinsicsLib.inf

  # Add support for GCC stack protector
  NULL|MdePkg/Library/BaseStackCheckLib/BaseStackCheckLib.inf


[LibraryClasses.common.UEFI_DRIVER, LibraryClasses.common.UEFI_APPLICATION, LibraryClasses.common.DXE_RUNTIME_DRIVER, LibraryClasses.common.DXE_DRIVER]
  PcdLib|MdePkg/Library/DxePcdLib/DxePcdLib.inf

[PcdsFixedAtBuild.common]
  gPhytiumPlatformTokenSpaceGuid.PcdCpuType|0x660
  #SMBIOS
  gKlSmbiosTokenSpaceGuid.PcdKunlunSmbiosType4ProcessorVersion|L"D2000"
  gKlSmbiosTokenSpaceGuid.PcdKunlunSmbiosType4MaxSpeed|2300
  gKlSmbiosTokenSpaceGuid.PcdKunlunSmbiosType7L1DataSize|0x20
  gKlSmbiosTokenSpaceGuid.PcdKunlunSmbiosType7L1InstructionSize|0x20
  gKlSmbiosTokenSpaceGuid.PcdKunlunSmbiosType7L2Size|0x800 #2M
  gKlSmbiosTokenSpaceGuid.PcdKunlunSmbiosType7L3Size|0x1000
  gKlSmbiosTokenSpaceGuid.PcdKunlunSmbiosType7CPUNUM|1
  #
  # NV Storage PCDs.
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableBase64|0xe00000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableSize|0x00020000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingBase64|0xe20000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingSize|0x00010000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareBase64|0xe30000
!if $(KUNLUN_SETUP) == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize|0x00030000
!else
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize|0x00020000
!endif
  #gEfiMdeModulePkgTokenSpaceGuid.PcdEmuVariableNvModeEnable|TRUE
  # Size of the region used by UEFI in permanent memory (Reserved 64MB)
  gArmPlatformTokenSpaceGuid.PcdSystemMemoryUefiRegionSize|0x04000000

  gArmPlatformTokenSpaceGuid.PcdCPUCoresStackBase|0x29810000
  gArmPlatformTokenSpaceGuid.PcdCPUCorePrimaryStackSize|0x10000
  gKunlunCrPkgTokenSpaceGuid.Serial0RegisterBase|0x28000000
  gKunlunCrPkgTokenSpaceGuid.Serial1RegisterBase|0x28001000
  ## PL011 - Serial Terminal
  gEfiMdeModulePkgTokenSpaceGuid.PcdSerialRegisterBase|0x28001000
  gEfiMdePkgTokenSpaceGuid.PcdUartDefaultReceiveFifoDepth|0
  gArmPlatformTokenSpaceGuid.PL011UartClkInHz|48000000
  gEfiMdePkgTokenSpaceGuid.PcdUartDefaultBaudRate|115200

  #
  #SBSA Watchdog
  #
  gArmTokenSpaceGuid.PcdGenericWatchdogControlBase|0x2800B000
  gArmTokenSpaceGuid.PcdGenericWatchdogRefreshBase|0x2800A000
  gArmTokenSpaceGuid.PcdGenericWatchdogEl2IntrNum|48

  # System IO space
  gPhytiumPlatformTokenSpaceGuid.PcdSystemIoBase|0x20000000
  gPhytiumPlatformTokenSpaceGuid.PcdSystemIoSize|0x20000000

  # System Memory (2GB ~ 4GB - 64MB), the top 64MB is reserved for PBF
  gArmTokenSpaceGuid.PcdSystemMemoryBase|0x80000000
  gArmTokenSpaceGuid.PcdSystemMemorySize|0x70000000

  #
  # Designware PCI Root Complex
  #
  gEmbeddedTokenSpaceGuid.PcdPrePiCpuIoSize|28
  gPhytiumPlatformTokenSpaceGuid.PcdPciConfigBase|0x40000000
  gPhytiumPlatformTokenSpaceGuid.PcdPciConfigSize|0x10000000
  gArmTokenSpaceGuid.PcdPciBusMin|0
  gArmTokenSpaceGuid.PcdPciBusMax|128
  gArmTokenSpaceGuid.PcdPciIoBase|0x01000
  gArmTokenSpaceGuid.PcdPciIoSize|0xf00000
!if $(KUNLUN_SETUP) == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdPciIoTranslation|0x50000000
!else
  gArmTokenSpaceGuid.PcdPciIoTranslation|0x50000000
!endif
  gArmTokenSpaceGuid.PcdPciMmio32Base|0x58000000
  gArmTokenSpaceGuid.PcdPciMmio32Size|0x28000000
!if $(KUNLUN_SETUP) == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdPciMmio32Translation|0x0
!else
  gArmTokenSpaceGuid.PcdPciMmio32Translation|0x0
!endif
  gArmTokenSpaceGuid.PcdPciMmio64Base|0x00000000
  gArmTokenSpaceGuid.PcdPciMmio64Size|0x1000000000
!if $(KUNLUN_SETUP) == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdPciMmio64Translation|0x1000000000
!else
  gArmTokenSpaceGuid.PcdPciMmio64Translation|0x1000000000
!endif
  gEfiMdeModulePkgTokenSpaceGuid.PcdPciDisableBusEnumeration|FALSE

  #
  # SPI Flash Control Register Base Address and Size
  #
  #gPhytiumPlatformTokenSpaceGuid.PcdSpiFlashBase|0x80000000000
  #gPhytiumPlatformTokenSpaceGuid.PcdSpiFlashSize|0x1000000
  #gPhytiumPlatformTokenSpaceGuid.PcdSpiControllerBase|0x8001fffff00
  #gPhytiumPlatformTokenSpaceGuid.PcdSpiControllerSize|0x100

  gPhytiumPlatformTokenSpaceGuid.PcdSpiFlashBase|0x0
  gPhytiumPlatformTokenSpaceGuid.PcdSpiFlashSize|0x1000000
  gPhytiumPlatformTokenSpaceGuid.PcdSpiControllerBase|0x28014000
  gPhytiumPlatformTokenSpaceGuid.PcdSpiControllerSize|0x1000
  gPhytiumPlatformTokenSpaceGuid.PcdDdrI2cAddress             | {0x50,0x54,0x51,0x55}
  gPhytiumPlatformTokenSpaceGuid.PcdDdrChannelCount           | 0x2
  gPhytiumPlatformTokenSpaceGuid.PcdDdrChannel0DimmI2cAddress | {0x50,0x54}
  gPhytiumPlatformTokenSpaceGuid.PcdDdrChannel1DimmI2cAddress | {0x51,0x55}

!ifdef $(FIRMWARE_VER)
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVersionString|L"$(FIRMWARE_VER)"
!endif

  gEfiMdePkgTokenSpaceGuid.PcdMaximumUnicodeStringLength|1000000
  gEfiMdePkgTokenSpaceGuid.PcdMaximumAsciiStringLength|1000000
  gEfiMdePkgTokenSpaceGuid.PcdMaximumLinkedListLength|1000000
  gEfiMdePkgTokenSpaceGuid.PcdSpinLockTimeout|10000000
  gEfiMdePkgTokenSpaceGuid.PcdDebugClearMemoryValue|0xAF
  gEfiMdePkgTokenSpaceGuid.PcdPerformanceLibraryPropertyMask|1
  #gEfiMdePkgTokenSpaceGuid.PcdPostCodePropertyMask|0
  gEfiMdePkgTokenSpaceGuid.PcdUefiLibMaxPrintBufferSize|320

!if $(HTTP_BOOT_ENABLE) == TRUE
  gEfiNetworkPkgTokenSpaceGuid.PcdAllowHttpConnections|TRUE
!endif



  # DEBUG_ASSERT_ENABLED       0x01
  # DEBUG_PRINT_ENABLED        0x02
  # DEBUG_CODE_ENABLED         0x04
  # CLEAR_MEMORY_ENABLED       0x08
  # ASSERT_BREAKPOINT_ENABLED  0x10
  # ASSERT_DEADLOOP_ENABLED    0x20

  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0xFF  #[jdzhang-0011]

  #  DEBUG_INIT      0x00000001  // Initialization
  #  DEBUG_WARN      0x00000002  // Warnings
  #  DEBUG_LOAD      0x00000004  // Load events
  #  DEBUG_FS        0x00000008  // EFI File system
  #  DEBUG_POOL      0x00000010  // Alloc & Free's
  #  DEBUG_PAGE      0x00000020  // Alloc & Free's
  #  DEBUG_INFO      0x00000040  // Verbose
  #  DEBUG_DISPATCH  0x00000080  // PEI/DXE Dispatchers
  #  DEBUG_VARIABLE  0x00000100  // Variable
  #  DEBUG_BM        0x00000400  // Boot Manager
  #  DEBUG_BLKIO     0x00001000  // BlkIo Driver
  #  DEBUG_NET       0x00004000  // SNI Driver
  #  DEBUG_UNDI      0x00010000  // UNDI Driver
  #  DEBUG_LOADFILE  0x00020000  // UNDI Driver
  #  DEBUG_EVENT     0x00080000  // Event messages
  #  DEBUG_GCD       0x00100000  // Global Coherency Database changes
  #  DEBUG_CACHE     0x00200000  // Memory range cachability changes
  #  DEBUG_VERBOSE   0x00400000  // Detailed debug messages that may
  #  DEBUG_ERROR     0x80000000  // Error
  gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x80000046

  #  REPORT_STATUS_CODE_PROPERTY_PROGRESS_CODE_ENABLED          0x00000001
  #  REPORT_STATUS_CODE_PROPERTY_ERROR_CODE_ENABLED             0x00000002
  #  REPORT_STATUS_CODE_PROPERTY_DEBUG_CODE_ENABLED             0x00000004
  gEfiMdePkgTokenSpaceGuid.PcdReportStatusCodePropertyMask|0x07

  gEmbeddedTokenSpaceGuid.PcdEmbeddedAutomaticBootCommand|""
  gEmbeddedTokenSpaceGuid.PcdEmbeddedDefaultTextColor|0x07
  gEmbeddedTokenSpaceGuid.PcdEmbeddedMemVariableStoreSize|0x10000
  # 20ms
  gEmbeddedTokenSpaceGuid.PcdTimerPeriod|200000


  #
  # Optional feature to help prevent EFI memory map fragments
  # Turned on and off via: PcdPrePiProduceMemoryTypeInformationHob
  # Values are in EFI Pages (4K). DXE Core will make sure that
  # at least this much of each type of memory can be allocated
  # from a single memory range. This way you only end up with
  # maximum of two fragements for each type in the memory map
  # (the memory used, and the free memory that was prereserved
  # but not used).
  #
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiACPIReclaimMemory|0
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiACPIMemoryNVS|0
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiReservedMemoryType|0
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiRuntimeServicesData|80
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiRuntimeServicesCode|65
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiBootServicesCode|400
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiBootServicesData|20000
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiLoaderCode|20
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiLoaderData|0

  # RunAxf support via Dynamic Shell Command protocol
  # We want to use the Shell Libraries but don't want it to initialise
  # automatically. We initialise the libraries when the command is called by the
  # Shell.
  gEfiShellPkgTokenSpaceGuid.PcdShellLibAutoInitialize|FALSE

  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange|FALSE
  #gEfiIntelFrameworkModulePkgTokenSpaceGuid.PcdShellFile|{ 0x83, 0xA5, 0x04, 0x7C, 0x3E, 0x9E, 0x1C, 0x4F, 0xAD, 0x65, 0xE0, 0x52, 0x68, 0xD0, 0xB4, 0xD1 }

!if $(SECURE_BOOT_ENABLE) == TRUE
  # override the default values from SecurityPkg to ensure images from all sources are verified in secure boot
  gEfiSecurityPkgTokenSpaceGuid.PcdOptionRomImageVerificationPolicy|0x04
  gEfiSecurityPkgTokenSpaceGuid.PcdFixedMediaImageVerificationPolicy|0x04
  gEfiSecurityPkgTokenSpaceGuid.PcdRemovableMediaImageVerificationPolicy|0x04
!endif

!if $(SECURE_BOOT_ENABLE) == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxVariableSize|0x10000
!else
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxVariableSize|0x4000
!endif

  # Default platform supported RFC 4646 languages: English & French & Chinese Simplified.
  # Default Value of PlatformLangCodes Variable.
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLangCodes|"en-US;zh-Hans"

  # Default current RFC 4646 language: Chinese Simplified.
  # Default Value of PlatformLang Variable.
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLang|"en-US"
  #gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLang|"zh-Hans"
  gEfiMdePkgTokenSpaceGuid.PcdDefaultTerminalType|4
  #
  # ACPI Table Version
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiExposedTableVersions|0x20
  gArmPlatformTokenSpaceGuid.PL011UartInterrupt|39

  gEfiMdeModulePkgTokenSpaceGuid.PcdSrIovSupport|FALSE

!if $(KUNLUN_SETUP) == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdBootManagerMenuFile                |{ 0xE6, 0x31, 0xD0, 0xBB, 0xD8, 0x97, 0x6E, 0x4C, 0x8C, 0x02, 0x1F, 0xD7, 0x65, 0x96, 0x66, 0x77 }
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultLangCodes             |"engchiengchi"
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLangCodes     |"zh-CN;en-US;"
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLang          |"zh-CN"
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVendor                     |L"KunlunTech"
  gKlModulePkgTokenSpaceGuid.PcdSecurityLinkToFormsetGuid|{GUID("ad77a8e6-a41c-4cdf-99ba-f833d391e4ec")}
!endif

  gEfiMdePkgTokenSpaceGuid.PcdPciExpressBaseAddress|0x40000000
  !if $(GMAC_SELECT) == 0
    gEfiPhytiumPlatformTokenSpaceGuid.PcdGMACxx|0x0
  !elseif $(GMAC_SELECT) == 1
    gEfiPhytiumPlatformTokenSpaceGuid.PcdGMACxx|0x1
  !elseif $(GMAC_SELECT) == 2
    gEfiPhytiumPlatformTokenSpaceGuid.PcdGMACxx|0x2
  !elseif $(GMAC_SELECT) == 3
    gEfiPhytiumPlatformTokenSpaceGuid.PcdGMACxx|0x3
  !else

  !endif

[PcdsDynamicDefault.common.DEFAULT]

    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|1024
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|768
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|128
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|40

    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoHorizontalResolution|1024
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoVerticalResolution|768
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutColumn|128
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutRow|40

   gEfiMdeModulePkgTokenSpaceGuid.PcdPcieResizableBarSupport|TRUE

[PcdsDynamicExDefault.common.DEFAULT]
   gEfiKunlunBdsPkgTokenSpaceGuid.PcdSetupYearMin|1900
   gEfiKunlunBdsPkgTokenSpaceGuid.PcdSetupYearMax|2099
   gKunlunCrPkgTokenSpaceGuid.PcdCrPortAddress|0x28001000

[PcdsFeatureFlag.common]
  # If TRUE, Graphics Output Protocol will be installed on virtual handle created by ConsplitterDxe.
  # It could be set FALSE to save size.
  gEfiMdeModulePkgTokenSpaceGuid.PcdConOutGopSupport|TRUE

  # Force the UEFI GIC driver to use GICv2 legacy mode. To use
  # GICv3 without GICv2 legacy in UEFI, the ARM Trusted Firmware needs
  # to configure the Non-Secure interrupts in the GIC Redistributors
  # which is not supported at the moment.
  gArmTokenSpaceGuid.PcdArmGicV3WithV2Legacy|FALSE
  gEfiMdePkgTokenSpaceGuid.PcdComponentNameDisable|FALSE
  gEfiMdePkgTokenSpaceGuid.PcdDriverDiagnosticsDisable|TRUE
  gEfiMdePkgTokenSpaceGuid.PcdComponentName2Disable|TRUE
  gEfiMdePkgTokenSpaceGuid.PcdDriverDiagnostics2Disable|TRUE

  #
  # Control what commands are supported from the UI
  # Turn these on and off to add features or save size
  #
  gEmbeddedTokenSpaceGuid.PcdEmbeddedMacBoot|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedDirCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedHobCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedHwDebugCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedPciDebugCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedIoEnable|FALSE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedScriptCmd|FALSE

  gEmbeddedTokenSpaceGuid.PcdCacheEnable|TRUE

  gEmbeddedTokenSpaceGuid.PcdPrePiProduceMemoryTypeInformationHob|TRUE

  gEfiMdeModulePkgTokenSpaceGuid.PcdTurnOffUsbLegacySupport|TRUE

  # Use the Vector Table location in CpuDxe. We will not copy the Vector Table at PcdCpuVectorBaseAddress
  gArmTokenSpaceGuid.PcdRelocateVectorTable|FALSE

  # Indicates if EFI 1.1 ISO 639-2 language supports are obsolete
  #   TRUE  - Deprecate global variable LangCodes.
  #   FALSE - Does not deprecate global variable LangCodes.
  # Deprecate Global Variable LangCodes.
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultLangDeprecate|TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdInstallAcpiSdtProtocol|TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdPciBusHotplugDeviceSupport|FALSE

[Components.common]
  #
  # PCD database
  #
  MdeModulePkg/Universal/PCD/Dxe/Pcd.inf {
    <LibraryClasses>
      SerialPortLib|KunlunCrPkg/Library/DxeCorePL011SerialPortLib/PL011SerialPortLib.inf
  }

  ShellPkg/DynamicCommand/TftpDynamicCommand/TftpDynamicCommand.inf
  ShellPkg/DynamicCommand/DpDynamicCommand/DpDynamicCommand.inf
  ShellPkg/Application/Shell/Shell.inf{
   <LibraryClasses>
      ShellCommandLib|ShellPkg/Library/UefiShellCommandLib/UefiShellCommandLib.inf
      NULL|ShellPkg/Library/UefiShellLevel2CommandsLib/UefiShellLevel2CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellLevel1CommandsLib/UefiShellLevel1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellLevel3CommandsLib/UefiShellLevel3CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellDriver1CommandsLib/UefiShellDriver1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellAcpiViewCommandLib/UefiShellAcpiViewCommandLib.inf
      NULL|ShellPkg/Library/UefiShellDebug1CommandsLib/UefiShellDebug1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellInstall1CommandsLib/UefiShellInstall1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellNetwork1CommandsLib/UefiShellNetwork1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellNetwork2CommandsLib/UefiShellNetwork2CommandsLib.inf
      #NULL|ShellPkg/Library/UefiShellCapsuleCommandsLib/UefiCapsuleCommandsLib.inf
      NULL|$(PLATFORM_PACKAGE)/Library/BiosUpdate/BiosUpdate.inf
      HandleParsingLib|ShellPkg/Library/UefiHandleParsingLib/UefiHandleParsingLib.inf
      PrintLib|MdePkg/Library/BasePrintLib/BasePrintLib.inf
      BcfgCommandLib|ShellPkg/Library/UefiShellBcfgCommandLib/UefiShellBcfgCommandLib.inf
  }
  #ShellBinPkg/UefiShell/UefiShell.inf
  #
  #
  # PEI Phase modules
  #
 #
  # PEI Phase modules
  #
  ArmPkg/Drivers/CpuPei/CpuPei.inf
  KunlunModulePkg/Drivers/MemoryInitPei/MemoryInitPeim.inf
  MdeModulePkg/Core/Pei/PeiMain.inf
  MdeModulePkg/Universal/PCD/Pei/Pcd.inf {
    <LibraryClasses>
      PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
  }
  MdeModulePkg/Universal/FaultTolerantWritePei/FaultTolerantWritePei.inf
  MdeModulePkg/Universal/Variable/Pei/VariablePei.inf
  MdeModulePkg/Core/DxeIplPeim/DxeIpl.inf {
    <LibraryClasses>
      NULL|MdeModulePkg/Library/LzmaCustomDecompressLib/LzmaCustomDecompressLib.inf
  }
  $(PLATFORM_PACKAGE)/PlatformPei/PlatformPeim.inf


  #
  # DXE
  #Dxe core entry
  #
  MdeModulePkg/Core/Dxe/DxeMain.inf {
    <LibraryClasses>
      PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
      NULL|MdeModulePkg/Library/DxeCrc32GuidedSectionExtractLib/DxeCrc32GuidedSectionExtractLib.inf
  }

  #DXE driver
  MdeModulePkg/Core/RuntimeDxe/RuntimeDxe.inf
  MdeModulePkg/Universal/CapsuleRuntimeDxe/CapsuleRuntimeDxe.inf
!if $(USE_SPI_FLASH) == TRUE
  MdeModulePkg/Universal/Variable/RuntimeDxe/VariableRuntimeDxe.inf {
    <LibraryClasses>
      NULL|MdeModulePkg/Library/VarCheckUefiLib/VarCheckUefiLib.inf
  }
!else
  MdeModulePkg/Universal/Variable/EmuRuntimeDxe/EmuVariableRuntimeDxe.inf
!endif
  MdeModulePkg/Universal/FaultTolerantWriteDxe/FaultTolerantWriteDxe.inf
  EmbeddedPkg/ResetRuntimeDxe/ResetRuntimeDxe.inf
  EmbeddedPkg/RealTimeClockRuntimeDxe/RealTimeClockRuntimeDxe.inf
  #
  # Common Arm Timer and Gic Components
  #
  ArmPkg/Drivers/CpuDxe/CpuDxe.inf
!if $(PLATFORM_NAME) == PhytiumD2000
  ArmPkg/Drivers/ArmGic/ArmGicDxe.inf
!else
  $(PLATFORM_PACKAGE)/Drivers/ArmGic/ArmGicDxe.inf
!endif
  EmbeddedPkg/MetronomeDxe/MetronomeDxe.inf
  ArmPkg/Drivers/TimerDxe/TimerDxe.inf
  !if $(I2C_RTC_USE) == TRUE
   $(PLATFORM_PACKAGE)/Drivers/GetSetRtc/GetSetRtc.inf
  !endif
  #
  # security system
  #
  MdeModulePkg/Universal/SecurityStubDxe/SecurityStubDxe.inf {
    <LibraryClasses>
      NULL|SecurityPkg/Library/DxeImageVerificationLib/DxeImageVerificationLib.inf
      !if $(TCM12_ENABLE) == TRUE
        NULL|KunlunSecurityPkg/Library/DxeTcmMeasureBootLib/DxeTcmMeasureBootLib.inf
      !endif

      !if $(TCM2_ENABLE) == TRUE
        NULL|KunlunSecurityPkg/Library/DxeTcm2MeasureBootLib/DxeTcm2MeasureBootLib.inf
      !endif
  }

  #
  #network,  mod for https boot.
  #
  !include NetworkPkg/Network.dsc.inc
#GMAC

!if $(GMAC_ENABLE) == TRUE
    $(PLATFORM_PACKAGE)/Drivers/DwEmacSnpDxe/DwEmacSnpDxe.inf
!endif
  #
  # Common Console Components
  #
  # ConIn,ConOut,StdErr
  MdeModulePkg/Universal/Console/ConPlatformDxe/ConPlatformDxe.inf
!if $(KUNLUN_SETUP) == FALSE
  $(GENERAL_PACKAGE)/Console/ConSplitterDxe/ConSplitterDxe.inf
  $(GENERAL_PACKAGE)/Console/GraphicsConsoleDxe/GraphicsConsoleDxe.inf
!else
  MdeModulePkg/Universal/Console/ConSplitterDxe/ConSplitterDxe.inf
!endif

  KunlunModulePkg/Drivers/PlatformBootManagerDxe/PlatformBootManagerDxe.inf
!if $(DISABLE_SPCR_TABLE)
  EmbeddedPkg/Drivers/ConsolePrefDxe/ConsolePrefDxe.inf
!endif



  #
  # hii database init
  #
!if $(KUNLUN_SETUP) == FALSE
  MdeModulePkg/Universal/HiiDatabaseDxe/HiiDatabaseDxe.inf
!endif
  #
  # FAT filesystem + GPT/MBR partitioning
  #
  MdeModulePkg/Universal/Disk/DiskIoDxe/DiskIoDxe.inf
  MdeModulePkg/Universal/Disk/PartitionDxe/PartitionDxe.inf
  MdeModulePkg/Universal/Disk/UnicodeCollation/EnglishDxe/EnglishDxe.inf
  FatPkg/EnhancedFatDxe/Fat.inf
  #
  # Flash
  #
!if $(SECURE_BOOT_ENABLE) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/NorFlashDxe/NorFlashAuthenticatedDxe.inf
!else
  $(PLATFORM_PACKAGE)/Drivers/NorFlashDxe/NorFlashDxe.inf
!endif
!if $(FLASH_LAYOUT_ENABLE) == TRUE
   KunlunCommonPkg/FlashLayoutPei/FlashLayoutPei.inf
!endif
  #
  # Generic Watchdog Timer
  #
  ArmPkg/Drivers/GenericWatchdogDxe/GenericWatchdogDxe.inf



  #
  # SeTempAndId
  #
!if $(SE_INFO_ENABLE) == TRUE
  $(GENERAL_PACKAGE)/Driver/TempAndIdDxe/TempAndIdDxe.inf
!endif
  # Usb Support
  #
!if $(USB_ENABLE) == TRUE
  MdeModulePkg/Bus/Pci/UhciDxe/UhciDxe.inf
  MdeModulePkg/Bus/Pci/EhciDxe/EhciDxe.inf
  MdeModulePkg/Bus/Pci/XhciDxe/XhciDxe.inf
  MdeModulePkg/Bus/Usb/UsbBusDxe/UsbBusDxe.inf
  MdeModulePkg/Bus/Usb/UsbKbDxe/UsbKbDxe.inf
  MdeModulePkg/Bus/Usb/UsbMouseDxe/UsbMouseDxe.inf
  MdeModulePkg/Bus/Usb/UsbMassStorageDxe/UsbMassStorageDxe.inf
!endif

  #
  # IDE/AHCI Support
  #
  MdeModulePkg/Bus/Pci/SataControllerDxe/SataControllerDxe.inf
  MdeModulePkg/Bus/Ata/AtaBusDxe/AtaBusDxe.inf
  MdeModulePkg/Bus/Scsi/ScsiBusDxe/ScsiBusDxe.inf
  MdeModulePkg/Bus/Scsi/ScsiDiskDxe/ScsiDiskDxe.inf
  MdeModulePkg/Bus/Ata/AtaAtapiPassThru/AtaAtapiPassThru.inf
  #
  # PCI Support
  #
  ArmPkg/Drivers/ArmPciCpuIo2Dxe/ArmPciCpuIo2Dxe.inf
  MdeModulePkg/Bus/Pci/PciHostBridgeDxe/PciHostBridgeDxe.inf
  $(PLATFORM_PACKAGE)/Bus/Pci/PciBusDxe/PciBusDxe.inf

  #
  # The following 2 module perform the same work except one operate variable.
  # Only one of both should be put into fdf.
  #
  MdeModulePkg/Universal/MonotonicCounterRuntimeDxe/MonotonicCounterRuntimeDxe.inf

  #
  # NVME Support
  #
  MdeModulePkg/Bus/Pci/NvmExpressDxe/NvmExpressDxe.inf



  #
  #Graphics Support
  #
!if $(GP101_SUPPORT) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/Gp101Dxe/UefiGmi.inf
!endif
!if $(SM768) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/Sm768/Smi768Dxe.inf
  !endif
!if $(JM7200_SUPPORT) == TRUE
  !if ($(BOARD) == "Z706_HTDD208")
    $(PLATFORM_PACKAGE)/Drivers/Jm7200/JmVideoDxe.inf
  !else
    $(PLATFORM_PACKAGE)/Drivers/Jm7200/JmVideoDxe.inf
  !endif
!endif
  #
  #Ps2 Keyboard Support
  #
!if $(PS2_SUPPORT) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/Ps2KeyboardDxe/Ps2keyboardDxe.inf
  $(PLATFORM_PACKAGE)/Drivers/LpcDxe/LpcDxe.inf
!endif
!if $(X100_PS2_KEYBOARD_ENABLE) == TRUE
  $(GENERAL_PACKAGE)/Driver/X100Ps2KeyboardDxe/Ps2keyboardDxe.inf
!endif
  # phytium platform
  $(PLATFORM_PACKAGE)/Drivers/PlatformDxe/PlatformDxe.inf
  # ACPI
!if $(ACPI_SUPPORT) == TRUE
  MdeModulePkg/Universal/Acpi/AcpiTableDxe/AcpiTableDxe.inf
  $(PLATFORM_PACKAGE)/Drivers/AcpiPlatformDxe/AcpiPlatformDxe.inf
  $(PLATFORM_PACKAGE)/Drivers/AcpiTables/AcpiTables.inf
  MdeModulePkg/Universal/Acpi/FirmwarePerformanceDataTableDxe/FirmwarePerformanceDxe.inf {
    <LibraryClasses>
      LockBoxLib|KunlunModulePkg/Library/LockBoxLib/LockBoxDxeLib.inf
  }
!endif

  $(PLATFORM_PACKAGE)/Drivers/Pptt/Pptt.inf

  #
  # SMBIOS
  #
  MdeModulePkg/Universal/SmbiosDxe/SmbiosDxe.inf
  #//[jiangfengzhang-0004-begin]
  #KunlunCommonPkg/Drivers/PowerManageDxe/PowerManageDxe.inf
  #//[jiangfengzhang-0004-end]
  #KunlunCommonPkg/Drivers/LpcConfig/LpcConfigInit.inf
!if $(USE_SCPI) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/ScpiDxe/ScpiDxe.inf
!endif
  $(PLATFORM_PACKAGE)/Drivers/SpiFlash/SpiDxe.inf

  MdeModulePkg/Universal/DevicePathDxe/DevicePathDxe.inf
!if $(KUNLUN_SETUP) == FALSE
  MdeModulePkg/Universal/BdsDxe/BdsDxe.inf

  MdeModulePkg/Application/BootManagerMenuApp/BootManagerMenuApp.inf

  #
  #Pcie Config Setup Formset
  #
  $(PLATFORM_PACKAGE)/setup/PcieConfigDxe/PcieConfigUiDxe.inf
  #
  #Cpu Config Setup Formset
  #
  $(PLATFORM_PACKAGE)/setup/CpuConfigDxe/CpuConfigUiDxe.inf
  $(GENERAL_PACKAGE)/Setup/DisplayEngineDxe/DisplayEngineDxe.inf
  $(GENERAL_PACKAGE)/Setup/SetupBrowserDxe/SetupBrowserDxe.inf
  $(GENERAL_PACKAGE)/Setup/UiApp/UiApp.inf
  $(GENERAL_PACKAGE)/Setup/DriverSampleDxe/DriverSampleDxe.inf {
      <LibraryClasses>
      #NULL|MdeModulePkg/Library/DeviceManagerUiLib/DeviceManagerUiLib.inf
      #NULL|MdeModulePkg/Library/BootManagerUiLib/BootManagerUiLib.inf
      NULL|$(GENERAL_PACKAGE)/Setup/Library/BootMaintenanceManagerUiLib/BootMaintenanceManagerUiLib.inf
      NULL|$(PLATFORM_PACKAGE)/Library/OemConfigUiLib/OemConfigUiLib.inf
      NULL|$(GENERAL_PACKAGE)/Setup/Library/AdvancedConfigUiLib/AdvancedConfigUiLib.inf
      #NULL|$(PLATFORM_PACKAGE)/Library/SecurityUiLib/SecurityConfigUiLib.inf
  }
!endif


  KunlunModulePkg/Drivers/KlS3SuspendUnlockHddMemInitDxe/KlS3SuspendUnlockHddMemInitDxe.inf

