/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef KL_BOARD_CONFIG_H_
#define KL_BOARD_CONFIG_H_

#define NIC_INFO_ENABLE   1
#define USB_INFO_ENABLE   1
#define SATA_INFO_ENABLE  1
#define NVME_INFO_ENABLE  1
#define PCI_INFO_ENABLE   0


#endif
