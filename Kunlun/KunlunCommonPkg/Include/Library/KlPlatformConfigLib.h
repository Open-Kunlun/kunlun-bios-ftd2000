/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef KL_PLATFORM_CONFIG_LIB_
#define KL_PLATFORM_CONFIG_LIB_

BOOLEAN
KeepPasswordWhenUpdate();

BOOLEAN
CompareOldPasswordSupport();

BOOLEAN
PrecisePasswordHelpInformationSupport();

BOOLEAN
GetClearCmosStatus();

VOID
SetClearCmosStatus();

VOID
SetOemAlarm();

VOID
SetCmosFlag(BOOLEAN Flag);

BOOLEAN
GetCmosFlag();

#endif //__KL_SYSTEM_SLOT_LIB__
