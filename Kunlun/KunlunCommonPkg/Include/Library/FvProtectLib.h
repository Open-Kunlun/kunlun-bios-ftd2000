/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/


#ifndef FV_PROTECT_LIB_
#define FV_PROTECT_LIB_

#include <Uefi/UefiBaseType.h>
#include <Library/PhytiumSpiNorFlashLib.h>


/**
  This function is used to turn off write protection during BIOS updates.
**/
VOID
CloseFvProtect (VOID);

/**
  This function is used to protect FV Section.

  @param  Number   The number of rows in the passed in two-dimensional arra.
  @param  Address  The incoming segment address consists of the start address and segment length.

  @retval EFI_INVALID_PARAMETER
  @retval EFI_SUCCESS

**/
EFI_STATUS
EFIAPI
FvSectionProtect (
  IN UINTN                  Number,
  IN UINTN                  (*Address)[2]
);

#endif 