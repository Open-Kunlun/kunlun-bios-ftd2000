/** @file
Brief description of the file's purpose.

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
All Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name: DfxSendInfoToCPLDLib.h

**/

#ifndef DFX_SEND_INFO_TO_CPLD_LIB_H_
#define DFX_SEND_INFO_TO_CPLD_LIB_H_


VOID SentinfoToCPLD(
  IN UINT8 Offset,
  IN UINT8 Data);
VOID
ReadinfofromCPLD(
  UINT8 Offset,
  UINT8 *Data
);
VOID
InitialRTC(VOID);
VOID GetWDTTimesClearNV(VOID);
VOID LedProcessIndicate(UINT8 StateCode);

#endif
