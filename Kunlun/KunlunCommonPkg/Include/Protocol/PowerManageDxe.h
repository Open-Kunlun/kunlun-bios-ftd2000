/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name: PowerManageDxe.h

**/


#ifndef EFI_POWER_MANAGE_DXE_PROTOCOL_H_
#define EFI_POWER_MANAGE_DXE_PROTOCOL_H_


typedef struct _EFI_POWER_MANAGE_DXE_PROTOCOL EFI_POWER_MANAGE_DXE_PROTOCOL;


typedef enum {
  PolicyClose     = 0,
  PolicyOnlyOnce,
  PolicyEveryDay,
  PolicyEveryWeek
} RTC_WAKE_UP_POLICY;

typedef struct {
  UINT16 Year;
  UINT8 Month;
  UINT8 Date;
  UINT8 Week;
  UINT8 Hour;
  UINT8 Min;
  UINT8 Sec;
} RTC_WAKE_UP_DATA;

//[gliu-0024]add-start
#define RTC_EVERY_WEEK_FLAG   (1<<6)
#define RTC_EVERY_DAYS_FLAG     (1<<7)
//[gliu-0024]add-end

typedef
EFI_STATUS
(EFIAPI *EFI_UPDATE_RTC_WAKE_UP_DATA) (
    IN  EFI_POWER_MANAGE_DXE_PROTOCOL    *This,
    IN  RTC_WAKE_UP_POLICY               RtcWakeUpPolicy,
    IN  RTC_WAKE_UP_DATA                 *RtcWakeUpData
);

typedef
EFI_STATUS
(EFIAPI *EFI_UPDATE_PCIE_WAKE_UP_DATA) (
    IN  EFI_POWER_MANAGE_DXE_PROTOCOL    *This
);


struct _EFI_POWER_MANAGE_DXE_PROTOCOL {
  EFI_UPDATE_RTC_WAKE_UP_DATA       UpdateRtcWakeUpData;
  EFI_UPDATE_PCIE_WAKE_UP_DATA      UpdatePcieWakeUpData;
};

extern EFI_GUID gEfiPowerManageProtocolGuid;
#endif

