/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name: FlashLayout.c


Abstract:


Revision History:

**/


#include "FlashLayout.h"
EFI_GUID FlashLayoutHobGuid = EFI_FLASH_LAYOUT_HOB_GUID;
EFI_GUID FlashRomInfoHobGuid = EFI_FLASH_ROM_INFO_HOB_GUID;

FLASH_SECTION FlashLayout[] =
{
  {
      (UINT32)(FixedPcdGet64(PcdFvBaseAddress)),
      (UINT32)(FixedPcdGet32(PcdFvSize)),
      FV_PEI,
      UNPROTECT
  }, //  FV_PEI

  {
      (UINT32)(FixedPcdGet64(PcdBiosEventLogStorageBase)),
      (UINT32)(FixedPcdGet32(PcdBiosEventLogSize)),
      NV_EVENT_LOG,
      UNPROTECT
  }, // NV_EVENT_LOG

  {
      (UINT32)(FixedPcdGet64(PcdKlFlashDmiRegionBase)),
      (UINT32)(FixedPcdGet32(PcdKlFlashDmiRegionSize)),
      SMBIOS,
      PROTECT
  }, //DMI

  {
      (UINT32)(FixedPcdGet64(PcdKlRetainSpaceOffset)),
      (UINT32)(FixedPcdGet32(PcdKlRetainSpaceSize)),
      KL_RETAIN_SPACE,
      PROTECT
  }, //KL_RETAIN_SPACE

  {
      (UINT32)(FixedPcdGet64(PcdFlashNvStorageVariableBase64)),
      (UINT32)(FixedPcdGet32(PcdFlashNvStorageVariableSize)),
      NV_VARIABLE_STORE,
      PROTECT
  }, //NV_VARIABLE_STORE

  {
      (UINT32)(FixedPcdGet64(PcdFlashNvStorageFtwWorkingBase64)),
      (UINT32)(FixedPcdGet32(PcdFlashNvStorageFtwWorkingSize)),
      NV_FTW_WORKING,
      UNPROTECT
  }, //NV_FTW_WORKING

  {
      (UINT32)(FixedPcdGet64(PcdFlashNvStorageFtwSpareBase64)),
      (UINT32)(FixedPcdGet32(PcdFlashNvStorageFtwSpareSize)),
      NV_FTW_SPARE,
      UNPROTECT
  }, //NV_FTW_SPARE

  {
      (UINT32)(FixedPcdGet64(PcdFlashHardwareHashBase)),
      (UINT32)(FixedPcdGet32(PcdFlashHardwareHashSize)),
      OS_ACTIVATION_KEY,
      UNPROTECT
  }, //OS_ACTIVATION_KEY

  {
      (UINT32)(FixedPcdGet64(PcdSpiFlashLpbtBinBase)),
      (UINT32)(FixedPcdGet32(PcdSpiFlashLpbtBinSize)),
      LPBT_BIN,
      PROTECT
  }, //LPBT_BIN
  {
      (UINT32)(FixedPcdGet64(PcdSpiFlashLpbtBinArgsBase)),
      (UINT32)(FixedPcdGet32(PcdSpiFlashLpbtBinArgsSize)),
      LPBT_BIN_ARGUMENTS,
      PROTECT
  } //LPBT_BIN
};

/**
  peim entrypoint.

  @param[in]  FileHandle        The file handle of the file for this peim.
  @param[in]  PeiServices       pointer to the peim services.

  @retval     EFI_SUCCESS.      Opertion is successful.

**/
EFI_STATUS
EFIAPI
PeimInitializeDriver (
  IN EFI_PEI_FILE_HANDLE        FileHandle,
  IN CONST EFI_PEI_SERVICES     **PeiServices
  )
{
  EFI_STATUS Status;
  #if _FLASH_ROM_INFO_SUPPORT_PEI

  EFI_HOB_GUID_TYPE*      FlashRomInfoHob;
  UINTN                   OneHobSize;
  FLASH_ROM_INFO_HOB*     OneHobData;
  OneHobSize = sizeof(EFI_HOB_GUID_TYPE)+sizeof(FLASH_ROM_INFO_HOB);

  Status = PeiServicesCreateHob(EFI_HOB_TYPE_GUID_EXTENSION,OneHobSize,(void**)&FlashRomInfoHob);
  FlashRomInfoHob->Name = FlashRomInfoHobGuid;
  OneHobData = (FLASH_ROM_INFO_HOB*)(FlashRomInfoHob+1);
  OneHobData->FlashSizeInfo = FLASH_ROM_INFO_FLASH_SIZE_INFO;//16M
  OneHobData->SectionSizeInfo = FLASH_ROM_INFO_SECTION_SIZE_INFO;//4KB
  #endif

  UINTN HobSize;
  EFI_HOB_GUID_TYPE* FlashLayoutHob;
  FLASH_SECTION* HobData;
  HobSize = sizeof(EFI_HOB_GUID_TYPE)+sizeof(FlashLayout);
  Status = PeiServicesCreateHob(EFI_HOB_TYPE_GUID_EXTENSION,HobSize,(void**)&FlashLayoutHob);
  DEBUG ((DEBUG_ERROR, "%a %d Status%r \n", __FUNCTION__, __LINE__,Status));
  FlashLayoutHob->Name = FlashLayoutHobGuid;
  HobData = (FLASH_SECTION*)(FlashLayoutHob+1);
  CopyMem(HobData,FlashLayout,sizeof(FlashLayout));
  return EFI_SUCCESS;
}
