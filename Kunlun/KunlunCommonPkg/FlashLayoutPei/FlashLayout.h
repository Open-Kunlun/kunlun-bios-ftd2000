/** @file
Brief description of the file's purpose.

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
All Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

Module Name: FlashLayout.h

**/


#ifndef FLASH_LAYOUT_H_
#define FLASH_LAYOUT_H_

#include <PiPei.h>
#include <Uefi.h>
#include <Library/DebugLib.h>
#include <Library/PeimEntryPoint.h>
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/PcdLib.h>
#include <Library/FlashLayoutLib.h>
#include <Library/HobLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>

typedef struct _FLASH_LAYOUT_HOB
{
    EFI_HOB_GUID_TYPE Header;
    FLASH_SECTION FlashLayout[];
} FLASH_LAYOUT_HOB;


#if _FLASH_ROM_INFO_SUPPORT_PEI

typedef struct _FLASH_ROM_INFO_HOB
{
    UINT32              FlashSizeInfo;
    UINT32              SectionSizeInfo;//Kb
} FLASH_ROM_INFO_HOB;
#endif
#endif
