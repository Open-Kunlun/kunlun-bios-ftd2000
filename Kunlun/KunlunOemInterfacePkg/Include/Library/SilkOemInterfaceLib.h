/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef SILK_OEM_INTERFACE_LIB_H_
#define SILK_OEM_INTERFACE_LIB_H_

#pragma pack (1)
typedef struct {
  CHAR16                      *SlotDesignationStr;
  CHAR16                      *DevicePathStr;
  BOOLEAN                     Match;
}NVME_SLOT_INFO;

typedef struct {
  CHAR16                      *SlotDesignationStr;
  CHAR16                      *DevicePathStr;
  BOOLEAN                     Match;
}SATA_SLOT_INFO;
#pragma pack ()

EFI_STATUS
OemSataSlotTable (
  OUT UINTN                     *SataSlotTableCount,
  OUT SATA_SLOT_INFO            **SataSlotTable
  );

EFI_STATUS
OemNvmeSlotTable (
  OUT UINTN                     *NvmeSlotTableCount,
  OUT NVME_SLOT_INFO            **NvmeSlotTable
  );

#endif
