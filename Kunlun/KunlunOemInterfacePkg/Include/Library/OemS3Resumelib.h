/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/

#ifndef  OEM_S3_RESUMELIB_H_
#define  OEM_S3_RESUMELIB_H_

typedef struct  {
  UINTN SystemOffEntry;
  UINTN SystemResetEntry;
  UINTN SuspendStartEntry;
  UINTN SuspendEndEntry;
  UINTN SuspendFinishEntry;
} OEM_PFDI_VECTORS;


EFI_STATUS
OemS3FuncRegister(
  volatile OEM_PFDI_VECTORS* PfdiV
);

VOID
KunlunSystemReboot();

#endif

