/** @file

Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
Rights Reserved.

You may not reproduce, distribute, publish, display, perform, modify, adapt,
transmit, broadcast, present, recite, release, license or otherwise exploit
any part of this publication in any form, by any means, without the prior
written permission of Kunlun Technology (Beijing) Co., Ltd..

**/
#ifndef DXE_OEM_INTERFACE_LIB_H_
#define DXE_OEM_INTERFACE_LIB_H_

#pragma pack (1)

typedef struct {
  UINT16 VendorId;
  UINT16 DeviceId;
  UINT16 Ssid;
  UINT16 Svid;
} PCI_SSID_TABLE;

#pragma pack ()

EFI_STATUS
OemPciSsidTable (
  OUT UINTN                     *PciSsidTableCount,
  OUT PCI_SSID_TABLE            **PciSsidTable
  );

#endif
