## @file
# KunlunOem Interface package project build description file.
#
#;*************************************************************************************************
#;*  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
#;*  Rights Reserved.
#;*
#;*  You may not reproduce, distribute, publish, display, perform, modify, adapt,
#;*  transmit, broadcast, present, recite, release, license or otherwise exploit
#;*  any part of this publication in any form, by any means, without the prior
#;*  written permission of Kunlun Technology (Beijing) Co., Ltd..
#;*
#;*************************************************************************************************

[Defines]

[PcdsFeatureFlag]


[LibraryClasses]
  OemServicesLib|KunlunOemInterfacePkg/Library/OemServicesLib/OemServicesLib.inf

[LibraryClasses.common.SEC]

[LibraryClasses.common.PEI_CORE]

[LibraryClasses.common.PEIM]


[LibraryClasses.common.DXE_CORE]
  #DxeOemInterfaceLib|KunlunOemInterfacePkg/Library/DxeOemInterfaceLib/DxeOemInterfaceLib.inf

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  #DxeOemInterfaceLib|KunlunOemInterfacePkg/Library/DxeOemInterfaceLib/DxeOemInterfaceLib.inf

[LibraryClasses.common.UEFI_DRIVER]
  #DxeOemInterfaceLib|KunlunOemInterfacePkg/Library/DxeOemInterfaceLib/DxeOemInterfaceLib.inf

[LibraryClasses.common.DXE_DRIVER]
  #DxeOemInterfaceLib|KunlunOemInterfacePkg/Library/DxeOemInterfaceLib/DxeOemInterfaceLib.inf
  #SilkOemInterfaceLib|KunlunOemInterfacePkg/Library/SilkOemInterfaceLib/SilkOemInterfaceLib.inf

[LibraryClasses.common.UEFI_APPLICATION]
  #DxeOemInterfaceLib|KunlunOemInterfacePkg/Library/DxeOemInterfaceLib/DxeOemInterfaceLib.inf

[LibraryClasses.common.USER_DEFINED]

[PcdsFeatureFlag]

[PcdsFixedAtBuild]

[PcdsDynamicDefault]

[PcdsDynamicExDefault]


[Components.$(PEI_ARCH)]


[BuildOptions.common.EDKII.DXE_RUNTIME_DRIVER, BuildOptions.common.EDKII.COMBINED_SMM_DXE, BuildOptions.common.EDKII.DXE_SMM_DRIVER, BuildOptions.common.EDKII.SMM_CORE, BuildOptions.Common.EDK.DXE_RUNTIME_DRIVER]
