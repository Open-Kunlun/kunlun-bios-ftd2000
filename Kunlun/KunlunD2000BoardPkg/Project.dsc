## @file
# KunlunD2000 Project package project build description file.
#
#;*************************************************************************************************
#;*  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
#;*  Rights Reserved.
#;*
#;*  You may not reproduce, distribute, publish, display, perform, modify, adapt,
#;*  transmit, broadcast, present, recite, release, license or otherwise exploit
#;*  any part of this publication in any form, by any means, without the prior
#;*  written permission of Kunlun Technology (Beijing) Co., Ltd..
#;*
#;*************************************************************************************************

################################################################################
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]
  PLATFORM_NAME                  = PhytiumD2000
  PLATFORM_GUID                  = 0de70077-9b3b-43bf-ba38-0ea37d77141b
  PLATFORM_VERSION               = 0.1
  DSC_SPECIFICATION              = 0x00010005
  DEFINE PROJECT_PACKAGE         = KunlunD2000BoardPkg
  OUTPUT_DIRECTORY               = Build/$(PROJECT_PACKAGE)
  SUPPORTED_ARCHITECTURES        = AARCH64
  BUILD_TARGETS                  = DEBUG|RELEASE
  SKUID_IDENTIFIER               = DEFAULT
  FLASH_DEFINITION               = $(PROJECT_PACKAGE)/Project.fdf
  PLATFORM_PACKAGE               = PhytiumPkg/PhytiumD2000Pkg
  GENERAL_PACKAGE                = PhytiumPkg/PhytiumGeneral

  DEFINE NETWORK_ENABLE          = FALSE
  DEFINE NETWORK_IP6_ENABLE      = FALSE
  DEFINE HTTP_BOOT_ENABLE        = FALSE
  DEFINE USE_NETWORKPKG_PXE      = FALSE
  DEFINE ACPI_SUPPORT            = TRUE
  DEFINE PCI_ENABLE              = TRUE
  DEFINE USE_SPI_FLASH           = TRUE
  DEFINE PXE_ENABLE              = FALSE
  DEFINE PS2_SUPPORT             = FALSE
  DEFINE GMAC_ENABLE             = TRUE
  DEFINE GP101_SUPPORT           = FALSE
  DEFINE USB_ENABLE              = TRUE
  DEFINE JM7200_SUPPORT          = FALSE
  DEFINE I2C_RTC_USE             = TRUE
  DEFINE USE_SCPI                = TRUE
  DEFINE SM768                   = FALSE
  DEFINE SECURE_BOOT_ENABLE      = FALSE
  DEFINE WARN_ALARM_ENABLE       = FALSE
  DEFINE SE_INFO_ENABLE          = FALSE
  DEFINE DEBUG_ENABLE            = FALSE
  DEFINE FW_SOFT_POWEROFF        = FALSE
  DEFINE SCTO_DXE                = FALSE
  DISABLE_SPCR_TABLE             = FALSE

  DEFINE FLASH_LAYOUT_ENABLE     = TRUE

  #config when use hanwei board
  DEFINE HANWEI_ENABLE           = FALSE
  DEFINE X100_GOP_ENABLE         = TRUE
  DEFINE X100_PS2_KEYBOARD_ENABLE    =  FALSE

  DEFINE PERFORMANCE_ENABLE      = TRUE
  DEFINE STATUSCODE_ENABLE       = TRUE
  DEFINE KUNLUN_CR_SETUP         = TRUE
  DEFINE KUNLUN_SETUP            = TRUE
  DEFINE X100_UPDATE_FW          = TRUE
  DEFINE S3_RESUME_UNLOCK_HDD    = FALSE
  #Requirement of version:XC_REG-xinchuang requirement;BASE_REQ-basic requirement
  #DEFINE VERSION_REQUIRE         = XC_REQ
  DEFINE VERSION_REQUIRE         = BASE_REQ
  DEFINE EC_UPDATE_FW            = FALSE
  DEFINE TCM_SECURITY            = FALSE
  DEFINE TCM_DEBUG_HARDWARE      = FALSE

  #Choose whether to open the key help information
  DEFINE KEY_HELP_INORMATION     = KEY_HELP_INORMATION_OPEN

  #Select the resolution default, the optional options are
  #RES_640_480，RES_1024_768，RES_1280_720，RES_1920_1080 and RES_1600_1200
  DEFINE DEFAULT_RESOLUTION      = RES_1024_768

  DEFINE BOARD                   = DEMO

  #DEFINE BOARD                    = HWQS
  #DEFINE BOARD                   = DOMESTIC
  #DEFINE BOARD                   = NOTEBOOK_V1
  #DEFINE BOARD                   = NOTEBOOK_V2
#tian gu D2000 Board
  #DEFINE BOARD                   = DT_D2008_2101
#706 D2000 Board
  #DEFINE BOARD                   = Z706_HTDD208
!if $(BOARD) == "DEMO"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = FALSE
  DEFINE I2C_RTC_USE             = TRUE
!elseif $(BOARD) == "DT_D2008_2101"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = FALSE
  DEFINE GMAC_ENABLE             = TRUE
  DEFINE GMAC_SELECT             = 3
  DEFINE I2C_RTC_USE             = TRUE
!elseif $(BOARD) == "Z706_HTDD208"
  HDA_SUPPORT             = FALSE
  SD3068                  = FALSE
  SETUP_RESOLUTION        = RES_1024_768
  GMAC_ENABLE             = TRUE
  GMAC_SELECT             = 2
  I2C_RTC_USE             = TRUE
  JM7200_SUPPORT          = TRUE
  EC_UPDATE_FW            = TRUE
  TCM_DEBUG_HARDWARE      = FALSE
  TCM_SECURITY            = TRUE
!elseif $(BOARD) == "HWQS"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = FALSE
  DEFINE GMAC_ENABLE             = TRUE
  DEFINE GMAC_SELECT             = 2
!elseif $(BOARD) == "DOMESTIC"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = TRUE
!elseif $(BOARD) == "NOTEBOOK_V1"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = FALSE
!elseif $(BOARD) == "NOTEBOOK_V2"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = FALSE
!endif
  DEFINE CAPSULE_ENABLE      = TRUE
  DEFINE RECOVERY_ENABLE     = FALSE



# !if $(TCM_SECURITY) == TRUE
#   !include Kunlun/KunlunSecurityPkg/Tcm.definition.inc
# !endif

!if $(KUNLUN_SETUP) == TRUE
  !include ../KunlunBdsPkg/Package.dsc
  !include ../KunlunJsonExPkg/Package.dsc
  !include ../KunlunOemInterfacePkg/Package.dsc
!endif
!if $(KUNLUN_CR_SETUP) == TRUE
  !include ../KunlunCrPkg/Package.dsc
!endif

  !include ../KunlunSmbiosPkg/Package.dsc
  !include ../KunlunCommonPkg/Package.dsc

  !include ../KunlunCapsulePkg/Package.dsc



# !if $(TCM_SECURITY) == TRUE
#   !include Kunlun/KunlunSecurityPkg/Tcm.dsc.inc
# !endif

[LibraryClasses.common]
  CustomizedDisplayLib|$(GENERAL_PACKAGE)/Setup/Library/CustomizedDisplayLib/CustomizedDisplayLib.inf

  ReportStatusCodeLib|MdeModulePkg/Library/DxeReportStatusCodeLib/DxeReportStatusCodeLib.inf
  OpensslLib|CryptoPkg/Library/OpensslLib/OpensslLib.inf
  TlsLib|CryptoPkg/Library/TlsLib/TlsLib.inf
!if ($(TCM_DEBUG_HARDWARE) == TRUE) || ($(TCM_SECURITY) == TRUE)
  Tcm2DeviceLib|$(PROJECT_PACKAGE)/Library/SPI0TcmLib/Tcm2DeviceLibDTcm.inf
!endif
  PostCodeLib|MdePkg/Library/BasePostCodeLibDebug/BasePostCodeLibDebug.inf

[LibraryClasses.common.SEC]

[LibraryClasses.common.PEI_CORE, LibraryClasses.common.PEIM]
  ReportStatusCodeLib|MdePkg/Library/BaseReportStatusCodeLibNull/BaseReportStatusCodeLibNull.inf

[LibraryClasses.common.PEI_CORE]

[LibraryClasses.common.PEIM]
  PlatformPeiLib|$(PLATFORM_PACKAGE)/PlatformPei/PlatformPeiLib.inf

[LibraryClasses.common.DXE_CORE]

[LibraryClasses.common.DXE_DRIVER]

[LibraryClasses.common.UEFI_APPLICATION]

[LibraryClasses.common.UEFI_DRIVER]

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibFmp/DxeRuntimeCapsuleLib.inf

!if $(SECURE_BOOT_ENABLE) == TRUE
  BaseCryptLib|CryptoPkg/Library/BaseCryptLib/RuntimeCryptLib.inf
!endif

[LibraryClasses.AARCH64.DXE_RUNTIME_DRIVER]

[LibraryClasses.ARM, LibraryClasses.AARCH64]

[LibraryClasses.common.UEFI_DRIVER, LibraryClasses.common.UEFI_APPLICATION, LibraryClasses.common.DXE_RUNTIME_DRIVER, LibraryClasses.common.DXE_DRIVER]

[BuildOptions]
  GCC:*_*_AARCH64_PLATFORM_FLAGS = -I$(WORKSPACE)/$(PLATFORM_PACKAGE)/Include -I$(WORKSPACE)/$(PLATFORM_PACKAGE)/Include/Library
  RVCT:RELEASE_*_*_CC_FLAGS  = -DMDEPKG_NDEBUG
  GCC:RELEASE_*_*_CC_FLAGS  = -DMDEPKG_NDEBUG
  *_*_*_PLATFORM_FLAGS = -D$(BOARD) -D$(VERSION_REQUIRE)  -D$(KEY_HELP_INORMATION)
  *_*_*_PLATFORM_FLAGS = -D$(DEFAULT_RESOLUTION)
!if $(S3_RESUME_UNLOCK_HDD)
  GCC:*_*_*_CC_FLAGS  = -DS3_RESUME_UNLOCK_HDD=1  -Wno-error
!else
  GCC:*_*_*_CC_FLAGS  = -DS3_RESUME_UNLOCK_HDD=0  -Wno-error
!endif

[BuildOptions.AARCH64.EDKII.DXE_RUNTIME_DRIVER]
  GCC:*_*_AARCH64_DLINK_FLAGS = -z common-page-size=0x10000

################################################################################
#
# Pcd Section - list of all EDK II PCD Entries defined by this Platform
#
################################################################################

[PcdsFeatureFlag.common]

!if $(CAPSULE_ENABLE) == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdSupportUpdateCapsuleReset|TRUE
!endif

[PcdsFixedAtBuild.common]
  gPhytiumPlatformTokenSpaceGuid.PcdCPUInfo|L"D2000"

  # This PCD should be a FeaturePcd. But we used this PCD as an '#if' in an ASM file.
  # Using a FeaturePcd make a '(BOOLEAN) casting for its value which is not understood by the preprocessor.
  gArmTokenSpaceGuid.PcdVFPEnabled|1

  # Use ClusterId + CoreId to identify the PrimaryCore
  gArmTokenSpaceGuid.PcdArmPrimaryCoreMask|0x303

  # Up to 8 cores on Base models. This works fine if model happens to have less.
  gArmPlatformTokenSpaceGuid.PcdCoreCount|8
  gArmPlatformTokenSpaceGuid.PcdClusterCount|4

  #For fingerprint
  gEfiMdeModulePkgTokenSpaceGuid.PcdSerial2RegisterBase|0x28002000
  gEfiMdePkgTokenSpaceGuid.PcdUart2DefaultBaudRate|115200
  gArmPlatformTokenSpaceGuid.PL011Uart2ClkInHz|48000000
  gEfiMdeModulePkgTokenSpaceGuid.PcdSerial2BaudRate|115200

  #
  # ARM General Interrupt Controller
  #
  gArmTokenSpaceGuid.PcdGicDistributorBase|0x29a00000
  gArmTokenSpaceGuid.PcdGicRedistributorsBase|0x29b00000
  gArmTokenSpaceGuid.PcdGicInterruptInterfaceBase|0x29c00000
  #
  # RTC I2C Controller Register Base Address and Speed
  #
!if $(HANWEI_ENABLE) == TRUE
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerBaseAddress|0x28006000
!elseif $(BOARD) == HWQS
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerBaseAddress|0x28008000
!else
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerBaseAddress|0x28007000
!endif
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerSpeed|400000
  gPhytiumPlatformTokenSpaceGuid.PcdXgeneBaseAddress|0x2800D000
!if $(SD3068) == TRUE
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerSlaveAddress|0x32
!elseif $(BOARD) == HWQS
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerSlaveAddress|0x64
!else
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerSlaveAddress|0x68
!endif
  #
  # DDR Info
  #
  gPhytiumPlatformTokenSpaceGuid.PcdSpdI2cControllerBaseAddress | 0x28006000
  gPhytiumPlatformTokenSpaceGuid.PcdSpdI2cControllerSpeed | 400000

  gPhytiumPlatformTokenSpaceGuid.PcdFlashNvStorageBlockSize|0x10000

  #gPhytiumPlatformTokenSpaceGuid.PcdBiosSegment|0x18
  #gPhytiumPlatformTokenSpaceGuid.PcdBiosSize|0xBF

  gEfiShellPkgTokenSpaceGuid.PcdShellScreenLogCount|16

# pci root(Domain)/pci bridge0(bus,device,function)/pci bridge1(bus,device,function)/.../pci device(bus,device,function):(bar number,bar address)
# using ';' to separate the different device groups
   #gPhytiumPlatformTokenSpaceGuid.PcdAhciDevCfg |"PciRoot(0x0)/Pci(0x0,0x2,0x0)/Sata(0x3,0x0,0x0):(0x5,0x58100000);PciRoot(0x0)/Pci(0x0,0x3,0x0)/Pci(0x4,0x0,0x0)/Pci(0x5,0x3,0x0)/Sata(0x8,0x0,0x0):(0x5,0x59100000)"
   #gPhytiumPlatformTokenSpaceGuid.PcdNvmeDevCfg |"PciRoot(0x0)/Pci(0x0,0x1,0x0)/Nvme(0x2,0x0,0x0):(0x0,0x58200000)"
!if $(BOARD) == DT_D2008_2101
   gPhytiumPlatformTokenSpaceGuid.PcdUsbDevCfg |"PciRoot(0x0)/Pci(0x0,0x0,0x0)/Usb(0x1,0x0,0x0)/Usb(0x2,0x2,0x0)/Usb(0x4,0x0,0x0):(0x0,0x58a00000)"
!elseif $(BOARD) == HWQS
   gPhytiumPlatformTokenSpaceGuid.PcdUsbDevCfg |"PciRoot(0x0)/Pci(0x0,0x0,0x0)/Usb(0x1,0x0,0x0):(0x0,0x58300000)"
!else
   gPhytiumPlatformTokenSpaceGuid.PcdUsbDevCfg |""
!endif
   #gKlModulePkgTokenSpaceGuid.PcdNicOnboardDevCfg|"PciRoot(0x0)/Pci(0x4,0x0)/Pci(0x0,0x0)"
   gKlModulePkgTokenSpaceGuid.PcdNicOnboardDevCfg|""
!if $(BOARD) == DT_D2008_2101
   gKlModulePkgTokenSpaceGuid.PcdDisplayOnboardDevCfg|"PciRoot(0x0)/Pci(0x0,0x0)/Pci(0x0,0x0)/Pci(0x1,0x0)/Pci(0x0,0x2)"
!else
   gKlModulePkgTokenSpaceGuid.PcdDisplayOnboardDevCfg|""
!endif

[PcdsDynamicDefault.common.DEFAULT]
  ## The number of seconds that the firmware will wait before initiating the original default boot selection.
  #  A value of 0 indicates that the default boot selection is to be initiated immediately on boot.
  #  The value of 0xFFFF then firmware will wait for user input before booting.
  # @Prompt Boot Timeout (s)
  gEfiMdePkgTokenSpaceGuid.PcdPlatformBootTimeOut|5
  gKlModulePkgTokenSpaceGuid.PcdMemoryInitPeimAddress|0x00180000

[PcdsDynamicExDefault.common.DEFAULT]

################################################################################
#
# Components Section - list of all EDK II Modules needed by this Platform
#
################################################################################
[Components.common]
KunlunD2000BoardPkg/PrePeiCore/PrePeiCoreUniCore.inf

  MdeModulePkg/Universal/ReportStatusCodeRouter/Pei/ReportStatusCodeRouterPei.inf
  MdeModulePkg/Universal/Acpi/FirmwarePerformanceDataTablePei/FirmwarePerformancePei.inf {
    <LibraryClasses>
      LockBoxLib|KunlunModulePkg/Library/LockBoxLib/LockBoxBaseLib.inf
  }
 $(PLATFORM_PACKAGE)/Drivers/GpioInitDxe/GpioInitDxe.inf

!if $(FW_SOFT_POWEROFF) == TRUE
  $(GENERAL_PACKAGE)/Driver/SoftShutdown/SoftShutdown.inf
!endif

  $(PLATFORM_PACKAGE)/Drivers/FixGcdDxe/FixGcdDxe.inf

!if $(SCTO_DXE) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/SctoDxe/Sm4Dxe.inf
!endif

  MdeModulePkg/Universal/ReportStatusCodeRouter/RuntimeDxe/ReportStatusCodeRouterRuntimeDxe.inf

!if $(TCM_DEBUG_HARDWARE) == TRUE
  $(PROJECT_PACKAGE)/Drivers/DebugTcmDxe/DebugTcmDxe.inf
!endif

