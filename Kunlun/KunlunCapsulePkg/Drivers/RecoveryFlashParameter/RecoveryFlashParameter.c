/** @file
  This code produces the Smbios protocol. It also responsible for constructing
  SMBIOS table into system table.

  Copyright (c) 2009 - 2021, Intel Corporation. All rights reserved.<BR>
  Copyright (c) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

//#include "RecoveryFlashParameter.h"
#include <PiPei.h>
#include <Library/DebugLib.h>
#include <Library/BaseLib.h>
#include <Library/PeiServicesLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/HobLib.h>
#include <Library/IoLib.h>
#include <Library/PcdLib.h>
#include <Library/PrintLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Ppi/RecoveryFlashParameter.h>

#include <Library/PeimEntryPoint.h>
#include <Library/ReportStatusCodeLib.h>
#include <Library/DebugAgentLib.h>
#include <Library/PeiServicesTablePointerLib.h>
#include <Library/PerformanceLib.h>

EFI_GUID gPhytiumDxeFvGuid =  { 0x87940482, 0xfc81, 0x41c3, { 0x87, 0xe6, 0x39, 0x9c, 0xf8, 0x5a, 0xc8, 0xa0} };

/**

  @param  This                   Pointer of PEI_RCY_PARAM_PPI
  @param  RecoveryFlashParam     Pointer of RCY_FLASH_PARAM

  @retval EFI_SUCCESS            Get Recy BaseAddress successfully
  @retval Other                  Failed to get Recy BaseAddress.

**/
EFI_STATUS
EFIAPI
GetDxeBaseAddress (
  IN EFI_PEI_RCY_PARAM_PPI  *This,
  OUT RCY_FLASH_PARAM       *RecoveryFlashParam
  );

/**

  @param  This           Pointer of PEI_RCY_PARAM_PPI
  @param  Argv           The recy address

  @retval EFI_SUCCESS    Set Recy BaseAddress successfully
  @retval Other          Failed to set Recy BaseAddress.

**/
EFI_STATUS
EFIAPI
SetRecyBaseAddress (
  IN EFI_PEI_RCY_PARAM_PPI  *This,
  IN UINTN                  RecyAddr
  );

EFI_PEI_RCY_PARAM_PPI      mPeiRcyParam = { GetDxeBaseAddress, SetRecyBaseAddress };

EFI_PEI_PPI_DESCRIPTOR mPeiRcyParamPpiList = {
  (EFI_PEI_PPI_DESCRIPTOR_PPI | EFI_PEI_PPI_DESCRIPTOR_TERMINATE_LIST),
  &gEfiPeiRcyParamPpiGuid,
  &mPeiRcyParam
};

// fun
/**

  @param This                 Pointer of EFI_PEI_RCY_PARAM_PPI
  @param RecoveryFlashParam   Pointer of RCY_FLASH_PARAM

  @retval EFI_SUCCESS    Set Recy Base Address successfully
  @retval Other          Failed to Set Recy Base Address.

**/
EFI_STATUS
EFIAPI
GetDxeBaseAddress (
  IN EFI_PEI_RCY_PARAM_PPI  *This,
  OUT RCY_FLASH_PARAM       *RecoveryFlashParam
  )
{
    RecoveryFlashParam->PcdFdBaseAddress = PcdGet64(PcdFdBaseAddress);
    RecoveryFlashParam->PcdCapsuleRecoveryBaseAddress = PcdGet64(PcdCapsuleRecoveryBaseAddress);
    RecoveryFlashParam->PcdFvDxeBaseAddress = PcdGet64(PcdFvDxeBaseAddress);
    RecoveryFlashParam->PcdFvDxeCheckSumBaseAddress = PcdGet64(PcdFvDxeCheckSumBaseAddress);
    RecoveryFlashParam->PcdSpiControllerBase = PcdGet64(PcdSpiControllerBase);
    RecoveryFlashParam->PcdFvDxeSize = PcdGet32(PcdFvDxeSize);
    RecoveryFlashParam->PcdFvDxeCheckSumSize = PcdGet32(PcdFvDxeCheckSumSize);
    //RecoveryFlashParam->PhytiumDxeFvGuid =  { 0x87940482, 0xfc81, 0x41c3, { 0x87, 0xe6, 0x39, 0x9c, 0xf8, 0x5a, 0xc8, 0xa0} };
    RecoveryFlashParam->PhytiumDxeFvGuid = gPhytiumDxeFvGuid;
    return EFI_SUCCESS;
}

/**

  @param This            Pointer of EFI_PEI_RCY_PARAM_PPI
  @param RecyAddr        Recy Address

  @retval EFI_SUCCESS    Set Recy Base Address successfully
  @retval Other          Failed to Set Recy Base Address.

**/
EFI_STATUS
EFIAPI
SetRecyBaseAddress (
  IN EFI_PEI_RCY_PARAM_PPI  *This,
  IN UINTN                  RecyAddr
  )
{
  PcdSet64S(PcdCapsuleRecoveryBaseAddress,RecyAddr);
  return EFI_SUCCESS;
}

//entry
/**

  @param FileHandle       The hanle of PEI file
  @param PeiServices      The PEI services

  @retval EFI_SUCCESS     Install FV_PPI for FFS3 file system successfully
  @retval Other           Failed to install FV_PPI for FFS3 file system.

**/
EFI_STATUS
EFIAPI
RecoveryFlashParameterEntryPoint(
    IN       EFI_PEI_FILE_HANDLE  FileHandle,
    IN CONST EFI_PEI_SERVICES     **PeiServices
)
{
  EFI_STATUS            Status;

  //
  // Install FV_PPI for FFS3 file system.
  //
  Status = PeiServicesInstallPpi (&mPeiRcyParamPpiList);
  return Status;
}
