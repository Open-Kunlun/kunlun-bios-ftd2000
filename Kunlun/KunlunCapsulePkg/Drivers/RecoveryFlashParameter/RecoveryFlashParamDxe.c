/** @file
  System Firmware descriptor producer.

  Copyright (c) 2016, Intel Corporation. All rights reserved.<BR>
  Copyright (c) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include <Uefi.h>
#include <Library/BaseLib.h>
#include <Library/UefiLib.h>
#include <Library/DebugLib.h>
#include <Library/PrintLib.h>
#include <Library/UefiDriverEntryPoint.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Protocol/RecoveryFlashParamDxe.h>
#include <Library/PcdLib.h>

EFI_STATUS
EFIAPI
GetRcyFlashParam (
  IN EFI_RCVY_PARAM_PROTOCOL     *This,
  IN RCY_FLASH_PARAM             *RcyFlashParam
  );

RCY_FLASH_PARARM_INFO_DRIVER_PRIVATE_DATA    RcyFlashParamDriverTemplate = {
  RCY_FLASH_PARARM_INFO_DRIVER_PRIVATE_DATA_SIGNATURE,   // Signature
  NULL,                                     // DriverHandle
  {
    GetRcyFlashParam
  }
};

/**

  @param This            Pointer of EFI_RCVY_PARAM_PROTOCOL
  @param RcyFlashParam   Pointer of RCY_FLASH_PARAM

  @retval EFI_SUCCESS    Get the RcyFlashParam successfully
  @retval Other          Failed to get the RcyFlashParam.

**/
EFI_STATUS
EFIAPI
GetRcyFlashParam (
  IN EFI_RCVY_PARAM_PROTOCOL     *This,
  IN RCY_FLASH_PARAM             *RcyFlashParam
  )
{
    RcyFlashParam->PcdCapsuleRecoveryBaseAddress = PcdGet64(PcdCapsuleRecoveryBaseAddress);
    RcyFlashParam->PcdFvDxeBaseAddress = PcdGet64(PcdFvDxeBaseAddress);
    RcyFlashParam->PcdFvDxeCheckSumBaseAddress = PcdGet64(PcdFvDxeCheckSumBaseAddress);
    RcyFlashParam->PcdFvDxeSize = PcdGet32(PcdFvDxeSize);
    return EFI_SUCCESS;
}

/**

  Driver to produce Smbios protocol and pre-allocate 1 page for the final SMBIOS table.

  @param ImageHandle     Module's image handle
  @param SystemTable     Pointer of EFI_SYSTEM_TABLE

  @retval EFI_SUCCESS    Smbios protocol installed
  @retval Other          No protocol installed, unload driver.

**/
EFI_STATUS
RecoveryFlashParamDxeEntryPoint (
  IN EFI_HANDLE         ImageHandle,
  IN EFI_SYSTEM_TABLE   *SystemTable
)
{
  EFI_STATUS                                   Status;
  RCY_FLASH_PARARM_INFO_DRIVER_PRIVATE_DATA    *PrivateData;

  PrivateData = AllocateCopyPool(
                  sizeof (RCY_FLASH_PARARM_INFO_DRIVER_PRIVATE_DATA),
                  &RcyFlashParamDriverTemplate
                  );
  if(PrivateData == NULL) {
    DEBUG((EFI_D_ERROR, "ERROR: No enouge memory for RecoveryFlashParamDxe module!\n"));
    return EFI_OUT_OF_RESOURCES;
  }

  Status = gBS->InstallProtocolInterface (
                     &PrivateData->DriverHandle,
                     &gEfiRcyParamProtocolGuid,
                     EFI_NATIVE_INTERFACE,
                     &PrivateData->RcyFlashParamProtocol
                     );
  if (EFI_ERROR (Status)) {
    DEBUG((EFI_D_ERROR, "ERROR: Install protocol for RecoveryFlashParamDxe module fail!\n"));
  }
  DEBUG((EFI_D_INFO, "RecoveryFlashParamDxe module success!\n"));

  return Status;
}

