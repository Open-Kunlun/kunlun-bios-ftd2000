## @file
# Kunlun BDS package project build description file.
#
#;*************************************************************************************************
#;*  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
#;*  Rights Reserved.
#;*
#;*  You may not reproduce, distribute, publish, display, perform, modify, adapt,
#;*  transmit, broadcast, present, recite, release, license or otherwise exploit
#;*  any part of this publication in any form, by any means, without the prior
#;*  written permission of Kunlun Technology (Beijing) Co., Ltd..
#;*
#;*************************************************************************************************

################################################################################
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]

################################################################################
#
# Library Class section - list of all Library Classes needed by this Platform.
#
################################################################################
[LibraryClasses]
!if $(CAPSULE_ENABLE)
  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibFmp/DxeCapsuleLib.inf
  BmpSupportLib|MdeModulePkg/Library/BaseBmpSupportLib/BaseBmpSupportLib.inf
  SafeIntLib|MdePkg/Library/BaseSafeIntLib/BaseSafeIntLib.inf
!else
  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibNull/DxeCapsuleLibNull.inf
!endif

  OpensslLib|CryptoPkg/Library/OpensslLib/OpensslLib.inf
  IntrinsicLib|CryptoPkg/Library/IntrinsicLib/IntrinsicLib.inf
  BaseCryptLib|CryptoPkg/Library/BaseCryptLib/BaseCryptLib.inf
  EdkiiSystemCapsuleLib|SignedCapsulePkg/Library/EdkiiSystemCapsuleLib/EdkiiSystemCapsuleLib.inf
  IniParsingLib|SignedCapsulePkg/Library/IniParsingLib/IniParsingLib.inf
  DisplayUpdateProgressLib|MdeModulePkg/Library/DisplayUpdateProgressLibGraphics/DisplayUpdateProgressLibGraphics.inf

[LibraryClasses.common.PEIM]
  BaseCryptLib|CryptoPkg/Library/BaseCryptLib/PeiCryptLib.inf


[LibraryClasses.common.DXE_RUNTIME_DRIVER]
!if $(CAPSULE_ENABLE)
  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibFmp/DxeRuntimeCapsuleLib.inf
!endif

################################################################################
#
# Pcd Section - list of all EDK II PCD Entries defined by this Platform
#
################################################################################
[PcdsDynamicExDefault.common.DEFAULT]
!if $(CAPSULE_ENABLE) || $(RECOVERY_ENABLE)
  gEfiSignedCapsulePkgTokenSpaceGuid.PcdEdkiiSystemFirmwareImageDescriptor|{0x0}|VOID*|0x100
  gEfiMdeModulePkgTokenSpaceGuid.PcdSystemFmpCapsuleImageTypeIdGuid|{ 0xc0, 0x20, 0xaf, 0x62, 0x16, 0x70, 0x4a, 0x42, 0x9b, 0xf8, 0x9c, 0xcc, 0x86, 0x58, 0x40, 0x90 }
  gEfiSignedCapsulePkgTokenSpaceGuid.PcdEdkiiSystemFirmwareFileGuid|{ 0x74, 0xA2, 0x51, 0x51, 0x14, 0x26, 0xA7, 0x4E, 0xC6, 0x60, 0x2D, 0xBE, 0x3C, 0xA2, 0xCC, 0xB3 }
!endif


!if $(RECOVERY_ENABLE)
  gEfiMdeModulePkgTokenSpaceGuid.PcdRecoveryFileName|L"RECOVERY.Cap"
!endif

[Components.common]
!if $(CAPSULE_ENABLE) || $(RECOVERY_ENABLE)
  KunlunCapsulePkg/Drivers/SystemFirmwareDescriptor/SystemFirmwareDescriptor.inf
!endif

 KunlunCapsulePkg/Drivers/RecoveryFlashParameter/RecoveryFlashParameter.inf
 KunlunCapsulePkg/Drivers/RecoveryBootModeManager/RecoveryBootModeManager.inf

 MdeModulePkg/Universal/CapsulePei/CapsulePei.inf

 MdeModulePkg/Universal/CapsuleRuntimeDxe/CapsuleRuntimeDxe.inf
 KunlunCapsulePkg/Drivers/RecoveryFlashParameter/RecoveryFlashParamDxe.inf
