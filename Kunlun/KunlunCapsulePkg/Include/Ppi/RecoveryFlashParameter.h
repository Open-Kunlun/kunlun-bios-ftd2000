/** @file
  This code supports the implementation of the Smbios protocol

  Copyright (c) 2009 - 2021, Intel Corporation. All rights reserved.<BR>
  Copyright (c) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _RECOVRY_FLASH_PARAMETER_H_
#define _RECOVRY_FLASH_PARAMETER_H_

///
/// Global ID for EFI_PEI_RCY_PARAM_PPI_GUID
///
#define EFI_PEI_RCY_PARAM_PPI_GUID \
  { \
    0x76f3c8dd, 0x85f1, 0x4789, {0x8F, 0xCC, 0x5A, 0xDF, 0x62, 0x27, 0xB1, 0x47 } \
  }

///
/// Forward declaration for EFI_PEI_RCY_PARAM_PPI
///
typedef struct _EFI_PEI_RCY_PARAM_PPI  EFI_PEI_RCY_PARAM_PPI;

typedef struct {
  UINT64  PcdFdBaseAddress;
  UINT64  PcdCapsuleRecoveryBaseAddress;
  UINT64  PcdFvDxeBaseAddress;
  UINT64  PcdFvDxeCheckSumBaseAddress;
  UINT64  PcdSpiControllerBase;
  UINT32  PcdFvDxeSize;
  UINT32  PcdFvDxeCheckSumSize;
  EFI_GUID PhytiumDxeFvGuid;
} RCY_FLASH_PARAM;


typedef
EFI_STATUS
(EFIAPI *EFI_PEI_GET_RCY_PARAM_PPI)(
  IN EFI_PEI_RCY_PARAM_PPI  *This,
  OUT RCY_FLASH_PARAM       *RecoveryFlashParam
  );

typedef
EFI_STATUS
(EFIAPI *EFI_PEI_SET_RCY_RECY_ADDR_PPI)(
  IN EFI_PEI_RCY_PARAM_PPI  *This,
  IN UINTN                 RecyAddr
  );
/**
**/
struct _EFI_PEI_RCY_PARAM_PPI {
  EFI_PEI_GET_RCY_PARAM_PPI  GetDxeBaseAddress;
  EFI_PEI_SET_RCY_RECY_ADDR_PPI  SetRecyBaseAddress;
};


extern EFI_GUID gEfiPeiRcyParamPpiGuid;

#endif
