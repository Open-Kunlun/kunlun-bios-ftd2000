/** @file
  This code supports the implementation of the Smbios protocol

  Copyright (c) 2009 - 2021, Intel Corporation. All rights reserved.<BR>
  Copyright (c) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.. All
  Rights Reserved.
  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _RECOVERY_FLASH_PARAM_DXE_H_
#define _RECOVERY_FLASH_PARAM_DXE_H_

#define EFI_RECOVERY_FLASH_PARAM_PROTOCOL_GUID \
    { 0x24b57096, 0xc11c, 0x4039, { 0x94, 0x7e, 0xb9, 0xb3, 0x9f, 0x4a, 0xfa, 0xf7 }}

typedef struct _EFI_RCVY_PARAM_PROTOCOL EFI_RCVY_PARAM_PROTOCOL;


typedef struct {
  UINT64   PcdFvDxeBaseAddress;
  UINT64   PcdFvDxeCheckSumBaseAddress;
  UINT64   PcdCapsuleRecoveryBaseAddress;
  UINT32   PcdFvDxeSize;
} RCY_FLASH_PARAM;

typedef
EFI_STATUS
(EFIAPI *EFI_RCVY_PARAM_GET)(
  IN            EFI_RCVY_PARAM_PROTOCOL     *This,
  IN            RCY_FLASH_PARAM             *RcyFlashParam
);

struct _EFI_RCVY_PARAM_PROTOCOL {
  EFI_RCVY_PARAM_GET           GetRcyFlashParam;
};

extern EFI_GUID gEfiRcyParamProtocolGuid;

typedef struct {
  UINT32                            Signature;
  EFI_HANDLE                        DriverHandle;
  EFI_RCVY_PARAM_PROTOCOL           RcyFlashParamProtocol;
} RCY_FLASH_PARARM_INFO_DRIVER_PRIVATE_DATA;

#define RCY_FLASH_PARARM_INFO_DRIVER_PRIVATE_DATA_SIGNATURE  SIGNATURE_32 ('r', 'c', 'v', 'y')
#define MEM_INFO_DRIVER_PRIVATE_FROM_THIS(a) \
  CR(a, RCY_FLASH_PARARM_INFO_DRIVER_PRIVATE_DATA, RcyFlashParamProtocol, RCY_FLASH_PARARM_INFO_DRIVER_PRIVATE_DATA_SIGNATURE)



#endif
