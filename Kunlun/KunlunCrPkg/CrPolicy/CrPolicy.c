/** @file
  CR Policy module file.

  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
  All Rights Reserved.

  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..

  Module Name: CrPolicy.c

**/

#include "CrPolicy.h"
#include <Protocol/CrPolicyProtocol.h>
#include <Library/DebugLib.h>

EFI_CR_POLICY_PROTOCOL    mCRPolicy = {0};


/**
  description: Cr Policy Entry Point.
  @param    ImageHandle
  @param    SystemTable
**/
EFI_STATUS
EFIAPI
CrPolicyEntry (
  IN  EFI_HANDLE                        ImageHandle,
  IN  EFI_SYSTEM_TABLE                  *SystemTable
  )
{
  EFI_HANDLE                            Handle;

  mCRPolicy.CREnable = 1;
  mCRPolicy.CRSerialPort = 1;
  mCRPolicy.CRBaudRate = 115200;
  mCRPolicy.CRDataBits = 8;
  mCRPolicy.CRFlowControl = 0;
  mCRPolicy.CRParity = 1;
  mCRPolicy.CRStopBits = 1;
  mCRPolicy.CRTerminalType = 3;
  mCRPolicy.AcpiSpcr = 0;
  mCRPolicy.DeviceType = PcdGet8(PcdCrDeviceType);
  mCRPolicy.PortAddress = PcdGet64(Serial1RegisterBase);;

  PcdSetEx64S(&gKunlunCrPkgTokenSpaceGuid, PcdCrPortAddress, mCRPolicy.PortAddress);


  mCRPolicy.PortIrq = PcdGet8(PcdCrPortIrq);
  mCRPolicy.PciBusNumber = PcdGet8(PcdCrPciBus);
  mCRPolicy.PciDeviceNumber = PcdGet8(PcdCrPciDevice);
  mCRPolicy.PciFunctionNumber = PcdGet8(PcdCrPciFunction);

  mCRPolicy.TerminalOutMode = ISOCH_MODE; //ASYNC_MODE;//ISOCH_MODE;
  DEBUG ((DEBUG_INFO, "CR Enable %d, CRBaudRate = %d, CRTerminalType = %d\n",
              mCRPolicy.CREnable, mCRPolicy.CRBaudRate, mCRPolicy.CRTerminalType));
  //
  // Install protocol
  //
  Handle = NULL;
  gBS->InstallMultipleProtocolInterfaces (
                  &Handle,
                  &gEfiCrPolicyProtocolGuid,
                  &mCRPolicy,
                  NULL
                  );

  return EFI_SUCCESS;
}
