## @file
# Kunlun Cr package project build description file.
#
#;******************************************************************************
#;*  Copyright (C) 2006 - 2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd..
#;*  All Rights Reserved.
#;*
#;*  You may not reproduce, distribute, publish, display, perform, modify, adapt,
#;*  transmit, broadcast, present, recite, release, license or otherwise exploit
#;*  any part of this publication in any form, by any means, without the prior
#;*  written permission of Kunlun Technology (Beijing) Co., Ltd..
#;*
#;******************************************************************************

################################################################################
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]
  DEFINE CR_BINARY = TRUE

################################################################################
#
# Library Class section - list of all Library Classes needed by this Platform.
#
################################################################################
[LibraryClasses]

[LibraryClasses.common]

[LibraryClasses.common.PEIM]


[LibraryClasses.common.DXE_DRIVER]


[LibraryClasses.common.DXE_SMM_DRIVER]


[LibraryClasses.common.COMBINED_SMM_DXE]

[LibraryClasses.common.DXE_RUNTIME_DRIVER]

[LibraryClasses.common.UEFI_DRIVER]

[LibraryClasses.common.UEFI_APPLICATION]

################################################################################
#
# Pcd Section - list of all EDK II PCD Entries defined by this Platform
#
################################################################################
[PcdsFeatureFlag]


[PcdsFixedAtBuild]


[Components.$(PEI_ARCH)]


[Components.common]

  KunlunCrPkg/TerminalDxeBin/TerminalDxe.inf
 
  KunlunCrPkg/SerialDxe/SerialDxe.inf{
    <LibraryClasses>
      SerialPortLib|KunlunCrPkg/Library/PL011SerialPortLib/PL011SerialPortLib.inf
  }
 
  KunlunCrPkg/CrPolicy/CrPolicy.inf
