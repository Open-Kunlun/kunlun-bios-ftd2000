/** @file
  Formset Guids, form ID and Config Data structure for CR Manager Config.

  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
  All Rights Reserved.

  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..

  Module Name: CrSetupForm.h

**/
#ifndef CR_SETUP_FORM_
#define CR_SETUP_FORM_

#define CR_SETUP_FORMSET_GUID \
  { 0xCC8FF786, 0x8EA7, 0x4EBB, { 0x83, 0x13, 0xFA, 0x75, 0xAB, 0x6A, 0x9A, 0x73 }}

#define CR_VARIABLE_GUID \
  {0x400BE17D, 0x36BC, 0x44B2, { 0x99, 0x9B, 0xB5, 0x3D, 0x15, 0x82, 0x6C, 0x6A } }

#define CR_SETUP_DATA_NAME                    L"CrSetupData"

//
// CR Setup definitions.
//
#define CR_SETUP_VARSTORE_ID              0x3C01
#define CR_SETUP_FORM_ID                  0x3C02

#pragma pack(1)

//
// IPMI OEM Configuration Structure.
//
typedef struct {
  #define _IMPORT_CR_GENERAL_CONFIG_
  #include <CrGeneralConfigData.h>
  #undef _IMPORT_CR_GENERAL_CONFIG_
} CR_SETUP_CONFIG;


#pragma pack()

#endif
