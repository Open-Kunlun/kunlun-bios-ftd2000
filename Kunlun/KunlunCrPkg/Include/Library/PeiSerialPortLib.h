/** @file
  This library class provides common serial I/O port functions.

  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
  All Rights Reserved.

  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..

  Module Name: PeiSerialPortLib.h

**/

#ifndef PEI_SERIAL_PORT_LIB_
#define PEI_SERIAL_PORT_LIB_

#include <Uefi/UefiBaseType.h>

/**
  Initialize the serial device hardware.

  If no initialization is required, then return RETURN_SUCCESS.
  If the serial device was successfully initialized, then return RETURN_SUCCESS.
  If the serial device could not be initialized, then return RETURN_DEVICE_ERROR.

  @retval RETURN_SUCCESS        The serial device was initialized.
  @retval RETURN_DEVICE_ERROR   The serial device could not be initialized.

**/
RETURN_STATUS
EFIAPI
PeiSerialPortInitialize (
  VOID
  );


#endif
