/** @file
  CR setup VFR library header file.

  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
  All Rights Reserved.

  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..

  Module Name: CrSetupVfrLib.h

**/


#ifndef CR_SETUP_VFR_LIB_H_
#define CR_SETUP_VFR_LIB_H_


#include <PiDxe.h>

#endif
