/** @file
  CR Policy Protocol header file.
  CR Policy Protocol provides an interface to config console redirection.

  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
  All Rights Reserved.

  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..

  Module Name: CrPolicyProtocol.h

**/
#ifndef CR_POLICY_PROTOCOL_H_
#define CR_POLICY_PROTOCOL_H_

#define EFI_CR_POLICY_PROTOCOL_GUID \
  { \
    0x9C0DE744, 0x7DAE, 0x40B7, 0x82, 0xD4, 0x2A, 0x4A, 0x1C, 0x5, 0xBA, 0xCF \
  }

typedef enum {
  ISA_SERIAL_DEVICE = 0,
  PCI_SERIAL_DEVICE,
  MAX_SERIAL_DEVICE,
} CR_SERIAL_DEVICE_TYPE;

typedef enum {
  ISOCH_MODE,
  ASYNC_MODE,
} TERMINAL_OUT_MODE;

typedef struct _EFI_CR_POLICY_PROTOCOL {
  BOOLEAN                           CREnable;
  UINT8                             CRSerialPort;
  UINT8                             CRParity;
  UINT8                             CRDataBits;
  UINT8                             CRStopBits;
  UINT8                             CRFlowControl;
  UINT8                             CRTerminalType;
  UINT64                            CRBaudRate;
  UINT8                             DeviceType;
  UINT64                            PortAddress;
  UINT8                             PortIrq;
  UINT8                             PciBusNumber;
  UINT8                             PciDeviceNumber;
  UINT8                             PciFunctionNumber;
  BOOLEAN                           AcpiSpcr;
  UINT8                             TerminalOutMode;
} EFI_CR_POLICY_PROTOCOL;


extern EFI_GUID gEfiCRPolicyProtocolGuid;

#endif
