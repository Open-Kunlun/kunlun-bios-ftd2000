/** @file
  CR General Config Data.

  Copyright (c) 2006-2023, Kunlun BIOS, Kunlun Technology (Beijing) Co., Ltd.,
  All Rights Reserved.

  You may not reproduce, distribute, publish, display, perform, modify, adapt,
  transmit, broadcast, present, recite, release, license or otherwise exploit
  any part of this publication in any form, by any means, without the prior
  written permission of Kunlun Technology (Beijing) Co., Ltd..

  Module Name: CrGeneralConfigData.h

**/
#ifndef CR_GENERAL_CONFIG_DATA_H_
#define CR_GENERAL_CONFIG_DATA_H_

#ifdef _IMPORT_CR_GENERAL_CONFIG_

UINT8   CREnable;
UINT8   CRSerialPort;
UINT8   CRTerminalType;
UINT8   CRBaudRate;
UINT8   CRDataBits;
UINT8   CRParity;
UINT8   CRStopBits;
UINT8   CRFlowControl;
UINT8   ACPISpcr;

#endif
#endif
