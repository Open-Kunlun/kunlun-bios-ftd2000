#  Copyright (c) 2011-2015, ARM Limited. All rights reserved.
#  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
#
#  This program and the accompanying materials
#  are licensed and made available under the terms and conditions of the BSD License
#  which accompanies this distribution.  The full text of the license may be found at
#  http://opensource.org/licenses/bsd-license.php
#
#  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
#  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.
#

################################################################################
#
# Defines Section - statements that will be processed to create a Makefile.
#
################################################################################
[Defines]
  PLATFORM_NAME                  = PhytiumD2000
  PLATFORM_GUID                  = 0de70077-9b3b-43bf-ba38-0ea37d77141b
  PLATFORM_VERSION               = 0.1
  DSC_SPECIFICATION              = 0x00010005
  OUTPUT_DIRECTORY               = Build/PhytiumPkg/PhytiumD2000Pkg/Phytium/
  SUPPORTED_ARCHITECTURES        = AARCH64
  BUILD_TARGETS                  = DEBUG|RELEASE
  SKUID_IDENTIFIER               = DEFAULT
  FLASH_DEFINITION               = PhytiumPkg/PhytiumD2000Pkg/PhytiumD2000Pkg.fdf
  PLATFORM_PACKAGE               = PhytiumPkg/PhytiumD2000Pkg
  GENERAL_PACKAGE                = PhytiumPkg/PhytiumGeneral
  
  RES_640_480                    = 0
  RES_1024_768                   = 1
  RES_1280_720                   = 2
  RES_1920_1080                  = 3

  DEFINE NETWORK_ENABLE          = TRUE
  DEFINE NETWORK_IP6_ENABLE      = FALSE
  DEFINE HTTP_BOOT_ENABLE        = TRUE
  DEFINE ACPI_SUPPORT            = TRUE
  DEFINE PCI_ENABLE              = TRUE
  DEFINE USE_SPI_FLASH           = TRUE
  DEFINE PXE_ENABLE              = TRUE
  DEFINE PS2_SUPPORT             = TRUE
  DEFINE GMAC_ENABLE             = TRUE
  DEFINE USB_ENABLE              = TRUE
  DEFINE I2C_RTC_USE             = TRUE
  DEFINE USE_SCPI                = FALSE
  DEFINE SECURE_BOOT_ENABLE      = TRUE
  DEFINE WARN_ALARM_ENABLE       = TRUE
  DEFINE SE_INFO_ENABLE          = FALSE
  DISABLE_SPCR_TABLE             = FALSE
  DEFINE DEBUG_ENABLE            = FALSE
  DEFINE FW_SOFT_POWEROFF        = TRUE
  DEFINE SCTO_DXE                = FALSE

  #config when use hanwei board
  DEFINE HANWEI_ENABLE           = FALSE
  DEFINE X100_GOP_ENABLE         = TRUE
  DEFINE X100_PS2_KEYBOARD_ENABLE    =  FALSE

  DEFINE BOARD                   = DEMO
  #DEFINE BOARD                   = DOMESTIC
  #DEFINE BOARD                   = NOTEBOOK_V1
  #DEFINE BOARD                   = NOTEBOOK_V2
!if $(BOARD) == "DEMO"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = FALSE
  DEFINE SETUP_RESOLUTION        = RES_1024_768
!elseif $(BOARD) == "DOMESTIC"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = TRUE
  DEFINE SETUP_RESOLUTION        = RES_1024_768
!elseif $(BOARD) == "NOTEBOOK_V1"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = FALSE
  DEFINE SETUP_RESOLUTION        = RES_1920_1080
!elseif $(BOARD) == "NOTEBOOK_V2"
  DEFINE HDA_SUPPORT             = FALSE
  DEFINE SD3068                  = FALSE
  DEFINE SETUP_RESOLUTION        = RES_1920_1080
!endif


[LibraryClasses.common]
  ShellCommandLib|ShellPkg/Library/UefiShellCommandLib/UefiShellCommandLib.inf
  HandleParsingLib|ShellPkg/Library/UefiHandleParsingLib/UefiHandleParsingLib.inf
  #DebugLib|MdePkg/Library/BaseDebugLibNull/BaseDebugLibNull.inf
  DebugLib|MdePkg/Library/BaseDebugLibSerialPort/BaseDebugLibSerialPort.inf
  DebugPrintErrorLevelLib|MdePkg/Library/BaseDebugPrintErrorLevelLib/BaseDebugPrintErrorLevelLib.inf

  BaseLib|MdePkg/Library/BaseLib/BaseLib.inf
  SynchronizationLib|MdePkg/Library/BaseSynchronizationLib/BaseSynchronizationLib.inf
  PerformanceLib|MdePkg/Library/BasePerformanceLibNull/BasePerformanceLibNull.inf
  PrintLib|MdePkg/Library/BasePrintLib/BasePrintLib.inf
  PeCoffGetEntryPointLib|MdePkg/Library/BasePeCoffGetEntryPointLib/BasePeCoffGetEntryPointLib.inf
  PeCoffLib|MdePkg/Library/BasePeCoffLib/BasePeCoffLib.inf
  IoLib|MdePkg/Library/BaseIoLibIntrinsic/BaseIoLibIntrinsic.inf
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiDecompressLib.inf
  CpuLib|MdePkg/Library/BaseCpuLib/BaseCpuLib.inf
  PhytiumPowerControlLib|PhytiumPkg/PhytiumD2000Pkg/Library/PhytiumPowerControlLib/PhytiumPowerControlLib.inf

  UefiLib|MdePkg/Library/UefiLib/UefiLib.inf
  HobLib|MdePkg/Library/DxeHobLib/DxeHobLib.inf
  UefiRuntimeServicesTableLib|MdePkg/Library/UefiRuntimeServicesTableLib/UefiRuntimeServicesTableLib.inf
  DevicePathLib|MdePkg/Library/UefiDevicePathLib/UefiDevicePathLib.inf
  UefiBootServicesTableLib|MdePkg/Library/UefiBootServicesTableLib/UefiBootServicesTableLib.inf
  DxeServicesTableLib|MdePkg/Library/DxeServicesTableLib/DxeServicesTableLib.inf
  UefiDriverEntryPoint|MdePkg/Library/UefiDriverEntryPoint/UefiDriverEntryPoint.inf
  UefiApplicationEntryPoint|MdePkg/Library/UefiApplicationEntryPoint/UefiApplicationEntryPoint.inf
  HiiLib|MdeModulePkg/Library/UefiHiiLib/UefiHiiLib.inf
  UefiHiiServicesLib|MdeModulePkg/Library/UefiHiiServicesLib/UefiHiiServicesLib.inf
  UefiRuntimeLib|MdePkg/Library/UefiRuntimeLib/UefiRuntimeLib.inf
  FileExplorerLib|MdeModulePkg/Library/FileExplorerLib/FileExplorerLib.inf
  ShellCEntryLib|ShellPkg/Library/UefiShellCEntryLib/UefiShellCEntryLib.inf
  #scto
  SctoLib|PhytiumPkg/PhytiumD2000Pkg/Drivers/AuthDxe/sm/Sm.inf
#add for https boot.
!if $(HTTP_BOOT_ENABLE) == TRUE
  #HttpLib|MdeModulePkg/Library/DxeHttpLib/DxeHttpLib.inf
  HttpLib|NetworkPkg/Library/DxeHttpLib/DxeHttpLib.inf
!endif

  #
  # Assume everything is fixed at build
  #
  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf

  # 1/123 faster than Stm or Vstm version
  BaseMemoryLib|MdePkg/Library/BaseMemoryLib/BaseMemoryLib.inf
  ArmMmuLib|ArmPkg/Library/ArmMmuLib/ArmMmuBaseLib.inf

  # ARM Architectural Libraries
  ArmLib|ArmPkg/Library/ArmLib/ArmBaseLib.inf
  TimerLib|ArmPkg/Library/ArmArchTimerLib/ArmArchTimerLib.inf
  CacheMaintenanceLib|ArmPkg/Library/ArmCacheMaintenanceLib/ArmCacheMaintenanceLib.inf
  DefaultExceptionHandlerLib|ArmPkg/Library/DefaultExceptionHandlerLib/DefaultExceptionHandlerLib.inf
  ArmDisassemblerLib|ArmPkg/Library/ArmDisassemblerLib/ArmDisassemblerLib.inf
  ArmGicLib|ArmPkg/Drivers/ArmGic/ArmGicLib.inf
  ArmGicArchLib|ArmPkg/Library/ArmGicArchLib/ArmGicArchLib.inf
  ArmSmcLib|ArmPkg/Library/ArmSmcLib/ArmSmcLib.inf

  ArmGenericTimerCounterLib|ArmPkg/Library/ArmGenericTimerPhyCounterLib/ArmGenericTimerPhyCounterLib.inf

  CpuExceptionHandlerLib|ArmPkg/Library/ArmExceptionLib/ArmExceptionLib.inf
  
  #PlatformPeiLib|$(PLATFORM_PACKAGE)/Library/PlatformPei/PlatformPeiLib.inf
  ArmPlatformStackLib|ArmPlatformPkg/Library/ArmPlatformStackLib/ArmPlatformStackLib.inf

  #
  # Uncomment (and comment out the next line) For RealView Debugger. The Standard IO window
  # in the debugger will show load and unload commands for symbols. You can cut and paste this
  # into the command window to load symbols. We should be able to use a script to do this, but
  # the version of RVD I have does not support scripts accessing system memory.
  #
  PeCoffExtraActionLib|MdePkg/Library/BasePeCoffExtraActionLibNull/BasePeCoffExtraActionLibNull.inf

  DebugAgentLib|MdeModulePkg/Library/DebugAgentLibNull/DebugAgentLibNull.inf
  DebugAgentTimerLib|EmbeddedPkg/Library/DebugAgentTimerLibNull/DebugAgentTimerLibNull.inf

  SemihostLib|ArmPkg/Library/SemihostLib/SemihostLib.inf


  AcpiLib|EmbeddedPkg/Library/AcpiLib/AcpiLib.inf
  DynamicAmlLib|DynamicTablesPkg/Library/Common/AmlLib/AmlLib.inf
  FdtLib|EmbeddedPkg/Library/FdtLib/FdtLib.inf

  ShellLib|ShellPkg/Library/UefiShellLib/UefiShellLib.inf
  FileHandleLib|MdePkg/Library/UefiFileHandleLib/UefiFileHandleLib.inf
  SortLib|MdeModulePkg/Library/UefiSortLib/UefiSortLib.inf

  #
  # Secure Boot dependencies
  #
!if $(SECURE_BOOT_ENABLE) == TRUE
  TpmMeasurementLib|SecurityPkg/Library/DxeTpmMeasurementLib/DxeTpmMeasurementLib.inf
  AuthVariableLib|SecurityPkg/Library/AuthVariableLib/AuthVariableLib.inf

  # re-use the UserPhysicalPresent() dummy implementation from the ovmf tree
  PlatformSecureLib|OvmfPkg/Library/PlatformSecureLib/PlatformSecureLib.inf
!else
  TpmMeasurementLib|MdeModulePkg/Library/TpmMeasurementLibNull/TpmMeasurementLibNull.inf
  AuthVariableLib|MdeModulePkg/Library/AuthVariableLibNull/AuthVariableLibNull.inf
!endif
  #PlatformSecureLib|SecurityPkg/Library/PlatformSecureLibNull/PlatformSecureLibNull.inf

  VarCheckLib|MdeModulePkg/Library/VarCheckLib/VarCheckLib.inf

  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibNull/DxeCapsuleLibNull.inf

  # PL011 UART Driver and Dependency Libraries
  PL011UartClockLib|ArmPlatformPkg/Library/PL011UartClockLib/PL011UartClockLib.inf
  PL011UartLib|ArmPlatformPkg/Library/PL011UartLib/PL011UartLib.inf
  SerialPortLib|ArmPlatformPkg/Library/PL011SerialPortLib/PL011SerialPortLib.inf
  SerialPortRunTimeLib|$(GENERAL_PACKAGE)/Library/PL011SerialPortLib/PL011SerialPortLib.inf

  # Scsi Requirements
  UefiScsiLib|MdePkg/Library/UefiScsiLib/UefiScsiLib.inf

  # USB Requirements
  UefiUsbLib|MdePkg/Library/UefiUsbLib/UefiUsbLib.inf

  # Networking Requirements
  #NetLib|MdeModulePkg/Library/DxeNetLib/DxeNetLib.inf
  NetLib|NetworkPkg/Library/DxeNetLib/DxeNetLib.inf
  #DpcLib|MdeModulePkg/Library/DxeDpcLib/DxeDpcLib.inf
  DpcLib|NetworkPkg/Library/DxeDpcLib/DxeDpcLib.inf
  #UdpIoLib|MdeModulePkg/Library/DxeUdpIoLib/DxeUdpIoLib.inf
  UdpIoLib|NetworkPkg/Library/DxeUdpIoLib/DxeUdpIoLib.inf
  #IpIoLib|MdeModulePkg/Library/DxeIpIoLib/DxeIpIoLib.inf
  IpIoLib|NetworkPkg/Library/DxeIpIoLib/DxeIpIoLib.inf

  # Phytium Platform library
  ArmPlatformLib|$(PLATFORM_PACKAGE)/Library/PhytiumPlatformLib/PhytiumPlatformLib.inf   
  
  #CustomizedDisplayLib|MdeModulePkg/Library/CustomizedDisplayLib/CustomizedDisplayLib.inf
  CustomizedDisplayLib|$(GENERAL_PACKAGE)/Setup/Library/CustomizedDisplayLib/CustomizedDisplayLib.inf
  IntrinsicLib|CryptoPkg/Library/IntrinsicLib/IntrinsicLib.inf
  OpensslLib|CryptoPkg/Library/OpensslLib/OpensslLib.inf  
  BaseCryptLib|CryptoPkg/Library/BaseCryptLib/BaseCryptLib.inf
  LogoLib|$(PLATFORM_PACKAGE)/Library/LogoLib/LogoLib.inf
  ReportStatusCodeLib|MdePkg/Library/BaseReportStatusCodeLibNull/BaseReportStatusCodeLibNull.inf 
  DxeServicesLib|MdePkg/Library/DxeServicesLib/DxeServicesLib.inf
  BmpSupportLib|MdeModulePkg/Library/BaseBmpSupportLib/BaseBmpSupportLib.inf  
  SafeIntLib|MdePkg/Library/BaseSafeIntLib/BaseSafeIntLib.inf  

  UefiBootManagerLib|$(GENERAL_PACKAGE)/Library/UefiBootManagerLib/UefiBootManagerLib.inf
  PlatformBootManagerLib|$(PLATFORM_PACKAGE)/Library/PlatformBootManagerLib/PlatformBootManagerLib.inf
  BootLogoLib|MdeModulePkg/Library/BootLogoLib/BootLogoLib.inf

  # SPI Flash library
  NorFlashPlatformLib|$(PLATFORM_PACKAGE)/Library/PhytiumSpiNorFlashLib/PhytiumSpiNorFlashLib.inf
  NorFlashBaseLib|$(PLATFORM_PACKAGE)/Library/PhytiumSpiNorFlashBaseLib/PhytiumSpiNorFlashBaseLib.inf
  # Board Specific I2C Library

  # Board Specific I2C Library
  I2CLib|$(PLATFORM_PACKAGE)/Library/Dw_I2CLib/Dw_I2CLib.inf


  !if $(WARN_ALARM_ENABLE) == TRUE
     WarnAlarmLib|$(PLATFORM_PACKAGE)/Library/WarnAlarmLib/WarnAlarm.inf
  !endif

  # Board Specific RealTimeClock Library
  !if $(I2C_RTC_USE) == TRUE
	RealTimeClockLib|$(PLATFORM_PACKAGE)/Library/Ds1339_RtcLib/Ds1339_RtcLib.inf
  !else
  	RealTimeClockLib|$(PLATFORM_PACKAGE)/Library/XgeneRealTimeClockLib/XgeneRealTimeClockLib.inf
  !endif
  TimeBaseLib|EmbeddedPkg/Library/TimeBaseLib/TimeBaseLib.inf

  
  MemoryAllocationLib|MdePkg/Library/UefiMemoryAllocationLib/UefiMemoryAllocationLib.inf
 
  #GMAC   20191018 by liyu
  DmaLib|EmbeddedPkg/Library/NonCoherentDmaLib/NonCoherentDmaLib.inf
  NonDiscoverableDeviceRegistrationLib|MdeModulePkg/Library/NonDiscoverableDeviceRegistrationLib/NonDiscoverableDeviceRegistrationLib.inf
  
  #
  ParameterTableLib|$(PLATFORM_PACKAGE)/Library/ParameterTableLib/ParameterTable.inf

  #EcLib
  PhytiumEcLib|$(GENERAL_PACKAGE)/Library/EcLib/Ec.inf
  PhytiumSeLib|$(GENERAL_PACKAGE)/Library/SeLib/Se.inf
  ScpLib|$(GENERAL_PACKAGE)/Library/ScpLib/ScpLib.inf

  #X100 Library
  X100ConfigLib|$(GENERAL_PACKAGE)/Library/X100ConfigLib/X100ConfigLib.inf 

  #setup  20191227 by liyu
[LibraryClasses.common.SEC]
  HobLib|MdePkg/Library/PeiHobLib/PeiHobLib.inf
  MemoryAllocationLib|MdePkg/Library/PeiMemoryAllocationLib/PeiMemoryAllocationLib.inf
  PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
  PeiServicesLib|MdePkg/Library/PeiServicesLib/PeiServicesLib.inf
  PeiServicesTablePointerLib|ArmPkg/Library/PeiServicesTablePointerLib/PeiServicesTablePointerLib.inf

  #ArmGicArchLib|ArmPkg/Library/ArmGicArchSecLib/ArmGicArchSecLib.inf

[LibraryClasses.common.PEI_CORE, LibraryClasses.common.PEIM]
  BaseMemoryLib|MdePkg/Library/BaseMemoryLib/BaseMemoryLib.inf
  HobLib|MdePkg/Library/PeiHobLib/PeiHobLib.inf
  MemoryAllocationLib|MdePkg/Library/PeiMemoryAllocationLib/PeiMemoryAllocationLib.inf
  PcdLib|MdePkg/Library/PeiPcdLib/PeiPcdLib.inf
  PeiServicesLib|MdePkg/Library/PeiServicesLib/PeiServicesLib.inf
  PeiServicesTablePointerLib|ArmPkg/Library/PeiServicesTablePointerLib/PeiServicesTablePointerLib.inf
  ReportStatusCodeLib|MdePkg/Library/BaseReportStatusCodeLibNull/BaseReportStatusCodeLibNull.inf
  ExtractGuidedSectionLib|MdePkg/Library/PeiExtractGuidedSectionLib/PeiExtractGuidedSectionLib.inf

[LibraryClasses.common.PEI_CORE]
  PeiCoreEntryPoint|MdePkg/Library/PeiCoreEntryPoint/PeiCoreEntryPoint.inf

[LibraryClasses.common.PEIM]
  PeimEntryPoint|MdePkg/Library/PeimEntryPoint/PeimEntryPoint.inf
  MemoryInitPeiLib|ArmPlatformPkg/MemoryInitPei/MemoryInitPeiLib.inf
  PlatformPeiLib|$(PLATFORM_PACKAGE)/PlatformPei/PlatformPeiLib.inf

[LibraryClasses.common.DXE_CORE]
  HobLib|MdePkg/Library/DxeCoreHobLib/DxeCoreHobLib.inf
  MemoryAllocationLib|MdeModulePkg/Library/DxeCoreMemoryAllocationLib/DxeCoreMemoryAllocationLib.inf
  DxeCoreEntryPoint|MdePkg/Library/DxeCoreEntryPoint/DxeCoreEntryPoint.inf
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiDecompressLib.inf
  PerformanceLib|MdeModulePkg/Library/DxeCorePerformanceLib/DxeCorePerformanceLib.inf

[LibraryClasses.common.DXE_DRIVER]
  SecurityManagementLib|MdeModulePkg/Library/DxeSecurityManagementLib/DxeSecurityManagementLib.inf
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf
  #
  # PCI
  #
  PciSegmentLib|$(PLATFORM_PACKAGE)/Library/PciSegmentLib/PciSegmentLib.inf
  PciHostBridgeLib|$(PLATFORM_PACKAGE)/Library/PciHostBridgeLib/PciHostBridgeLib.inf

[LibraryClasses.common.UEFI_APPLICATION]
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiTianoCustomDecompressLib.inf
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf

[LibraryClasses.common.UEFI_DRIVER]
  UefiDecompressLib|MdePkg/Library/BaseUefiDecompressLib/BaseUefiTianoCustomDecompressLib.inf
  ExtractGuidedSectionLib|MdePkg/Library/DxeExtractGuidedSectionLib/DxeExtractGuidedSectionLib.inf
  PerformanceLib|MdeModulePkg/Library/DxePerformanceLib/DxePerformanceLib.inf

[LibraryClasses.common.DXE_RUNTIME_DRIVER]
  CapsuleLib|MdeModulePkg/Library/DxeCapsuleLibNull/DxeCapsuleLibNull.inf

!if $(SECURE_BOOT_ENABLE) == TRUE
  BaseCryptLib|CryptoPkg/Library/BaseCryptLib/RuntimeCryptLib.inf
!endif

[LibraryClasses.AARCH64.DXE_RUNTIME_DRIVER]
  #
  # PSCI support in EL3 may not be available if we are not running under a PSCI
  # compliant secure firmware, but since the default VExpress EfiResetSystemLib
  # cannot be supported at runtime (due to the fact that the syscfg MMIO registers
  # cannot be runtime remapped), it is our best bet to get ResetSystem functionality
  # on these platforms.
  #
  EfiResetSystemLib|ArmPkg/Library/ArmPsciResetSystemLib/ArmPsciResetSystemLib.inf

[LibraryClasses.ARM, LibraryClasses.AARCH64]
  #
  # It is not possible to prevent the ARM compiler for generic intrinsic functions.
  # This library provides the instrinsic functions generate by a given compiler.
  # [LibraryClasses.ARM] and NULL mean link this library into all ARM images.
  #
  NULL|ArmPkg/Library/CompilerIntrinsicsLib/CompilerIntrinsicsLib.inf

  # Add support for GCC stack protector
  NULL|MdePkg/Library/BaseStackCheckLib/BaseStackCheckLib.inf


[LibraryClasses.common.UEFI_DRIVER, LibraryClasses.common.UEFI_APPLICATION, LibraryClasses.common.DXE_RUNTIME_DRIVER, LibraryClasses.common.DXE_DRIVER]
  PcdLib|MdePkg/Library/DxePcdLib/DxePcdLib.inf
  #SetupBootUiLib|$(PLATFORM_PACKAGE)/Library/SetupBootUiLib/SetupBootUiLib.inf

[BuildOptions]
  GCC:*_*_AARCH64_PLATFORM_FLAGS == -I$(WORKSPACE)/$(PLATFORM_PACKAGE)/Include -I$(WORKSPACE)/$(PLATFORM_PACKAGE)/Include/Library
  RVCT:RELEASE_*_*_CC_FLAGS  = -DMDEPKG_NDEBUG
  GCC:RELEASE_*_*_CC_FLAGS  = -DMDEPKG_NDEBUG
  *_*_*_PLATFORM_FLAGS = -D$(BOARD)

[BuildOptions.AARCH64.EDKII.DXE_RUNTIME_DRIVER]
  GCC:*_*_AARCH64_DLINK_FLAGS = -z common-page-size=0x10000

################################################################################
#
# Pcd Section - list of all EDK II PCD Entries defined by this Platform
#
################################################################################

[PcdsFeatureFlag.common]
  # If TRUE, Graphics Output Protocol will be installed on virtual handle created by ConsplitterDxe.
  # It could be set FALSE to save size.
  gEfiMdeModulePkgTokenSpaceGuid.PcdConOutGopSupport|TRUE

  # Force the UEFI GIC driver to use GICv2 legacy mode. To use
  # GICv3 without GICv2 legacy in UEFI, the ARM Trusted Firmware needs
  # to configure the Non-Secure interrupts in the GIC Redistributors
  # which is not supported at the moment.
  gArmTokenSpaceGuid.PcdArmGicV3WithV2Legacy|FALSE
  gEfiMdePkgTokenSpaceGuid.PcdComponentNameDisable|FALSE
  gEfiMdePkgTokenSpaceGuid.PcdDriverDiagnosticsDisable|TRUE
  gEfiMdePkgTokenSpaceGuid.PcdComponentName2Disable|TRUE
  gEfiMdePkgTokenSpaceGuid.PcdDriverDiagnostics2Disable|TRUE

  #
  # Control what commands are supported from the UI
  # Turn these on and off to add features or save size
  #
  gEmbeddedTokenSpaceGuid.PcdEmbeddedMacBoot|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedDirCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedHobCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedHwDebugCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedPciDebugCmd|TRUE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedIoEnable|FALSE
  gEmbeddedTokenSpaceGuid.PcdEmbeddedScriptCmd|FALSE

  gEmbeddedTokenSpaceGuid.PcdCacheEnable|TRUE

  gEmbeddedTokenSpaceGuid.PcdPrePiProduceMemoryTypeInformationHob|TRUE

  gEfiMdeModulePkgTokenSpaceGuid.PcdTurnOffUsbLegacySupport|TRUE

  # Use the Vector Table location in CpuDxe. We will not copy the Vector Table at PcdCpuVectorBaseAddress
  gArmTokenSpaceGuid.PcdRelocateVectorTable|FALSE

  # Indicates if EFI 1.1 ISO 639-2 language supports are obsolete
  #   TRUE  - Deprecate global variable LangCodes.
  #   FALSE - Does not deprecate global variable LangCodes.
  # Deprecate Global Variable LangCodes.
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultLangDeprecate|TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdInstallAcpiSdtProtocol|TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdPciBusHotplugDeviceSupport|FALSE
  
[PcdsFixedAtBuild.common]
  gPhytiumPlatformTokenSpaceGuid.PcdCpuType|0x660

  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVendor|L"Phytium D2000"

  # This PCD should be a FeaturePcd. But we used this PCD as an '#if' in an ASM file.
  # Using a FeaturePcd make a '(BOOLEAN) casting for its value which is not understood by the preprocessor.
  gArmTokenSpaceGuid.PcdVFPEnabled|1

  # Use ClusterId + CoreId to identify the PrimaryCore
  gArmTokenSpaceGuid.PcdArmPrimaryCoreMask|0x303

  # Up to 8 cores on Base models. This works fine if model happens to have less.
  gArmPlatformTokenSpaceGuid.PcdCoreCount|4
  gArmPlatformTokenSpaceGuid.PcdClusterCount|2

  #
  # NV Storage PCDs.
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableBase64|0xe00000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableSize|0x00020000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingBase64|0xe20000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingSize|0x00010000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareBase64|0xe30000
  gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize|0x00020000
  #gEfiMdeModulePkgTokenSpaceGuid.PcdEmuVariableNvModeEnable|TRUE
  # Size of the region used by UEFI in permanent memory (Reserved 64MB)
  gArmPlatformTokenSpaceGuid.PcdSystemMemoryUefiRegionSize|0x04000000

  gArmPlatformTokenSpaceGuid.PcdCPUCoresStackBase|0x29810000
  gArmPlatformTokenSpaceGuid.PcdCPUCorePrimaryStackSize|0x4000
  ## PL011 - Serial Terminal
  gEfiMdeModulePkgTokenSpaceGuid.PcdSerialRegisterBase|0x28001000
  gEfiMdePkgTokenSpaceGuid.PcdUartDefaultReceiveFifoDepth|0
  gArmPlatformTokenSpaceGuid.PL011UartClkInHz|48000000
  gEfiMdePkgTokenSpaceGuid.PcdUartDefaultBaudRate|115200

  #
  #SBSA Watchdog
  #
  gArmTokenSpaceGuid.PcdGenericWatchdogControlBase|0x2800B000
  gArmTokenSpaceGuid.PcdGenericWatchdogRefreshBase|0x2800A000
  gArmTokenSpaceGuid.PcdGenericWatchdogEl2IntrNum|48

  #
  # ARM General Interrupt Controller
  #
  gArmTokenSpaceGuid.PcdGicDistributorBase|0x29a00000
  gArmTokenSpaceGuid.PcdGicRedistributorsBase|0x29b00000
  gArmTokenSpaceGuid.PcdGicInterruptInterfaceBase|0x29c00000

  # System IO space
  gPhytiumPlatformTokenSpaceGuid.PcdSystemIoBase|0x20000000
  gPhytiumPlatformTokenSpaceGuid.PcdSystemIoSize|0x20000000
  
  # System Memory (2GB ~ 4GB - 64MB), the top 64MB is reserved for PBF
  gArmTokenSpaceGuid.PcdSystemMemoryBase|0x80000000
  gArmTokenSpaceGuid.PcdSystemMemorySize|0x70000000
  
  # 
  # Designware PCI Root Complex
  #
  gEmbeddedTokenSpaceGuid.PcdPrePiCpuIoSize|28
  gPhytiumPlatformTokenSpaceGuid.PcdPciConfigBase|0x40000000
  gPhytiumPlatformTokenSpaceGuid.PcdPciConfigSize|0x10000000
  gArmTokenSpaceGuid.PcdPciBusMin|0
  gArmTokenSpaceGuid.PcdPciBusMax|128
  gArmTokenSpaceGuid.PcdPciIoBase|0x01000
  gArmTokenSpaceGuid.PcdPciIoSize|0xf00000
  gArmTokenSpaceGuid.PcdPciIoTranslation|0x50000000
  gArmTokenSpaceGuid.PcdPciMmio32Base|0x58000000
  gArmTokenSpaceGuid.PcdPciMmio32Size|0x28000000
  gArmTokenSpaceGuid.PcdPciMmio32Translation|0x0
  gArmTokenSpaceGuid.PcdPciMmio64Base|0x00000000
  gArmTokenSpaceGuid.PcdPciMmio64Size|0x1000000000
  gArmTokenSpaceGuid.PcdPciMmio64Translation|0x1000000000

  gEfiMdeModulePkgTokenSpaceGuid.PcdPciDisableBusEnumeration|FALSE

  #
  # SPI Flash Control Register Base Address and Size
  #
  #gPhytiumPlatformTokenSpaceGuid.PcdSpiFlashBase|0x80000000000
  #gPhytiumPlatformTokenSpaceGuid.PcdSpiFlashSize|0x1000000
  #gPhytiumPlatformTokenSpaceGuid.PcdSpiControllerBase|0x8001fffff00
  #gPhytiumPlatformTokenSpaceGuid.PcdSpiControllerSize|0x100

  gPhytiumPlatformTokenSpaceGuid.PcdSpiFlashBase|0x0
  gPhytiumPlatformTokenSpaceGuid.PcdSpiFlashSize|0x1000000
  gPhytiumPlatformTokenSpaceGuid.PcdSpiControllerBase|0x28014000
  gPhytiumPlatformTokenSpaceGuid.PcdSpiControllerSize|0x1000

  gPhytiumPlatformTokenSpaceGuid.PcdUartRedirection|TRUE
  #
  # RTC I2C Controller Register Base Address and Speed
  #
!if $(HANWEI_ENABLE) == TRUE
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerBaseAddress|0x28006000
!else
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerBaseAddress | 0x28007000
!endif
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerSpeed|400000
  gPhytiumPlatformTokenSpaceGuid.PcdXgeneBaseAddress|0x2800D000

!if $(SD3068) == TRUE
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerSlaveAddress|0x32
!else
  gPhytiumPlatformTokenSpaceGuid.PcdRtcI2cControllerSlaveAddress|0x68
!endif

  #
  # DDR Info
  #
  gPhytiumPlatformTokenSpaceGuid.PcdSpdI2cControllerBaseAddress | 0x28006000
  gPhytiumPlatformTokenSpaceGuid.PcdSpdI2cControllerSpeed | 400000

  gPhytiumPlatformTokenSpaceGuid.PcdDdrI2cAddress             | {0x50,0x54,0x51,0x55}
  gPhytiumPlatformTokenSpaceGuid.PcdDdrChannelCount           | 0x2
  gPhytiumPlatformTokenSpaceGuid.PcdDdrChannel0DimmI2cAddress | {0x50,0x54}
  gPhytiumPlatformTokenSpaceGuid.PcdDdrChannel1DimmI2cAddress | {0x51,0x55}
  gPhytiumPlatformTokenSpaceGuid.PcdDdrTrainInfoSaveBaseAddress|0xF00000

  gPhytiumPlatformTokenSpaceGuid.PcdBiosSegment|0x18
  gPhytiumPlatformTokenSpaceGuid.PcdBiosSize|0xBF

!ifdef $(FIRMWARE_VER)
  gEfiMdeModulePkgTokenSpaceGuid.PcdFirmwareVersionString|L"$(FIRMWARE_VER)"
!endif

  gEfiMdePkgTokenSpaceGuid.PcdMaximumUnicodeStringLength|1000000
  gEfiMdePkgTokenSpaceGuid.PcdMaximumAsciiStringLength|1000000
  gEfiMdePkgTokenSpaceGuid.PcdMaximumLinkedListLength|1000000
  gEfiMdePkgTokenSpaceGuid.PcdSpinLockTimeout|10000000
  gEfiMdePkgTokenSpaceGuid.PcdDebugClearMemoryValue|0xAF
  gEfiMdePkgTokenSpaceGuid.PcdPerformanceLibraryPropertyMask|0
  gEfiMdePkgTokenSpaceGuid.PcdPostCodePropertyMask|0
  gEfiMdePkgTokenSpaceGuid.PcdUefiLibMaxPrintBufferSize|320

!if $(HTTP_BOOT_ENABLE) == TRUE
  gEfiNetworkPkgTokenSpaceGuid.PcdAllowHttpConnections|TRUE
!endif

  # DEBUG_ASSERT_ENABLED       0x01
  # DEBUG_PRINT_ENABLED        0x02
  # DEBUG_CODE_ENABLED         0x04
  # CLEAR_MEMORY_ENABLED       0x08
  # ASSERT_BREAKPOINT_ENABLED  0x10
  # ASSERT_DEADLOOP_ENABLED    0x20

!if $(DEBUG_ENABLE) == TRUE
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x2f
!else
  gEfiMdePkgTokenSpaceGuid.PcdDebugPropertyMask|0x0
!endif


  #  DEBUG_INIT      0x00000001  // Initialization
  #  DEBUG_WARN      0x00000002  // Warnings
  #  DEBUG_LOAD      0x00000004  // Load events
  #  DEBUG_FS        0x00000008  // EFI File system
  #  DEBUG_POOL      0x00000010  // Alloc & Free's
  #  DEBUG_PAGE      0x00000020  // Alloc & Free's
  #  DEBUG_INFO      0x00000040  // Verbose
  #  DEBUG_DISPATCH  0x00000080  // PEI/DXE Dispatchers
  #  DEBUG_VARIABLE  0x00000100  // Variable
  #  DEBUG_BM        0x00000400  // Boot Manager
  #  DEBUG_BLKIO     0x00001000  // BlkIo Driver
  #  DEBUG_NET       0x00004000  // SNI Driver
  #  DEBUG_UNDI      0x00010000  // UNDI Driver
  #  DEBUG_LOADFILE  0x00020000  // UNDI Driver
  #  DEBUG_EVENT     0x00080000  // Event messages
  #  DEBUG_GCD       0x00100000  // Global Coherency Database changes
  #  DEBUG_CACHE     0x00200000  // Memory range cachability changes
  #  DEBUG_ERROR     0x80000000  // Error
  gEfiMdePkgTokenSpaceGuid.PcdDebugPrintErrorLevel|0x80000000

  gEfiMdePkgTokenSpaceGuid.PcdReportStatusCodePropertyMask|0x07

  gEmbeddedTokenSpaceGuid.PcdEmbeddedAutomaticBootCommand|""
  gEmbeddedTokenSpaceGuid.PcdEmbeddedDefaultTextColor|0x07
  gEmbeddedTokenSpaceGuid.PcdEmbeddedMemVariableStoreSize|0x10000
  # 20ms 
  gEmbeddedTokenSpaceGuid.PcdTimerPeriod|200000

  #
  # Optional feature to help prevent EFI memory map fragments
  # Turned on and off via: PcdPrePiProduceMemoryTypeInformationHob
  # Values are in EFI Pages (4K). DXE Core will make sure that
  # at least this much of each type of memory can be allocated
  # from a single memory range. This way you only end up with
  # maximum of two fragements for each type in the memory map
  # (the memory used, and the free memory that was prereserved
  # but not used).
  #
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiACPIReclaimMemory|0
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiACPIMemoryNVS|0
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiReservedMemoryType|0
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiRuntimeServicesData|80
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiRuntimeServicesCode|65
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiBootServicesCode|400
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiBootServicesData|20000
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiLoaderCode|20
  gEmbeddedTokenSpaceGuid.PcdMemoryTypeEfiLoaderData|0

  # RunAxf support via Dynamic Shell Command protocol
  # We want to use the Shell Libraries but don't want it to initialise
  # automatically. We initialise the libraries when the command is called by the
  # Shell.
  gEfiShellPkgTokenSpaceGuid.PcdShellLibAutoInitialize|FALSE

  gEfiMdeModulePkgTokenSpaceGuid.PcdResetOnMemoryTypeInformationChange|FALSE
  #gEfiIntelFrameworkModulePkgTokenSpaceGuid.PcdShellFile|{ 0x83, 0xA5, 0x04, 0x7C, 0x3E, 0x9E, 0x1C, 0x4F, 0xAD, 0x65, 0xE0, 0x52, 0x68, 0xD0, 0xB4, 0xD1 }

!if $(SECURE_BOOT_ENABLE) == TRUE
  # override the default values from SecurityPkg to ensure images from all sources are verified in secure boot
  gEfiSecurityPkgTokenSpaceGuid.PcdOptionRomImageVerificationPolicy|0x04
  gEfiSecurityPkgTokenSpaceGuid.PcdFixedMediaImageVerificationPolicy|0x04
  gEfiSecurityPkgTokenSpaceGuid.PcdRemovableMediaImageVerificationPolicy|0x04
!endif

!if $(SECURE_BOOT_ENABLE) == TRUE
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxVariableSize|0x10000
!else
  gEfiMdeModulePkgTokenSpaceGuid.PcdMaxVariableSize|0x4000
!endif

  # Default platform supported RFC 4646 languages: English & French & Chinese Simplified.
  # Default Value of PlatformLangCodes Variable.
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLangCodes|"en-US;zh-Hans"

  # Default current RFC 4646 language: Chinese Simplified.
  # Default Value of PlatformLang Variable.
  gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLang|"en-US"
  #gEfiMdePkgTokenSpaceGuid.PcdUefiVariableDefaultPlatformLang|"zh-Hans"
  gEfiMdePkgTokenSpaceGuid.PcdDefaultTerminalType|4
  #
  # ACPI Table Version
  #
  gEfiMdeModulePkgTokenSpaceGuid.PcdAcpiExposedTableVersions|0x20
  gArmPlatformTokenSpaceGuid.PL011UartInterrupt|39 

  gEfiMdeModulePkgTokenSpaceGuid.PcdSrIovSupport|FALSE
  
  gPhytiumGeneralPlatGuid.PcdEcBaseAddress|0x20000000  

[PcdsDynamicDefault.common.DEFAULT]
  ## This PCD defines the video horizontal resolution.
  ##  This PCD could be set to 0 then video resolution could be at highest resolution.
  #gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|800
  ## This PCD defines the video vertical resolution.
  ##  This PCD could be set to 0 then video resolution could be at highest resolution.
  #gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|600

  #  This PCD could be set to 0 then video resolution could be at highest resolution.
  ## This PCD defines the Console output row and the default value is 80 according to UEFI spec.
  ##This PCD could be set to 0 then console output could be at max column and max row.
  #gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|128
  ## This PCD defines the Console output column and the default value is 25 according to UEFI spec.
  ##  This PCD could be set to 0 then console output could be at max column and max row.
  #gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|40
  #
  ## Specify the video horizontal resolution of text setup.
  ## @Prompt Video Horizontal Resolution of Text Setup
  #gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoHorizontalResolution|800

  ## Specify the video vertical resolution of text setup.
  ## @Prompt Video Vertical Resolution of Text Setup
  #gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoVerticalResolution|600
  # @Prompt Video Horizontal Resolution of Text Setup

  ## Specify the console output column of text setup.
  ## @Prompt Console Output Column of Text Setup
  #gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutColumn|128
  ## Specify the console output row of text setup.
  ## @Prompt Console Output Row of Text Setup
  #gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutRow|40

  !if $(SETUP_RESOLUTION) == RES_640_480
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|640
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|480
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|80
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|25

    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoHorizontalResolution|640
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoVerticalResolution|480
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutColumn|80
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutRow|25
  !elseif $(SETUP_RESOLUTION) == RES_1024_768
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|1024
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|768
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|128
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|40
    
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoHorizontalResolution|1024
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoVerticalResolution|768
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutColumn|128
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutRow|40
  !elseif $(SETUP_RESOLUTION) == RES_1280_720
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|1280
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|720
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|160
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|37
    
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoHorizontalResolution|1280
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoVerticalResolution|720
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutColumn|160
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutRow|37
  !elseif $(SETUP_RESOLUTION) == RES_1920_1080
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|1920
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|1080
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|240
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|56
    
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoHorizontalResolution|1920
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoVerticalResolution|1080
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutColumn|240
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutRow|56
  !else
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoHorizontalResolution|640
    gEfiMdeModulePkgTokenSpaceGuid.PcdVideoVerticalResolution|480
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutColumn|80
    gEfiMdeModulePkgTokenSpaceGuid.PcdConOutRow|25
    
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoHorizontalResolution|640
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupVideoVerticalResolution|480
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutColumn|80
    gEfiMdeModulePkgTokenSpaceGuid.PcdSetupConOutRow|25
  !endif

  ## The number of seconds that the firmware will wait before initiating the original default boot selection.
  #  A value of 0 indicates that the default boot selection is to be initiated immediately on boot.
  #  The value of 0xFFFF then firmware will wait for user input before booting.
  # @Prompt Boot Timeout (s)
  gEfiMdePkgTokenSpaceGuid.PcdPlatformBootTimeOut|5



################################################################################
#
# Components Section - list of all EDK II Modules needed by this Platform
#
################################################################################
[Components.common]
  #
  # PCD database
  #
  MdeModulePkg/Universal/PCD/Dxe/Pcd.inf
  
  ShellPkg/DynamicCommand/TftpDynamicCommand/TftpDynamicCommand.inf
  ShellPkg/Application/Shell/Shell.inf{
   <LibraryClasses>
      ShellCommandLib|ShellPkg/Library/UefiShellCommandLib/UefiShellCommandLib.inf
      NULL|ShellPkg/Library/UefiShellLevel2CommandsLib/UefiShellLevel2CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellLevel1CommandsLib/UefiShellLevel1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellLevel3CommandsLib/UefiShellLevel3CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellDriver1CommandsLib/UefiShellDriver1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellAcpiViewCommandLib/UefiShellAcpiViewCommandLib.inf
      NULL|ShellPkg/Library/UefiShellDebug1CommandsLib/UefiShellDebug1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellInstall1CommandsLib/UefiShellInstall1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellNetwork1CommandsLib/UefiShellNetwork1CommandsLib.inf
      NULL|ShellPkg/Library/UefiShellOemCommandLib/UefiShellOemCommandsLib.inf
      NULL|$(PLATFORM_PACKAGE)/Library/BiosUpdate/BiosUpdate.inf
      HandleParsingLib|ShellPkg/Library/UefiHandleParsingLib/UefiHandleParsingLib.inf
      PrintLib|MdePkg/Library/BasePrintLib/BasePrintLib.inf
      BcfgCommandLib|ShellPkg/Library/UefiShellBcfgCommandLib/UefiShellBcfgCommandLib.inf
  }
  #ShellBinPkg/UefiShell/UefiShell.inf
  #
  #
  # PEI Phase modules
  #
 #
  # PEI Phase modules
  #
  ArmPkg/Drivers/CpuPei/CpuPei.inf
  ArmPlatformPkg/MemoryInitPei/MemoryInitPeim.inf
  ArmPlatformPkg/PrePeiCore/PrePeiCoreUniCore.inf
  #ArmPlatformPkg/PlatformPei/PlatformPeim.inf
  MdeModulePkg/Core/Pei/PeiMain.inf
  MdeModulePkg/Universal/PCD/Pei/Pcd.inf {
    <LibraryClasses>
      PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
  }
  MdeModulePkg/Universal/FaultTolerantWritePei/FaultTolerantWritePei.inf
  MdeModulePkg/Universal/Variable/Pei/VariablePei.inf
  MdeModulePkg/Core/DxeIplPeim/DxeIpl.inf {
    <LibraryClasses>
      NULL|MdeModulePkg/Library/LzmaCustomDecompressLib/LzmaCustomDecompressLib.inf
  }
  #Platform/ARM/SgiPkg/Library/SgiPlatformPei/SgiPlatformPei.inf
  $(PLATFORM_PACKAGE)/PlatformPei/PlatformPeim.inf

  #
  # DXE
  #Dxe core entry
  #
  MdeModulePkg/Core/Dxe/DxeMain.inf {
    <LibraryClasses>
      PcdLib|MdePkg/Library/BasePcdLibNull/BasePcdLibNull.inf
      NULL|MdeModulePkg/Library/DxeCrc32GuidedSectionExtractLib/DxeCrc32GuidedSectionExtractLib.inf
  }

  #DXE driver
  MdeModulePkg/Core/RuntimeDxe/RuntimeDxe.inf
  MdeModulePkg/Universal/CapsuleRuntimeDxe/CapsuleRuntimeDxe.inf
  MdeModulePkg/Universal/Variable/RuntimeDxe/VariableRuntimeDxe.inf {
    <LibraryClasses>
      NULL|MdeModulePkg/Library/VarCheckUefiLib/VarCheckUefiLib.inf
  }
  MdeModulePkg/Universal/Variable/EmuRuntimeDxe/EmuVariableRuntimeDxe.inf
  MdeModulePkg/Universal/FaultTolerantWriteDxe/FaultTolerantWriteDxe.inf
  EmbeddedPkg/ResetRuntimeDxe/ResetRuntimeDxe.inf
  EmbeddedPkg/RealTimeClockRuntimeDxe/RealTimeClockRuntimeDxe.inf

  PhytiumPkg/PhytiumD2000Pkg/Drivers/GpioInitDxe/GpioInitDxe.inf
  #
  # Common Arm Timer and Gic Components
  #
  ArmPkg/Drivers/CpuDxe/CpuDxe.inf
  ArmPkg/Drivers/ArmGic/ArmGicDxe.inf
  EmbeddedPkg/MetronomeDxe/MetronomeDxe.inf
  ArmPkg/Drivers/TimerDxe/TimerDxe.inf

  #
  # security system
  #
  MdeModulePkg/Universal/SecurityStubDxe/SecurityStubDxe.inf {
    <LibraryClasses>
      NULL|SecurityPkg/Library/DxeImageVerificationLib/DxeImageVerificationLib.inf
  }

  #
  #network,  mod for https boot.
  #
!if $(NETWORK_ENABLE) == TRUE
  NetworkPkg/SnpDxe/SnpDxe.inf
  NetworkPkg/DpcDxe/DpcDxe.inf
  NetworkPkg/MnpDxe/MnpDxe.inf
  NetworkPkg/ArpDxe/ArpDxe.inf
  NetworkPkg/Dhcp4Dxe/Dhcp4Dxe.inf
  NetworkPkg/Ip4Dxe/Ip4Dxe.inf
  NetworkPkg/Mtftp4Dxe/Mtftp4Dxe.inf
  NetworkPkg/Udp4Dxe/Udp4Dxe.inf
  NetworkPkg/VlanConfigDxe/VlanConfigDxe.inf
  NetworkPkg/TcpDxe/TcpDxe.inf
!endif

!if $(NETWORK_IP6_ENABLE) == TRUE
  NetworkPkg/Ip6Dxe/Ip6Dxe.inf
  NetworkPkg/Udp6Dxe/Udp6Dxe.inf
  NetworkPkg/Dhcp6Dxe/Dhcp6Dxe.inf
  NetworkPkg/Mtftp6Dxe/Mtftp6Dxe.inf
  NetworkPkg/TcpDxe/TcpDxe.inf
!endif

!if $(PXE_ENABLE) == TRUE
  NetworkPkg/UefiPxeBcDxe/UefiPxeBcDxe.inf
!endif

!if $(HTTP_BOOT_ENABLE) == TRUE
  NetworkPkg/DnsDxe/DnsDxe.inf
  NetworkPkg/HttpUtilitiesDxe/HttpUtilitiesDxe.inf
  NetworkPkg/HttpDxe/HttpDxe.inf
  NetworkPkg/HttpBootDxe/HttpBootDxe.inf
!endif
#GMAC

!if $(GMAC_ENABLE) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/DwEmacSnpDxe/DwEmacSnpDxe.inf
!endif

#
# Auth
#
$(PLATFORM_PACKAGE)/Drivers/AuthDxe/PhytiumAuthDxe.inf

# end of Network.

!if $(USE_SCPI) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/ScpiDxe/ScpiDxe.inf
!endif

  #
  # Common Console Components
  #
  # ConIn,ConOut,StdErr
  MdeModulePkg/Universal/Console/ConPlatformDxe/ConPlatformDxe.inf
  #MdeModulePkg/Universal/Console/ConSplitterDxe/ConSplitterDxe.inf
  $(GENERAL_PACKAGE)/Console/ConSplitterDxe/ConSplitterDxe.inf
  #MdeModulePkg/Universal/Console/GraphicsConsoleDxe/GraphicsConsoleDxe.inf
  $(GENERAL_PACKAGE)/Console/GraphicsConsoleDxe/GraphicsConsoleDxe.inf
  $(GENERAL_PACKAGE)/Console/TerminalDxe/TerminalDxe.inf
  MdeModulePkg/Universal/SerialDxe/SerialDxe.inf

!if $(DISABLE_SPCR_TABLE)             
  EmbeddedPkg/Drivers/ConsolePrefDxe/ConsolePrefDxe.inf
!endif

!if $(SECURE_BOOT_ENABLE) == TRUE
  SecurityPkg/VariableAuthenticated/SecureBootConfigDxe/SecureBootConfigDxe.inf
!endif
  #
  # hii database init
  #
  MdeModulePkg/Universal/HiiDatabaseDxe/HiiDatabaseDxe.inf 

  #
  # FAT filesystem + GPT/MBR partitioning
  #
  #$(PLATFORM_PACKAGE)/Drivers/DiskIoDxe/DiskIoDxe.inf
  MdeModulePkg/Universal/Disk/DiskIoDxe/DiskIoDxe.inf
  MdeModulePkg/Universal/Disk/PartitionDxe/PartitionDxe.inf
  MdeModulePkg/Universal/Disk/UnicodeCollation/EnglishDxe/EnglishDxe.inf
  FatPkg/EnhancedFatDxe/Fat.inf

  #
  # Flash 
  #
  $(PLATFORM_PACKAGE)/Drivers/NorFlashDxe/NorFlashDxe.inf
  $(PLATFORM_PACKAGE)/Drivers/SpiFlash/SpiDxe.inf
  $(PLATFORM_PACKAGE)/Drivers/UpdateBiosApp/UpdateBiosApp.inf
  #$(GENERAL_PACKAGE)/App/EcUpdate/EcUpdate.inf
  $(GENERAL_PACKAGE)/App/X100SeUpdate/X100SeUpdate.inf

  #
  # Generic Watchdog Timer
  #
  ArmPkg/Drivers/GenericWatchdogDxe/GenericWatchdogDxe.inf


  #
  # SeTempAndId
  #
!if $(SE_INFO_ENABLE) == TRUE
  $(GENERAL_PACKAGE)/Driver/TempAndIdDxe/TempAndIdDxe.inf
!endif

!if $(FW_SOFT_POWEROFF) == TRUE
  $(GENERAL_PACKAGE)/Driver/SoftShutdown/SoftShutdown.inf
!endif

  # Usb Support
  #
!if $(USB_ENABLE) == TRUE
  MdeModulePkg/Bus/Pci/UhciDxe/UhciDxe.inf
  MdeModulePkg/Bus/Pci/EhciDxe/EhciDxe.inf
  MdeModulePkg/Bus/Pci/XhciDxe/XhciDxe.inf
  MdeModulePkg/Bus/Usb/UsbBusDxe/UsbBusDxe.inf
  MdeModulePkg/Bus/Usb/UsbKbDxe/UsbKbDxe.inf
  MdeModulePkg/Bus/Usb/UsbMouseDxe/UsbMouseDxe.inf
  MdeModulePkg/Bus/Usb/UsbMassStorageDxe/UsbMassStorageDxe.inf
!endif
  
  #
  # IDE/AHCI Support
  #
  #$(PLATFORM_PACKAGE)/Drivers/AHCI/AHCI.inf
  MdeModulePkg/Bus/Pci/SataControllerDxe/SataControllerDxe.inf
  MdeModulePkg/Bus/Ata/AtaBusDxe/AtaBusDxe.inf
  MdeModulePkg/Bus/Scsi/ScsiBusDxe/ScsiBusDxe.inf
  MdeModulePkg/Bus/Scsi/ScsiDiskDxe/ScsiDiskDxe.inf
  MdeModulePkg/Bus/Ata/AtaAtapiPassThru/AtaAtapiPassThru.inf 

  #
  # PCI Support
  #
  ArmPkg/Drivers/ArmPciCpuIo2Dxe/ArmPciCpuIo2Dxe.inf
  MdeModulePkg/Bus/Pci/PciHostBridgeDxe/PciHostBridgeDxe.inf
  #MdeModulePkg/Bus/Pci/PciBusDxe/PciBusDxe.inf
  $(PLATFORM_PACKAGE)/Bus/Pci/PciBusDxe/PciBusDxe.inf

  #
  # The following 2 module perform the same work except one operate variable.
  # Only one of both should be put into fdf.
  #
  MdeModulePkg/Universal/MonotonicCounterRuntimeDxe/MonotonicCounterRuntimeDxe.inf
 
  #
  # NVME Support
  #
  MdeModulePkg/Bus/Pci/NvmExpressDxe/NvmExpressDxe.inf




  $(PLATFORM_PACKAGE)/Drivers/FixGcdDxe/FixGcdDxe.inf

  #
  #Ps2 Keyboard Support
  #
!if $(PS2_SUPPORT) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/Ps2KeyboardDxe/Ps2keyboardDxe.inf
  $(PLATFORM_PACKAGE)/Drivers/LpcDxe/LpcDxe.inf
!endif


!if $(SCTO_DXE) == TRUE
  $(PLATFORM_PACKAGE)/Drivers/SctoDxe/Sm4Dxe.inf
!endif

!if $(X100_PS2_KEYBOARD_ENABLE) == TRUE
  $(GENERAL_PACKAGE)/Driver/X100Ps2KeyboardDxe/Ps2keyboardDxe.inf
!endif

  # phytium platform 
  $(PLATFORM_PACKAGE)/Drivers/PlatformDxe/PlatformDxe.inf
  # ACPI
!if $(ACPI_SUPPORT) == TRUE 
  MdeModulePkg/Universal/Acpi/AcpiTableDxe/AcpiTableDxe.inf
  $(PLATFORM_PACKAGE)/Drivers/AcpiPlatformDxe/AcpiPlatformDxe.inf
  $(PLATFORM_PACKAGE)/Drivers/AcpiTables/AcpiTables.inf
!endif
  PhytiumPkg/PhytiumD2000Pkg/Drivers/Pptt/Pptt.inf

  #
  # SMBIOS
  #
  MdeModulePkg/Universal/SmbiosDxe/SmbiosDxe.inf
  $(PLATFORM_PACKAGE)/Drivers/SmbiosPlatformDxe/SmbiosPlatformDxe.inf

  #
  #Update bios app
  #
  $(PLATFORM_PACKAGE)/Drivers/UpdateBiosApp/UpdateBiosApp.inf
 
  MdeModulePkg/Universal/LoadFileOnFv2/LoadFileOnFv2.inf

  #
  #Pcie Config Setup Formset
  #
  $(PLATFORM_PACKAGE)/setup/PcieConfigDxe/PcieConfigUiDxe.inf
  #
  #Cpu Config Setup Formset
  #
  $(PLATFORM_PACKAGE)/setup/CpuConfigDxe/CpuConfigUiDxe.inf

  #
  # Bds
  #
  MdeModulePkg/Universal/DevicePathDxe/DevicePathDxe.inf
  #MdeModulePkg/Universal/DisplayEngineDxe/DisplayEngineDxe.inf
  #MdeModulePkg/Universal/SetupBrowserDxe/SetupBrowserDxe.inf
  MdeModulePkg/Universal/BdsDxe/BdsDxe.inf
  
  #MdeModulePkg/Universal/DriverSampleDxe/DriverSampleDxe.inf
  #MdeModulePkg/Application/UiApp/UiApp.inf {
  #  <LibraryClasses>
  #    NULL|MdeModulePkg/Library/DeviceManagerUiLib/DeviceManagerUiLib.inf
  #    NULL|MdeModulePkg/Library/BootManagerUiLib/BootManagerUiLib.inf
  #    NULL|MdeModulePkg/Library/BootMaintenanceManagerUiLib/BootMaintenanceManagerUiLib.inf
  #}
  MdeModulePkg/Application/BootManagerMenuApp/BootManagerMenuApp.inf
  #add by liyu 20191216 phytium setup
  ################################
  #$(PLATFORM_PACKAGE)/setup/DisplayEngineDxe/DisplayEngineDxe.inf
  #$(PLATFORM_PACKAGE)/setup/SetupBrowserDxe/SetupBrowserDxe.inf
  #$(PLATFORM_PACKAGE)/setup/UiApp/UiApp.inf
  #$(PLATFORM_PACKAGE)/setup/DriverSampleDxe/DriverSampleDxe.inf {
  #    <LibraryClasses>
  #    NULL|MdeModulePkg/Library/DeviceManagerUiLib/DeviceManagerUiLib.inf
  #    NULL|$(PLATFORM_PACKAGE)/Library/BootManagerUiLib/BootManagerUiLib.inf
  #    NULL|MdeModulePkg/Library/BootMaintenanceManagerUiLib/BootMaintenanceManagerUiLib.inf
  #    NULL|$(PLATFORM_PACKAGE)/Library/OemConfigUiLib/OemConfigUiLib.inf
  #    #NULL|$(PLATFORM_PACKAGE)/Library/SecurityUiLib/SecurityConfigUiLib.inf
  #}
  ##########################################
  $(GENERAL_PACKAGE)/Setup/DisplayEngineDxe/DisplayEngineDxe.inf
  $(GENERAL_PACKAGE)/Setup/SetupBrowserDxe/SetupBrowserDxe.inf
  $(GENERAL_PACKAGE)/Setup/UiApp/UiApp.inf
  $(GENERAL_PACKAGE)/Setup/DriverSampleDxe/DriverSampleDxe.inf {
      <LibraryClasses>
      #NULL|MdeModulePkg/Library/DeviceManagerUiLib/DeviceManagerUiLib.inf
      #NULL|MdeModulePkg/Library/BootManagerUiLib/BootManagerUiLib.inf
      NULL|$(GENERAL_PACKAGE)/Setup/Library/BootMaintenanceManagerUiLib/BootMaintenanceManagerUiLib.inf
      NULL|$(PLATFORM_PACKAGE)/Library/OemConfigUiLib/OemConfigUiLib.inf
      NULL|$(GENERAL_PACKAGE)/Setup/Library/AdvancedConfigUiLib/AdvancedConfigUiLib.inf
      #NULL|$(PLATFORM_PACKAGE)/Library/SecurityUiLib/SecurityConfigUiLib.inf
  }
  #chinese font
  #$(PLATFORM_PACKAGE)/setup/UnicodeFontDxe/UnicodeFontDxe.inf
  #$(PLATFORM_PACKAGE)/setup/ChineseDxe/ChineseDxe.inf
