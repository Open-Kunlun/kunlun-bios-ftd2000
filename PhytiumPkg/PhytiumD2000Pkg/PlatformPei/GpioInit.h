/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef __GPIO_INIT_H__
#define __GPIO_INIT_H__
EFI_STATUS
EFIAPI
InitializeGpio (
  VOID
  );
#endif
