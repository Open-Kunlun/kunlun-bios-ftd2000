/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef  _S3_VAR_H_
#define  _S3_VAR_H_

#define S3_FUNC_USE

#define FD_BASE_ADDR    			0x400000
#define	SEC_FV_SIZE					0x20000

#endif
