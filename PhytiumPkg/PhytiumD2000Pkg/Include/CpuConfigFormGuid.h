/** @file
  The guid define header file of HII Config Access protocol implementation of cpu configuration module.

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef  _CPUCONFIG_FORM_GUID_H_
#define  _CPUCONFIG_FORM_GUID_H_

#define CPUCONFIG_FORMSET_GUID\
  {0x22490520, 0x5c02, 0x11ec, {0x82, 0xa0, 0x6f, 0xb0, 0xb3, 0xe4, 0xef, 0x22}}

#define  CPU_CONFIG_VARIABLE          L"CpuConfigSetup"

#define  FORM_CPUCONFIG_OPEN         0x8101
#define  FORM_CPUCONFIG_ID            0x8001
#define  VARSTORE_ID_CPU_CONFIG       0x8100

#define  LABEL_FORM_CPUCONFIG_START   0xff0c
#define  LABEL_FORM_CPUCONFIG_END     0xff0d
#define  LABEL_END                    0xffff

#endif
