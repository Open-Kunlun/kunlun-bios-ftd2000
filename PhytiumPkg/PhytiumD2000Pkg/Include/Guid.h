/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef __PHY_SETUP_FORMSET_GUID_H__
#define __PHY_SETUP_FORMSET_GUID_H__


  #define FORMSET_GUID_MAIN {0x5352f8fd, 0xc529, 0x441d, { 0x94, 0x5c, 0x6f, 0x07, 0x07, 0xae, 0x39, 0x36 }}

  #define FORMSET_GUID_ADVANCE {0x2425b05a, 0xb5fb, 0x4923, { 0xb8, 0x2b, 0x8f, 0xa6, 0x51, 0x9f, 0x06, 0x3d }}

  #define FORMSET_GUID_EXIT {0xb453bbbd, 0xb7bc, 0x4aca, { 0x8f, 0xc4, 0x35, 0xb9, 0x4a, 0x80, 0x28, 0x99 }}
  
  #define FORMSET_GUID_PHYTIUMYCONFIG	{0x7f240e87, 0x0e66, 0x4cde, { 0x80, 0x56, 0xa0, 0xbd, 0x53, 0xa1, 0x80, 0xc6 }}

  #define FORMSET_GUID_BOOT   {0x26f0c4d3, 0x9cf8, 0x4612, { 0x89, 0xa6, 0xad, 0x08, 0x61, 0xd1, 0xda, 0x60 }}


  

#ifndef VFRCOMPILE
  extern EFI_GUID gEfiFormsetGuidMain;
  extern EFI_GUID gEfiFormsetGuidAdvance;
  extern EFI_GUID gEfiFormsetGuidBoot;
  extern EFI_GUID gEfiFormsetGuidSecurity;
  extern EFI_GUID gEfiFormsetGuidExit;
  extern EFI_GUID gEfiFormsetGuidPhytiumConfig;
  extern EFI_GUID gPlatformSetupVariableGuid;
#endif

#endif

