/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _EFI_SPI_FLASH_H_
#define _EFI_SPI_FLASH_H_

//
// Define the SPI protocol GUID
//
#define EFI_SPI_FLASH_PROTOCOL_GUID \
  { \
    0x1156efc6, 0xea32, 0x4396, 0xb5, 0xd5, 0x26, 0x93, 0x2e, 0x83, 0xc3, 0x13 \
  }

//
// Extern the GUID for protocol users.
//
extern EFI_GUID                   gEfiSpiFlashProtocolGuid;

//
// Forward reference for ANSI C compatibility
//
typedef struct _EFI_SPI_FLASH_PROTOCOL  EFI_SPI_FLASH_PROTOCOL;

typedef
EFI_STATUS
(EFIAPI *SPI_FLASH_ERASE_INTERFACE) (
  IN EFI_SPI_FLASH_PROTOCOL  *This,
  IN UINT64                  Offset,
  IN UINT64                  Length
  );

typedef
EFI_STATUS
(EFIAPI *SPI_FLASH_WRITE_INTERFACE) (
  IN  EFI_SPI_FLASH_PROTOCOL *This,
  IN  UINT64                 Offset,
  IN  UINT8                  *Buffer,
  IN  UINT64                 Length
  );

typedef
EFI_STATUS
(EFIAPI *SPI_FLASH_READ_INTERFACE) (
  IN EFI_SPI_FLASH_PROTOCOL  *This,
  IN UINT64                  Offset,
  IN OUT UINT8               *Buffer,
  IN UINT64                  Length
  );

typedef
EFI_STATUS
(EFIAPI *SPI_FLASH_ERASE_WRITE_INTERFACE) (
  IN  EFI_SPI_FLASH_PROTOCOL  *This,
  IN  UINT64                  Offset,
  IN  UINT8                   *Buffer,
  IN  UINT64                  Length
  );

typedef
EFI_STATUS
(EFIAPI *SPI_FLASH_DEFAULT_UPDATE)(
  IN  EFI_SPI_FLASH_PROTOCOL         *This,
  IN  UINT8                          *NewBiosData,
  IN  UINTN                          NewBiosDataSize
  );

struct _EFI_SPI_FLASH_PROTOCOL {
  SPI_FLASH_ERASE_INTERFACE             Erase;
  SPI_FLASH_WRITE_INTERFACE             Write;
  SPI_FLASH_READ_INTERFACE              Read;
  SPI_FLASH_ERASE_WRITE_INTERFACE       EraseWrite;
  SPI_FLASH_DEFAULT_UPDATE              DefaultUpdate;
};

#endif
