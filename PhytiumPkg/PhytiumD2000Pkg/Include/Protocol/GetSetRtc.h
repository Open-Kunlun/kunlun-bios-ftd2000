/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _EFI_GET_SET_RTC_H_
#define _EFI_GET_SET_RTC_H_

#define EFI_GET_SET_RTC_PROTOCOL_GUID \
  { \
    0x40b1dd4e, 0x5653, 0x4457, 0xb2, 0x37, 0x61, 0xda, 0xdc, 0xe, 0xa4, 0xcb \
  }

extern EFI_GUID					gEfiGetSetRtcProtocolGuid;

typedef struct _EFI_GET_SET_RTC_PROTOCOL  EFI_GET_SET_RTC_PROTOCOL;

typedef
EFI_STATUS
(EFIAPI *RtcGetTime) (
  OUT EFI_TIME               *Time,
  OUT EFI_TIME_CAPABILITIES  *Capabilities
);

typedef
EFI_STATUS
(EFIAPI *RtcSetTime) (
  IN EFI_TIME         *Time
);

typedef
EFI_STATUS
(EFIAPI *RtcSetWakeUpTime) (
  IN BOOLEAN          Enabled,
  OUT EFI_TIME        *Time

);

typedef
EFI_STATUS
(EFIAPI *RtcGetWakeUpTime) (
  OUT BOOLEAN        *Enabled,
  OUT BOOLEAN        *Pending,
  OUT EFI_TIME       *Time

);

struct _EFI_GET_SET_RTC_PROTOCOL {
  RtcGetTime            GetTime;
  RtcSetTime            SetTime;
  RtcSetWakeUpTime      SetWakeUpTime;
  RtcGetWakeUpTime      GetWakeUpTime;
};

#endif
