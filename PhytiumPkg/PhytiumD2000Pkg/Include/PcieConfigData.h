/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef  _PCIE_CONFIG_DATA_H_
#define  _PCIE_CONFIG_DATA_H

#define VAR_PCIE_CONFIG_NAME                     L"PcieConfigVar"
#define PLATFORM_SETUP_VARIABLE_FLAG             (EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE)

typedef struct _PCIE_CONTROLLER {
  UINT8  DisEnable;
  UINT8  Speed;
  UINT8  EqualValue;
} PCIE_CONTROLLER;

typedef struct _PEU_CONFIG {
  UINT8            SplitMode;
  PCIE_CONTROLLER  PcieControl[3];
} PEU_CONFIG;


typedef struct _PCIE_CONFIG_DATA {
  PEU_CONFIG  PcieConfig[2];
} PCIE_CONFIG_DATA;

#endif
