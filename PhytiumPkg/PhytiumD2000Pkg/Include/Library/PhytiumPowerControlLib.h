/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef __POWERCONTROL__
#define __POWERCONTROL__
#include <Library/DebugLib.h>
#include <Library/PcdLib.h>
#include <Library/IoLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <CpuConfigData.h>

void SystemOff(void);
void SystemReboot(void);
UINT32 get_s3_flag(void);
void set_s3_flag(void);
void clean_s3_flag(void);
void enable_vtt(void);
void disable_vtt(void);
void mdelay(UINT32 time);
UINT32 pm_get_s3_flag_source(void);
VOID send_se_ctr(UINT32 cmd);
VOID
AutoWakeUp(
  AUTO_WAKE_UP_CONFIG_DATA   *AutoWakeUpConfigData
);

#endif
