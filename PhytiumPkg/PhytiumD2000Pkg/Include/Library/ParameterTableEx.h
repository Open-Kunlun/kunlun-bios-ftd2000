/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _PARAMETER_EX_H
#define _PARAMETER_EX_H

#include <Library/BaseMemoryLib.h>
#include <Library/BaseLib.h>
#include <Library/DebugLib.h>
#include <Uefi/UefiBaseType.h>

#define PM_PBF_OFFSET               (0x600)
#define PARAMETER_PBF_MAGIC         0x54460000
#define PM_PBF                      6

typedef struct pbf_config {
        PARAMETER_HEADER head;
        UINT8 UartLevel;
        UINT8 UartRes;
        UINT8 LogLevel;
        UINT8 Res;
        UINT32 UartBaud;
        UINT8 QspiFreqRank;
        UINT8 QspiMode;
        UINT8 CpuBaseFreq;
}__attribute__((aligned(sizeof(unsigned long)))) PBF_CONFIG;

VOID *
GetParameterBase(
  IN UINT32 Id
  );

#endif
