/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef __WARNALARM_H_
#define __WARNALARM_H_


VOID
WarnAlarm(
  VOID
  );


VOID
Alarm(
  UINTN  ErrorType
  );

#endif
