/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef __NORFLASH_PHYTIUM_LIB_H__
#define __NORFLASH_PHYTIUM_LIB_H__

#include <Uefi/UefiBaseType.h>
/*
 * SPI registers
 */
#define REG_FLASH_CAP 	0x00
#define REG_RD_CFG		0x04
#define REG_WR_CFG		0x08
#define REG_FLUSH_REG	0x0c
#define REG_CMD_PORT	0x10
#define REG_ADDR_PORT	0x14
#define REG_HD_PORT		0x18
#define REG_LD_PORT		0x1C
#define REG_FUN_SET		0x20
#define REG_ERR_LOG		0x24
#define REG_WP_REG		0x28
#define REG_MODE_REG	0x2c

//
// Platform Nor Flash Functions
//

typedef struct {
  UINTN       DeviceBaseAddress;    // Start address of the Device Base Address (DBA)
  UINTN       RegionBaseAddress;    // Start address of one single region
  UINTN       Size;
  UINTN       BlockSize;
  EFI_GUID    Guid;
} NOR_FLASH_DESCRIPTION_PHYTIUM;

UINT32
ReadDeviceId (
  VOID
  );

EFI_STATUS
NorFlashPlatformEraseSingleBlock (
  IN UINTN                BlockAddress
  );

EFI_STATUS
NorFlashPlatformRead (
  IN UINTN                Address,
  IN VOID                 *Buffer,
  OUT UINT32              Len
  );

EFI_STATUS
NorFlashPlatformWrite (
  IN UINTN                Address,
  IN VOID                 *Buffer,
  IN UINT32               Len
  );

EFI_STATUS
NorFlashPlatformGetDevices (
	OUT NOR_FLASH_DESCRIPTION_PHYTIUM	**NorFlashDevices,
	OUT UINT32					*Count
);

EFI_STATUS
NorFlashPlatformInitialization (
  VOID
  );

//[jwluo-0015-start]
EFI_STATUS
NorFlashPlatformWriteAtRunTime (
  IN UINTN            BaseAddr,
  IN UINTN            Address,
  IN VOID             *Buffer,
  IN UINT32           BufferSizeInBytes
  );
//[jwluo-0015-end]

//[jqmao-001]
EFI_STATUS
EFIAPI
ProtectW25Q (
  IN UINTN                  BlockLockAdr,
  IN UINTN                  Length
) ;
//[jqmao-001]
#endif /* __NORFLASH_PHYTIUM_LIB_H__ */
