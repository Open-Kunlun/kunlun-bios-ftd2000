/*
 * (C) Copyright 2009
 * Vipin Kumar, ST Micoelectronics, vipin.kumar@st.com.
 * Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#ifndef __I2CLib_H_
#define __I2CLib_H_

extern void *i2c_regs_p;

int i2c_read(UINTN BusAddress, UINT32 Chip, UINT16 Addr, int Alen, unsigned char *Buffer, int Len);

int i2c_write(UINTN BusAddress, unsigned char Chip, UINT64 Addr, int Alen, unsigned char *Buffer, int Len);

void i2c_init(UINTN BusAddress, unsigned int Speed, int SlaveAddress);

void spd_setpage(UINTN BusAddress, unsigned char chip, int page_num);

int enable_i2c_as_slaver(UINTN BusAddress, UINT8 slave_address);
#endif /* __I2CLib_H_ */
