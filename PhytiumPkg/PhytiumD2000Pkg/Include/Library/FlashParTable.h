/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef		_FLASH_PARTABLE_H_
#define		_FLASH_PARTABLE_H_

#include <PlatformSetupVariable.h>

/*
 * SPI registers
 */
#define REG_FLASH_CAP 	0x00
#define REG_RD_CFG		0x04
#define REG_WR_CFG		0x08
#define REG_FLUSH_REG	0x0c
#define REG_CMD_PORT	0x10
#define REG_ADDR_PORT	0x14
#define REG_HD_PORT		0x18
#define REG_LD_PORT		0x1C
#define REG_FUN_SET		0x20
#define REG_ERR_LOG		0x24
#define REG_WP_REG		0x28
#define REG_MODE_REG	0x2c

EFI_STATUS
ParTableRead (
	IN	UINTN	len,
	OUT UINT8	*Buffer
  );

EFI_STATUS
ParTableWrite (
  IN UINTN                  len,
  IN UINT8                  *Buffer
  );

VOID
ParTableParse(
  IN UINT8           *Buffer,
  OUT	SETUP_PHYTIUM_CONFIG	*info
);

VOID
ParTableCode(
  IN	SETUP_PHYTIUM_CONFIG	*info,
  OUT UINT8           *Buffer
);

VOID
DebugParTable(
	IN	SETUP_PHYTIUM_CONFIG	*info
);

EFI_STATUS
ParTableSpiInit(
	VOID
);

UINT32
SecurityInfoRead(
    SECURITY_PARTABLE  *secInfo
);

UINT32
SecurityInfoWrite(
	SECURITY_PARTABLE  *secInfo
);


#endif
