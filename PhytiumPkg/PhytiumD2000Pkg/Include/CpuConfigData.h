/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef  _CPU_CONFIG_DATA_H_
#define  _CPU_CONFIG_DATA_H_

#define VAR_CPU_CONFIG_NAME                      L"CpuConfigVar"
#define PLATFORM_SETUP_VARIABLE_FLAG             (EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE)
#define VAR_CPU_DDR_CONFIG_NAME                  L"CpuDdrConfigVar"
#define VAR_CPU_BIOSMODE_CONFIG_NAME             L"CpuBiosModeVar"
#define VAR_CPU_AUTO_WAKE_UP_CONFIG_NAME         L"CpuSeAutoWakeUpVar"

#define PLATFORM_BS_VARIABLE_FLAG                (EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_NON_VOLATILE)
#define DDR_TRAIN_INFO_CHECK  0x524444

typedef struct _DDR_CONFIG {
  UINT8  FastTrainEnable;
} DDR_CONFIG_DATA;

#pragma pack(1)
typedef struct _AUTO_WAKE_UP_CONFIG {
  UINT8   AutoS3;           //0- Disable, 1- Enable
  UINT32  S3WakeUpTime;
  UINT8   AutoS4S5;         //0- Disable, 1- Enable
  UINT32  S4S5WakeUpTime;
} AUTO_WAKE_UP_CONFIG_DATA;
#pragma pack()

typedef struct _CPU_CONFIG_DATA {
  DDR_CONFIG_DATA             DdrConfig;
  UINT8                       BiosMode;          //0- normal, 1- compatibility
  AUTO_WAKE_UP_CONFIG_DATA    AutoWakeUpConfig;
} CPU_CONFIG_DATA;

#endif
