/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _SETUP_VARIABLE_H
#define _SETUP_VARIABLE_H

//#include <Uefi/UefiBaseType.h>

#define PLATFORM_SETUP_VARIABLE_GUID \
  { \
     0xEC87D643, 0xEBA4, 0x4BB5, {0xA1, 0xE5, 0x3F, 0x3E, 0x36, 0xB2, 0x0D, 0xA9} \
  }

#pragma pack(1)
// NOTE: When you add anything to this structure,
//   you MUST add it to the very bottom!!!!
//   You must make sure the structure size is able to divide by 32!
typedef struct {
	UINT8 NicEnable;
	UINT8 GMAC0Enable;
	UINT8 GMAC1Enable;
	UINT8 BootTime;
} SYSTEM_CONFIGURATION;

#define	CORE_MAX_COUNT	4
#define	DDR_MAX_COUNT	2
#define	PCIE_MAX_COUNT	2
#define	PCIE_CONTROLLER_NUM_PERPCIE	3

typedef struct{
	UINT8	tMode;			//0:EP,1:RC
	UINT8	tGen;			//0:Auto, 1:Gen1, 2:Gen2 3:Gen3
	UINT32	tEqual;
}PCIE_CONTROLLER_CONFIG;

typedef	struct{
	UINT8	FuncConfig;		//0:No init,1:X16,3:X8
	PCIE_CONTROLLER_CONFIG	ControllerConfig[PCIE_CONTROLLER_NUM_PERPCIE];
}PCIE_CONFIG_INFO;
#if 0
typedef struct{
	UINT8   ChEnable;		//1:Enable, 0:Disable
	UINT8	EccEnable;		//1:Enable, 0:Disable
	UINT8	DMEnable;		//1:Enable, 0:Disable
	UINT8	DimmType;		//1:RDIMM,2:UDIMM,3:SODIMM,4:LRDIMM
	UINT8	DataWidth;		//0:X4,1:X8,2:X16,3:X32
	UINT8	MirrorType;		//O:stardard, 1:mirror
	UINT8	EccType;		//0:no ecc, 1:ecc
	UINT8	DramType;		//0x0b:DDR3, 0x0c:DDR4
	UINT8	RankNum;		//1:1 rank,2:2 rank, 4:4 rank
	UINT8	RowNum;
	UINT8	ColumnNum;
	UINT8	BankGroupNum;	//only DDR4
	UINT8	BankNum;
	UINT16	tRCDMin;
	UINT16	tRPMin;
	UINT16	tRASMin;
	UINT16	tRCMin;
	UINT16	tFAWMin;
	UINT16	tRRDSMin;		//only DDR4
	UINT16	tRRDLMin;		//only DDR4
	UINT16	tCCDLMin;		//only DDR4
	UINT16	tWRMin;
	UINT16	tWTRSMin;		//only DDR4
	UINT16	tWTRLMin;		//only DDR4
	UINT16	tWTRMin;		//only DDR3
	UINT16	tRRDMin;		//only DDR3
	UINT8	DDRSeFlag;		//1:manual configuration, 0:read configuration from DDR */
}DDR_SPD_INFO;
#endif
typedef	struct {
	//CPU Config
	UINT32 CoreFreq;
	UINT32 DDRSpeed;
	UINT8  CoreState[CORE_MAX_COUNT];
	//PCIe Config
	PCIE_CONFIG_INFO	PcieInfo[PCIE_MAX_COUNT];
	//device interface
	UINT8  FlashWriteCmd;	//Falsh Write Comand
	UINT8  FalshEarseCmd;	//Falsh Earse Comand
	UINT8  FlashPPCmd;		//Flash Page Program Comand
	UINT32 FalshS3FS;		//Flash S3 Flag Source
	//MCU Config
	//DDR_SPD_INFO	DDRInfo[DDR_MAX_COUNT];

}PHYTIUM_CONFIG;

typedef struct {
  UINT8  PlatId;
  UINT8  BmcPresent;
} SETUP_VOLATILE_DATA;

typedef struct _SECURITY_PARTABLE {
    UINT8  SecurityLevel;
}SECURITY_PARTABLE;


#pragma pack()

#ifndef VFRCOMPILE
  //extern EFI_GUID    gPlatformSetupVariableGuid;
  #define PLATFORM_SETUP_VARIABLE_NAME    L"Setup"
  #define SETUP_VOLATILE_VARIABLE_NAME    L"SetupVolatileData"
  #define SETUP_PHYTIUMCONFIG_NAME		  L"SetupPhytiumConfigData"
  #define PLATFORM_SETUP_VARIABLE_FLAG    (EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS | EFI_VARIABLE_NON_VOLATILE)
#endif

#ifndef SETUP_DATA
  #define SETUP_DATA SYSTEM_CONFIGURATION
#endif

#ifndef	SETUP_PHYTIUM_CONFIG
  #define	SETUP_PHYTIUM_CONFIG	PHYTIUM_CONFIG
#endif

#ifndef SETUP_DATA_VARSTORE
  #define SETUP_DATA_VARSTORE \
    efivarstore SETUP_DATA, varid = 1, attribute = 0x7, name  = Setup, guid  = PLATFORM_SETUP_VARIABLE_GUID;
#endif

#ifndef TSESETUP_VARSTORE
  #define TSESETUP_VARSTORE \
    efivarstore TSESETUP, varid = 0x11, attribute = 0x7, name  = SysPs, guid = TSESETUP_GUID;
#endif

#ifndef SETUP_PHYTIUM_CONFIG_VARSTORE
  #define SETUP_PHYTIUM_CONFIG_VARSTORE \
	  efivarstore SETUP_PHYTIUM_CONFIG, varid = 2, attribute = 0x7, name	= SetupPhytiumConfigData, guid  = PLATFORM_SETUP_VARIABLE_GUID;
#endif

#ifndef SETUP_VOLATILE_DATA_VARSTORE
  #define SETUP_VOLATILE_DATA_VARSTORE \
    efivarstore SETUP_VOLATILE_DATA, varid = 0x12, attribute = 0x6, name = SetupVolatileData, guid = PLATFORM_SETUP_VARIABLE_GUID;
#endif

#endif //End,#ifndef _SETUP_VARIABLE
