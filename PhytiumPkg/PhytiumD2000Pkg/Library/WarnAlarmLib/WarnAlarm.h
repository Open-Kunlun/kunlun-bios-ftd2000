/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _WARNALARM_H_
#define _WARNALARM_H_

#include <Base.h>
#include <Library/BaseLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DebugLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/IoLib.h>
#include <Library/TimerLib.h>
#include <Library/PhytiumPowerControlLib.h>
#include <Library/ArmPlatformLib.h>

#include <Protocol/Usb2HostController.h>
#include <Protocol/UsbHostController.h>
#include <Protocol/DiskInfo.h>
#include <Protocol/GraphicsOutput.h>
#include <Protocol/LoadedImage.h>
#include <Protocol/PciIo.h>
#include <Protocol/PciRootBridgeIo.h>

#define GPIO_0_BASE       0x28004000
#define PIN_REUSE_CTL     0x28180000

#define PWR_CTR0_REG      0x28180480
#define PWR_CTR1_REG      0x28180484

#define GPIO_O_A2_REUSE   0x0200
#define GPIO_O_A2_CONF    0x50000

#define GPIO_SWPORT_DR      0x00
#define GPIO_SWPORT_DDR     0x04
#define GPIO_EXT_PORTA      0x08

#define FUNC1               0x01

#define NONE_RESISTOR       0x00
#define PULL_DOWN_RESISTOR  0x01
#define PULL_UP_RESISTOR    0x10

#define PULL_UP      0x04
#define PULL_DOWN    0x0

#define ERROR_TYPE0  0
#define ERROR_TYPE1  1
#define ERROR_TYPE2  2
#define ERROR_TYPE3  3
#define ERROR_TYPE4  4
#define ERROR_TYPE5  5
#define ERROR_TYPE6  6
#define ERROR_TYPE7  7

#define SETBIT(x, y)     (x |= (1 << y))
#define CLEARBIT(x, y)   (x &= ~(1 << y))

VOID
WarnAlarm(
  VOID
  );

VOID
Alarm(
  UINTN   ErrorType
  );

#endif
