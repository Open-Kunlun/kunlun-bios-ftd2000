/*
 * Copyright (c) 2016-2017, ARM Limited and Contributors. All rights reserved.
 * Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __SE_H__
#define __SE_H__
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>


VOID
send_se_ctr(
  UINT32 cmd
  );

UINTN
read_se_state(
  VOID
  );


#endif

