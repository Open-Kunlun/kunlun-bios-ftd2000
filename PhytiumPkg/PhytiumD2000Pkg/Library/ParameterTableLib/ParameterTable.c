/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#include <Library/ParameterTable.h>
//[gliu-0046]add-start
#include <Library/ParameterTableEx.h>
//[gliu-0046]add-end
/**
  Get partmeter table base address.

  @param[in]  Id parameter tabe identifier

  @retval  pointer to parameter base address
**/
VOID *
GetParameterBase(
  IN UINT32 Id
  )
{
  PARAMETER_HEADER *Head;
  UINT32 Offset, Magic;

  switch(Id)
  {
    case PM_PLL:
      Offset = PM_PLL_OFFSET;
      Magic = PARAMETER_PLL_MAGIC;
      break;
    case PM_PCIE:
      Offset = PM_PCIE_OFFSET;
      Magic = PARAMETER_PCIE_MAGIC;
      break;
    case PM_COMMON:
      Offset = PM_COMMON_OFFSET;
      Magic = PARAMETER_COMMON_MAGIC;
      break;
    case PM_DDR:
      Offset = PM_DDR_OFFSET;
      Magic = PARAMETER_DDR_MAGIC;
      break;
    case PM_BOARD:
      Offset = PM_BOARD_OFFSET;
      Magic = PARAMETER_BOARD_MAGIC;
      break;
   //[gliu-0046]add-start
    case PM_PBF:
      Offset = PM_PBF_OFFSET;
      Magic = PARAMETER_PBF_MAGIC;
      break;
   //[gliu-0046]add-end
    default:
      DEBUG((EFI_D_ERROR, "Invalid Id\n"));
        while(1);
  }

  Head = (PARAMETER_HEADER *)(UINT64)(O_PARAMETER_BASE + Offset);
  if(Magic != Head->Magic) {
    return NULL;
  } else {
    return (VOID *)(Head);
  }
}

/**
  Get Parameter information.

  @param[in]  Id parameter tabe identifier
  @param[in,out]  Data  pointer to paramter information

  @retval  EFI_SUCCESS    get information suceess
  @retval  EFI_NOT_FOUND  get fail
**/
EFI_STATUS GetParameterInfo(UINT32 Id, UINT8 *Data, UINT32 DataSize)
{
  PARAMETER_HEADER *Head;

  if((Data == NULL) || (DataSize == 0)) {
    return EFI_INVALID_PARAMETER;
  }

  Head = (PARAMETER_HEADER *)GetParameterBase(Id);
  if(Head == NULL) {
    return EFI_NOT_FOUND;
  }

  if(DataSize < Head->Size){
    DEBUG((EFI_D_ERROR, "DataSize is too small\n", DataSize));
    return EFI_BUFFER_TOO_SMALL;
  }

  CopyMem(Data, Head, Head->Size);

  return EFI_SUCCESS;
}
