/** @file
  PCI Host Bridge Library instance for ARM SOC

  Copyright (c) 2017, Linaro Ltd. All rights reserved.<BR>
  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  This program and the accompanying materials are licensed and made available
  under the terms and conditions of the BSD License which accompanies this
  distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php.

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS, WITHOUT
  WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include <PiDxe.h>
#include <IndustryStandard/Pci22.h>
#include <Library/DebugLib.h>
#include <Library/DevicePathLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/PcdLib.h>
#include <Library/PciHostBridgeLib.h>
#include <Library/HobLib.h>
#include <Protocol/PciHostBridgeResourceAllocation.h>
#include <Protocol/PciRootBridgeIo.h>
#include <Pcie.h>
#include <OEMSvc.h>

#pragma pack(1)
typedef struct {
  ACPI_HID_DEVICE_PATH     AcpiDevicePath;
  EFI_DEVICE_PATH_PROTOCOL EndDevicePath;
} EFI_PCI_ROOT_BRIDGE_DEVICE_PATH;
#pragma pack ()

#define END_DEVICE_PATH_DEF { END_DEVICE_PATH_TYPE, \
                              END_ENTIRE_DEVICE_PATH_SUBTYPE, \
                              { END_DEVICE_PATH_LENGTH, 0 } \
                            }

#define ACPI_DEVICE_PATH_DEF(UID) {{ ACPI_DEVICE_PATH, ACPI_DP, \
                                    {(UINT8)(sizeof (ACPI_HID_DEVICE_PATH)), (UINT8)(sizeof (ACPI_HID_DEVICE_PATH) >> 8)} \
                                    }, \
                                    EISA_PNP_ID (0x0A03), UID \
                                  }

STATIC CONST EFI_PCI_ROOT_BRIDGE_DEVICE_PATH mEfiPciRootBridgeDevicePath[] = {
  {
    ACPI_DEVICE_PATH_DEF(0),
    END_DEVICE_PATH_DEF
  },
  
};

GLOBAL_REMOVE_IF_UNREFERENCED
CHAR16 *mPciHostBridgeLibAcpiAddressSpaceTypeStr[] = {
  L"Mem", L"I/O", L"Bus"
};

#define PCI_ALLOCATION_ATTRIBUTES       EFI_PCI_HOST_BRIDGE_COMBINE_MEM_PMEM | \
                                        EFI_PCI_HOST_BRIDGE_MEM64_DECODE


STATIC PCI_ROOT_BRIDGE mPciRootBridges[PCI_HOST_MAX_COUNT] = {
  {
    0,                                      // Segment
    0,                                      // Supports
    0,                                      // Attributes
    TRUE,                                   // DmaAbove4G
    FALSE,                                  // NoExtendedConfigSpace
    FALSE,                                  // ResourceAssigned
    PCI_ALLOCATION_ATTRIBUTES,              // AllocationAttributes
    { 0, 0 },      // Bus
    { 0, 0 },      // Io
    { 0, 0 },      // Mem
    { 0, 0 },      // MemAbove4G
    { MAX_UINT64, 0x0 },                    // PMem
    { MAX_UINT64, 0x0 },                    // PMemAbove4G
    (EFI_DEVICE_PATH_PROTOCOL *)&mEfiPciRootBridgeDevicePath[0]
  }, 
  
};

/**
  Return all the root bridge instances in an array.

  @param Count  Return the count of root bridge instances.

  @return All the root bridge instances in an array.
          The array should be passed into PciHostBridgeFreeRootBridges()
          when it's not used.
**/
PCI_ROOT_BRIDGE *
EFIAPI
PciHostBridgeGetRootBridges (
  OUT UINTN     *Count
  )
{
  EFI_HOB_GUID_TYPE             *GuidHob;
  PHYTIUM_PCI_HOST_BRIDGE       *PciHostBridge;
  PCI_HOST_BLOCK                *PciHostBlock;
  UINTN                         Index;
  
  GuidHob = GetFirstGuidHob(&gPlatformPciHostInforGuid);
  if (NULL == GuidHob){
    DEBUG((EFI_D_ERROR, "Could not get Pci Host infor  Guid hob.\n"));
    return NULL;
  }
  PciHostBridge = (PHYTIUM_PCI_HOST_BRIDGE *) GET_GUID_HOB_DATA(GuidHob);

  if (PciHostBridge->PciHostCount > PCI_HOST_MAX_COUNT) {
    DEBUG((EFI_D_ERROR, "ERROR !!! PciHostCount more than %X!!!\n", PCI_HOST_MAX_COUNT));
    PciHostBridge->PciHostCount = PCI_HOST_MAX_COUNT;
  }

  *Count = PciHostBridge->PciHostCount;
  PciHostBlock = (PCI_HOST_BLOCK *)(UINTN)(PciHostBridge->PciHostBlock);
  for (Index = 0; Index < PciHostBridge->PciHostCount; Index ++) {
      DEBUG ((EFI_D_INFO,"HOST:%x, IoBase:%lx, IoSize:%x, Mem32Base:%lx, Mem32Size:%x, Mem64Base:%lx, Mem64Size:%lx\n",
        Index, PciHostBlock->IoBase,PciHostBlock->IoSize, PciHostBlock->Mem32Base,PciHostBlock->Mem32Size,PciHostBlock->Mem64Base, PciHostBlock->Mem64Size));
    mPciRootBridges[Index].Bus.Base  = PciHostBlock->BusStart;
    mPciRootBridges[Index].Bus.Limit = PciHostBlock->BusEnd;
    mPciRootBridges[Index].Io.Base   = PciHostBlock->IoBase & PHYTIUM_IOBASE_MASK;
    mPciRootBridges[Index].Io.Limit  = mPciRootBridges[Index].Io.Base + PciHostBlock->IoSize - 1;
    mPciRootBridges[Index].Mem.Base  = PciHostBlock->Mem32Base & PHYTIUM_MEMIO32_MASK;
    mPciRootBridges[Index].Mem.Limit = mPciRootBridges[Index].Mem.Base + PciHostBlock->Mem32Size - 1;
    mPciRootBridges[Index].MemAbove4G.Base  = PciHostBlock->Mem64Base & PHYTIUM_MEMIO64_MASK;
    mPciRootBridges[Index].MemAbove4G.Limit = mPciRootBridges[Index].MemAbove4G.Base + PciHostBlock->Mem64Size - 1;
    PciHostBlock ++;
  }

  return mPciRootBridges;
}

/**
  Free the root bridge instances array returned from PciHostBridgeGetRootBridges().

  @param Bridges The root bridge instances array.
  @param Count   The count of the array.
**/
VOID
EFIAPI
PciHostBridgeFreeRootBridges (
  PCI_ROOT_BRIDGE *Bridges,
  UINTN           Count
  )
{
}

/**
  Inform the platform that the resource conflict happens.

  @param HostBridgeHandle Handle of the Host Bridge.
  @param Configuration    Pointer to PCI I/O and PCI memory resource
                          descriptors. The Configuration contains the resources
                          for all the root bridges. The resource for each root
                          bridge is terminated with END descriptor and an
                          additional END is appended indicating the end of the
                          entire resources. The resource descriptor field
                          values follow the description in
                          EFI_PCI_HOST_BRIDGE_RESOURCE_ALLOCATION_PROTOCOL
                          .SubmitResources().
**/
VOID
EFIAPI
PciHostBridgeResourceConflict (
  EFI_HANDLE                        HostBridgeHandle,
  VOID                              *Configuration
  )
{
  EFI_ACPI_ADDRESS_SPACE_DESCRIPTOR *Descriptor;
  UINTN                             RootBridgeIndex;
  DEBUG ((DEBUG_ERROR, "PciHostBridge: Resource conflict happens!\n"));

  RootBridgeIndex = 0;
  Descriptor = (EFI_ACPI_ADDRESS_SPACE_DESCRIPTOR *) Configuration;
  while (Descriptor->Desc == ACPI_ADDRESS_SPACE_DESCRIPTOR) {
    DEBUG ((DEBUG_ERROR, "RootBridge[%d]:\n", RootBridgeIndex++));
    for (; Descriptor->Desc == ACPI_ADDRESS_SPACE_DESCRIPTOR; Descriptor++) {
      ASSERT (Descriptor->ResType <
              ARRAY_SIZE (mPciHostBridgeLibAcpiAddressSpaceTypeStr));
      DEBUG ((DEBUG_ERROR, " %s: Length/Alignment = 0x%lx / 0x%lx\n",
              mPciHostBridgeLibAcpiAddressSpaceTypeStr[Descriptor->ResType],
              Descriptor->AddrLen, Descriptor->AddrRangeMax
              ));
      if (Descriptor->ResType == ACPI_ADDRESS_SPACE_TYPE_MEM) {
        DEBUG ((DEBUG_ERROR, "     Granularity/SpecificFlag = %ld / %02x%s\n",
                Descriptor->AddrSpaceGranularity, Descriptor->SpecificFlag,
                ((Descriptor->SpecificFlag &
                  EFI_ACPI_MEMORY_RESOURCE_SPECIFIC_FLAG_CACHEABLE_PREFETCHABLE
                  ) != 0) ? L" (Prefetchable)" : L""
                ));
      }
    }
    //
    // Skip the END descriptor for root bridge
    //
    ASSERT (Descriptor->Desc == ACPI_END_TAG_DESCRIPTOR);
    Descriptor = (EFI_ACPI_ADDRESS_SPACE_DESCRIPTOR *)(
                   (EFI_ACPI_END_TAG_DESCRIPTOR *)Descriptor + 1
                   );
  }
}
