/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#include <Library/IoLib.h>
#include <GetSetRtcDxe.h>

STATIC UINTN mI2CSlaveAddress;
UINTN                  mRtcBase;
//[gliu-0009]add-start
#if 0
BOOLEAN
IsLeapYear (
  IN EFI_TIME   *Time
  )
{
  if (Time->Year % 4 == 0) {
    if (Time->Year % 100 == 0) {
      if (Time->Year % 400 == 0) {
        return TRUE;
      } else {
        return FALSE;
      }
    } else {
      return TRUE;
    }
  } else {
    return FALSE;
  }
}

BOOLEAN
IsDayValid (
  IN  EFI_TIME  *Time
  )
{
  STATIC CONST INTN DayOfMonth[12] = { 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

  if (Time->Day < 1 ||
      Time->Day > DayOfMonth[Time->Month - 1] ||
      (Time->Month == 2 && (!IsLeapYear (Time) && Time->Day > 28))
     ) {
    return FALSE;
  }

  return TRUE;
}
#endif
//[gliu-0009]add-end
EFI_STATUS
EFIAPI
GetRtcTime (
  OUT EFI_TIME                *Time,
  OUT EFI_TIME_CAPABILITIES   *Capabilities
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
  rtc_time    RtcTime;
  int         ret;
  //UINT8 Buffer;
  //DEBUG((DEBUG_ERROR, " %a %a line %d:\n",__FILE__,__func__,__LINE__));
  // Ensure Time is a valid pointer
  if (Time == NULL) {
    Status = EFI_INVALID_PARAMETER;
    goto EXIT;
  }

  //i2c_read(mI2CSlaveAddress,0x0f,1,&Buffer,1);
  //DEBUG((EFI_D_INFO," 0x0f value is 0x%x!!!!!\n",Buffer));
  //i2c_read(mI2CSlaveAddress,0x10,1,&Buffer,1);
  //DEBUG((EFI_D_INFO," 0x0f value is 0x%x!!!!!\n",Buffer));

  ret = rtc_get( &RtcTime ,mI2CSlaveAddress);
  if (ret == 0) {
    Time->Year       = (UINT16)RtcTime.tm_year;
    Time->Month      = (UINT8)RtcTime.tm_mon;
    Time->Day        = (UINT8)RtcTime.tm_mday;
    Time->Hour       = (UINT8)RtcTime.tm_hour;
    Time->Minute     = (UINT8)RtcTime.tm_min;
    Time->Second     = (UINT8)RtcTime.tm_sec;
    Time->Nanosecond = 0;
  } else {
    //DEBUG((EFI_D_ERROR, " LibGetTime-error\n",Status));
    Status = EFI_UNSUPPORTED;
    }
  EXIT:
  return Status;
}

EFI_STATUS
EFIAPI
SetRtcTime (
  IN  EFI_TIME                *Time
  )
{
  EFI_STATUS  Status = EFI_SUCCESS;
  rtc_time    RtcTime;
  int         ret;

  // Check the input parameters are within the range specified by UEFI
  if ((Time->Year   < 1900) ||
      (Time->Year   > 9999) ||
      (Time->Month  < 1   ) ||
      (Time->Month  > 12  ) ||
      (!IsDayValid (Time)   ) ||
      (Time->Hour   > 23  ) ||
      (Time->Minute > 59  ) ||
      (Time->Second > 59  ) ||
      (Time->Nanosecond > 999999999) ||
      (!((Time->TimeZone == EFI_UNSPECIFIED_TIMEZONE) || ((Time->TimeZone >= -1440) && (Time->TimeZone <= 1440)))) ||
      (Time->Daylight & (~(EFI_TIME_ADJUST_DAYLIGHT | EFI_TIME_IN_DAYLIGHT)))
    ) {
    Status = EFI_INVALID_PARAMETER;
    goto EXIT;
  }

  // Because the PL031 is a 32-bit counter counting seconds,
  // the maximum time span is just over 136 years.
  // Time is stored in Unix Epoch format, so it starts in 1970,
  // Therefore it can not exceed the year 2106.
  if ((Time->Year < 1970) || (Time->Year >= 2106)) {
    Status = EFI_UNSUPPORTED;
    goto EXIT;
  }
  //[gliu-0010]
  //DEBUG((EFI_D_INFO,"hour is %d\n",Time->Hour));
  //DEBUG((EFI_D_INFO,"minute is %d\n",Time->Minute));
  //DEBUG((EFI_D_INFO,"Second is %d\n",Time->Second));
  //[gliu-0010]
  ret = rtc_get (&RtcTime,mI2CSlaveAddress);
  if (ret == 0) {
    rtc_set(Time, &RtcTime,mI2CSlaveAddress);
  } else {
    Status = EFI_UNSUPPORTED;
  }

  EXIT:
  return Status;
}
//[gliu-0009]
EFI_STATUS
EFIAPI
SetRtcWakeUpTime (
  IN BOOLEAN         Enabled,
  OUT EFI_TIME      *Time
  )
{
  EFI_STATUS Status;
  Status = EFI_SUCCESS;
  //[wyj-0001]add-start
  if (Enabled == TRUE)
  {
    // Check the input parameters are within the range specified by UEFI
    if (Time == NULL )
    {
      Status = EFI_INVALID_PARAMETER;
      goto EXIT;
    }
    if ((Time->Year   < 1900) ||
        (Time->Year   > 9999) ||
        (Time->Month  < 1   ) ||
        (Time->Month  > 12  ) ||
        (!IsDayValid (Time)   ) ||
        (Time->Hour   > 23  ) ||
        (Time->Minute > 59  ) ||
        (Time->Second > 59  ) ||
        (Time->Nanosecond > 999999999) ||
        (!((Time->TimeZone == EFI_UNSPECIFIED_TIMEZONE) || ((Time->TimeZone >= -1440) && (Time->TimeZone <= 1440)))) ||
        (Time->Daylight & (~(EFI_TIME_ADJUST_DAYLIGHT | EFI_TIME_IN_DAYLIGHT)))
      ) {
      Status = EFI_INVALID_PARAMETER;
      goto EXIT;
      }
  }
  //[wyj-0001]add-end

  SetWakeUp(Enabled, Time, mI2CSlaveAddress);
  EXIT:
  return Status;
}

EFI_STATUS
EFIAPI
GetRtcWakeUpTime (
  OUT BOOLEAN         *Enabled,
  OUT BOOLEAN         *Pending,
  OUT EFI_TIME        *Time
  )
{
  EFI_STATUS Status;
  Status = EFI_SUCCESS;

  GetWakeUp(Enabled, Pending, Time, mI2CSlaveAddress);

  return Status;
}
//[gliu-0009]
EFI_GET_SET_RTC_PROTOCOL mGetSetRtc = {
    GetRtcTime,
    SetRtcTime,
  //[gliu-0009]
    SetRtcWakeUpTime,
    GetRtcWakeUpTime,
  //[gliu-0009]
};
//[gliu-0009]add-start
//extern void *i2c_regs_p;
//[gliu-0009]add-end

//[gliu-0009]add-start

//[gliu-0010]
VOID
EFIAPI
VirtualNotifyEvent (
  IN EFI_EVENT        Event,
  IN VOID             *Context
  )
{
   EfiConvertPointer (0x0, (VOID **) &mRtcBase);

  return;
}
//[gliu-0010]
//[gliu-0009]add-start

EFI_STATUS
EFIAPI
GetSetRtcEntryPoint (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS    Status;
  UINT64        I2CBaseAddress;
  UINT32        I2CSpeed;
  //[gliu-0009]add-start
  //[gliu-0010]
  //EFI_EVENT     Event;
  EFI_EVENT     Event;
  //[gliu-0010]
  //[gliu-0009]add-end
  UINTN   Reg;
  UINT8 Buffer;
  
  I2CBaseAddress = PcdGet64 (PcdRtcI2cControllerBaseAddress);
  //[gliu-0010]
  mRtcBase = I2CBaseAddress;
  //[gliu-0010]
  I2CSpeed = PcdGet32 (PcdRtcI2cControllerSpeed);
  mI2CSlaveAddress = PcdGet32 (PcdRtcI2cControllerSlaveAddress);

  Reg = MmioRead32(0x28180200);
  MmioWrite32(0x28180200, (Reg & (~(0xff << 24))) | (0x22 << 24));
  Reg = MmioRead32(0x28180204);
  MmioWrite32(0x28180204, (Reg & (~(0xf << 28))) | (0x1 << 28));

  // Initialize I2C Bus which contain RTC chip
  i2c_init(I2CBaseAddress, I2CSpeed, mI2CSlaveAddress);
  if(i2c_read(I2CBaseAddress, mI2CSlaveAddress, 0x0, 1, &Buffer, 1)){
      mI2CSlaveAddress = CONFIG_RTC_SD3068;
      DEBUG((EFI_D_ERROR,"GetSetRtcEntryPoint:I2c configed to sd3068!\n"));
  }
//[jckuang-0019]
  #if 0
  Buffer = 0x80;
  i2c_write(I2CBaseAddress, mI2CSlaveAddress,0x10,1,&Buffer,1);
  //DEBUG((EFI_D_INFO," 0x0f value is 0x%x!!!!!\n",Buffer));

  Buffer = 0xff;
  i2c_write(I2CBaseAddress, mI2CSlaveAddress,0x0f,1,&Buffer,1);
  //DEBUG((EFI_D_INFO," 0x0f value is 0x%x!!!!!\n",Buffer));
  #endif
//[jckuang-0019]
  // Declare the controller as EFI_MEMORY_RUNTIME
  Status = gDS->AddMemorySpace (
                  EfiGcdMemoryTypeMemoryMappedIo,
                  I2CBaseAddress, 
                  SIZE_4KB,
                  EFI_MEMORY_UC | EFI_MEMORY_RUNTIME
                  );
  if (EFI_ERROR (Status)) {
    DEBUG((EFI_D_ERROR,"GetSetRtcEntryPoint:AddMemorySpace Failed!!!!!\n"));
    return Status;
  }

  Status = gDS->SetMemorySpaceAttributes (I2CBaseAddress, SIZE_4KB, EFI_MEMORY_UC | EFI_MEMORY_RUNTIME);
  if (EFI_ERROR (Status)) {
    DEBUG((EFI_D_ERROR,"GetSetRtcEntryPoint:SetMemorySpaceAttributes Failed!!!!!\n"));
    return Status;
  }

  Status = gBS->InstallProtocolInterface(
                  &ImageHandle,
                  &gEfiGetSetRtcProtocolGuid,
                  EFI_NATIVE_INTERFACE,
                  &mGetSetRtc
                  );
  ASSERT_EFI_ERROR (Status);

//[gliu-0009]add-start
  //
  // Register for the virtual address change event
  //
  //[gliu-0010
  Status = gBS->CreateEventEx (
                  EVT_NOTIFY_SIGNAL,
                  TPL_NOTIFY,
                  VirtualNotifyEvent,
                  NULL,
                  &gEfiEventVirtualAddressChangeGuid,
                  &Event
                  );
  ASSERT_EFI_ERROR (Status);
  //[gliu-0010]
//[gliu-0009]add-end

  return Status;
}
