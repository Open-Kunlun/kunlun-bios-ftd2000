/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _EFI_GET_SET_RTC_DXE_H_
#define _EFI_GET_SET_RTC_DXE_H_

#include <Guid/EventGroup.h>
#include <Protocol/GetSetRtc.h>
#include <Library/DebugLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/DxeServicesTableLib.h>
#include <Library/UefiRuntimeLib.h>
#include <Library/I2CLib.h>
//[gliu-0009]
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/TimeBaseLib.h>
//[gliu-0009]

/* I2C related */
#define CONFIG_DW_I2C
#define CONFIG_CMD_I2C
#define CONFIG_HARD_I2C

/* RTC related */
#define CONFIG_CMD_DATE
#define CONFIG_RTC_DS1337   0x68
#define CONFIG_RTC_DS1339
#define CONFIG_RTC_SD3068   0x32
#define CONFIG_SYS_I2C_RTC_ADDR

typedef UINT64 u64;
typedef UINT32 u32;
typedef UINT16 u16;
typedef UINT8  u8;

typedef unsigned char uchar;

typedef struct {
  int tm_sec;
  int tm_min;
  int tm_hour;
  int tm_mday;
  int tm_mon;
  int tm_year;
  int tm_wday;
  int tm_yday;
  int tm_isdst;
} rtc_time;

typedef struct {
  char min;
  char hour;
  char day;
  char mon;
  char year;
} rtc_time_valid;
//[gliu-0009]
EFI_STATUS
GetWakeUp (
  OUT BOOLEAN       *Enabled,
  OUT BOOLEAN       *Pending,
  OUT EFI_TIME      *tmp,
  IN UINTN           slave_addr
  );

EFI_STATUS
SetWakeUp (
  IN BOOLEAN        Enabled,
  OUT EFI_TIME     *tmp,
  IN UINTN          slave_addr
  );

  int rtc_get (rtc_time *tmp,UINTN slave_addr);
  int rtc_set (EFI_TIME *tmp, rtc_time *current,UINTN slave_addr);
  void rtc_reset (void);

unsigned int
bcd2bin (
  UINT8 val
  );


UINT8
bin2bcd (
  unsigned int val
  );
//[gliu-0009]
#endif
