/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#include <Library/UefiBootServicesTableLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>


#define GPIO_ALTERNATE_BASE 0x28180000
#define I2C1_ALTERNATE     0x200
#define I2C2_ALTERNATE     0x204
#define I2C3_ALTERNATE_SCL 0x204
#define I2C3_ALTERNATE_SDA 0x208
#define GPIO0_PORT7        0x204
#define GPIO1_PORT5        0x208
#define GPIO1_PORT6        0x208
#define GMAC0_DELAY		   0x360
#define GMAC1_DELAY		   0x37C
#define GMAC1_ALTERNATE	   0x228

STATIC inline VOID GpioClrSetBits(UINTN RegOffSet, UINT32 Mask, UINT32 Value)
{
  UINT32 Tmp;

  Tmp = MmioRead32(GPIO_ALTERNATE_BASE + RegOffSet);
  Tmp &= ~(Mask);
  Tmp |= Value;
  MmioWrite32(GPIO_ALTERNATE_BASE + RegOffSet, Tmp);
}

EFI_STATUS
EFIAPI
InitializeGpioDxe (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  DEBUG((EFI_D_INFO, "InitalizeGpio:\n"));
  //I2C1 Alternate
  GpioClrSetBits(I2C1_ALTERNATE, 0xff000000, 0x22000000);
  //I2C2 Alternate
  GpioClrSetBits(I2C2_ALTERNATE, 0xff0, 0x220);
  //I2C3 SCL
  GpioClrSetBits(I2C3_ALTERNATE_SCL, 0xf, 0x2);
  //I2C3 SDA
  GpioClrSetBits(I2C3_ALTERNATE_SDA, 0xf0000000, 0x20000000);
  //GMAC0 Delay Config
  GpioClrSetBits(GMAC0_DELAY, 0xff000000, 0x21000000);
  //GMAC1 Delay Config
  GpioClrSetBits(GMAC1_DELAY, 0xff000000, 0x21000000);
  //GMAC1 Alternate
  GpioClrSetBits(GMAC1_ALTERNATE, 0x0fffff00, 0x01111100);
  //Gpio0 port7  use for ec interrupt
  GpioClrSetBits(GPIO0_PORT7, 0xf0000000, 0x10000000);
  //Gpio1 port5  ust for touchpad interrupt
  GpioClrSetBits(GPIO1_PORT5, 0xf0000, 0x10000);
  //Gpio1 port6  use for power button interrupt
  GpioClrSetBits(GPIO1_PORT6, 0xf000, 0x1000);

  return EFI_SUCCESS;
}



