/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#include <Library/DxeServicesTableLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/DxeServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/UefiLib.h>
#include <Library/BaseLib.h>

EFI_STATUS
EFIAPI
InitializeFixGcdDxe (
  IN EFI_HANDLE        ImageHandle,
  IN EFI_SYSTEM_TABLE  *SystemTable
  )
{
  EFI_STATUS                  Status;

  Status = gDS->AddMemorySpace (
                  EfiGcdMemoryTypeReserved,
                  0x1000000, 0x27007000,
                  EFI_MEMORY_UC
                  );
  ASSERT_EFI_ERROR (Status);

  Status = gDS->SetMemorySpaceAttributes (
                  0x1000000, 0x27007000,
                  EFI_MEMORY_UC
                  );
  ASSERT_EFI_ERROR (Status);

  Status = gDS->AddMemorySpace (
                  EfiGcdMemoryTypeReserved,
                  0x28015000, 0x2FFEB000,
                  EFI_MEMORY_UC
                  );
  ASSERT_EFI_ERROR (Status);

  Status = gDS->SetMemorySpaceAttributes (
                  0x28015000, 0x2FFEB000,
                  EFI_MEMORY_UC
                  );
  ASSERT_EFI_ERROR (Status);

  Status = gDS->AddMemorySpace (
                  EfiGcdMemoryTypeReserved,
                  0xFC000000, 0x4000000,
                  EFI_MEMORY_UC
                  );
  ASSERT_EFI_ERROR (Status);

  Status = gDS->SetMemorySpaceAttributes (
                  0xFC000000, 0x4000000,
                  EFI_MEMORY_UC
                  );
  ASSERT_EFI_ERROR (Status);
//[gliu-0013]add-start
// It was declared in GetSetRtc.c as EFI_MEMORY_RUNTIME
/*
  Status = gDS->AddMemorySpace (
                  EfiGcdMemoryTypeReserved,
                  0x28008000, 0x5000,
                  EFI_MEMORY_UC
                  );
  ASSERT_EFI_ERROR (Status);

  Status = gDS->SetMemorySpaceAttributes (
                  0x28008000, 0x5000,
                  EFI_MEMORY_UC
                  );
  ASSERT_EFI_ERROR (Status);
*/
//[gliu-0013]add-end
  return Status;
}



