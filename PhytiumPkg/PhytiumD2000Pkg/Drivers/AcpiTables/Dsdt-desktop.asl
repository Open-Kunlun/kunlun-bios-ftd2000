/** @file
  Differentiated System Description Table Fields (DSDT)

  Copyright (c) 2014-2015, ARM Ltd. All rights reserved.<BR>
  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  This program and the accompanying materials
  are licensed and made available under the terms and conditions of the BSD License
  which accompanies this distribution.  The full text of the license may be found at
  http://opensource.org/licenses/bsd-license.php

  THE PROGRAM IS DISTRIBUTED UNDER THE BSD LICENSE ON AN "AS IS" BASIS,
  WITHOUT WARRANTIES OR REPRESENTATIONS OF ANY KIND, EITHER EXPRESS OR IMPLIED.

**/

#include "ArmPlatform.h"

//#define	I2CRTC_USE

DefinitionBlock("DsdtTable.aml", "DSDT", 2, "ARMLTD", "PHYTIUM ", EFI_ACPI_ARM_OEM_REVISION) {
  OperationRegion(CPUS, SystemMemory, 0xFFFF1000, 0x1000)
  Field(CPUS, AnyAcc, Lock, Preserve)
  {
      Offset(8),    // Signature

      pSt0,8,
      pSt1,8,
	  pSt2,8,
	  pSt3,8,
  }
  
  Scope(_SB) {
    //
    // A57x2-A53x4 Processor declaration
    //
    Method (_OSC, 4, Serialized)  { // _OSC: Operating System Capabilities
      CreateDWordField (Arg3, 0x00, STS0)
      CreateDWordField (Arg3, 0x04, CAP0)
      If ((Arg0 == ToUUID ("0811b06e-4a27-44f9-8d60-3cbbc22e7b48") /* Platform-wide Capabilities */)) {
      DBGC(2, "_OSC OSPM Arg1:",Arg1)
      DBGC(2, "CAP0:", CAP0)
      DBGC(2, "STS0:", STS0)
        If (!(Arg1 == One)) {
          STS0 &= ~0x1F
          STS0 |= 0x0A
        } Else {
          If ((CAP0 & 0x100)) {
            CAP0 &= ~0x100 /* No support for OS Initiated LPI */
            STS0 &= ~0x1F
            STS0 |= 0x12
          }
        }
      } Else {
        STS0 &= ~0x1F
        STS0 |= 0x06
      }
      Return (Arg3)
    }

    Device (CLU0) { // Cluster0 state
      Name(_HID, "ACPI0010")
      Name(_UID, 1)
      Method (_STA, 0, NotSerialized) {
      	if(OR(\pSt0,\pSt1)){
        	Return(0x0F)
        }
        else{
		    Return(0x00)
        }
      }
      Device(CPU0) { //  Cluster 0, Cpu 0
        Name(_HID, "ACPI0007")
        Name(_UID, 0)
        Name (_DSD, Package () {
			ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
	        Package () {
				Package () {"clock-name","c0"},
				Package () {"clock-domain",0},
	        }
        })
        Method (_STA, 0, NotSerialized) {
	      if(Lequal(\pSt0,1)){
	          Return(0x0F)
	      }
	      else{
			  Return(0x00)
	      }
        }
	  }

	  Device(CPU1) { //  Cluster 0, Cpu 1
        Name(_HID, "ACPI0007")
        Name(_UID, 1)
        Name (_DSD, Package () {
			ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
	        Package () {
				Package () {"clock-name","c0"},
				Package () {"clock-domain",0},
	        }
        })
        Method (_STA, 0, NotSerialized) {
	      if(Lequal(\pSt1,1)){
	          Return(0x0F)
	      }
	      else{
		      Return(0x00)
	      }
        }
      }
    }

    Device (CLU1) { // Cluster1 state
      Name(_HID, "ACPI0010")
      Name(_UID, 2)
      Method (_STA, 0, NotSerialized) {
	      if(OR(\pSt2,\pSt3)){
	          Return(0x0F)
	      }
	      else{
			  Return(0x00)
	      }
      }
      Device(CPU2) { //  Cluster 0, Cpu 2
        Name(_HID, "ACPI0007")
        Name(_UID, 2)
        Name (_DSD, Package () {
			ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
	        Package () {
				Package () {"clock-name","c1"},
				Package () {"clock-domain",1},
	        }
        })
        Method (_STA, 0, NotSerialized) {
	      if(Lequal(\pSt2,1)){
	          Return(0x0F)
	      }
	      else{
			  Return(0x00)
	      }
        }
      }

      Device(CPU3) { // Cluster 0, Cpu 3
        Name(_HID, "ACPI0007")
        Name(_UID, 3)
         Name (_DSD, Package () {
			ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
	        Package () {
				Package () {"clock-name","c1"},
				Package () {"clock-domain",1},
	        }
        })
        Method (_STA, 0, NotSerialized) {
	      if(Lequal(\pSt3,1)){
	          Return(0x0F)
	      }
	      else{
			  Return(0x00)
	      }
        }
      }
    }

    //UART 1
   	Device(UAR1) {
      Name(_HID, "ARMH0011")
      Name(_UID, 1)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28001000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {39}
      })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }

    //UART 0
	Device(UAR0) {
      Name(_HID, "ARMH0011")
      Name(_UID, 0)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28000000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) { 38 }
      })

      Method (_STA, 0, NotSerialized) {
      Return(0x0F)
      }
    }

    //UART 2
   	Device(UAR2) {
      Name(_HID, "ARMH0011")
      Name(_UID, 2)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28002000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {40}
      })

      Method (_STA, 0, NotSerialized) {
      Return(0x0F)
      }
    }

	//UART 3
	Device(UAR3) {
      Name(_HID, "ARMH0011")
      Name(_UID, 3)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28003000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {41}
      })

      Method (_STA, 0, NotSerialized) {
      Return(0x0F)
      }
    }

#if 0
	Device (EP0) {
	Name (_HID, "FTEP0001")
	Name (_UID, 0)
	Name (_CRS, ResourceTemplate () {
	Memory32Fixed (ReadWrite, 0x29101000, 0x1000)
	Memory32Fixed (ReadWrite, 0x29040000, 0x10000)
	QWordMemory (ResourceConsumer, PosDecode,
          MinFixed, MaxFixed,
          NonCacheable, ReadWrite,
          0x00000000,               // Granularity
          0x2300000000,               // Min Base Address
          0x23003fffff,               // Max Base Address
          0x0000000000,               // Translate
          0x0000400000                // Length
         )
	QWordMemory (ResourceConsumer, PosDecode,
          MinFixed, MaxFixed,
           NonCacheable, ReadWrite,
           0x00000000,               // Granularity
          0x2300400000,               // Min Base Address
          0x23007fffff,               // Max Base Address
          0x0000000000,               // Translate
          0x0000400000                // Length
         )
	QWordMemory (ResourceConsumer, PosDecode,
          MinFixed, MaxFixed,
           NonCacheable, ReadWrite,
           0x00000000,               // Granularity
          0x1100000000,               // Min Base Address
          0x11ffffffff,               // Max Base Address
          0x0000000000,               // Translate
          0x0100000000                // Length
         )
	QWordMemory (ResourceConsumer, PosDecode,
          MinFixed, MaxFixed,
           NonCacheable, ReadWrite,
           0x00000000,               // Granularity
          0x0000000000,               // Min Base Address
          0x00ffffffff,               // Max Base Address
          0x0000000000,               // Translate
          0x0100000000                // Length
         )
	Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 64,59}
	})
	}
#endif

	Device (ETH0) {
      Name (_HID, "FTGM0001")
      Name (_UID, 0)
      Name (_CCA, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x2820c000, 0x2000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 81 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"interrupt-names", "macirq"},
          Package () {"clocks", 3},
          Package () {"clock-names", "stmmaceth"},
          Package () {"snps,pbl", 0x10},
          Package () {"snps,abl", 0x20},
          Package () {"snps,burst_len", 0x0e},
          Package () {"snps,multicast-filter-bins", 0x40},                                                                                                                                   
          Package () {"snps,perfect-filter-entries", 0x41},
          Package () {"max-frame-size", 0x2328},
          Package () {"phy-mode", "rgmii"},
		  Package () {"clock-frequency",250000000},
		  Package () {"bus_id",0},
          Package () {"txc-skew-ps", 0x3e8},
          Package () {"rxc-skew-ps", 0x3e8},
        }
      })

      Method (_STA, 0, NotSerialized) {
      Return(0x0F)
      }
    }

	Device (ETH1) {
      Name (_HID, "FTGM0001")
      Name (_UID, 1)
      Name (_CCA, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28210000, 0x2000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 82 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"interrupt-names", "macirq"},
          Package () {"clocks", 3},
          Package () {"clock-names", "stmmaceth"},
          Package () {"snps,pbl", 0x10},
          Package () {"snps,abl", 0x20},
          Package () {"snps,burst_len", 0x0e},
          Package () {"snps,multicast-filter-bins", 0x40},
          Package () {"snps,perfect-filter-entries", 0x41},
          Package () {"max-frame-size", 0x2328},
          Package () {"phy-mode", "rgmii"},
		  Package () {"clock-frequency",250000000},
          Package () {"bus_id",1},
		  Package () {"txc-skew-ps", 0x3e8},
          Package () {"rxc-skew-ps", 0x3e8},
        }
      })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }

    Device(GPI0) {
      Name(_HID, "FTGP0001")  //FTGPIO01
      Name(_UID, 0)
      Name (_CRS, ResourceTemplate ()  {
        Memory32Fixed (ReadWrite, 0x28004000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) { 42 }
      })

      Device(GP00) {
	    Name(_HID, "FTGP0001")
	    Name(_ADR,0)
		Name(_CRS,ResourceTemplate() {
		})

        Name (_DSD, Package () {
          ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package () {"reg",0},
            Package () {"snps,nr-gpios",8},
        }
        })
      }          

      Device(GP01) {
        Name(_HID, "FTGP0001")  //FTGPIO01
	    Name(_ADR, 1)
    	Name(_CRS,ResourceTemplate() {
		})
	    Name (_DSD, Package () {
          ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package () {"reg",1},
            Package () {"snps,nr-gpios",8},
          }
        })
      }          
    }

    Device(GPI1) {
      Name(_HID, "FTGP0001")  //FTGPIO01
      Name(_UID, 1)
      Name (_CRS, ResourceTemplate ()  {
        Memory32Fixed (ReadWrite, 0x28005000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) { 43 }
      })

      Device(GP00) {
        Name(_HID, "FTGP0001")  //FTGPIO01
	    Name(_ADR, 0)
	   	Name(_CRS,ResourceTemplate() {
		})
	
        Name (_DSD, Package () {
          ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package () {"reg",0},
            Package () {"snps,nr-gpios",8},
          }
        })
      }          
	
      Device(GP01) {
        Name(_HID, "FTGP0001")  //FTGPIO01
	    Name(_ADR, 1)
	   	Name(_CRS,ResourceTemplate() {
		})
	
        Name (_DSD, Package () {
          ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package () {"reg",1},
            Package () {"snps,nr-gpios",8},
          }
        })
      }          
    }

    Device (CLK2) {
      Name (_HID, "FTCK0002")
      Name (_UID, 0x01)
      Name (_DSD, Package() {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package(2) {"clock-frequency", 48000000}
          }
        })

      Method (FREQ, 0x0, NotSerialized) {
        Return (50000000)
      }
    }
#if 1
    Device (I2C0) {
      Name (_HID, "FTI20001")  //FTI2C001
      Name (_UID, Zero)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28006000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 44 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
		}
      })

	    Device (EPR0) {
          Name (_HID, "INT0002")  
          Name (_UID, Zero)  
          Name (_DSD, Package () {
          ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {Package () {"pagesize", 16}}
          })

          Name (_CRS, ResourceTemplate () { 
            I2cSerialBus (0x0057, ControllerInitiated, 400000,
            AddressingMode7Bit, "\\_SB.I2C0",
            0x00, ResourceConsumer, ,
            )
          })
        }

    }
#endif


    Device (I2C1) {
      Name (_HID, "FTI20001")  //FTI2C001
      Name (_UID, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28007000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 45 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
        } 
      })
    #ifdef	I2CRTC_USE
			Device (RTC0) {
             Name (_HID, "FTDS1339")
             Name (_UID, Zero)
             Name (_CRS, ResourceTemplate () {
                 I2CSerialBusV2 (0x68, ControllerInitiated, 100000,AddressingMode7Bit, "\\_SB.I2C1", 0, ResourceConsumer, ,)
             })  
         } 
    #endif
    }
#if 0
    Device (I2C2) {
      Name (_HID, "FTI20001")  //FTI2C001
      Name (_UID, 2)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28008000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 46 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
        }
      })
    }

    Device (I2C3) {
      Name (_HID, "FTI20001")  //FTI2C001
      Name (_UID, 3)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28009000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 47 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
        }
      })
    }
#endif

#if 0
 	Device(SPI0) {
      Name(_HID, "FHTY000E")
      Name(_UID, 0)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x2800c000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {50}
      })
      Name (_DSD, Package() {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package() {"clocks", Package () {"\\_SB.CLK2"}},
            Package() {"num-cs", 4}
          }
      })

      Device(FLAS) {
        Name(_HID, "SPT0001")
        Name(_UID, 0)
		Name(_CRS, ResourceTemplate () {
    	  // Index 0
          SPISerialBus (           // SCKL - GPIO 11 - Pin 23
                                   // MOSI - GPIO 10 - Pin 19
                                   // MISO - GPIO 9  - Pin 21
                                   // CE0  - GPIO 8  - Pin 24
            0,                     // Device selection (CE0)
            PolarityLow,           // Device selection polarity
            FourWireMode,          // WireMode
            8,                     // DataBit len
            ControllerInitiated,   // Slave mode
            4000000,               // Connection speed
            ClockPolarityLow,      // Clock polarity
            ClockPhaseFirst,       // Clock phase
            "\\_SB.SPI0",          // ResourceSource: SPI bus controller name
            0,                     // ResourceSourceIndex
                                   // Resource usage
                                   // DescriptorName: creates name for offset of resource descriptor
          )                        // Vendor Data
        })
      }
	}

 	Device(SPI1) {
      Name(_HID, "FHTY000E")
      Name(_UID, 1)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28013000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {51}
      })
      Name (_DSD, Package() {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package() {"clocks", Package () {"\\_SB.CLK2"}},
            Package() {"num-cs", 4}
          }
      })

      Device(FLAS) {
        Name(_HID, "SPT0001")
        Name(_UID, 1)
		Name(_CRS, ResourceTemplate () {
    	  // Index 0
          SPISerialBus (           // SCKL - GPIO 11 - Pin 23
                                   // MOSI - GPIO 10 - Pin 19
                                   // MISO - GPIO 9  - Pin 21
                                   // CE0  - GPIO 8  - Pin 24
            0,                     // Device selection (CE0)
            PolarityLow,           // Device selection polarity
            FourWireMode,          // WireMode
            8,                     // DataBit len
            ControllerInitiated,   // Slave mode
            4000000,               // Connection speed
            ClockPolarityLow,      // Clock polarity
            ClockPhaseFirst,       // Clock phase
            "\\_SB.SPI1",          // ResourceSource: SPI bus controller name
            0,                     // ResourceSourceIndex
                                   // Resource usage
                                   // DescriptorName: creates name for offset of resource descriptor
          )                        // Vendor Data
        })
      }
	}
#endif

#if 0
	Device (CLK3) {
      Name (_HID, "FTCK0003")
      Name (_UID, 0x0)
      Name (_DSD, Package() {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package(2) {"clock-frequency", 600000000}
          }
        })

      Method (FREQ, 0x0, NotSerialized) {
        Return (600000000)
      }
    }

	//can0
    Device(CAN0) {
      Name(_HID, "FTCN0001")
      Name(_UID, 0)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28207000, 0x400)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {119}
      })

      Name (_DSD, Package () {
      ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
      Package () {
        Package () {"clocks", Package (){"\\_SB.CLK3"}},
        Package () {"clock-names", "ftcan_clk"},
        Package () {"tx-fifo-depth", 0x40},
        Package () {"rx-fifo-depth", 0x40}
      }
    })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }

	//can1
    Device(CAN1) {
      Name(_HID, "FTCN0001")
      Name(_UID, 1)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28207400, 0x400)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {123}
      })

      Name (_DSD, Package () {
      ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
      Package () {
        Package () {"clocks", Package (){"\\_SB.CLK3"}},
        Package () {"clock-names", "ftcan_clk"},
        Package () {"tx-fifo-depth", 0x40},
        Package () {"rx-fifo-depth", 0x40}
      }
    })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }

	//can2
    Device(CAN2) {
      Name(_HID, "FTCN0001")
      Name(_UID, 2)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28207800, 0x400)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {124}
      })

      Name (_DSD, Package () {
      ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
      Package () {
        Package () {"clocks", Package (){"\\_SB.CLK3"}},
        Package () {"clock-names", "ftcan_clk"},
        Package () {"tx-fifo-depth", 0x40},
        Package () {"rx-fifo-depth", 0x40}
      }
    })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }
#endif

#if 0
	//SCTO
    Device (SCTO) {
      Name (_HID, "SCTO0001")
      Name (_UID, 0)
      Name (_CRS, ResourceTemplate () {
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 122 }
      })

      Method (_STA, 0, NotSerialized) {
      Return(0x0F)
      }
    }
#endif

	//SD controller
	Device (SDC0) {
      Name (_HID, "FTSD0001")
      Name (_UID, 0)
      Name (_CCA, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28207C00, 0x100)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 52,53,54}
      })

      Method (_STA, 0, NotSerialized) {
      Return(0x0F)
      }
	}
    
	// HDAudio device
	Device (HDA0) {
      Name (_HID, "FTHD0001")
      Name (_UID, 0)
      Name (_CCA, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28206000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 55}
      })

      Method (_STA, 0, NotSerialized) {
      Return(0x0F)
      }
	}

  OperationRegion(COM0, SystemMemory, FixedPcdGet64 (PcdSerialRegisterBase), 0x1000)
      Field(COM0, DWordAcc, NoLock, Preserve) {
        DAT8, 8,
        Offset(5),
        , 5,
        TRDY, 1,
	    }

	Method(TCOM, 1, Serialized) {
	Add(SizeOf(Arg0), One, Local0)
    Name(BUF0, Buffer(Local0){})
    Store(Arg0, BUF0)
    store(0, Local1)
    Decrement(Local0)
    While(LNotEqual(Local1, Local0)){
      while(LEqual(TRDY, ONE)){}
      Store(DerefOf(Index(BUF0, Local1)), DAT8)
      Increment(Local1)
    }
  }

    Method(DBGC, 3, Serialized) {      // DBGC(count, string, int)
      Name(CRLF, Buffer(2){0x0D, 0x0A})
      TCOM(Arg1)
      if(LEqual(Arg0, 2)){
        TCOM(ToHexString(Arg2))
      }
      TCOM(CRLF)
    }

#if 0
	Device (OPTE) {
      Name (_HID, "FTOP0001")
      Name (_UID, 0)
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"compatible", "linaro,optee-tz"},
          //Package () {"method", "smc"},
        }
      })
	}
#endif

#ifndef	I2CRTC_USE
	Device (RTC0) {
      Name (_HID, "FTRT0001")
      Name (_UID, Zero) 
	  Name (_CRS, ResourceTemplate () {
		Memory32Fixed (ReadWrite, 0x2800d000, 0x00001000,)
		Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive, ,, ) { 36 }	
	  })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
	}			
#endif

	//SCPI 
	Device (SCPI) {
	  Name (_HID, "FTSC0001")
 	  Name (_UID, 0)
	  Name (_DSD, Package () {
		ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
		  Package () {
			Package () {"mbox", \_SB.MBX0},  
			Package () {"shmem", \_SB.SHM0}, 
			Package () {"shmem_start", 0x2a007000}, 
			Package () {"shmem_size", 0x800} 
		  }
	  })

	  Device(CLK1){
		Name(_HID, "FTCK0001")
		Name(_UID,0)
		Name (_DSD, Package () {
		  ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
		  Package () {
			Package () {"compatible", "arm,scpi-clocks"}
		  }
	    })
				
		Device(CK10){
		  Name(_ADR,0)
		  Name (_DSD, Package () {
			ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
			  Package () {
				Package () {"compatible", "arm,scpi-dvfs-clocks"},
				Package () {"#clock-cells", 1},
				Package () {"clock-indices", Package () {0,1}},
				Package () {"clock-output-names", Package () {"c0","c1"}}
			  }
		  })
		}
	  }	

	  Device(SEN1){
		Name(_HID, "FTSS0001")
		Name (_DSD, Package () {
		  ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
		  Package () {
			Package () {"compatible", "arm,scpi-sensors"},
			Package () {"#thermal-sensor-cells", 1}
		  }	
		})
	  }
    }
		
	//Mailbox
	Device(MBX0){
	  Name (_HID, "FTMB0001")
	  Name (_UID, 0)
	  Name (_CRS, ResourceTemplate () {
		Memory32Fixed (ReadWrite, 0x2A000000, 0x1000)
		Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 80 }
		Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 79 }
	  })
	}

	//Shared memory
	Device(SHM0){
	  Name (_HID, "FTSH0001")
	  Name (_UID, 0)
	  Name (_CRS, ResourceTemplate () {
	    Memory32Fixed(ReadWrite, 0x2a006000, 0x2000)
	  })
	
	  Device(SH00){
		Name (_ADR,0)
		Name (_DSD, Package () {
		  ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
		  Package () {
			Package () {"start", 0x1000},
			Package () {"size", 0x800}
		  }
		})
	  }
			
	  Device(SH01){
		Name(_ADR,1)
		Name (_DSD, Package () {
		  ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
		  Package () {
			Package () {"start", 0x1000},
			Package () {"size", 0x800}
		  }
		})
	  }
	}
  } // Scope(_SB)
}
