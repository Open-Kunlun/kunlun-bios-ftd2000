/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

#include "ArmPlatform.h"

DefinitionBlock("DsdtTable.aml", "DSDT", 2, "PHYLTD", "PHYTIUM.", EFI_ACPI_ARM_OEM_REVISION) {

  OperationRegion(CPUS, SystemMemory, 0xFFFF1000, 0x1000)
  Field(CPUS, AnyAcc, Lock, Preserve)
  {
      Offset(8),    // Signature

      pSt0,8,
      pSt1,8,
	  pSt2,8,
	  pSt3,8,
      Tee,8,
      Scp,8,
      Ec,8,
      DisE,8,
      Dp,8
  }

  include ("Cpu.asl")
  include ("Uart.asl")
  include ("Gmac.asl")
  include ("Gpio.asl")
  include ("I2c.asl")
  include ("Sd.asl")
  include ("Hda.asl")
  include ("Scp.asl")
  include ("Opentee.asl")
  include ("Can.asl")
  include ("Spi.asl")
  include ("Scto.asl")
  //include ("I2s.asl")
  include ("Lpc.asl")
  include ("X100.asl")
  include ("Pwrbtn.asl")
}
