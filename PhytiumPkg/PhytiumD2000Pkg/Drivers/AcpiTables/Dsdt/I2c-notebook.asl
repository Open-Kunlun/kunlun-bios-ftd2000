/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (CLK2) {
      Name (_HID, "PHYT8001")
      Name (_CID, "FTCK0002") /* compatible with v1.0 */
      Name (_UID, 0x01)
      Name (_DSD, Package() {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package(2) {"clock-frequency", 48000000}
          }
        })

      Method (FREQ, 0x0, NotSerialized) {
        Return (48000000)
      }
    }

    Device (I2C0) {
      Name (_HID, "PHYT0003")
      Name (_CID, "FTI20001") /* compatible with v1.0 */
      Name (_UID, Zero)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28006000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 44 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
		}
      })
	}

    Device (I2C1) {
      Name (_HID, "PHYT0003")
      Name (_CID, "FTI20001") /* compatible with v1.0 */
      Name (_UID, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28007000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 45 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
        } 
      })

      Device (RTC0) {
        Name (_HID, "FTDS1339")
        Name (_CID, "DS1339")
        Name (_UID, Zero)
        Name (_CRS, ResourceTemplate () {
          I2CSerialBusV2 (0x68, ControllerInitiated, 100000,AddressingMode7Bit, "\\_SB.I2C1", 0, ResourceConsumer, ,)
        })
      }
    }

    Device (I2C2) {
      Name (_HID, "PHYT0003")
      Name (_CID, "FTI20001") /* compatible with v1.0 */
      Name (_UID, 2)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28008000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 46 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
        }
      })
    }

    Device (I2C3) {
      Name (_HID, "PHYT0003")
      Name (_CID, "FTI20001") /* compatible with v1.0 */
      Name (_UID, 3)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28009000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 47 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
        }
      })
    }
}
