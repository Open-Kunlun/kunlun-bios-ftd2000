/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB) {
    Method(_OSC,4) {
        If(LEqual(Arg0,ToUUID("0811B06E-4A27-44F9-8D60-3CBBC22E7B48"))) {
          //Create DWord filelds
          CreateDWordField(Arg3,0,CDW1)
          CreateDWordField(Arg3,4,CDW2)
          //Set Interrupt ResourceSource support bit13
          Or(CDW2,0x2000,CDW2)
          Return(Arg3)
        } Else {
          And(CDW2,0,CDW2)
          Return(Arg3)
        }
    }    
}
