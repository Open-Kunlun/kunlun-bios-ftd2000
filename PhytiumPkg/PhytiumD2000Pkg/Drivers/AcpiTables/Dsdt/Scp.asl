/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device(CLK1){
    Name (_HID, "PHYT8001") /* scpi-clock */
    Name (_CID, "FTCK0001") /* compatible with v1.0 */
    Name(_UID,0)
    Name (_DSD, Package () {
      ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
      Package () {
        Package () {"compatible", "arm,scpi-clocks"}
      }
    })

      Device(CK10) {
        Name(_ADR,0)
        Name (_DSD, Package () {
          ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package () {"compatible", "arm,scpi-dvfs-clocks"},
            Package () {"#clock-cells", 1},
            Package () {"clock-indices", Package () {0,1,2,3}},
            Package () {"clock-output-names", Package () {"c0","c1", "c2", "c3"}}
          }
        })
      }
    }

    Device (SCPI) {
      Name (_HID, "PHYT0008") /* SCPI */
      Name (_CID, "FTSC0001") /* compatible with v1.0 */
      Name (_UID, 0)
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"mbox", \_SB.MBX0},  
          Package () {"shmem_start", 0x2a007000}, 
          Package () {"shmem_size", 0x800} 
        }
      })

	  Device(SEN1){
		Name(_HID, "PHYT000D")
		Name(_CID, "FTSS0001")
		Name (_DSD, Package () {
		  ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
		  Package () {
			Package () {"compatible", "arm,scpi-sensors"},
			Package () {"#thermal-sensor-cells", 1}
		  }	
		})
	  }
    }

    //Mailbox
    Device(MBX0){
      Name (_HID, "PHYT0009") /* MailBox */
      Name (_CID, "FTMB0001") /* compatible with v1.0 */
      Name (_UID, 0)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x2A000000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 80 }
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 79 }
      })
    }
}
