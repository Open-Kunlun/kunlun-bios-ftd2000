/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope (_SB) {
  Device (Dec0) {
    Name (_HID, "ESSX8336")
    /*Name (_CID, "FHYT8336")*/
    Name (_UID, Zero)
    Name (_CRS, ResourceTemplate () {
      I2CSerialBusV2 (0x10, ControllerInitiated, 100000,AddressingMode7Bit, "\\_SB.PCI0.RP01.BRG1.BRG2.I2C2", 0, ResourceConsumer, ,)
      GpioIo (Exclusive, PullUp, 0, 0, IoRestrictionOutputOnly, "\\_SB.PCI0.RP01.BRG1.BRG2.Gp00", 0, ResourceConsumer)
        { 26 }

      GpioInt(Edge, ActiveHigh, ExclusiveAndWake, PullUp,,"\\_SB.PCI0.RP01.BRG1.BRG2.Gp00") {27}
    })

    Name (_DSD, Package()
    {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"det-gpios", Package(){^Dec0, 1, 0, 0}},
          Package () {"sel-gpios", Package () {^Dec0, 0, 0, 0}},
        }
    })

    Name (_STA, 0xF)
  }

  //machine node
  Device (Dec1) 
  {
    Name (_HID, "PHYT8005")
    /*Name (_CID, "FHYT6666")*/

    Name (_DSD, Package()
    {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"dmic-exist", 1},
          Package () {"headset-mic", 2},
          Package () {"internal-mic", 0},
          Package () {"headphone-exist", 1},
          Package () {"speaker-exist", 1},
        }
    })

    Name (_STA, 0xF)
  }

  Device (Dec2) {
    Name (_HID, "ESSX8388")
    /*Name (_CID, "FHYT8388")*/
    Name (_UID, Zero)
    Name (_CRS, ResourceTemplate () {
      I2CSerialBusV2 (0x11, ControllerInitiated, 100000,AddressingMode7Bit, "\\_SB.PCI0.RP01.BRG1.BRG2.I2C0", 0, ResourceConsumer, ,)
    })

    Name (_STA, 0xF)
  }

  Device (Dec3) 
  {
    Name (_HID, "PHYT8004")
    /*Name (_CID, "FHYT8888")*/
    Name (_UID, Zero)
    Name (_CRS, ResourceTemplate () {
      GpioInt(Edge, ActiveHigh, ExclusiveAndWake, PullUp,,"\\_SB.PCI0.RP01.BRG1.BRG2.Gp00") {28}
    })

    Name (_DSD, Package()
    {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"det-gpios", Package(){^Dec3, 0, 0, 0}},
          Package () {"headset-mic", 0},
          Package () {"internal-mic", 0},
          Package () {"headphone-exist", 0},
          Package () {"speaker-exist", 0},
        }
    })

    Name (_STA, 0xF)
  }

  //machine node for DC i2s
  Device (Dec4) 
  {
    Name (_HID, "PHYT8006")
    Name (DPCN, 0x2)
    Name (_DSD, Package()
    {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"num-dp", DPCN},
        }
    })
    Name (_STA, 0xF)
  }

  /*Device (Dec4) {*/
    /*Name (_HID, "FHYT8988")*/
    /*Name (_UID, Zero)*/
    /*Name (_CRS, ResourceTemplate () {*/
      /*I2CSerialBusV2 (0x1a, ControllerInitiated, 100000,AddressingMode7Bit, "\\_SB.PCI0.RP01.BRG1.BRG2.I2C1", 0, ResourceConsumer, ,)*/
    /*})*/
  /*}*/

}
