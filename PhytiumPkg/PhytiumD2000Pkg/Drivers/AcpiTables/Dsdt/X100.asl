/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (XDIS) {
      Name (_HID, "PHYT8007")
      Name (_STA, 0xF)
    }
}
