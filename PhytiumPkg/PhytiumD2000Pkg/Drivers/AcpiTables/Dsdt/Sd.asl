/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (SDC0) {
      Name (_HID, "PHYT0005")
      Name (_CID, "FTSD0001") /* compatible with v1.0 */
      Name (_UID, 0)
      Name (_CCA, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28207C00, 0x100)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 52,53,54}
      })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
	}

}

