/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (ETH0) {
      Name (_HID, "PHYT0004")
      Name (_CID, "FTGM0001") /* compatible with v1.0 */
      Name (_UID, 0)
      Name (_CCA, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x2820c000, 0x2000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 81 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"interrupt-names", "macirq"},
          Package () {"clocks", 3},
          Package () {"clock-names", "stmmaceth"},
          Package () {"snps,pbl", 0x10},
          Package () {"snps,abl", 0x20},
          Package () {"snps,burst_len", 0x0e},
          Package () {"snps,multicast-filter-bins", 0x40},
          Package () {"snps,perfect-filter-entries", 0x41},
          Package () {"max-frame-size", 0x2328},
          Package () {"phy-mode", "rgmii-rxid"},
          Package () {"clock-frequency",250000000},
          Package () {"bus_id",0},
          Package () {"txc-skew-ps", 0x3e8},
          Package () {"rxc-skew-ps", 0x3e8},
          Package () {"mdc_clock_selection", 0x5},
        }
      })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }

	Device (ETH1) {
      Name (_HID, "PHYT0004")
      Name (_CID, "FTGM0001") /* compatible with v1.0 */
      Name (_UID, 1)
      Name (_CCA, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28210000, 0x2000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 82 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"interrupt-names", "macirq"},
          Package () {"clocks", 3},
          Package () {"clock-names", "stmmaceth"},
          Package () {"snps,pbl", 0x10},
          Package () {"snps,abl", 0x20},
          Package () {"snps,burst_len", 0x0e},
          Package () {"snps,multicast-filter-bins", 0x40},
          Package () {"snps,perfect-filter-entries", 0x41},
          Package () {"max-frame-size", 0x2328},
          Package () {"phy-mode", "rgmii-rxid"},
          Package () {"clock-frequency",250000000},
          Package () {"bus_id",1},
          Package () {"txc-skew-ps", 0x3e8},
          Package () {"rxc-skew-ps", 0x3e8},
          Package () {"mdc_clock_selection", 0x5},
        }
      })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }
}
