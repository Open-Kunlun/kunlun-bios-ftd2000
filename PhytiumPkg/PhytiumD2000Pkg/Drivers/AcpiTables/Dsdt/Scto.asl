/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (SCTO) {
      Name (_HID, "PHYT0012")
      Name (_UID, 0)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28220000, 0x10000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) { 122 }
      })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
	}
}

