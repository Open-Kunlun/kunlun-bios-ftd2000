/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Device(GPI0) {
	Name(_HID, "FTGP0001")
	Name(_ADR, 0)
	Name(_UID, 0)
	Name(_CRS, ResourceTemplate() {
		Memory32Fixed (ReadWrite, 0x28004000, 0x1000)
		Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) { 42 }
	})

	Device(GP00) {
		Name(_ADR, 0)
		Name(_DSD, Package() {
			ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
			Package() {
				Package () {"reg",0},
				Package () {"nr-gpios",8},
			}
		})
	}

	Device(GP01) {
		Name(_ADR, 1)
		Name(_DSD, Package() {
			ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
			Package() {
				Package () {"reg",1},
				Package () {"nr-gpios",8},
			}
		})
	}

}

Device(GPI1) {
	Name(_HID, "FTGP0001")
	Name(_ADR, 0)
	Name(_UID, 1)
	Name(_CRS, ResourceTemplate() {
		Memory32Fixed (ReadWrite, 0x28005000, 0x1000)
		Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) { 43 }
	})

	Device(GP00) {
		Name(_ADR, 0)
		Name(_DSD, Package() {
			ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
			Package() {
				Package () {"reg",0},
				Package () {"nr-gpios",8},
			}
		})
	}

	Device(GP01) {
		Name(_ADR, 1)
		Name(_DSD, Package() {
			ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
			Package() {
				Package () {"reg",1},
				Package () {"nr-gpios",8},
			}
		})
	}

}
