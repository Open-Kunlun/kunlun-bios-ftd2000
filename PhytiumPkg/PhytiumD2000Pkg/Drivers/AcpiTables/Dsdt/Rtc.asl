/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (RTC0) {
      Name (_HID, "FTRT0001")
      Name (_UID, Zero) 
	  Name (_CRS, ResourceTemplate () {
		Memory32Fixed (ReadWrite, 0x2800d000, 0x00001000,)
		Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive, ,, ) { 36 }	
	  })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
	}		
}
