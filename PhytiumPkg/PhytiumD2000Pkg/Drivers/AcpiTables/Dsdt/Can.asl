/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (CLK3) {
      Name (_HID, "PHYT8002")
      Name (_CID, "FTCK0002") /* compatible with v1.0 */
      Name (_UID, 0x0)
      Name (_DSD, Package() {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package(2) {"clock-frequency", 600000000}
          }
        })

      Method (FREQ, 0x0, NotSerialized) {
        Return (600000000)
      }
    }

	//can0
    Device(CAN0) {
      Name (_HID, "PHYT000A")
      Name (_CID, "FTCN0001") /* compatible with v1.0 */
      Name(_UID, 0)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28207000, 0x400)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {119}
      })

      Name (_DSD, Package () {
      ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
      Package () {
        Package () {"clocks", Package (){"\\_SB.CLK3"}},
        Package () {"clock-frequency", 600000000},
        Package () {"clock-names", "ftcan_clk"},
        Package () {"tx-fifo-depth", 0x40},
        Package () {"rx-fifo-depth", 0x40}
      }
    })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }

	//can1
    Device(CAN1) {
      Name (_HID, "PHYT000A")
      Name (_CID, "FTCN0001") /* compatible with v1.0 */
      Name(_UID, 1)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28207400, 0x400)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {123}
      })

      Name (_DSD, Package () {
      ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
      Package () {
        Package () {"clocks", Package (){"\\_SB.CLK3"}},
        Package () {"clock-frequency", 600000000},
        Package () {"clock-names", "ftcan_clk"},
        Package () {"tx-fifo-depth", 0x40},
        Package () {"rx-fifo-depth", 0x40}
      }
    })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }

	//can2
    Device(CAN2) {
      Name (_HID, "PHYT000A")
      Name (_CID, "FTCN0001") /* compatible with v1.0 */
      Name(_UID, 2)
      Name(_CRS, ResourceTemplate() {
        Memory32Fixed(ReadWrite, 0x28207800, 0x400)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) {124}
      })

      Name (_DSD, Package () {
      ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
      Package () {
        Package () {"clocks", Package (){"\\_SB.CLK3"}},
        Package () {"clock-frequency", 600000000},
        Package () {"clock-names", "ftcan_clk"},
        Package () {"tx-fifo-depth", 0x40},
        Package () {"rx-fifo-depth", 0x40}
      }
    })

      Method (_STA, 0, NotSerialized) {
        Return(0x0F)
      }
    }
}
