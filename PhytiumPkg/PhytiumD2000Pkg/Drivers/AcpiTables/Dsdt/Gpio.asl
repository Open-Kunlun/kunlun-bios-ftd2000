/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/
Scope(_SB)
{
	Device(GPI0) {
      Name(_HID, "PHYT0001")
      Name(_CID, "FTGP0001")  /* compatible with v1.0 */
      Name(_UID, 0)
      Name (_CRS, ResourceTemplate ()  {
        Memory32Fixed (ReadWrite, 0x28004000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) { 42 }
      })

      Device(GP00) {
		Name(_ADR, 0)
        Name (_DSD, Package () {
          ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package () {"reg",0},
            Package () {"snps,nr-gpios",8},
	        Package () {"nr-gpios",8},
          }
        })
      }          
    }

    Device(GPI1) {
      Name(_HID, "PHYT0001")
      Name(_CID, "FTGP0001")  /* compatible with v1.0 */
      Name(_UID, 1)
      Name (_CRS, ResourceTemplate ()  {
        Memory32Fixed (ReadWrite, 0x28005000, 0x1000)
        Interrupt(ResourceConsumer, Level, ActiveHigh, Exclusive) { 43 }
      })

      Device(GP00) {
		Name(_ADR, 0)
        Name (_DSD, Package () {
          ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package () {"reg",0},
            Package () {"snps,nr-gpios",8},
	        Package () {"nr-gpios",8},
          }
        })
      }          
//[gliu-0047]add-start
#if 0
      Name(_AEI, ResourceTemplate ()
      {
        GpioInt (Edge, ActiveLow, ExclusiveAndWake, PullUp, 0x0000,
                    "\\_SB.GPI1", 0x00, ResourceConsumer, ,
                    )
                    {   // Pin list
                        0x0006
                    }
      })

      Method (_E06, 0x0, NotSerialized) 
      {
        Notify (\_SB.PWRB, 0x80)
      }
#endif
//[gliu-0047]add-end
    }
}
