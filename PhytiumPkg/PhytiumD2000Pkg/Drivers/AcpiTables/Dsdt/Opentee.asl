/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (OPTE) {
      Name (_HID, "PHYT8003")
      Name (_CID, "FTOP0001")
      Name (_UID, 0)

  
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"compatible", "linaro,optee-tz"},
          Package () {"method", "smc"},
        }
      })

	  Name (_STA, 1)
    }
}
