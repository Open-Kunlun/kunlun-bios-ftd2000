/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (CLK2) {
      Name (_HID, "PHYT8002")
      Name (_CID, "FTCK0002") /* compatible with v1.0 */
      Name (_UID, 0x01)
      Name (_DSD, Package() {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package(2) {"clock-frequency", 48000000}
          }
        })

      Method (FREQ, 0x0, NotSerialized) {
        Return (48000000)
      }
    }

    Device (I2C0) {
      Name (_HID, "PHYT0003")
      Name (_CID, "FTI20001") /* compatible with v1.0 */
      Name (_UID, Zero)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28006000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 44 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
		}
      })

      Method (SSCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0xef,
            0xcb,
            0x00
        })
      }

      Method (FPCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0x28,
            0x4C,
            0x00
        })
      }

      Method (HSCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0x6,
            0x10,
            0x00
        })
      }

	  Device (EPR0) {
        Name (_HID, "INT0002")  
        Name (_UID, Zero)  
        Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
          Package () {
            Package () {"pagesize", 16}
          }
        })

        Name (_CRS, ResourceTemplate () { 
          I2cSerialBus (0x0057, ControllerInitiated, 400000, AddressingMode7Bit, "\\_SB.I2C0", 0x00, ResourceConsumer, ,)
        })
      }
	}

    Device (I2C1) {
      Name (_HID, "PHYT0003")
      Name (_CID, "FTI20001") /* compatible with v1.0 */
      Name (_UID, 1)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28007000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 45 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
        } 
      })

      Method (SSCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0xef,
            0xcb,
            0x00
        })
      }

      Method (FPCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0x28,
            0x4C,
            0x00
        })
      }

      Method (HSCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0x6,
            0x10,
            0x00
        })
      }
#if 0
      Device (RTC0) {
        Name (_HID, "FTDS1339")
        Name (_CID, "DS1339")
        Name (_UID, Zero)
        Name (_CRS, ResourceTemplate () {
          I2CSerialBusV2 (0x68, ControllerInitiated, 100000,AddressingMode7Bit, "\\_SB.I2C1", 0, ResourceConsumer, ,)
        })
      }
#endif
    }

    Device (I2C2) {
      Name (_HID, "PHYT0003")
      Name (_CID, "FTI20001") /* compatible with v1.0 */
      Name (_UID, 2)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28008000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 46 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
        }
      })

      Method (SSCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0xef,
            0xcb,
            0x00
        })
      }

      Method (FPCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0x28,
            0x4C,
            0x00
        })
      }

      Method (HSCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0x6,
            0x10,
            0x00
        })
      }
#if 0
      Device (^TP1) {
        Name (_HID, "PNP0C50")
        Name (_DEP, Package(0x1) 
        {
          GPI1
        })
        Name (SCCG, ResourceTemplate() 
        {
          I2cSerialBusV2 (0x38, ControllerInitiated, 100000, AddressingMode7Bit, "\\_SB.I2C2", 0, ResourceConsumer,,)
          GpioInt(Edge, ActiveLow, ExclusiveAndWake, PullDefault, 0x0, "\\_SB.GPI1", 0x0, ResourceConsumer,,) 
          {
            //Pin list
            0x5
          }
        })
        Method (_CRS, 0, NotSerialized)
        {
          Return(SCCG)
        }

        Method (_DSM, 4, NotSerialized)
        {
            If ((Arg0 == ToUUID ("3cdff6f7-4267-4555-ad05-b30a3d8938de"))) 
            {
                If (Arg2 == Zero) 
                {
                    If (Arg1 == One) 
                    {
                        Return (Buffer(one)
                        {
                            0x3  
                        })
                    } 
                    Else
                    {
                        Return (Buffer(One)
                        {
                            0x0
                        })
                    } 
                }

                If (Arg2 == One)
                {
                  Return (One)
                }
            } 
            Else 
            {
                Return (Buffer(One)
                {
                    0x0
                })
            }
        }
      }
#endif
    }
    Device (I2C3) {
      Name (_HID, "PHYT0003")
      Name (_CID, "FTI20001") /* compatible with v1.0 */
      Name (_UID, 3)
      Name (_CRS, ResourceTemplate () {
        Memory32Fixed (ReadWrite, 0x28009000, 0x1000)
        Interrupt (ResourceConsumer, Level, ActiveHigh, Exclusive) { 47 }
      })
      Name (_DSD, Package () {
        ToUUID("daffd814-6eba-4d8c-8a91-bc9bbf4aa301"),
        Package () {
          Package () {"clock-frequency", 0x186a0},
          Package () {"clocks", Package () {"\\_SB.CLK2"}}
        }
      })

      Method (SSCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0xef,
            0xcb,
            0x00
        })
      }

      Method (FPCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0x28,
            0x4C,
            0x00
        })
      }

      Method (HSCN, 0, NotSerialized)
      {
        Return (Package (0x03)
        {
            0x6,
            0x10,
            0x00
        })
      }

      //
      /*Device (Dec0) {*/
         /*Name (_HID, "FHYT8388")*/
         /*Name (_UID, Zero)*/
         /*Name (_CRS, ResourceTemplate () {*/
           /*I2CSerialBusV2 (0x10, ControllerInitiated, 100000,AddressingMode7Bit, "\\_SB.I2C3", 0, ResourceConsumer, ,)*/
         /*})*/
       /*}*/
    }
}
