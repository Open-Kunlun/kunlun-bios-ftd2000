/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

Scope(_SB)
{
    Device (PWRB) {
	    Name(_HID, EISAID("PNP0C0C"))
    }
}       
 
