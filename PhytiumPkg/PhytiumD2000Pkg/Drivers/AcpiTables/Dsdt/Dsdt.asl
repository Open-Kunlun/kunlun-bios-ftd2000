/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

#include "ArmPlatform.h"

DefinitionBlock("DsdtTable.aml", "DSDT", 2, "PHYLTD", "PHYTIUM.", EFI_ACPI_ARM_OEM_REVISION) {

  include ("Cpu.asl")
  include ("Uart.asl")
  include ("Gmac.asl")
  include ("Gpio.asl")
  include ("I2c.asl")
  include ("Sd.asl")
  include ("Hda.asl")
  // include ("Scp.asl")
  include ("Opentee.asl")
  include ("Can.asl")
  include ("Spi.asl")
  include ("Scto.asl")
  // include ("I2s.asl")
  include ("Lpc.asl")
  include ("X100.asl")
  //include ("Pwrbtn.asl")//[gliu-0044]
}
