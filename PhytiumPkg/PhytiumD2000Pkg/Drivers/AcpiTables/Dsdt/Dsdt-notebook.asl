/** @file
    Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
**/

#include "ArmPlatform.h"

DefinitionBlock("DsdtTable.aml", "DSDT", 2, "PHYLTD", "PHYTIUM.", EFI_ACPI_ARM_OEM_REVISION) {

  OperationRegion(CPUS, SystemMemory, 0xFFFF1000, 0x1000)
  Field(CPUS, AnyAcc, Lock, Preserve)
  {
      Offset(8),    // Signature

      pSt0,8,
      pSt1,8,
	  pSt2,8,
	  pSt3,8,
  }

  include ("Cpu.asl")
  include ("Uart.asl")
  include ("Gmac.asl")
  include ("Gpio.asl")
  include ("Lpc.asl")
  include ("I2c-notebook.asl")
  include ("Sd.asl")
  include ("Hda.asl")
  include ("Scp.asl")
}
