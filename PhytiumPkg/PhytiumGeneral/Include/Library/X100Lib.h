/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef  _X100_LIB_H_
#define  _X100_LIB_H_

#include <Library/UefiLib.h>
#include  <Uefi.h>

BOOLEAN
EFIAPI
IsX100Existed (
  VOID
  );
  
// [jdzhang-0010]>>>
EFI_STATUS
X100ConfigParaInit (
  IN SYSTEM_SETUP_CONFIGURATION  *Configuration
  );
// [jdzhang-0010]<<<

EFI_STATUS
EFIAPI
GetDpChannel (
  VOID
  );

EFI_STATUS
ControlX100DevEnable (
  IN SYSTEM_SETUP_CONFIGURATION  *Configuration
  );

EFI_STATUS
ModifyPciRootPortTag (
  IN SYSTEM_SETUP_CONFIGURATION  *Configuration
  );

#endif
