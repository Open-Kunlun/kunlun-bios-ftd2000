/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _SCP_LIB_H
#define _SCP_LIB_H

int FuncGetScpCapability(int method);
int FuncGetSensorValue(int method, int sensor_id);
int FuncSetFreqWithTemp(int method);
int FuncSetI2cPort(int method);
int FuncSetVotage(int method);
#endif
