/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef   _SE_LIB_H_
#define   _SE_LIB_H_

#define TIME_WAKE_UP_S3_CMD    0x81
#define TIME_WAKE_UP_S45_CMD   0x82
#define SE_CMD  0x1
#define SE_DATA 0X2

/*SE REG*/
#define SE_DEV_ADDR         (0x18)
#define SE_REG_S3_FLAG      (0x007002f00cUL)
#define SE_REG_HAND0        (0x0070021020UL) 
#define SE_REG_HAND1        (0x0070021024UL)
#define SE_REG_HAND2        (0x0070021028UL)
#define SE_REG_DATA         (0x0070021040UL)
#define SE_REG_CHECK        (0x007002f010UL)

INT32
FtI2cReadSe(
  UINT32  dev_addr,
  UINT64  reg_addr,
  UINT32  *read_value
  );


INT32
FtI2cWriteSe(
  UINT32 dev_addr,
  UINT64 reg_addr,
  UINT64 write_value,
  UINT8  flag
  );

VOID
SendSeCtr(
  UINT32 cmd
  );

UINTN
ReadSeState(
  VOID
  );



#endif
