/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _EFI_TEMP_ID_H_
#define _EFI_TEMP_ID_H_

//
// Define the  protocol GUID
//
#define EFI_TEMP_ID_GUID \
  { \
    0xd2289576,0xae30,0x11eb,0xbf,0xe9,0x1b,0xea,0x72,0x56,0xd3,0x57 \
  }
//
// Extern the GUID for protocol users.
//
extern EFI_GUID                   gEfitTempIdProtocolGuid;

//
// Forward reference for ANSI C compatibility
//
typedef struct _EFI_TEMP_ID_PROTOCOL  EFI_TEMP_ID_PROTOCOL;

typedef
VOID
(EFIAPI *TEMP_ID_GETTEMP_INTERFACE) (
  IN EFI_TEMP_ID_PROTOCOL  *This
  );

typedef
VOID
(EFIAPI *TEMP_ID_GETDEVID_INTERFACE) (
  IN EFI_TEMP_ID_PROTOCOL  *This
  );

struct _EFI_TEMP_ID_PROTOCOL {
  TEMP_ID_GETTEMP_INTERFACE              SeGetTemp;
  TEMP_ID_GETDEVID_INTERFACE             SeGetDevId;
};

#endif
