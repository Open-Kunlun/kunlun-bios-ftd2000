/*
 * Copyright (c) 2016-2017, ARM Limited and Contributors. All rights reserved.
 * Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef __SE_H__
#define __SE_H__
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/ArmPlatformLib.h>
#include <Library/DebugLib.h>
#include <Library/HobLib.h>
#include <Library/PcdLib.h>
#include <Library/IoLib.h>
#include <Library/MemoryAllocationLib.h>
#include <Library/BaseMemoryLib.h>
#include <Library/ArmSmcLib.h>
#include <Library/TimerLib.h>
#include <Library/SeLib.h>
#include <Library/SerialPortLib.h>
#include <Library/PrintLib.h>


#define  TICKS_PER_US   48
#define ReadB(a)           (*(volatile unsigned char *)(a))
#define ReadW(a)           (*(volatile unsigned short *)(a))
#define ReadL(a)           (*(volatile unsigned int *)(a))

#define WriteB(v,a)        (*(volatile unsigned char *)(a) = (v))
#define WriteW(v,a)        (*(volatile unsigned short *)(a) = (v))
#define WriteL(v,a)        (*(volatile unsigned int *)(a) = (v))
#define WriteQ(v,a)        (*(volatile unsigned long *)(a) = (v))
#define FT_I2C1_BASE        (0x28007000)

#define IC_CON              (0x00)
#define IC_TAR              (0x04)  //target address
#define IC_SAR              (0x08)  //slave address
#define IC_DATA_CMD         (0x10)  //Rx/Tx data buffer, Cmd reg
#define IC_SS_SCL_HCNT      (0x14)
#define IC_SS_SCL_LCNT      (0x18)
#define IC_FS_SCL_HCNT      (0x1c)
#define IC_FS_SCL_LCNT      (0x20)
#define IC_INTR_STAT        (0x2c)
#define IC_INTR_MASK        (0x30)
#define IC_RAW_INTR_STAT    (0x34)
#define IC_RX_TL            (0x38)
#define IC_TX_TL            (0x3c)
#define IC_RXFLR            (0x78)
#define IC_CLR_TX_ABORT     (0x54)
#define IC_CLR_STOP_DET     (0x60)
#define IC_ENABLE           (0x6c)
#define IC_STATUS           (0x70)
#define IC_SDA_HOLD         (0x7c)
#define IC_TX_ABRT_SOURCE   (0x80)
#define IC_CLR_INTR         (0x40)
#define IC_CLR_TX_ABRT      (0x54)

/*define IC_STATUS bit*/
#define IC_STATUS_MA        (0x20) //1: MASTER not dile
#define IC_STATUS_RFF       (0x10) 
#define IC_STATUS_RENE      (0x08)
#define IC_STATUS_TFE       (0x04)
#define IC_STATUS_TFNF      (0x02)

/*define IC_TX_ABRT_SOURCE*/
#define ABRT_7B_ADDR_NOACK          (0x01)
#define ABRT_TXDATA_NOACK           (0x08)
#define ABRT_LOST                   (0x1000)

#endif

