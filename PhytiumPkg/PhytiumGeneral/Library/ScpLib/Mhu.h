/** @file

  Copyright (C) 2022 - 2023, Phytium Technology Co., Ltd. All rights reserved.<BR>

  SPDX-License-Identifier: BSD-2-Clause-Patent

**/

#ifndef _MHU_H_
#define _MHU_H_

//#include <Library/UefiBootServicesTableLib.h>
#include <Library/BaseMemoryLib.h>
//#include <Library/DxeServicesLib.h>
#include <Library/DebugLib.h>
#include <Library/IoLib.h>
//#include <Library/PcdLib.h>

/* MHU address space */
#define MHU_BASE   (0x2A000000)
#define MHU_SCP_UBOOT_BASE  (MHU_BASE + 0x20)
#define MHU_UBOOT_SCP_BASE  (MHU_BASE + 0x120)
#define MHU_CONFIG_BASE   (MHU_BASE + 0x500)
/*MHU os chanle address*/
#define MHU_SCP_OS_BASE     (MHU_BASE + 0x0)
#define MHU_OS_SCP_BASE        (MHU_BASE + 0x100)
#define SCP_TO_AP_OS_MEM_BASE     (0x2A006000 + 0x1000)
#define AP_TO_SCP_OS_MEM_BASE    (0x2A006000 + 0x1400)

/* shared memroy base */
#define SCP_TO_AP_SHARED_MEM_BASE  (0x2A006000 + 0x800)
#define AP_TO_SCP_SHARED_MEM_BASE  (0x2A006000 + 0xC00)

#define printf(arg...) DEBUG((EFI_D_ERROR,## arg))
/* MHU channel type */
typedef enum {
    MHU_SCP_UBOOT_CHN = 0,
    MHU_UBOOT_SCP_CHN,
    MHU_SCP_OS_CHN,
    MHU_OS_SCP_CHN
} MHU_CHN_TYPE;

/* u-boot MHU channel registers */
#define MHU_SCP_CONFIG    MHU_CONFIG_BASE + 0x4
#define MHU_AP_CONFIG    MHU_CONFIG_BASE + 0xc
#define MHU_STAT    0x0
#define MHU_SET     0x8
#define MHU_CLEAR   0x10

/*os MHU channel registers */
#define SCP_OS_CONFIG MHU_CONFIG_BASE
#define AP_OS_CONFIG  MHU_CONFIG_BASE + 0x8

/*
 * Slot 31 is Reserved because the MHU hardware uses this register bit to
 * indicate a non-secure access attempt. The total number of available slots is
 * therefore 31 [30:0].
 */
#define MHU_MAX_SLOT_ID        30


INT32 MhuChnMessageStart(UINT32 ChnType, UINT32 SlotId);
INT32 MhuChnMessageSend(UINT32 ChnType, UINT32 SlotId);
INT32 MhuChnMessageWait(UINT32 ChnType, UINT32 SlotId);
INT32 MhuChnMessageEnd(UINT32 ChnType, UINT32 SlotId);

VOID MhuChnInit(UINT32 ChnType);


#endif /* _MHU_H_ */
