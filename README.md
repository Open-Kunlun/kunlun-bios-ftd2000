# Open-Kunlun KunlunBiosFTD2000项目

## 介绍

Open-Kunlun KunlunBiosFTD2000项目是可适配国产飞腾腾锐D2000硬件的开源UEFI
BIOS固件项目，基于edk2-stable-202111开发。项目代码只在飞腾腾锐D2000开发板平台上进行过测试验证，支持麒麟V10版本OS的引导、S3以及USB唤醒功能。昆仑太科将支持社区开发者持续迭代优化代码，推动代码合入OpenKunlun社区。  

    注： ① 项目代码中只包括了飞腾X100 GOP显卡驱动，无其他显卡GOP，如需支持其他显卡开发者需自行添加显卡驱动。  
        ② 项目代码中不包含PBF文件，开发者需使用PBF文件进行正确配置并和UEFI BIOS进行打包才能在实际硬件平台上运行。  

## 代码下载与编译

1. 开发环境
   Ubuntu 20.04.3 LTS， gcc-linaro-7.4.1及以上。

2. 代码下载  
   
   （1）下载base代码
   
    git clone https://gitee.com/Open-Kunlun/kunlun-bios-ftd2000  
    cd kunlun-bios-ftd2000
   
   （2）下载edk2代码  
    git pull origin master  
    git submodule update --init --recursive(如下载失败，请手动下载edk2并拷贝致kunlun-bios-ftd2000目录)  
    cd edk2  
    git checkout -b edk2-stable202111  edk2-stable202111  
    cd BaseTools/Source/C/BrotliCompress/brotli  
    git checkout -b v1.0.6 v1.0.6  
    cd -  
    cd CryptoPkg/Library/OpensslLib/openssl  
    git checkout -b OpenSSL_1_1_1j OpenSSL_1_1_1j  
    cd -  
    cd ..    
   
    注：搭配edk2-stable202111的brotli为v1.0.6版本，openssl为OpenSSL_1_1_1j版本  

4. 代码编译  
    cd Kunlun  
    ./buildD2000.sh d 编译debug版本  
    ./buildD2000.sh r 编译release版本  
    生成的UEFI BIOS为根目录下的buiosD2000.bin文件  

## 支持的功能

* 基本信息显示
* 日期时间设置（RTC：DS1337）
* 语言设置（中/英文）
* 处理器信息显示（型号、速率、核数、缓存大小)
* 处理器核数和频率配置
* PCIE拆分及参数配置
* 内存信息显示和频率配置
* 内存快速训练配置
* 启动顺序配置
* 串口重定向功能
* S3/S4功能

## 版权及免责声明

本项目为昆仑太科主导建立的仓库，仓库内容不代表本项目团队和公司立场及观点。由于传播、利用此项目中的一切内容而造成的任何直接或者间接的后果及损失，均由使用者本人负责，本项目团队和社区不为此承担任何责任。

更多开源社区资料请查看 https://gitee.com/Open-Kunlun/kunlun-bios-ftd2000

联系邮箱: [jdzhang@kunluntech.com.cn](mailto:jdzhang@kunluntech.com.cn) ; [jwluo@kunluntech.com.cn](mailto:jwluo@kunluntech.com.cn)
